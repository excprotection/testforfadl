﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('editProfileController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';
    var map;
    var latitude;
    var longitiude;
    var contact;
    var lang = localStorage.getItem('Dalal_lang');

    function GetStoredContract() {
        var contract = JSON.parse(CookieService.getCookie('StoredContract'));
        return contract;
    }

    function LoadCountries(callBack) {
        var countries = [];
        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            $('#linkEditProfileCountry .item-after').html('الدولة');
        }
        else {
            $('#linkEditProfileCountry .item-after').html('Country');
        }

        appServices.CallService('editProfile', "GET", "api/Nationality/QuickAll", '', function (res) {
            if (res != null && res.length > 0) {
                $scope.countries = res;
                $$('#linkEditProfileCountry select').html('');

                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    app.smartSelectAddOption('#linkEditProfileCountry select', '<option value="" selected>الدولة</option>');
                }
                else {
                    app.smartSelectAddOption('#linkEditProfileCountry select', '<option value="" selected>Country</option>');
                }
                angular.forEach($scope.countries, function (country) {
                    if (country.key == userLoggedIn.nationalityId) {
                        $('#linkEditProfileCountry .item-after').html(country.value);
                        app.smartSelectAddOption('#linkEditProfileCountry select', '<option value="' + country.key + '" selected>' + country.value + '</option>');
                    }
                    else {
                        app.smartSelectAddOption('#linkEditProfileCountry select', '<option value="' + country.key + '">' + country.value + '</option>');
                    }
                });
            }

            callBack(true);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    function LoadCities(callBack) {
        var cities = [];
        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            $('#linkEditProfileCity .item-after').html('المدينة');
        }
        else {
            $('#linkEditProfileCity .item-after').html('City');
        }

        appServices.CallService('editProfile', "GET", "api/city/QuickAll", '', function (res) {
            if (res != null && res.length > 0) {
                $scope.cities = res;
                $$('#linkEditProfileCity select').html('');

                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    app.smartSelectAddOption('#linkEditProfileCity select', '<option value="" selected>المدينة</option>');
                }
                else {
                    app.smartSelectAddOption('#linkEditProfileCity select', '<option value="" selected>City</option>');
                }
                angular.forEach($scope.cities, function (city) {
                    if (city.key == userLoggedIn.cityId) {
                        $('#linkEditProfileCity .item-after').html(city.value);
                        app.smartSelectAddOption('#linkEditProfileCity select', '<option value="' + city.key + '" selected>' + city.value + '</option>');
                    }
                    else {
                        app.smartSelectAddOption('#linkEditProfileCity select', '<option value="' + city.key + '">' + city.value + '</option>');
                    }
                });
            }

            callBack(true);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    function LoadGenders(callBack) {
        var genders = [];
        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            $('#linkEditProfileGender .item-after').html('النوع');
        }
        else {
            $('#linkEditProfileGender .item-after').html('Gender');
        }

        appServices.CallService('editProfile', "GET", "api/contact/Genders", '', function (res) {
            app.hideIndicator();

            if (res != null && res.length > 0) {
                $scope.genders = res;
                $$('#linkEditProfileGender select').html('');

                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    app.smartSelectAddOption('#linkEditProfileGender select', '<option value="" selected>النوع</option>');
                }
                else {
                    app.smartSelectAddOption('#linkEditProfileGender select', '<option value="" selected>Gender</option>');
                }
                angular.forEach($scope.genders, function (gender) {
                    if (gender.key == userLoggedIn.genderId) {
                        $('#linkEditProfileGender .item-after').html(gender.value);
                        app.smartSelectAddOption('#linkEditProfileGender select', '<option value="' + gender.key + '" selected>' + gender.value + '</option>');
                    }
                    else {
                        app.smartSelectAddOption('#linkEditProfileGender select', '<option value="' + gender.key + '">' + gender.value + '</option>');
                    }
                });
            }

            callBack(true);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    function LoadUserDetails(callBack) {
        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

        app.showIndicator();

        appServices.CallService('editProfile', "GET", "api/contact/GetDetails/" + userLoggedIn.crmUserId, '', function (res) {
           
            if (res != null) {
                contact = res.contact;
                $scope.editProfileForm.fullName = contact.fullName;
                $scope.editProfileForm.email = contact.email;
                $scope.editProfileForm.idNumber = contact.idNumber;

                setTimeout(function () {
                    $scope.$apply();
                    callBack(true);
                }, fw7.DelayBeforeScopeApply);
            }
            else {
                callBack(null);
            }

        });
    }


    $(document).ready(function () {
        app.onPageInit('editProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'editProfile') return;
            $rootScope.currentOpeningPage = 'editProfile';

        });

        app.onPageReinit('editProfile', function (page) {
            //$scope.resetForm();
        });

        app.onPageBeforeAnimation('editProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'editProfile') return;
            $rootScope.currentOpeningPage = 'editProfile';

            $scope.isLoaded = true;

            $scope.resetForm();

            LoadUserDetails(function (result) {
                LoadCountries(function (result) {
                    LoadCities(function (result) {
                        LoadGenders(function (result) {

                        });
                    });
                });
            });

            

            LoadGenders(function (result) { });

            $$('#linkEditProfileCountry select').on('change', function () {
                var countryId = $(this).val();
                var country = $scope.countries.filter(function (obj) {
                    return obj.key == countryId;
                })[0];

                $scope.formFields.country = country;
            });

            $$('#linkEditProfileCity select').on('change', function () {
                var cityId = $(this).val();
                var city = $scope.cities.filter(function (obj) {
                    return obj.key == cityId;
                })[0];

                $scope.formFields.city = city;
            });

            $$('#linkEditProfileGender select').on('change', function () {
                var genderId = $(this).val();
                var gender = $scope.genders.filter(function (obj) {
                    return obj.key == genderId;
                })[0];

                $scope.formFields.gender = gender;
            });

        });

        app.onPageReinit('editProfile', function (page) {
            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('editProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'editProfile') return;
            $rootScope.currentOpeningPage = 'editProfile';

        });

        $scope.form = {};
        $scope.editProfileForm = {};
        $scope.formFields = {};

        $scope.resetForm = function () {
            $scope.editProfileReset = false;
            $scope.submittedCompleteProfile = false;

            $scope.isCountryValid = true;
            $scope.isCityValid = true;
            $scope.isGenderValid = true;

            $scope.editProfileForm.country = null;
            $scope.editProfileForm.fullName = null;
            $scope.editProfileForm.city = null;
            $scope.editProfileForm.gender = null;
            $scope.editProfileForm.email = null;
            $scope.editProfileForm.idNumber = null;

            if (typeof $scope.CompleteProfileForm != 'undefined' && $scope.CompleteProfileForm != null) {
                $scope.CompleteProfileForm.$setPristine(true);
                $scope.CompleteProfileForm.$setUntouched();
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        }

        $scope.submitForm = function (CompleteProfileForm) {
            $scope.editProfileReset = true;
            $scope.submittedCompleteProfile = true;

            var countryId = $('#linkEditProfileCountry select').val();
            var cityId = $('#linkEditProfileCity select').val();
            var GenderId = $('#linkEditProfileGender select').val();
            var email = $scope.editProfileForm.email;
            var idNumber = $scope.editProfileForm.idNumber;
            var fullName = $scope.editProfileForm.fullName;

            $scope.isCountryValid = $scope.editProfileReset && $scope.submittedCompleteProfile && countryId != null && countryId != '' && countryId != ' ' ? true : false;
            $scope.isCityValid = $scope.editProfileReset && $scope.submittedCompleteProfile && cityId != null && cityId != '' && cityId != ' ' ? true : false;
            $scope.isGenderValid = $scope.editProfileReset && $scope.submittedCompleteProfile && GenderId != null && GenderId != '' && GenderId != ' ' ? true : false;

            if ($scope.isCountryValid && $scope.isCityValid && $scope.isGenderValid && !CompleteProfileForm.email.$invalid && !CompleteProfileForm.idNumber.$invalid) {
                var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

                var params = {
                    'ContactId': contact.contactId,
                    'MobilePhone': contact.mobilePhone,
                    'Email': email,
                    'FullName': fullName,
                    'CityId': cityId,
                    'NationalityId': countryId,
                    'genderId': GenderId,
                    'IdNumber': idNumber
                };

                app.showIndicator();
                appServices.CallService('editProfile', "POST", "api/Account/EditProfile?UserId=" + userLoggedIn.id, params, function (res) {
                    app.hideIndicator();
                    if (res != null) {
                        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

                        userLoggedIn.fullName = res.user.fullName;
                        userLoggedIn.nationalityId = countryId;
                        userLoggedIn.cityId = cityId;
                        userLoggedIn.genderId = GenderId;
                        userLoggedIn.email = email;
                        userLoggedIn.idNumber = idNumber;

                        CookieService.setCookie('userLoggedIn', JSON.stringify(userLoggedIn));
                        helpers.GoToPage('userProfile', null);
                    }
                });

            }
        };



        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('intro', null);
        };

        $scope.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        $scope.nameRegex = /^[A-Za-z0-9]*$/;
        $scope.mobileRegex = /^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/;

        app.init();
    });

}]);

