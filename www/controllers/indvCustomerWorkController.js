﻿/// <reference path="../js/angular.js" />
/// <reference path="../js/my app.js" />

myApp.angular.controller('indvCustomerWorkController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
	'use strict';

	var fw7 = myApp.fw7;
	var app = myApp.fw7.app;
	var fromPage = '';
	var lang = localStorage.getItem('Dalal_lang');


	function loadPrevioseWorkAddress() {
		app.showIndicator();
		var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
		appServices.CallService('indvPreviousLocations', "GET", "api/IndivContract/GetContractCustomerWork_M/" + userLoggedIn.crmUserId, '', function (res) {
			app.hideIndicator();
			if (res) {
				$scope.SectorForm = res;
				$scope.SectorForm.workingSector = res.workingSector.toString();

				if ($scope.isArabic) {
					switch (res.workingSector) {
						case "1":
							$$('#indvLinkSectorType .item-after').html('حكومي');
							break;
						case "2":
							$$('#indvLinkSectorType .item-after').html('قطاع خاص');
							break;
						default:
							$$('#indvLinkSectorType .item-after').html('قطاع العمل');
					}


				} else {
					switch (res.workingSector) {
						case "1":
							$$('#indvLinkSectorType .item-after').html('Governmental');
							break;
						case "2":
							$$('#indvLinkSectorType .item-after').html('Private sector');
							break;
						default:
							$$('#indvLinkSectorType .item-after').html('Working Sector');
					}
				}
				
			}

		});
	}
	$(document).ready(function () {
		app.onPageInit('indvCustomerWork', function (page) {
			if ($rootScope.currentOpeningPage != 'indvCustomerWork') return;
			$rootScope.currentOpeningPage = 'indvCustomerWork';
            loadPrevioseWorkAddress();
		});

		app.onPageBeforeAnimation('indvCustomerWork', function (page) {
			if ($rootScope.currentOpeningPage != 'indvCustomerWork') return;
			$rootScope.currentOpeningPage = 'indvCustomerWork';
			var lang = localStorage.getItem('Dalal_lang');
			$scope.isArabic = lang == 'AR' || typeof lang == 'undefined' || lang == null ? true : false;

			$$('#indvLinkSectorType select').on('change', function () {
				$scope.SectorForm.workingSector = $('#indvLinkSectorType select').val();
				setTimeout(function () {
					$scope.$apply();
				}, fw7.DelayBeforeScopeApply);
			});
			$scope.Contract = JSON.parse(CookieService.getCookie('indvContract'));
			
		});

		app.onPageReinit('indvCustomerWork', function (page) {
			if ($rootScope.currentOpeningPage != 'indvCustomerWork') return;
			$rootScope.currentOpeningPage = 'indvCustomerWork';

		});

		app.onPageAfterAnimation('indvCustomerWork', function (page) {
			if ($rootScope.currentOpeningPage != 'indvCustomerWork') return;
			$rootScope.currentOpeningPage = 'indvCustomerWork';

		});

		$scope.isArabic = true;
		$scope.form = {};
		$scope.SectorForm = {};
		$scope.formFields = {};

		$scope.resetForm = function () {
			$scope.sectorReset = false;
			$scope.submittedSector = false;

			$scope.isSectorTypeValid = true;
			$scope.isSectorAddressValid = true;
			$scope.isWorkingPlaceValid = true;

			$scope.SectorForm.sectorType = null;
			$scope.SectorForm.sectorAddress = null;
			$scope.SectorForm.workingPlace = null;

			if (typeof $scope.SectorForm != 'undefined' && $scope.SectorForm != null) {
				$scope.SectorForm.$setPristine(true);
				$scope.SectorForm.$setUntouched();
			}

			setTimeout(function () {
				$scope.$apply();
			}, fw7.DelayBeforeScopeApply);
		}

		$scope.submitForm = function (isValid) {
			$scope.sectorReset = true;
			$scope.submittedSector = true;

			var sectorTypeId = $('#indvLinkSectorType select').val();

			var sectorAddress = $('#txtSectorAddress').val();
			var workingPlace = $('#txtIndvWorkingPlace').val();

			$scope.isSectorTypeValid = true;
			$scope.isSectorAddressValid = true;
			$scope.isWorkingPlaceValid = true;

			$scope.isSectorTypeValid = $scope.sectorReset && $scope.submittedSector && sectorTypeId != null && sectorTypeId != '' && sectorTypeId != ' ' ? true : false;

			if ($scope.isSectorTypeValid) {
				$scope.Contract.sectorType = sectorTypeId;
				$scope.Contract.sectorAddress = sectorAddress;
				$scope.Contract.workingPlace = workingPlace;
				CookieService.setCookie('indvContract', JSON.stringify($scope.Contract));
				console.log($scope.SectorForm.sectorType);
				helpers.GoToPage('indvContractDetails', null);

			}
		};

		$scope.GoBack = function () {
			helpers.GoBack();
		};

		$scope.GoToIntro = function () {
			helpers.GoToPage('intro', null);
		};

		app.init();
	});




}]);