﻿/// <reference path="../js/angular.js" />
/// <reference path="../scripts/Service.js" />

myApp.angular.controller('indvPackagesController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
	'use strict';

	var fw7 = myApp.fw7;
	var app = myApp.fw7.app;
	var lang = localStorage.getItem('Dalal_lang');
	var PackagesSwiper;

	function InitiateSwiper(id) {
		PackagesSwiper = new Swiper(id, {
			loop: false,
			autoplayDisableOnInteraction: false,
			speed: 2000,
			slidesPerView: 1,
			centeredSlides: true,
			autoplay: 2000,
		});

		setTimeout(function () {
			PackagesSwiper.update();
			$scope.$apply();
		}, fw7.DelayBeforeScopeApply);

		setTimeout(function () {
			SpinnerPlugin.activityStop();
		}, 1000);
	}

	function StartSwiper() {
		if (PackagesSwiper) PackagesSwiper.destroy(true, true);
		InitiateSwiper('#PackagesSwiper');
	}


	function GetPackages(nationalityId, professionId) {
		app.showIndicator();
		appServices.CallService('indvPackages', "GET", "api/IndivContract/GetIndivPrices_M?nationalityId=" + nationalityId + "&professionId=" + professionId, '', function (res) {
			app.hideIndicator();
			if (res != null) {
				$scope.Packages = res;
				StartSwiper();
			}
		});
	}

	$(document).ready(function () {
		app.onPageInit('indvPackages', function (page) {
			if ($rootScope.currentOpeningPage != 'indvPackages') return;
			$rootScope.currentOpeningPage = 'indvPackages';

		});

		app.onPageReinit('indvPackages', function (page) {

		});

		app.onPageBeforeAnimation('indvPackages', function (page) {
			if ($rootScope.currentOpeningPage != 'indvPackages') return;
			$rootScope.currentOpeningPage = 'indvPackages';

			$scope.professionName = page.query.professionName;

			GetPackages(page.query.nationalityId, page.query.professionId);

			setTimeout(function () {
				$scope.$apply();
			}, fw7.DelayBeforeScopeApply);
		});

		app.onPageAfterAnimation('indvPackages', function (page) {
			if ($rootScope.currentOpeningPage != 'indvPackages') return;
			$rootScope.currentOpeningPage = 'indvPackages';

		});

		$scope.GetAvailableEmployees = function (nationalityId, professionId, packageId) {
			// check show employee or not
			//app.showIndicator();
			//appServices.CallService('indvPackages', "GET", "api/IndivContract/GetShowEmploteeData_M", '', function (res) {
			//	app.hideIndicator();
			//	if (res != null) {
			//		{

			//			    if (res.toLowerCase() == 'true')
			//				//goto workers
			//			    	helpers.GoToPage('indvWorkers', { nationalityId: nationalityId, professionId: professionId, professionName: $scope.professionName, packageId: packageId });
			//			    else {
			//					//update employee id

			//				//goto prevLocation
			//			    	helpers.GoToPage('indvPrevLocations', { nationalityId: nationalityId, employeeId: '-1', packageId: packageId, professionId: professionId });
			//			    }
			//		}
			//	}
			//});


			//get setting values
			var setting = JSON.parse(CookieService.getCookie('indvEmployeeSettings'));
			if (setting.isShowEmployeeData) {
				helpers.GoToPage('indvWorkers', { nationalityId: nationalityId, professionId: professionId, professionName: $scope.professionName, packageId: packageId });

			}
			else {
				helpers.GoToPage('indvPrevLocations', { nationalityId: nationalityId, employeeId: '-1', packageId: packageId, professionId: professionId });

			}

		}

		$scope.GetPackageClass = function (reqPackage) {
			return "pkg0" + reqPackage.typeId;
		}

		$scope.GoBack = function () {
			helpers.GoBack();
		};

		$scope.GoToLogin = function () {
			helpers.GoToPage('login', null);
		};


		app.init();

	});
}]);
