﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('createController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
	'use strict';

	var fw7 = myApp.fw7;
	var app = myApp.fw7.app;
	var fromPage = '';
	var availableDaysSelected = [];
	var disabledDays = [];
	var selectedPackage = {};
	var lang = localStorage.getItem('Dalal_lang');
	var IsArabic = lang == 'AR' || typeof lang == 'undefined' || lang == null ? true : false;
	var calendarDisabled;
	var allDisableDates = [];

	function ReloadForm() {
		if (CookieService.getCookie('StoredContract')) {
			var contract = JSON.parse(CookieService.getCookie('StoredContract'));

			setTimeout(function () {
				$scope.$apply();
			}, fw7.DelayBeforeScopeApply);
		}
	}

	function GetFirstVisitExpire(callBack) {
	    if (typeof $scope.firstVisitExpireAfter != 'undefined' && $scope.firstVisitExpireAfter != null && $scope.firstVisitExpireAfter != '' && $scope.firstVisitExpireAfter != ' ') {
	        callBack($scope.firstVisitExpireAfter);
	    }
	    else {
	        app.showIndicator();
	        appServices.CallService('create', "GET", "api/General/FirstVisitExpireAfter", '', function (res) {
	            app.hideIndicator();
	            if (res != null && res.firstVisitExpireAfter != null) {
	                $scope.firstVisitExpireAfter = res.firstVisitExpireAfter;
	                callBack($scope.firstVisitExpireAfter);
	            }
	        });
	    }
	}

	function compareDates(date, dates) {
	    for (var i = 0; i < dates.length; i++) {
	        if (dates[i].getDate() == date.getDate() &&
                dates[i].getMonth() == date.getMonth() &&
                dates[i].getYear() == date.getYear()) {
	            return true
	        }
	    }
	}

	function InitCalender(callBack) {

	    if (typeof allDisableDates != 'undefined' && allDisableDates != null && availableDaysSelected.length != allDisableDates.length) {
	        GetFirstVisitExpire(function (firstVisitExpireAfter) {
	            var calenderCloseText = 'Done';
	            lang = localStorage.getItem('IRC_lang');
	            if (lang == 'AR' || lang == null || typeof lang == 'undefined') {
	                calenderCloseText = 'تم';
	            }
	            else {
	                calenderCloseText = 'Done';
	            }

	            var today = new Date();
	            var maxDate = new Date();
	            maxDate.setDate(maxDate.getDate() + parseInt(firstVisitExpireAfter));

	            calendarDisabled = app.calendar({
	                input: '#calendar-disabled',
	                dateFormat: 'dd/mm/yyyy',
	                toolbarCloseText: calenderCloseText,
	                minDate: new moment().add('days', -1).toDate(),
	                maxDate: maxDate,
	                disabled: function (date) {
	                    var dateObj = new Date(date);
	                    allDisableDates = [];

	                    var fromDate = new moment().add('days', -1).toDate();
	                    var toDate = new Date();
	                    toDate.setDate(toDate.getDate() + parseInt(firstVisitExpireAfter));

	                    if (dateObj > fromDate && dateObj < toDate) {

	                        angular.forEach(availableDaysSelected, function (day, index) {
	                            var dayValue = 0;
	                            if (day.name == 'الأحد' || day.name == 'Sunday') dayValue = 0;
	                            else if (day.name == 'الإثنين' || day.name == 'Monday') dayValue = 1;
	                            else if (day.name == 'الثلاثاء' || day.name == 'Tuesday') dayValue = 2;
	                            else if (day.name == 'الأربعاء' || day.name == 'Wednesday') dayValue = 3;
	                            else if (day.name == 'الخميس' || day.name == 'Thursday') dayValue = 4;
	                            else if (day.name == 'الجمعة' || day.name == 'Friday') dayValue = 5;
	                            else if (day.name == 'السبت' || day.name == 'Saturday') dayValue = 6;

	                            allDisableDates.push(dayValue);
	                        });

	                        if (allDisableDates.length > 0) {
	                            if (allDisableDates.length == 1) {
	                                if (dateObj.getDay() != allDisableDates[0] || dateObj.getDay() == 5)
	                                    return true;
	                            }
	                            else if (allDisableDates.length == 2) {
	                                if (dateObj.getDay() != allDisableDates[0] && dateObj.getDay() != allDisableDates[1])
	                                    return true;
	                            }
	                            else if (allDisableDates.length == 3) {
	                                if (dateObj.getDay() != allDisableDates[0] && dateObj.getDay() != allDisableDates[1] && dateObj.getDay() != allDisableDates[2])
	                                    return true;
	                            }
	                            else if (allDisableDates.length == 4) {
	                                if (dateObj.getDay() != allDisableDates[0] && dateObj.getDay() != allDisableDates[1] &&
                                        dateObj.getDay() != allDisableDates[2] && dateObj.getDay() != allDisableDates[3])
	                                    return true;
	                            }
	                            else if (allDisableDates.length == 5) {
	                                if (dateObj.getDay() != allDisableDates[0] && dateObj.getDay() != allDisableDates[1] &&
                                        dateObj.getDay() != allDisableDates[2] && dateObj.getDay() != allDisableDates[3] && dateObj.getDay() != allDisableDates[4])
	                                    return true;
	                            }
	                            else if (allDisableDates.length == 6) {
	                                if (dateObj.getDay() != allDisableDates[0] && dateObj.getDay() != allDisableDates[1] &&
                                        dateObj.getDay() != allDisableDates[2] && dateObj.getDay() != allDisableDates[3] && dateObj.getDay() != allDisableDates[4] &&
                                        dateObj.getDay() != allDisableDates[5])
	                                    return true;
	                            }
	                            else if (allDisableDates.length == 7) {
	                                if (dateObj.getDay() == allDisableDates[0] && dateObj.getDay() == allDisableDates[1] &&
                                        dateObj.getDay() == allDisableDates[2] && dateObj.getDay() == allDisableDates[3] && dateObj.getDay() == allDisableDates[4] &&
                                        dateObj.getDay() == allDisableDates[5] && dateObj.getDay() == allDisableDates[6])
	                                    return true;
	                            }
	                        }
	                    }
	                },
	                rangesClasses: [
                {
                    cssClass: 'dayAvailable',
                    range: function (date) {
                        var dateObj = new Date(date);

                        allDisableDates = [];

                        var fromDate = new moment().add('days', -1).toDate();
                        var toDate = new Date();
                        toDate.setDate(toDate.getDate() + parseInt(firstVisitExpireAfter));

                        if (dateObj > fromDate && dateObj < toDate) {

                            angular.forEach(availableDaysSelected, function (day, index) {
                                var dayValue = 0;
                                if (day.name == 'الأحد' || day.name == 'Sunday') dayValue = 0;
                                else if (day.name == 'الإثنين' || day.name == 'Monday') dayValue = 1;
                                else if (day.name == 'الثلاثاء' || day.name == 'Tuesday') dayValue = 2;
                                else if (day.name == 'الأربعاء' || day.name == 'Wednesday') dayValue = 3;
                                else if (day.name == 'الخميس' || day.name == 'Thursday') dayValue = 4;
                                else if (day.name == 'الجمعة' || day.name == 'Friday') dayValue = 5;
                                else if (day.name == 'السبت' || day.name == 'Saturday') dayValue = 6;

                                allDisableDates.push(dayValue);
                            });

                            if (allDisableDates.length > 0) {
                                if (allDisableDates.length == 1) {
                                    if (dateObj.getDay() != allDisableDates[0] || dateObj.getDay() == 5)
                                        return true;
                                }
                                else if (allDisableDates.length == 2) {
                                    if (dateObj.getDay() != allDisableDates[0] && dateObj.getDay() != allDisableDates[1])
                                        return true;
                                }
                                else if (allDisableDates.length == 3) {
                                    if (dateObj.getDay() != allDisableDates[0] && dateObj.getDay() != allDisableDates[1] && dateObj.getDay() != allDisableDates[2])
                                        return true;
                                }
                                else if (allDisableDates.length == 4) {
                                    if (dateObj.getDay() != allDisableDates[0] && dateObj.getDay() != allDisableDates[1] &&
                                        dateObj.getDay() != allDisableDates[2] && dateObj.getDay() != allDisableDates[3])
                                        return true;
                                }
                                else if (allDisableDates.length == 5) {
                                    if (dateObj.getDay() != allDisableDates[0] && dateObj.getDay() != allDisableDates[1] &&
                                        dateObj.getDay() != allDisableDates[2] && dateObj.getDay() != allDisableDates[3] && dateObj.getDay() != allDisableDates[4])
                                        return true;
                                }
                                else if (allDisableDates.length == 6) {
                                    if (dateObj.getDay() != allDisableDates[0] && dateObj.getDay() != allDisableDates[1] &&
                                        dateObj.getDay() != allDisableDates[2] && dateObj.getDay() != allDisableDates[3] && dateObj.getDay() != allDisableDates[4] &&
                                        dateObj.getDay() != allDisableDates[5])
                                        return true;
                                }
                                else if (allDisableDates.length == 7) {
                                    if (dateObj.getDay() == allDisableDates[0] && dateObj.getDay() == allDisableDates[1] &&
                                        dateObj.getDay() == allDisableDates[2] && dateObj.getDay() == allDisableDates[3] && dateObj.getDay() == allDisableDates[4] &&
                                        dateObj.getDay() == allDisableDates[5] && dateObj.getDay() == allDisableDates[6])
                                        return true;
                                }
                            }
                        }
                    }
                }],
	                onDayClick: function (p, dayContainer, year, month, day) {
	                    //26/01/2019
	                    var monthComponent = parseInt(month) + 1;
	                    monthComponent = parseInt(monthComponent) < 10 ? '0' + monthComponent : monthComponent;
	                    var dayComponent = parseInt(day) < 10 ? '0' + day : day;
	                    $scope.createForm.firstVisit = dayComponent + '/' + monthComponent + '/' + year;
	                    $scope.isFirstVisitDateSelected = true;
	                    setTimeout(function () {
	                        $scope.$apply();
	                        $scope.LoadPackages();
	                    }, fw7.DelayBeforeScopeApply);

	                },
	                closeOnSelect: true
	            });

	            callBack(calendarDisabled);
	        });
	    }
	    else {
	        callBack(calendarDisabled);
	    }
	}

	function CheckPromotionWithShift(selectedPackage,callBack) {
		var city = $scope.formFields.city;
		var area = $scope.formFields.area;
		var worker = $scope.formFields.worker;
		var visit = $scope.formFields.visit;
		var hour = $scope.formFields.hour;
		var firstVisit = $scope.createForm.firstVisit;
		var duration = $scope.formFields.duration;
		var code = $scope.createForm.code;
		var nationality = $scope.formFields.nationality;
		var selectedDays = '';

		var lang = localStorage.getItem('Dalal_lang');

		var checkedDays = availableDaysSelected;

		if (lang == 'AR' || lang == null || typeof lang == 'undefined') {
			angular.forEach(checkedDays, function (day, index) {

				if (day.name == 'السبت') day.name = 'Saturday';
				else if (day.name == 'الأحد') day.name = 'Sunday';
				else if (day.name == 'الإثنين') day.name = 'Monday';
				else if (day.name == 'الثلاثاء') day.name = 'Tuesday';
				else if (day.name == 'الأربعاء') day.name = 'Wednesday';
				else if (day.name == 'الخميس') day.name = 'Thursday';
				else if (day.name == 'الجمعة') day.name = 'Friday';

			});
		}

		angular.forEach(checkedDays, function (day, index) {
			if (index == 0) {
				selectedDays += day.name;
			}
			else {
				selectedDays += ',' + day.name;
			}
		});

		if (lang == 'AR' || lang == null || typeof lang == 'undefined') {
			angular.forEach(checkedDays, function (day, index) {

				if (day.name == 'Saturday') day.name = 'السبت';
				else if (day.name == 'Sunday') day.name = 'الأحد';
				else if (day.name == 'Monday') day.name = 'الإثنين';
				else if (day.name == 'Tuesday') day.name = 'الثلاثاء';
				else if (day.name == 'Wednesday') day.name = 'الأربعاء';
				else if (day.name == 'Thursday') day.name = 'الخميس';
				else if (day.name == 'Friday') day.name = 'الجمعة';

			});
		}

		$scope.isAllSelected = true;
		var params = '';
		var packageShift = '';

		var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

		if (selectedPackage.shift == 'صباحي' || selectedPackage.shift == 'Morning') {
			packageShift = 'Morning';
		}
		else {
			packageShift = 'Evening';
		}

		var cityId = typeof city.key != 'undefined' && city.key != null ? city.key : city.cityId;

		params = 'CityId=' + cityId + '&DistrictId=' + area.districtId + '&Empcount=' + worker.key + '&NoOfVisits=' + visit.key + '&Weeklyvisits=' + visit.key +
            '&HoursCount=' + hour.key + '&Days=' + selectedDays + '&ContractStartDate=' + firstVisit + '&ContractDuration=' + duration.key +
        '&NationalityId=' + nationality.key + '&PromotionCode=' + code + '&CrmUserId=' + userLoggedIn.crmUserId + '&ShiftType=' + packageShift;

		var packages = [];

		app.showIndicator();

		appServices.CallService('create', "POST", "api/HourlyPricing/TestPromotionValidation?" + params, '', function (res) {
			app.hideIndicator();
			if (res != null && res.promotion != null) {
			    if (res.promotion.promotionStatus == 1) {//no-promotion
			        $scope.PromotionName = '';
			        $scope.isPromotionCorrect = false;
			    }
			    else if (res.promotion.promotionStatus == 2) {//Valid
			        $scope.PromotionName = res.promotion.name;
			        $scope.isPromotionCorrect = true;
			    }
			    else if (res.promotion.promotionStatus == 3) {//Invalid
			        $scope.PromotionName = lang == 'AR' || lang == null || typeof lang == 'undefined' ? 'كود الخصم غير مناسب مع الباقة المختارة' : 'Code is not valid with selected package';
			        $scope.isPromotionCorrect = false;
			    }
			    else {//Incorrect
			        $scope.PromotionName = lang == 'AR' || lang == null || typeof lang == 'undefined' ? 'كود الخصم غير صحيح أو منتهي' : 'Code is incorrect or expired';
			        $scope.isPromotionCorrect = false;
			    }
			}
			else {
			    $scope.isPromotionCorrect = true;
			    $scope.PromotionName = null;
			}

			if (res.promotion.name != null && res.promotion.name != '' && res.promotion.name != ' ') {
			    callBack(true);
			}
			else {
			    callBack(false);
			}

			setTimeout(function () {
				$scope.$apply();
			}, fw7.DelayBeforeScopeApply);
		});
	}

	function LoadCities(callBack) {
		var cities = [];

		app.showIndicator();
		if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
			$('#linkCreateCity .item-after').html('المدينة');
		}
		else {
			$('#linkCreateCity .item-after').html('City');
		}

		appServices.CallService('create', "GET", "api/HourlyContract/Lookups/AvailableCities_M", '', function (res) {
			//app.hideIndicator();
			if (res != null && res.length > 0) {
				$scope.cities = res;
				$$('#linkCreateCity select').html('');

				if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
					app.smartSelectAddOption('#linkCreateCity select', '<option disabled value="" selected>المدينة</option>');
				}
				else {
					app.smartSelectAddOption('#linkCreateCity select', '<option disabled value="" selected>City</option>');
				}

				angular.forEach($scope.cities, function (city) {
					if (CookieService.getCookie('StoredCity')) {
						var storedCity = CookieService.getCookie('StoredCity');
						if (city.arabicName.toString().toLowerCase() == storedCity.toString().toLowerCase() || city.englishName.toString().toLowerCase() == storedCity.toString().toLowerCase()) {
							$$('#linkCreateCity .item-after').html(city.value);
							app.smartSelectAddOption('#linkCreateCity select', '<option value="' + city.cityId + '" selected>' + city.name + '</option>');
							$scope.formFields.city = city;
							$scope.LoadAreas(city);
						}
						else {
							app.smartSelectAddOption('#linkCreateCity select', '<option value="' + city.cityId + '">' + city.name + '</option>');
						}
					}
					else {
						app.smartSelectAddOption('#linkCreateCity select', '<option value="' + city.cityId + '">' + city.name + '</option>');
					}
				});

			}
			else {
				app.hideIndicator();
			}

			callBack(true);

			setTimeout(function () {
				$scope.$apply();
			}, fw7.DelayBeforeScopeApply);
		});
	}

	function LoadWorkers(callBack) {
		var workers = [];

		// app.showIndicator();
		if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
			$('#linkCreateWorker .item-after').html('العاملات');
		}
		else {
			$('#linkCreateWorker .item-after').html('Worker');
		}

		appServices.CallService('create', "GET", "api/HourlyContract/options/Labours", '', function (res) {
			//app.hideIndicator();
			if (res != null && res.length > 0) {
				$scope.workers = res;
				$$('#linkCreateWorker select').html('');

				if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
					app.smartSelectAddOption('#linkCreateWorker select', '<option disabled value="" selected>العاملات</option>');
				}
				else {
					app.smartSelectAddOption('#linkCreateWorker select', '<option disabled value="" selected>Worker</option>');
				}
				angular.forEach($scope.workers, function (worker) {
					app.smartSelectAddOption('#linkCreateWorker select', '<option value="' + worker.key + '">' + worker.value + '</option>');
				});
			}

			callBack(true);

			setTimeout(function () {
				$scope.$apply();
			}, fw7.DelayBeforeScopeApply);
		});
	}

	function LoadNumberOfVisits(callBack) {
		var visits = [];

		//app.showIndicator();
		if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
			$('#linkCreateVisit .item-after').html('الزيارات');
		}
		else {
			$('#linkCreateVisit .item-after').html('Visits');
		}

		appServices.CallService('create', "GET", "api/HourlyContract/options/Visits", '', function (res) {
			//app.hideIndicator();
			if (res != null && res.length > 0) {
				$scope.visits = res;
				$$('#linkCreateVisit select').html('');

				if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
					app.smartSelectAddOption('#linkCreateVisit select', '<option disabled value="" selected>الزيارات</option>');
				}
				else {
					app.smartSelectAddOption('#linkCreateVisit select', '<option disabled value="" selected>Visits</option>');
				}
				angular.forEach($scope.visits, function (visit) {
					app.smartSelectAddOption('#linkCreateVisit select', '<option value="' + visit.key + '">' + visit.value + '</option>');
				});
			}

			callBack(true);

			setTimeout(function () {
				$scope.$apply();
			}, fw7.DelayBeforeScopeApply);
		});
	}

	function LoadNumberOfHours(callBack) {
		var hours = [];

		//app.showIndicator();
		if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
			$('#linkCreateHour .item-after').html('الساعات');
		}
		else {
			$('#linkCreateHour .item-after').html('Hours');
		}

		appServices.CallService('create', "GET", "api/HourlyContract/options/Hours", '', function (res) {
			//app.hideIndicator();
			if (res != null && res.length > 0) {
				$scope.hours = res;
				$$('#linkCreateHour select').html('');

				if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
					app.smartSelectAddOption('#linkCreateHour select', '<option disabled value="" selected>الساعات</option>');
				}
				else {
					app.smartSelectAddOption('#linkCreateHour select', '<option disabled value="" selected>Hours</option>');
				}
				angular.forEach($scope.hours, function (hour) {
					app.smartSelectAddOption('#linkCreateHour select', '<option value="' + hour.key + '">' + hour.value + '</option>');
				});
			}

			callBack(true);

			setTimeout(function () {
				$scope.$apply();
			}, fw7.DelayBeforeScopeApply);
		});
	}

	function LoadDurations(callBack) {
		var durations = [];

		//app.showIndicator();
		if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
			$('#linkCreateDuration .item-after').html('مدة التعاقد');
		}
		else {
			$('#linkCreateDuration .item-after').html('Duration');
		}

		appServices.CallService('create', "GET", "api/HourlyContract/options/ContractDuration", '', function (res) {
			app.hideIndicator();
			if (res != null && res.length > 0) {
				$scope.durations = res;
				$$('#linkCreateDuration select').html('');

				if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
					app.smartSelectAddOption('#linkCreateDuration select', '<option disabled value="" selected>مدة التعاقد</option>');
				}
				else {
					app.smartSelectAddOption('#linkCreateDuration select', '<option disabled value="" selected>Duration</option>');
				}
				angular.forEach($scope.durations, function (duration) {
					app.smartSelectAddOption('#linkCreateDuration select', '<option value="' + duration.key + '">' + duration.value + '</option>');
				});
			}

			$scope.isLoaded = true;

			callBack(true);

			setTimeout(function () {
				$scope.$apply();
			}, fw7.DelayBeforeScopeApply);
		});
	}

	$(document).ready(function () {
		app.onPageInit('create', function (page) {
			if ($rootScope.currentOpeningPage != 'create') return;
			$rootScope.currentOpeningPage = 'create';

		});

		app.onPageReinit('create', function (page) {
		    //ReloadForm();
		    $scope.isLoaded = true;
		    setTimeout(function () {
		        $scope.$apply();
		    }, fw7.DelayBeforeScopeApply);
		});

		app.onPageBeforeAnimation('create', function (page) {
			if ($rootScope.currentOpeningPage != 'create') return;
			$rootScope.currentOpeningPage = 'create';

			$scope.isLoaded = false;

			if (!CookieService.getCookie('StoredContract')) {
				$scope.resetForm();

				InitCalender(function (result) {
					lang = localStorage.getItem('Dalal_lang');
					$scope.isArabic = lang == 'AR' || typeof lang == 'undefined' || lang == null ? true : false;
					if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
						$scope.lblOriginalPrice = 'السعر الأصلي';
						$scope.lblVatRate = 'قيمة الضريبة';
						$scope.lblPriceWithVat = 'السعر بعد الضريبة';
						$scope.lblLoylityAmount = 'خصم نقاط ولاء';
						$scope.Riyal = 'ريال';
						$scope.lblDiscountAmount = 'الخصم';
						$('#linkCreateArea .item-after').html('الحي');
					}
					else {
						$scope.lblOriginalPrice = 'Original Price';
						$scope.lblVatRate = 'Vat Rate';
						$scope.lblPriceWithVat = 'Price With Vat';
						$scope.lblLoylityAmount = 'Loyality Points Discount';
						$scope.Riyal = 'SAR';
						$scope.lblDiscountAmount = 'Discount';
						$('#linkCreateArea .item-after').html('Area');
					}

					setTimeout(function () {
						$scope.$apply();
					}, fw7.DelayBeforeScopeApply);

					LoadCities(function (result) {
						LoadWorkers(function (result1) {
							LoadNumberOfVisits(function (result2) {
								LoadNumberOfHours(function (result3) {
									LoadDurations(function (result4) { });
								});
							});
						});
					});
				});



				$$('#linkCreateCity select').on('change', function () {
					var cityId = $(this).val();
					var city = $scope.cities.filter(function (obj) {
						return obj.cityId == cityId;
					})[0];

					$scope.isCitySelected = true;

					$scope.formFields.city = city;
					$scope.LoadAreas(city);
				});

				$$('#linkCreateArea select').on('change', function () {
					var areaId = $(this).val();
					var area = $scope.areas.filter(function (obj) {
						return obj.districtId == areaId;
					})[0];

					$scope.formFields.area = area;
					$scope.selectArea(area);
				});

				$$('#linkCreateWorker select').on('change', function () {
					var workerId = $(this).val();
					var worker = $scope.workers.filter(function (obj) {
						return obj.key == workerId;
					})[0];

					$scope.formFields.worker = worker;
					$scope.selectWorker(worker);
				});

				$$('#linkCreateVisit select').on('change', function () {
					var visitId = $(this).val();
					var visit = $scope.visits.filter(function (obj) {
						return obj.key == visitId;
					})[0];

					$scope.formFields.visit = visit;
					$scope.selectNumberOfVisits(visit);
				});

				$$('#linkCreateHour select').on('change', function () {
					var hourId = $(this).val();
					var hour = $scope.hours.filter(function (obj) {
						return obj.key == hourId;
					})[0];

					$scope.formFields.hour = hour;
					$scope.selectHour(hour);
				});

				$$('#linkCreateDuration select').on('change', function () {
					var durationId = $(this).val();
					var duration = $scope.durations.filter(function (obj) {
						return obj.key == durationId;
					})[0];

					$scope.formFields.duration = duration;
					$scope.selectDuration(duration);
				});

				$$('#linkCreateNationality select').on('change', function () {
					var nationalityId = $(this).val();
					var nationality = $scope.nationalities.filter(function (obj) {
						return obj.key == nationalityId;
					})[0];

					$scope.formFields.nationality = nationality;
					$scope.selectNationality(nationality);
				});
			}

		});

		app.onPageReinit('create', function (page) {
		    $rootScope.RemoveEditPagesFromHistory();
		    $scope.isLoaded = true;
		    setTimeout(function () {
		        $scope.$apply();
		    }, fw7.DelayBeforeScopeApply);
		});

		app.onPageAfterAnimation('create', function (page) {
			if ($rootScope.currentOpeningPage != 'create') return;
			$rootScope.currentOpeningPage = 'create';

		});


		$scope.resetForm = function () {
			$scope.createReset = false;
			$scope.submittedCreate = false;
			$scope.isCityValid = true;
			$scope.isAreaValid = true;
			$scope.createForm.city = null;
			$scope.createForm.area = null;
			$scope.createForm.worker = null;
			$scope.createForm.visit = null;
			$scope.createForm.hour = null;
			$scope.createForm.firstVisit = null;
			$scope.createForm.duration = null;
			$scope.createForm.nationality = null;
			$scope.createForm.code = null;
			$scope.formFields = {};
			availableDaysSelected = [];
			selectedPackage = {};
			$scope.packages = selectedPackage;
			$scope.PromotionName = null;

			$scope.packageSelected = null;

			$scope.isCitySelected = false;
			$scope.isAreaSelected = false;
			$scope.isAvailableDaySelected = false;
			$scope.isWorkerSelected = false;
			$scope.isNumberOfVisitsSelected = false;
			$scope.isDurationSelected = false;
			$scope.isHoursSelected = false;
			$scope.isNationalitySelected = false;
			$scope.isAllSelected = false;
			$scope.isAvailableDaysExceedsNoOfVisits = false;
			$scope.isFirstVisitDateSelected = false;

			$scope.LoadAreas(null);
			$scope.LoadNationalities(null);

			$scope.user = {};
			if (typeof $scope.CreateForm != 'undefined' && $scope.CreateForm != null) {
				$scope.CreateForm.$setPristine(true);
				$scope.CreateForm.$setUntouched();
			}

			setTimeout(function () {
				$scope.$apply();
			}, fw7.DelayBeforeScopeApply);
		}

		function isEmpty(obj) {
			for (var prop in obj) {
				if (obj.hasOwnProperty(prop))
					return false;
			}

			return JSON.stringify(obj) === JSON.stringify({});
		}

		function ProceedToNextPage(selectedPackage, availableDaysSelected) {
		    var isMorningShift = selectedPackage.shift == 'صباحي' || selectedPackage.shift == 'morning' | selectedPackage.shift == 'Morning' ? true : false;

		    // var daysArray = $scope.availableDays;
		    var selectedDays = '';

		    var checkedDays = availableDaysSelected;
		    lang = localStorage.getItem('Dalal_lang');
		    if (lang == 'AR' || lang == null || typeof lang == 'undefined') {
		        angular.forEach(checkedDays, function (day, index) {

		            if (day.name == 'السبت') day.name = 'Saturday';
		            else if (day.name == 'الأحد') day.name = 'Sunday';
		            else if (day.name == 'الإثنين') day.name = 'Monday';
		            else if (day.name == 'الثلاثاء') day.name = 'Tuesday';
		            else if (day.name == 'الأربعاء') day.name = 'Wednesday';
		            else if (day.name == 'الخميس') day.name = 'Thursday';
		            else if (day.name == 'الجمعة') day.name = 'Friday';

		        });
		    }

		    angular.forEach(checkedDays, function (day, index) {
		        if (index == 0) {
		            selectedDays += day.name;
		        }
		        else {
		            selectedDays += ',' + day.name;
		        }
		    });

		    var contractToStore = {
		        city: $scope.formFields.city,
		        area: $scope.formFields.area,
		        worker: $scope.formFields.worker,
		        visit: $scope.formFields.visit,
		        hour: $scope.formFields.hour,
		        firstVisit: $scope.createForm.firstVisit,
		        duration: $scope.formFields.duration,
		        nationality: $scope.formFields.nationality,
		        code: $scope.createForm.code,
		        selectedPackage: selectedPackage,
		        daysArray: selectedDays,
		        availableDays: selectedDays,
		        englishAvailableDays: checkedDays,
		        isMorningShift: isMorningShift,
		        promotionExtraVisits: selectedPackage.promotionExtraVisits
		    };

		    if (lang == 'AR' || lang == null || typeof lang == 'undefined') {
		        angular.forEach(checkedDays, function (day, index) {

		            if (day.name == 'Saturday') day.name = 'السبت';
		            else if (day.name == 'Sunday') day.name = 'الأحد';
		            else if (day.name == 'Monday') day.name = 'الإثنين';
		            else if (day.name == 'Tuesday') day.name = 'الثلاثاء';
		            else if (day.name == 'Wednesday') day.name = 'الأربعاء';
		            else if (day.name == 'Thursday') day.name = 'الخميس';
		            else if (day.name == 'Friday') day.name = 'الجمعة';

		        });
		    }

		    CookieService.setCookie('StoredContract', JSON.stringify(contractToStore));

		    helpers.GoToPage('shifts', null);

		    setTimeout(function () {
		        $scope.$apply();
		    }, fw7.DelayBeforeScopeApply);
		}

		$scope.submitForm = function (isValid, $event) {

			var visit = $scope.createForm.visit;
			var lang = localStorage.getItem('Dalal_lang');

			var visitId = $('#linkCreateVisit select').val();

			if (visitId != null && availableDaysSelected != null && availableDaysSelected.length == visitId) {
				$scope.createReset = true;
				$scope.submittedCreate = true;

				var cityId = $('#linkCreateCity select').val();
				var areaId = $('#linkCreateArea select').val();
				var workerId = $('#linkCreateWorker select').val();
				var hourId = $('#linkCreateHour select').val();
				var durationId = $('#linkCreateDuration select').val();
				var nationalityId = $('#linkCreateNationality select').val();
				var firstVisitText = $('#calendar-disabled').val();

				$scope.isCityValid = $scope.createReset && $scope.submittedCreate && cityId != null && cityId != '' && cityId != ' ' ? true : false;
				$scope.isAreaValid = $scope.createReset && $scope.submittedCreate && areaId != null && areaId != '' && areaId != ' ' ? true : false;
				$scope.isWorkerValid = $scope.createReset && $scope.submittedCreate && workerId != null && workerId != '' && workerId != ' ' ? true : false;
				$scope.isVisitValid = $scope.createReset && $scope.submittedCreate && visitId != null && visitId != '' && visitId != ' ' ? true : false;
				$scope.isHourValid = $scope.createReset && $scope.submittedCreate && hourId != null && hourId != '' && hourId != ' ' ? true : false;
				$scope.isDurationValid = $scope.createReset && $scope.submittedCreate && durationId != null && durationId != '' && durationId != ' ' ? true : false;
				$scope.isNationalityValid = $scope.createReset && $scope.submittedCreate && nationalityId != null && nationalityId != '' && nationalityId != ' ' ? true : false;
				$scope.availableDaysValid = $scope.createReset && $scope.submittedCreate && typeof $scope.availableDays != 'undefined' && $scope.availableDays != null && $scope.availableDays.length > 0;
				$scope.isFirstVisitValid = $scope.createReset && $scope.submittedCreate && firstVisitText != null && firstVisitText != '' && firstVisitText != ' ' ? true : false;

				if ($scope.isCityValid && $scope.isAreaValid && $scope.isWorkerValid && $scope.isVisitValid && $scope.isHourValid &&
                    $scope.isDurationValid && $scope.isNationalityValid && $scope.availableDaysValid && $scope.isFirstVisitValid) {

					var isSelectedPackageValid = false;
					angular.forEach($scope.packages, function (packageToSelect, index) {
						if (packageToSelect.isActive) {
							isSelectedPackageValid = true;
						}
					});

					if (!isSelectedPackageValid) {
						if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
							language.openFrameworkModal('خطأ', 'يجب اختيار الباقة المناسبة', 'alert', function () { });
						}
						else {
							language.openFrameworkModal('خطأ', 'Packages is required', 'alert', function () { });
						}
						$event.preventDefault();
					}
					else {

					    if ($scope.createForm.code != '' && $scope.createForm.code != ' ' && $scope.createForm.code != null) {
					        CheckPromotionWithShift(selectedPackage, function (result) {
					            $scope.isPromotionCorrect = result;
					            if (result == true) {
					                ProceedToNextPage(selectedPackage, availableDaysSelected);
					            }
					        });
					    }
					    else {
					        $scope.isPromotionCorrect = true;
					        ProceedToNextPage(selectedPackage, availableDaysSelected);
					    }

						

					}
				}
				else {
					setTimeout(function () {
						$scope.$apply();
					}, fw7.DelayBeforeScopeApply);
				}
			}
			else {
				if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
					language.openFrameworkModal('خطأ', 'عدد الزيارات يجب ان يكون مساوي لعدد الايام المختارة', 'alert', function () { });
				}
				else {
					language.openFrameworkModal('خطأ', 'Number of visits must be equal to number of selected days ', 'alert', function () { });
				}
			}

		};

		$scope.LoadAreas = function (city) {
			var areas = [];

			$scope.LoadPackages();

			app.showIndicator();
			if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
				$('#linkCreateArea .item-after').html('الحي');
			}
			else {
				$('#linkCreateArea .item-after').html('Area');
			}
			if (city) {
				appServices.CallService('create', "GET", "api/city/Districts/" + city.cityId, '', function (res) {
					app.hideIndicator();
					if (res != null && res.length > 0) {
						$scope.areas = res;
						$$('#linkCreateArea select').html('');

						if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
							app.smartSelectAddOption('#linkCreateArea select', '<option disabled value="" selected>الحي</option>');
						}
						else {
							app.smartSelectAddOption('#linkCreateArea select', '<option disabled value="" selected>Area</option>');
						}

						angular.forEach($scope.areas, function (area) {//performance issue
							if (CookieService.getCookie('StoredState')) {
								var StoredState = CookieService.getCookie('StoredState');

								if (area.arabicName.toString().toLowerCase() == StoredState.toString().toLowerCase() || area.englishName.toString().toLowerCase() == StoredState.toString().toLowerCase()) {
									$$('#linkCreateArea .item-after').html(area.name);
									app.smartSelectAddOption('#linkCreateArea select', '<option value="' + area.districtId + '" selected>' + area.name + '</option>');
									$scope.formFields.area = area;
									$scope.isAreaSelected = true;
									$scope.selectArea(area);
								}
								else {
									app.smartSelectAddOption('#linkCreateArea select', '<option value="' + area.districtId + '">' + area.name + '</option>');
								}
							}
							else {
								app.smartSelectAddOption('#linkCreateArea select', '<option value="' + area.districtId + '">' + area.name + '</option>');
							}

						});
					}

					setTimeout(function () {
						$scope.$apply();
					}, fw7.DelayBeforeScopeApply);
				});
			}
			else {
			    $$('#linkCreateArea select').html('');

			    if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
			        app.smartSelectAddOption('#linkCreateArea select', '<option disabled value="" selected>الحي</option>');
			    }
			    else {
			        app.smartSelectAddOption('#linkCreateArea select', '<option disabled value="" selected>Area</option>');
			    }
			}
		};

		$scope.selectArea = function (area) {
			var availableDaysToShow = [];
			var lang = localStorage.getItem('Dalal_lang');

			angular.forEach(area.days.split(','), function (day, index) {
				if (lang == 'AR' || lang == null || typeof lang == 'undefined') {
					if (day == 'Saturday') day = 'السبت';
					else if (day == 'Sunday') day = 'الأحد';
					else if (day == 'Monday') day = 'الإثنين';
					else if (day == 'Tuesday') day = 'الثلاثاء';
					else if (day == 'Wednesday') day = 'الأربعاء';
					else if (day == 'Thursday') day = 'الخميس';
					else if (day == 'Friday') day = 'الجمعة';
				}
				availableDaysToShow.push({ id: index, name: day });
			});

			$scope.isAreaSelected = availableDaysToShow != null && availableDaysToShow.length > 0 ? true : false;
			$scope.availableDays = availableDaysToShow;
			$scope.selectedAreaDays = availableDaysToShow;

			$scope.LoadPackages();

			$scope.LoadNationalities(area.districtId);
		};

		$scope.selectAvailableDay = function (availableDay, e) {
			var element = document.getElementById('chkDay_' + availableDay.id);
			var isChecked = $(element).prop('checked');
			var hasClassChecked = $('#iosIcon_' + availableDay.id).hasClass('iosIcon');
			var visit = $('#linkCreateVisit select').val();

			if (availableDaysSelected.length > 0) {
			    if (availableDaysSelected.length >= parseInt(visit) && !hasClassChecked) {
			        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
			            language.openFrameworkModal('خطأ', 'لا يمكنك إختيار عدد أيام أكثر من عدد الزيارات الأسبوعية', 'alert', function () { });
			        }
			        else {
			            language.openFrameworkModal('خطأ', 'You Cannot Select Days More Than Visits Per Week', 'alert', function () { });
			        }
			        e.preventDefault();
			        return false;
			    }
			}

			app.showIndicator();

			if (isChecked == false && !hasClassChecked) {
			    $('#iosIcon_' + availableDay.id).addClass('iosIcon');
				availableDaysSelected.push(availableDay);
			}
			else {
			    $('#iosIcon_' + availableDay.id).removeClass('iosIcon');
				availableDaysSelected = availableDaysSelected.filter(function (obj) {
					return obj.name !== availableDay.name;
				});
			}

			$scope.createForm.firstVisit = null;

			$scope.isAvailableDaySelected = availableDaysSelected != null && availableDaysSelected.length > 0 && $scope.isNumberOfVisitsSelected ? true : false;

			InitCalender(function (result) {
			    setTimeout(function () {
			        app.hideIndicator();
			    }, 500);
			});
			//$scope.LoadPackages();
		};

		$scope.LoadNationalities = function (areaId) {
		    var nationalities = [];
		    if (areaId) {
		        app.showIndicator();

		        lang = localStorage.getItem('Dalal_lang');
		        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
		            $('#linkCreateNationality .item-after').html('الجنسية');
		        }
		        else {
		            $('#linkCreateNationality .item-after').html('Nationality');
		        }

		        appServices.CallService('create', "GET", "api/HourlyContract/Lookups/DistrictNationalities/" + areaId, '', function (res) {
		            app.hideIndicator();
		            if (res != null && res.length > 0) {
		                $scope.nationalities = res;
		                $$('#linkCreateNationality select').html('');

		                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
		                    app.smartSelectAddOption('#linkCreateNationality select', '<option disabled value="" selected>الجنسية</option>');
		                }
		                else {
		                    app.smartSelectAddOption('#linkCreateNationality select', '<option disabled value="" selected>Nationality</option>');
		                }
		                angular.forEach($scope.nationalities, function (nationality) {
		                    app.smartSelectAddOption('#linkCreateNationality select', '<option value="' + nationality.key + '">' + nationality.value + '</option>');
		                });
		            }

		            setTimeout(function () {
		                $scope.$apply();
		            }, fw7.DelayBeforeScopeApply);
		        });
		    }
		    else {
		        $$('#linkCreateNationality select').html('');
		        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
		            app.smartSelectAddOption('#linkCreateNationality select', '<option disabled value="" selected>الجنسية</option>');
		        }
		        else {
		            app.smartSelectAddOption('#linkCreateNationality select', '<option disabled value="" selected>Nationality</option>');
		        }
		    }
		};

		var validateStartDateWithOffsetHours = function (offsetHours) {
			var dateTime_Now = new moment();

			var startDate_moment = moment($scope.createForm.firstVisit, 'DD/MM/YYYY').toDate();
			var startDate_withOffset = new moment(startDate_moment).set({ 'hour': 0, 'minute': 0, 'second': 0 }).add(offsetHours, 'hour');
			return dateTime_Now < startDate_withOffset;
		};

		$scope.LoadPackages = function () {
			var city = $scope.formFields.city;
			var area = $scope.formFields.area;
			var worker = $scope.formFields.worker;
			var visit = $scope.formFields.visit;
			var hour = $scope.formFields.hour;
			var firstVisit = $scope.createForm.firstVisit;
			var duration = $scope.formFields.duration;
			var code = $scope.createForm.code;
			var nationality = $scope.formFields.nationality;
			var selectedDays = '';

			if (city != null && area != null && worker != null && visit != null && duration != null && firstVisit != null && nationality != null && availableDaysSelected.length > 0 && hour != null) {

				var lang = localStorage.getItem('Dalal_lang');

				var checkedDays = availableDaysSelected;

				if (lang == 'AR' || lang == null || typeof lang == 'undefined') {
					angular.forEach(checkedDays, function (day, index) {

						if (day.name == 'السبت') day.name = 'Saturday';
						else if (day.name == 'الأحد') day.name = 'Sunday';
						else if (day.name == 'الإثنين') day.name = 'Monday';
						else if (day.name == 'الثلاثاء') day.name = 'Tuesday';
						else if (day.name == 'الأربعاء') day.name = 'Wednesday';
						else if (day.name == 'الخميس') day.name = 'Thursday';
						else if (day.name == 'الجمعة') day.name = 'Friday';

					});
				}

				angular.forEach(checkedDays, function (day, index) {
					if (index == 0) {
						selectedDays += day.name;
					}
					else {
						selectedDays += ',' + day.name;
					}
				});

				if (lang == 'AR' || lang == null || typeof lang == 'undefined') {
					angular.forEach(checkedDays, function (day, index) {

						if (day.name == 'Saturday') day.name = 'السبت';
						else if (day.name == 'Sunday') day.name = 'الأحد';
						else if (day.name == 'Monday') day.name = 'الإثنين';
						else if (day.name == 'Tuesday') day.name = 'الثلاثاء';
						else if (day.name == 'Wednesday') day.name = 'الأربعاء';
						else if (day.name == 'Thursday') day.name = 'الخميس';
						else if (day.name == 'Friday') day.name = 'الجمعة';

					});
				}

				$scope.isAllSelected = true;
				var params = '';

				var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

				params = 'CityId=' + city.cityId + '&DistrictId=' + area.districtId + '&Empcount=' + worker.key + '&NoOfVisits=' + visit.key + '&Weeklyvisits=' + visit.key +
                    '&HoursCount=' + hour.key + '&Days=' + selectedDays + '&ContractStartDate=' + firstVisit + '&ContractDuration=' + duration.key +
                '&NationalityId=' + nationality.key + '&PromotionCode=' + code + '&CrmUserId=' + userLoggedIn.crmUserId;

				var packages = [];

				app.showIndicator();

				appServices.CallService('create', "GET", "api/HourlyPricing?" + params, '', function (res) {
					app.hideIndicator();
					if (res != null && res.length > 0) {
						var lang = localStorage.getItem('Dalal_lang');

						angular.forEach(res, function (packageToSelect, index) {
							packageToSelect.isActive = false;
							packageToSelect.index = index;

							if (typeof $scope.packageSelected != 'undefined' && $scope.packageSelected != null) {
							    if (packageToSelect.index == $scope.packageSelected.index) {
							        packageToSelect.isActive = true;
							        selectedPackage = packageToSelect;
							    }
							    else {
							        packageToSelect.isActive = false;
							    }
							}

							var isValidWithOffset;
							if (packageToSelect.shift == 'Morning') {
								isValidWithOffset = validateStartDateWithOffsetHours(packageToSelect.requestOffsetHours);
							}
							else {
								isValidWithOffset = validateStartDateWithOffsetHours(packageToSelect.requestOffsetHours);
							}

							packageToSelect.IsDiscountFound = parseFloat(packageToSelect.totalPrice) == parseFloat(packageToSelect.totalPriceAfterPromotion) ? false : true;
							packageToSelect.IsLoyalityDiscountFound = parseFloat(packageToSelect.totalLoyaltyPointsDiscountAmount) > 0 ? true : false;
							packageToSelect.totalPromotionDiscountPercentage = Math.ceil((parseFloat(packageToSelect.totalPromotionDiscountAmount) / parseFloat(packageToSelect.totalPrice)) * 100);

							if (packageToSelect.shift == 'Morning') {
								if (lang == 'AR' || lang == null || typeof lang == 'undefined') {
									packageToSelect.shift = 'صباحي';
								}
								else {
									packageToSelect.shift = 'Morning';
								}
							}
							else {
								if (lang == 'AR' || lang == null || typeof lang == 'undefined') {
									packageToSelect.shift = 'مسائي';
								}
								else {
									packageToSelect.shift = 'Evening';
								}
							}

							packageToSelect.isMorningShift = packageToSelect.shift == 'صباحي' || packageToSelect.shift == 'morning' | packageToSelect.shift == 'Morning' ? true : false;

							packageToSelect.isValidWithOffset = true;

							if (!packageToSelect.isAvailable || (!packageToSelect.isMorningShift && hour.key == 8)) {
								packageToSelect.isAvailable = false;
							}
							else {
								if (!isValidWithOffset) {
									packageToSelect.isValidWithOffset = false;
								}
							}

						});
						$scope.packages = res;

						if ($scope.createForm.code != '' && $scope.createForm.code != ' ' && $scope.createForm.code != null) {
							if (lang == 'AR' || lang == null || typeof lang == 'undefined') {
								$scope.PromotionName = $scope.packages[0].promotionName != null && $scope.packages[0].promotionName != '' && $scope.packages[0].promotionName != ' ' ? $scope.packages[0].promotionName : 'كود الخصم غير صحيح أو منتهي';
							}
							else {
								$scope.PromotionName = $scope.packages[0].promotionName != null && $scope.packages[0].promotionName != '' && $scope.packages[0].promotionName != ' ' ? $scope.packages[0].promotionName : 'Promotion Code Is Invalid Or Expired';
							}
							$scope.isPromotionCorrect = $scope.packages[0].promotionName != null && $scope.packages[0].promotionName != '' && $scope.packages[0].promotionName != ' ' ? true : false;
						}
					}

					setTimeout(function () {
						$scope.$apply();
					}, fw7.DelayBeforeScopeApply);
				});
			}
			else {
				$scope.PromotionName = null;
				$scope.isAllSelected = false;
				setTimeout(function () {
					$scope.$apply();
				}, fw7.DelayBeforeScopeApply);
			}
		}

		$scope.selectNationality = function (nationality) {
			$scope.isNationalitySelected = true;
			$scope.LoadPackages();
		};

		$scope.selectWorker = function (worker) {
			$scope.isWorkerSelected = true;
			$scope.LoadPackages();
		};

		$scope.selectHour = function (hour) {
			$scope.isHoursSelected = true;
			$scope.LoadPackages();
		};

		$scope.selectDuration = function (duration) {
			$scope.LoadPackages();
		};

		$scope.selectNumberOfVisits = function (visits) {
			$scope.isNumberOfVisitsSelected = true;

			if (availableDaysSelected != null && availableDaysSelected.length > 0) {
				$scope.availableDays = null;
				$scope.isAvailableDaysExceedsNoOfVisits = false;

				setTimeout(function () {
					$scope.$apply();
					$scope.availableDays = $scope.selectedAreaDays;
					$scope.isAvailableDaySelected = false;
				}, fw7.DelayBeforeScopeApply);

				setTimeout(function () {
					$scope.$apply();
					availableDaysSelected = [];
					InitCalender(function (result) { });
				}, 50);
			}

			$scope.isAvailableDaySelected = availableDaysSelected != null && availableDaysSelected.length > 0 && $scope.isNumberOfVisitsSelected ? true : false;
			$scope.LoadPackages();
		};

		$scope.SelectPackage = function (packageSelected) {
			if (packageSelected.isAvailable && packageSelected.isValidWithOffset) {
			    selectedPackage = packageSelected;

			    $scope.packageSelected = packageSelected;
				angular.forEach($scope.packages, function (packageToSelect, index) {
					if (packageToSelect.index == packageSelected.index) {
						packageToSelect.isActive = true;
					}
					else {
						packageToSelect.isActive = false;
					}
				});

				if ($scope.createForm.code != '' && $scope.createForm.code != ' ' && $scope.createForm.code != null) {
				    CheckPromotionWithShift(packageSelected, function (result) {
				        $scope.isPromotionCorrect = result;
				        $scope.LoadPackages();
				    });
				}
				else {
				    $scope.isPromotionCorrect = true;
				}
			}
			else {
				packageSelected.isActive = false;
			}



			setTimeout(function () {
				$scope.$apply();
			}, fw7.DelayBeforeScopeApply);
		};

		$scope.form = {};
		$scope.createForm = {};
		$scope.formFields = {};

		$scope.GoBack = function () {
			helpers.GoBack();
		};

		$scope.GoToIntro = function () {
			helpers.GoToPage('intro', null);
		};

		$scope.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		$scope.nameRegex = /^[A-Za-z0-9]*$/;
		$scope.mobileRegex = /^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/;



		app.init();
	});

}]);

