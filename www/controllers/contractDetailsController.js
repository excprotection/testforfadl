﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('contractDetailsController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';

    function GetStoredContract() {
        var contract = JSON.parse(CookieService.getCookie('StoredContract'));
        return contract;
    }

    $(document).ready(function () {
        app.onPageInit('contractDetails', function (page) {
            if ($rootScope.currentOpeningPage != 'contractDetails') return;
            $rootScope.currentOpeningPage = 'contractDetails';

        });

        app.onPageBeforeAnimation('contractDetails', function (page) {
            if ($rootScope.currentOpeningPage != 'contractDetails') return;
            $rootScope.currentOpeningPage = 'contractDetails';

            $scope.isLoaded = true;

            var lang = localStorage.getItem('Dalal_lang');
            $scope.isArabic = lang == 'AR' || typeof lang == 'undefined' || lang == null ? true : false;
         
          
            if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            	$scope.Riyal = 'ريال';
            }
            else {
            	$scope.Riyal = 'SAR';
            }
            var contract = GetStoredContract();
            contract.availableDaysNumber = contract.availableDays != null && contract.availableDays != '' ? contract.availableDays.split(',').length : 0;
        	//check descount  
            contract.IsDiscountFound = parseFloat(contract.selectedPackage.totalPrice) == parseFloat(contract.selectedPackage.totalPriceAfterPromotion) ? false : true;

            $scope.contract = contract;

        });

        app.onPageReinit('contractDetails', function (page) {
            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('contractDetails', function (page) {
            if ($rootScope.currentOpeningPage != 'contractDetails') return;
            $rootScope.currentOpeningPage = 'contractDetails';

        });


        $scope.GoBack = function () {
            helpers.GoBack();
        };

        var btnCreateContract = document.getElementById("btnCreateContract");
        btnCreateContract.addEventListener("touchend", btnIsTouched, false);

        function btnIsTouched(event) {
            $('#btnCreateContract').prop("disabled", true);
            $scope.GoToSuccess();
            event.preventDefault();
            return false;
        }

        $scope.GoToSuccess = function () {
            var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

            if (userLoggedIn != null) {
                app.showIndicator();
                appServices.CallService('terms', "GET", "api/HourlyContract/GetLatestNotPaidContract?crmUserId=" + userLoggedIn.crmUserId, '', function (res) {
                    if (res == '' || res == null || res == undefined) {
                        appServices.CallService('contractDetails', "GET", "api/contact/IsProfileCompleted_M/" + userLoggedIn.crmUserId, '', function (res) {
                            if (res != null && res.completed == true) {

                                var selectedDays = '';

                                angular.forEach($scope.contract.englishAvailableDays, function (day, index) {

                                    if (day.name == 'السبت') day.name = 'Saturday';
                                    else if (day.name == 'الأحد') day.name = 'Sunday';
                                    else if (day.name == 'الإثنين') day.name = 'Monday';
                                    else if (day.name == 'الثلاثاء') day.name = 'Tuesday';
                                    else if (day.name == 'الأربعاء') day.name = 'Wednesday';
                                    else if (day.name == 'الخميس') day.name = 'Thursday';
                                    else if (day.name == 'الجمعة') day.name = 'Friday';

                                });

                                angular.forEach($scope.contract.englishAvailableDays, function (day, index) {
                                    if (index == 0) {
                                        selectedDays += day.name;
                                    }
                                    else {
                                        selectedDays += ',' + day.name;
                                    }
                                });

                                var params = {
                                    'CityId': $scope.contract.city.cityId,
                                    'DistrictId': $scope.contract.area.districtId,
                                    'NationalityId': $scope.contract.nationality.key,
                                    'NumOfVisits': $scope.contract.visit.key,
                                    'NumOfHours': $scope.contract.hour.key,
                                    'NumOfWorkers': $scope.contract.worker.key,
                                    'AvailableDays': selectedDays,
                                    'HourlyPricingId': $scope.contract.selectedPackage.hourlypricingId,
                                    'StartDay': $scope.contract.firstVisit,
                                    'ContractDuration': $scope.contract.duration.key,
                                    'PromotionCode': $scope.contract.code,
                                    'PostpondedDays': $scope.contract.PostpondedDays,
                                    'DaysToPostpond': $scope.contract.DaysToPostpond,
                                    'Latitude': $scope.contract.Latitude,
                                    'Longitude': $scope.contract.Longtitude,
                                    'HouseNo': $scope.contract.HouseNumber,
                                    'HouseType': $scope.contract.HouseType.key,
                                    'FloorNo': $scope.contract.FloorNumber.key,
                                    'PartmentNo': $scope.contract.AppartmentNumber,
                                    'AddressNotes': $scope.contract.HouseAddress,
                                    'CustomerId': userLoggedIn.crmUserId
                                };

                                appServices.CallService('contractDetails', "POST", "api/HourlyContract/Create", params, function (res) {
                                    $('#btnCreateContract').prop("disabled", false);
                                    app.hideIndicator();
                                    if (res != null) {

                                        var newContract = $scope.contract;
                                        newContract.ContractId = res.contract.contractId;
                                        newContract.ContractNum = res.contract.contractNum;
                                        $scope.contract = newContract;
                                        CookieService.setCookie('StoredContract', JSON.stringify($scope.contract));

                                        language.openFrameworkModal('نجاح', 'تم إنشاء العقد بنجاح .', 'alert', function () { });

                                        if (typeof newContract.selectedPackage.totalPriceAfterLoyalty != 'undefined' && newContract.selectedPackage.totalPriceAfterLoyalty != null && parseFloat(newContract.selectedPackage.totalPriceAfterLoyalty) > 0) {
                                            helpers.GoToPage('success', null);
                                        }
                                        else {
                                            helpers.GoToPage('contractInfo', { contract: newContract });
                                        }
                                    }
                                });
                            }
                            else {
                                app.hideIndicator();
                                $('#btnCreateContract').prop("disabled", false);
                                helpers.GoToPage('completeProfile', null);
                            }
                        });
                    }
                    else {

                        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                            language.openFrameworkModal('خطأ', 'برجاء سداد قيمة العقد السابق أو إلغاؤه , قبل انشاء عقد جديد', 'alert', function () { });
                        }
                        else {
                            language.openFrameworkModal('خطأ', 'Please pay the value of the previous contract or cancel it before creating a new contract', 'alert', function () { });
                        }

                        app.hideIndicator();

                        helpers.GoToPage('contractInfo', {
                            contract:
                                {
                                    contractId: res
                                }
                        });

                    }

                });
               
            }
            else {
                $('#btnCreateContract').prop("disabled", false);
                helpers.GoToPage('completeProfile', null);
            }



        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('intro', null);
        };

        app.init();
    });

}]);

