﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('indvPrevLocationsController', ['$document', '$scope', '$q', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $q, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
	'use strict';

	var fw7 = myApp.fw7;
	var app = myApp.fw7.app;
	var fromPage = '';
	var map;
	var latitude;
	var longitiude;
	var lang = localStorage.getItem('Dalal_lang');

	function GetStoredContract() {
		var contract = JSON.parse(CookieService.getCookie('indvContract'));
		return contract;
	}

	function GetAddressFromLatLang(geocoder, addressesList) {
		var addresses = [];

		var promise1 = new Promise(function (resolve, reject) {
			angular.forEach(addressesList, function (address, index) {
				var latlng = { lat: parseFloat(address.new_latitude), lng: parseFloat(address.new_longitude) };

				var floorNumber = address.new_floorNumber;
				var houseType = address.new_houseType;
				var partmentNumber = address.new_partmentNumber;
				address.new_ContractNumber = address.new_ContractNumber == null ? "" : address.new_ContractNumber;
				partmentNumber = address.new_partmentNumber == null || address.new_partmentNumber == '' || address.new_partmentNumber == ' ' ? '0' : address.new_partmentNumber;

				if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
					floorNumber = address.new_floorNumber == '0' ? 'أرضي' : address.new_floorNumber;
					houseType = address.new_houseType == '0' ? 'فيلا' : 'عمارة';
				}
				else {
					floorNumber = address.new_floorNumber == '0' ? 'Ground' : address.new_floorNumber;
					houseType = address.new_houseType == '0' ? 'Villa' : 'Appartment';
				}

				geocoder.geocode({ 'location': latlng }, function (results, status) {
					if (status === 'OK') {
						if (results[0]) {
							addresses.push({
								id: index,
								name: /*address.new_ContractNumber + ' - ' +*/ houseType + ' ' + address.new_houseNumber + ' - ' + floorNumber + ' - ' + partmentNumber + ' - ' + results[0].formatted_address,
								lat: address.new_latitude,
								lng: address.new_longitude,
								houseType: houseType,
								houseNumber: address.new_houseNumber,
								floorNumber: floorNumber,
								partmentNUmber: partmentNumber,
								houseAddress: results[0].formatted_address,
								contractNumber: address.new_ContractNumber
							});
						}
					}
					else {
						addresses.push({
							id: -1,
							name:/* address.new_ContractNumber + ' - ' + */houseType + ' ' + address.new_houseNumber + ' - ' + floorNumber + ' - ' + partmentNumber + ' -  لا يوجد عنوان ',
							lat: address.new_latitude,
							lng: address.new_longitude,
							houseType: houseType,
							houseNumber: address.new_houseNumber,
							floorNumber: floorNumber,
							partmentNUmber: partmentNumber,
							houseAddress: 'لا يوجد عنوان',
							contractNumber: address.new_ContractNumber
						});
					}


					if (addresses.length == addressesList.length) {
						var newArray = addresses.filter(function (el) {
							return parseInt(el.id) > -1;
						});

						resolve(newArray);
					}
				});

			});


		});

		promise1.then(function (addresses) {
			app.hideIndicator();
			$scope.previousLocations = addresses;
			$scope.NoLocations = false;
			setTimeout(function () {
				$scope.$apply();
			}, fw7.DelayBeforeScopeApply);
		});
	}

	function LoadPreviousLocations( callBack) {
		var previousLocations = [];

		var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

		app.showIndicator();                                      

		appServices.CallService('indvPreviousLocations', "GET", "api/IndivContract/GetContractLocations_M/" + userLoggedIn.crmUserId, '', function (res) {
			app.hideIndicator();

			if (res != null && res.result != null && res.result.length > 0) {
				var geocoder = new google.maps.Geocoder;
				GetAddressFromLatLang(geocoder, res.result);
			}
			else {
				$scope.NoLocations = true;
				$scope.previousLocations = null;
				setTimeout(function () {
					helpers.GoToPage('indvLocation', null);
				}, fw7.DelayBeforeScopeApply);
			}
		});
	}

	function ClearPreviousLocationSelected() {
		$('.myCheckBox').removeAttr('checked');

		$scope.previousLocation = null;
		$scope.isMapValid = false;
	}

	$(document).ready(function () {
		app.onPageInit('indvPrevLocations', function (page) {
			if ($rootScope.currentOpeningPage != 'indvPrevLocations') return;
			$rootScope.currentOpeningPage = 'indvPrevLocations';

		});

		app.onPageReinit('indvPrevLocations', function (page) {

		});

		app.onPageBeforeAnimation('indvPrevLocations', function (page) {
			if ($rootScope.currentOpeningPage != 'indvPrevLocations') return;
			$rootScope.currentOpeningPage = 'indvPrevLocations';

			$scope.isLoaded = true;
			$scope.packageId = page.query.packageId;
			$scope.employeeId = page.query.employeeId;
			$scope.nationalityId = page.query.nationalityId;
			$scope.professionId = page.query.professionId;

			var lang = localStorage.getItem('Dalal_lang');

			$scope.isArabic = lang == 'AR' || typeof lang == 'undefined' || lang == null ? true : false;
			$scope.previousLocations = null;

			var contract = GetStoredContract();
			$scope.resetForm();

			if (contract)
			{
				contract.EmployeeId = contract.EmployeeId ? contract.EmployeeId : $scope.employeeId;
				contract.packageId = contract.packageId ? contract.packageId : $scope.packageId;
				contract.nationalityId = contract.nationalityId ? contract.nationalityId : $scope.nationalityId;
				contract.professionId = contract.professionId ? contract.professionId : $scope.professionId;

				LoadPreviousLocations(function () { });
				CookieService.setCookie('indvContract', JSON.stringify(contract));

			}

		});

		app.onPageReinit('indvPrevLocations', function (page) {
			$rootScope.RemoveEditPagesFromHistory();
		});

		app.onPageAfterAnimation('indvPrevLocations', function (page) {
			if ($rootScope.currentOpeningPage != 'indvPrevLocations') return;
			$rootScope.currentOpeningPage = 'indvPrevLocations';

		});

		$scope.resetForm = function () {
			$scope.previousLocation = null;
			$scope.NoLocations = true;


			setTimeout(function () {
				$scope.$apply();
			}, fw7.DelayBeforeScopeApply);
		}

		$scope.selectLocation = function (location, position) {
			var element = document.getElementById('chkLocation_' + location.id);
			var isChecked = $(element).prop('checked');
			$scope.previousLocation = location;
		};

		$scope.GoToPickLocation = function () {
			if ($scope.previousLocation != 'undefined' && $scope.previousLocation != null) {
				var storedContract = GetStoredContract();

				storedContract.HouseType = $scope.previousLocation.houseType;
				storedContract.HouseNumber = $scope.previousLocation.houseNumber;
				storedContract.FloorNumber = $scope.previousLocation.floorNumber;
				storedContract.AppartmentNumber = $scope.previousLocation.partmentNUmber;
				storedContract.HouseAddress = $scope.previousLocation.houseAddress;
				storedContract.city = $scope.previousLocation.city;



				storedContract.Latitude = $scope.previousLocation.lat;
				storedContract.Longitude = $scope.previousLocation.lng;
				CookieService.setCookie('indvContract', JSON.stringify(storedContract));

				helpers.GoToPage('indvCustomerWork', null);
			}
			else {
				var lang = localStorage.getItem('Dalal_lang');

				if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
					app.confirm('لم يتم إختيار موقع سابق , هل تريد إختيار موقع آخر؟', 'تأكيد', function () {
						helpers.GoToPage('indvLocations', null);
					}, function () {

					});
				}
				else {
					app.confirm('You Have Not Selected Any Location , Do You Want To Select Another Location ? ', 'Confirm', function () {
						helpers.GoToPage('indvLocations', null);
					}, function () {

					});
				}
			}
		};

		$scope.GoBack = function () {
			helpers.GoBack();
		};

		$scope.GoToIntro = function () {
			helpers.GoToPage('intro', null);
		};

		app.init();
	});

}]);



