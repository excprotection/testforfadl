﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('userProfileController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var advCount;
    var favCount;

    $(document).ready(function () {

        app.onPageInit('userProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'userProfile') return;
            $rootScope.currentOpeningPage = 'userProfile';

        });

        app.onPageBeforeAnimation('userProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'userProfile') return;
            $rootScope.currentOpeningPage = 'userProfile';

            var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

            if (CookieService.getCookie('StoredContract')) CookieService.removeCookie('StoredContract');

            $scope.isLoaded = false;

            $rootScope.RemoveEditPagesFromHistory();

            app.showIndicator();
            appServices.CallService('userProfile', 'GET', "api/contact/GetDetails/" + userLoggedIn.crmUserId, '', function (userData) {
                app.hideIndicator();
                if (userData) {
                    userLoggedIn.nationalityId = userData.contact.nationalityId;
                    userLoggedIn.cityId = userData.contact.cityId;
                    userLoggedIn.genderId = userData.contact.genderId;
                    userLoggedIn.idNumber = userData.contact.idNumber;

                    CookieService.setCookie('userLoggedIn', JSON.stringify(userLoggedIn));
                    $scope.user = userData.contact;
                    $scope.isLoaded = true;
                }
            });

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageAfterAnimation('userProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'userProfile') return;
            $rootScope.currentOpeningPage = 'userProfile';

        });

        app.onPageReinit('userProfile', function (page) {
            $rootScope.RemoveEditPagesFromHistory();
        });

        $scope.GoToIntro = function () {
            helpers.GoToPage('intro', null);
        };

        $scope.GoToEditProfile = function (user) {
            helpers.GoToPage('editProfile', null);
        };

        $scope.GoToComplaints = function (user) {
            helpers.GoToPage('userComplaints', null);
        };

        $scope.GoToContracts = function (user) {
            helpers.GoToPage('userContracts', null);
        };
        $scope.GoToMyRequests = function (user) {
            helpers.GoToPage('userIndvContractRequests', null);
        };
        $scope.GoToMyInvoices = function (user) {
            helpers.GoToPage('userInvoices', null);
        };

        app.init();
    });

}]);

