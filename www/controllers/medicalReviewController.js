﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('medicalReviewController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;

    $(document).ready(function () {
        app.onPageInit('medicalReview', function (page) {
            if ($rootScope.currentOpeningPage != 'medicalReview') return;
            $rootScope.currentOpeningPage = 'medicalReview';

        });

        app.onPageBeforeAnimation('medicalReview', function (page) {
            if ($rootScope.currentOpeningPage != 'medicalReview') return;
            $rootScope.currentOpeningPage = 'medicalReview';

            $scope.isLoaded = true;

            setTimeout(function () {
                $rootScope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageReinit('medicalReview', function (page) {
            
        });

        app.onPageAfterAnimation('medicalReview', function (page) {
            if ($rootScope.currentOpeningPage != 'medicalReview') return;
            $rootScope.currentOpeningPage = 'medicalReview';

            
        });

        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.CallUs = function () {
            var contactPhone = '+966920033660';

            window.plugins.CallNumber.callNumber(
              function onSuccess(successResult) {

              }, function onError(errorResult) {

                  console.log("Error:" + errorResult);
              }, contactPhone, true);
        };

        $scope.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        $scope.mobileRegex = /^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/;

        app.init();
    });

}]);

