﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('addComplaintController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';
    var map;
    var latitude;
    var longitiude;
    var contact;
    var lang = localStorage.getItem('Dalal_lang');

    function GetStoredContract() {
        var contract = JSON.parse(CookieService.getCookie('StoredContract'));
        return contract;
    }

    function LoadProblemTypes(callBack) {
        var problemTypes = [];

        app.showIndicator();
        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            $('#linkAddComplaintProblemType .item-after').html('نوع المساعدة');
        }
        else {
            $('#linkAddComplaintProblemType .item-after').html('Problem Type');
        }

        appServices.CallService('addComplaint', "GET", "api/CustomerTicket/Dalal/ProblemTypes", '', function (res) {
            if (res != null && res.length > 0) {
                $scope.problemTypes = res;
                $$('#linkAddComplaintProblemType select').html('');
                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    app.smartSelectAddOption('#linkAddComplaintProblemType select', '<option value="" selected>نوع المساعدة</option>');
                }
                else {
                    app.smartSelectAddOption('#linkAddComplaintProblemType select', '<option value="" selected>Problem Type</option>');
                }
                angular.forEach($scope.problemTypes, function (problemType) {
                    app.smartSelectAddOption('#linkAddComplaintProblemType select', '<option value="' + problemType.key + '">' + problemType.value + '</option>');
                });
            }

            callBack(true);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    function LoadWorkers(callBack) {
        var workers = [];

        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            $('#linkAddComplaintWorker .item-after').html('العاملة');
        }
        else {
            $('#linkAddComplaintWorker .item-after').html('Worker');
        }

        appServices.CallService('addComplaint', "GET", "api/CustomerTicket/Dalal/ServedEmployees_M?userId=" + userLoggedIn.crmUserId, '', function (res) {
            app.hideIndicator();

            if (res != null && res.length > 0) {
                $scope.workers = res;
                $$('#linkAddComplaintWorker select').html('');

                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                	app.smartSelectAddOption('#linkAddComplaintWorker select', '<option style="font-size: 12px;" value="" selected>العاملة</option>');
                }																		
                else {																	
                	app.smartSelectAddOption('#linkAddComplaintWorker select', '<option style="font-size: 12px;" value="" selected>Worker</option>');
                }																		
                angular.forEach($scope.workers, function (worker) {						
                	app.smartSelectAddOption('#linkAddComplaintWorker select', '<option style="font-size: 12px;" value="' + worker.key + '">' + worker.value + '</option>');
                });
            }

            callBack(true);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    function LoadContracts(callBack) {
        var contracts = [];

        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            $('#linkAddComplaintContract .item-after').html('العقد');
        }
        else {
            $('#linkAddComplaintContract .item-after').html('Contract');
        }

        appServices.CallService('addComplaint', "GET", "api/CustomerTicket/Dalal/ActiveContracts?userId=" + userLoggedIn.crmUserId, '', function (res) {
            app.hideIndicator();

            if (res != null && res.length > 0) {
                $scope.contracts = res;
                $$('#linkAddComplaintContract select').html('');

                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    app.smartSelectAddOption('#linkAddComplaintContract select', '<option value="" selected>العقد</option>');
                }
                else {
                    app.smartSelectAddOption('#linkAddComplaintContract select', '<option value="" selected>Contract</option>');
                }
                angular.forEach($scope.contracts, function (contract) {
                    app.smartSelectAddOption('#linkAddComplaintContract select', '<option value="' + contract.key + '">' + contract.value + '</option>');
                });
            }

            callBack(true);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }


    $(document).ready(function () {
        app.onPageInit('addComplaint', function (page) {
            if ($rootScope.currentOpeningPage != 'addComplaint') return;
            $rootScope.currentOpeningPage = 'addComplaint';

        });

        app.onPageReinit('addComplaint', function (page) {
            //$scope.resetForm();
        });

        app.onPageBeforeAnimation('addComplaint', function (page) {
            if ($rootScope.currentOpeningPage != 'addComplaint') return;
            $rootScope.currentOpeningPage = 'addComplaint';

            $scope.isLoaded = true;

            $scope.isWorkerVisible = false;
            $scope.isContractVisible = false;

            $scope.resetForm();

            LoadProblemTypes(function (result) {
                LoadWorkers(function (result) {
                    LoadContracts(function (result) {

                    });
                });
            });

            $$('#linkAddComplaintProblemType select').on('change', function () {
                var problemTypeId = $(this).val();
                var problemType = $scope.problemTypes.filter(function (obj) {
                    return obj.key == problemTypeId;
                })[0];

                $scope.isWorkerVisible = problemTypeId == 100000009 ? true : false;
                $scope.isContractVisible = problemTypeId == 100000005 || problemTypeId == 100000006 || problemTypeId == 100000007 ? false : true;

                $scope.formFields.problemType = problemType;

                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            });

            $$('#linkAddComplaintContract select').on('change', function () {
                var contractId = $(this).val();
                var contract = $scope.contracts.filter(function (obj) {
                    return obj.key == contractId;
                })[0];

                $scope.formFields.contract = contract;
            });

        });

        app.onPageReinit('addComplaint', function (page) {
            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('addComplaint', function (page) {
            if ($rootScope.currentOpeningPage != 'addComplaint') return;
            $rootScope.currentOpeningPage = 'addComplaint';

        });

        $scope.form = {};
        $scope.addComplaintForm = {};
        $scope.formFields = {};

        $scope.resetForm = function () {
            $scope.addComplaintReset = false;
            $scope.submittedAddComplaint = false;

            $scope.isProblemTypeValid = true;
            $scope.isWorkerValid = true;
            $scope.isContractValid = true;

            $scope.isWorkerVisible = false;
            $scope.isContractVisible = false;

            $scope.addComplaintForm.problemType = null;
            $scope.addComplaintForm.worker = null;
            $scope.addComplaintForm.contract = null;
            $scope.addComplaintForm.description = null;

            if (typeof $scope.AddComplaintForm != 'undefined' && $scope.AddComplaintForm != null) {
                $scope.AddComplaintForm.$setPristine(true);
                $scope.AddComplaintForm.$setUntouched();
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        }

        $scope.submitForm = function (AddComplaintForm) {
            $scope.addComplaintReset = true;
            $scope.submittedAddComplaint = true;

            var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

            var problemTypeId = $('#linkAddComplaintProblemType select').val();
            var contractId = $('#linkAddComplaintContract select').val();
            var workerId = $('#linkAddComplaintWorker select').val();
            var description = $scope.addComplaintForm.description;

            $scope.isProblemTypeValid = $scope.addComplaintReset && $scope.submittedAddComplaint && problemTypeId != null && problemTypeId != '' && problemTypeId != ' ' ? true : false;
            $scope.isWorkerValid = $scope.addComplaintReset && $scope.submittedAddComplaint && ((workerId != null && workerId != '' && workerId != ' ') || problemTypeId != 100000009) ? true : false;
            $scope.isContractValid = $scope.addComplaintReset && $scope.submittedAddComplaint && contractId != null && contractId != '' && contractId != ' ' ? true : false;

            if (($scope.isProblemTypeValid && $scope.isWorkerValid && $scope.isContractValid && !AddComplaintForm.description.$invalid) || 
                (problemTypeId == 100000005 || problemTypeId == 100000006 || problemTypeId == 100000007)) {

                workerId = workerId == '' || workerId == ' ' ? workerId = null : workerId;

                var params = {
                    'ContactId': userLoggedIn.crmUserId,
                    'SectorTypeId': 4,
                    'ProblemTypeId': problemTypeId,
                    'ContractId': contractId,
                    'EmployeeId': workerId,
                    'ProblemDetails': description
                };

                app.showIndicator();
                appServices.CallService('addComplaint', "POST", "api/CustomerTicket/Dalal/Create_M", params, function (res) {
                    app.hideIndicator();
                    if (res != null) {
                        helpers.GoToPage('userComplaints', null);
                    }
                });

            }
        };



        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('intro', null);
        };

        $scope.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        $scope.nameRegex = /^[A-Za-z0-9]*$/;
        $scope.mobileRegex = /^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/;

        app.init();
    });

}]);

