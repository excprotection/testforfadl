﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('soonController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;

    $(document).ready(function () {
        app.onPageInit('soon', function (page) {
            if ($rootScope.currentOpeningPage != 'soon') return;
            $rootScope.currentOpeningPage = 'soon';

        });

        app.onPageBeforeAnimation('soon', function (page) {
            if ($rootScope.currentOpeningPage != 'soon') return;
            $rootScope.currentOpeningPage = 'soon';

            $scope.isLoaded = true;

            setTimeout(function () {
                $rootScope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageReinit('soon', function (page) {

        });

        app.onPageAfterAnimation('soon', function (page) {
            if ($rootScope.currentOpeningPage != 'soon') return;
            $rootScope.currentOpeningPage = 'soon';


        });

        $scope.GoBack = function () {
            helpers.GoBack();
        }

        $scope.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        $scope.mobileRegex = /^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/;

        app.init();
    });

}]);

