﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('userProfileReviewController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var advCount;
    var favCount;

    $(document).ready(function () {

        app.onPageInit('userProfileReview', function (page) {
            if ($rootScope.currentOpeningPage != 'userProfileReview') return;
            $rootScope.currentOpeningPage = 'userProfileReview';

        });

        app.onPageBeforeAnimation('userProfileReview', function (page) {
            if ($rootScope.currentOpeningPage != 'userProfileReview') return;
            $rootScope.currentOpeningPage = 'userProfileReview';

            var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

            if (CookieService.getCookie('StoredContract')) CookieService.removeCookie('StoredContract');

            $scope.isLoaded = false;

            $rootScope.RemoveEditPagesFromHistory();

            app.showIndicator();
            appServices.CallService('userProfileReview', 'GET', "api/contact/GetDetails/" + userLoggedIn.crmUserId, '', function (userData) {
                app.hideIndicator();
                if (userData) {
                    userLoggedIn.nationalityId = userData.contact.nationalityId;
                    userLoggedIn.cityId = userData.contact.cityId;
                    userLoggedIn.genderId = userData.contact.genderId;
                    userLoggedIn.idNumber = userData.contact.idNumber;

                    CookieService.setCookie('userLoggedIn', JSON.stringify(userLoggedIn));
                    $scope.user = userData.contact;
                    $scope.isLoaded = true;
                }
            });

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageAfterAnimation('userProfileReview', function (page) {
            if ($rootScope.currentOpeningPage != 'userProfileReview') return;
            $rootScope.currentOpeningPage = 'userProfileReview';

        });

        app.onPageReinit('userProfileReview', function (page) {
            $rootScope.RemoveEditPagesFromHistory();
        });

        $scope.GoBack = function () {
            helpers.GoToPage('introReview', null);
        };

        app.init();
    });

}]);

