﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('branchesController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';
    var lang = localStorage.getItem('Dalal_lang');

    function LoadBranches(callBack) {
        app.showIndicator();

        appServices.CallService('branches', "GET", "api/Branches", '', function (res) {
            app.hideIndicator();
            if (res != null && res.length > 0) {

                var branches = res;

                angular.forEach(branches, function (branch, index) {
                    branch.branchAddress = lang == 'AR' || typeof lang == 'undefined' || lang == null ? branch.arabicAddress : branch.englishAddress;
                    branch.branchName = lang == 'AR' || typeof lang == 'undefined' || lang == null ? branch.arabicName : branch.englishName;
                    branch.branchMapURL = branch.mapUrl;
                    branch.isAnotherMobileFound = branch.phoneNumber.indexOf(',') > -1 ? true : false;
                    branch.firstBranchMobile = branch.isAnotherMobileFound ? '+' + branch.phoneNumber.split(',')[0] : '+' + branch.phoneNumber;
                    branch.secondBranchMobile = branch.isAnotherMobileFound ? '+' + branch.phoneNumber.split(',')[1] : '+' + branch.phoneNumber;
                });

                callBack(branches);
            }
            else {
                callBack(null);
            }
        });
    }

    $(document).ready(function () {
        app.onPageInit('branches', function (page) {
            if ($rootScope.currentOpeningPage != 'branches') return;
            $rootScope.currentOpeningPage = 'branches';

        });

        app.onPageBeforeAnimation('branches', function (page) {
            if ($rootScope.currentOpeningPage != 'branches') return;
            $rootScope.currentOpeningPage = 'branches';

            LoadBranches(function (branches) {
                if (branches != null) {
                    $scope.noBranches = false;
                    $scope.branches = branches;
                }
                else {
                    $scope.noBranches = true;
                    $scope.branches = null;
                }

                setTimeout(function () {
                    $scope.isLoaded = true;
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            });

            $scope.isLoaded = false;

        });

        app.onPageReinit('branches', function (page) {
            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('branches', function (page) {
            if ($rootScope.currentOpeningPage != 'branches') return;
            $rootScope.currentOpeningPage = 'branches';

        });

        $scope.GoToURL = function (url) {
            cordova.InAppBrowser.open(url, '_blank', 'location=no,toolbar=yes,zoom=no');
        };

        $scope.CallEPDemo = function (mobile) {
            window.plugins.CallNumber.callNumber(
                      function onSuccess(successResult) {

                      }, function onError(errorResult) {
                          console.log("Error:" + errorResult);
                      }, mobile, true);
        };


        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('intro', null);
        };

        app.init();
    });

}]);

