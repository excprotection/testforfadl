﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('myOrdersReviewController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';
    var loadingContracts;
    var allContracts = [];
    var methodName;
    var isPull = true;
    $scope.noOrders = false;

    $(document).ready(function () {
        app.onPageInit('myOrdersReview', function (page) {
            if ($rootScope.currentOpeningPage != 'myOrdersReview') return;
            $rootScope.currentOpeningPage = 'myOrdersReview';

            fromPage = page.fromPage.name;
         
        });

        app.onPageBeforeAnimation('myOrdersReview', function (page) {
            if ($rootScope.currentOpeningPage != 'myOrdersReview') return;
            $rootScope.currentOpeningPage = 'myOrdersReview';

            $scope.isLoaded = true;
            if (localStorage.getItem("data")) {
                $scope.noOrders = false;
                var jsonData = JSON.parse(localStorage.getItem("data"));
                $scope.orders = jsonData;
                $scope.myOrders = [];
                angular.forEach($scope.orders, function (order, index) {
                        $scope.myOrders[index] = order.order;
                    
                });
            } else {
                $scope.noOrders = true;
            }
            fromPage = page.fromPage.name;
            isPull = false;
        
            var lang = localStorage.getItem('Dalal_lang');
            if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                $scope.lblUserFullName = ' الاسم بالكامل :';
                $scope.lblUserMobile = ' رقم الجوال :';
                $scope.lblOrderDetails = ' تفاصيل الطلب :';
                $scope.lblLocationAreaName = 'اسم المنطقة :';
                $scope.lblLocationStreetName = 'اسم الشارع :';
            }
           else {
                $scope.lblUserFullName = 'Full Name :';
                $scope.lblUserMobile = 'Mobile Number :';
                $scope.lblOrderDetails = 'Order Details :';
                $scope.lblLocationAreaName = 'Area Name :';
                $scope.lblLocationStreetName = 'Street Name :';
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);

        });

        app.onPageReinit('myOrdersReview', function (page) {
            isPull = false;

            $('.advNoResult').hide();

            fromPage = page.fromPage.name;

            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('myOrdersReview', function (page) {
            if ($rootScope.currentOpeningPage != 'myOrdersReview') return;
            $rootScope.currentOpeningPage = 'myOrdersReview';

            fromPage = page.fromPage.name;

        });

        $scope.GoBack = function () {
                helpers.GoBack();
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('introReview', null);
        };

        $scope.AddNewContract = function () {
            $rootScope.CheckAppInReview(function (InReview) {
                if (InReview == true) {
                    var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
                    if (userLoggedIn) {
                        helpers.GoToPage('individualSector', { requiredProfessionId: '5bc7f260-292f-e311-b3fd-00155d010303', InReview: InReview });
                    }
                    else {
                        helpers.GoToPage('login', null);
                    }
                }
                else {
                    helpers.GoToPage('terms', null);
                }
            });
        };

        app.init();
    });

}]);

