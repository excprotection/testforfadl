﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('changePasswordReviewController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    $scope.txtAlignValue = "left";

    $(document).ready(function () {
        app.onPageInit('changePassReview', function (page) {
            if ($rootScope.currentOpeningPage != 'changePassReview') return;
            $rootScope.currentOpeningPage = 'changePassReview';

        });

        app.onPageReinit('changePassReview', function (page) {
            $scope.resetForm();
        });

        app.onPageBeforeAnimation('changePassReview', function (page) {
            if ($rootScope.currentOpeningPage != 'changePassReview') return;
            $rootScope.currentOpeningPage = 'changePassReview';
            var lang = localStorage.getItem('Dalal_lang');
            if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                $scope.txtAlignValue = "right";
            } else {
                $scope.txtAlignValue = "left";
            }
            $scope.resetForm();
        });

        app.onPageAfterAnimation('changePassReview', function (page) {
            if ($rootScope.currentOpeningPage != 'changePassReview') return;
            $rootScope.currentOpeningPage = 'changePassReview';

            
        });

        $scope.resetForm = function () {
            $scope.changePasswordReset = false;
            $scope.changePassForm.oldPassword = null;
            $scope.changePassForm.newPassword = null;
            $scope.changePassForm.confrmNewPassword = null;
            if (typeof $scope.$parent.ChangePasswordForm != 'undefined' && $scope.$parent.ChangePasswordForm != null) {
                $scope.$parent.ChangePasswordForm.$setPristine(true);
                $scope.$parent.ChangePasswordForm.$setUntouched();
            }
        }

        $scope.form = {};
        $scope.changePassForm = {};

        $scope.submitForm = function (isValid) {
            var userLoggedIn=JSON.parse(CookieService.getCookie('userLoggedIn'));
            
            $scope.changePasswordReset = true;
            if (isValid) {
                var params = {
                    'userId': userLoggedIn.id,
                    'oldPassword': $scope.changePassForm.oldPassword,
                    'newPassword': $scope.changePassForm.newPassword,
                    'confirmPassword': $scope.changePassForm.confrmNewPassword
                }

                app.showIndicator();
                appServices.CallService('changePass', "POST", "api/account/ChangePassword", params, function (res) {
                    app.hideIndicator();
                    if (res != null || res == true) {
                        language.openFrameworkModal('نجاح', 'تم تعديل كلمة السر بنجاح .', 'alert', function () {});
                        $scope.resetForm();
                        helpers.GoToPage('introReview', null);
                    }
                });

            }
        };

        $scope.GoBack = function () {
            helpers.GoBack();
        }

        $scope.passRegex = /^[A-Za-z0-9]*$/;

        app.init();
    });

}]);

