﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('previousLocationsController', ['$document', '$scope', '$q', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $q, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';
    var map;
    var latitude;
    var longitiude;
    var lang = localStorage.getItem('Dalal_lang');

    function GetStoredContract() {
        var contract = JSON.parse(CookieService.getCookie('StoredContract'));
        return contract;
    }

    function GetAddressFromLatLang(geocoder, addressesList) {
        var addresses = [];

        var promise1 = new Promise(function (resolve, reject) {
            angular.forEach(addressesList, function (address, index) {
                var latlng = { lat: parseFloat(address.new_latitude), lng: parseFloat(address.new_longitude) };

                var floorNumber = address.new_floorNumber;
                var houseType = address.new_houseType;
                var partmentNumber = address.new_partmentNumber;

                partmentNumber = address.new_partmentNumber == null || address.new_partmentNumber == '' || address.new_partmentNumber == ' ' ? '0' : address.new_partmentNumber;

                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    floorNumber = address.new_floorNumber == '0' ? 'أرضي' : address.new_floorNumber;
                    houseType = address.new_houseType == '0' ? 'فيلا' : 'عمارة';
                }
                else {
                    floorNumber = address.new_floorNumber == '0' ? 'Ground' : address.new_floorNumber;
                    houseType = address.new_houseType == '0' ? 'Villa' : 'Appartment';
                }

                geocoder.geocode({ 'location': latlng }, function (results, status) {
                    if (status === 'OK') {
                        if (results[0]) {
                            addresses.push({
                                id: index,
                                name: address.new_ContractNumber + ' - ' + houseType + ' ' + address.new_houseNumber + ' - ' + floorNumber + ' - ' + partmentNumber + ' - ' + results[0].formatted_address,
                                lat: address.new_latitude,
                                lng: address.new_longitude,
                                houseType: houseType,
                                houseNumber: address.new_houseNumber,
                                floorNumber: floorNumber,
                                partmentNUmber: partmentNumber,
                                houseAddress:results[0].formatted_address,
                                contractNumber: address.new_ContractNumber
                            });
                        }
                    }
                    else {
                        addresses.push({
                            id: -1,
                            name: address.new_ContractNumber + ' - ' + houseType + ' ' + address.new_houseNumber + ' - ' + floorNumber + ' - ' + partmentNumber + ' -  لا يوجد عنوان ',
                            lat: address.new_latitude,
                            lng: address.new_longitude,
                            houseType: houseType,
                            houseNumber: address.new_houseNumber,
                            floorNumber: floorNumber,
                            partmentNUmber: partmentNumber,
                            houseAddress:'لا يوجد عنوان',
                            contractNumber: address.new_ContractNumber
                        });
                    }


                    if (addresses.length == addressesList.length) {
                        var newArray = addresses.filter(function (el) {
                            return parseInt(el.id) > -1;
                        });

                        resolve(newArray);
                    }
                });

            });


        });

        promise1.then(function (addresses) {
            app.hideIndicator();
            $scope.previousLocations = addresses;
            $scope.NoLocations = false;
            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    function LoadPreviousLocations(contractId, callBack) {
        var previousLocations = [];

        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

        app.showIndicator();

        appServices.CallService('previousLocations', "GET", "api/HourlyContract/GetContractLocations/" + userLoggedIn.crmUserId, '', function (res) {
            app.hideIndicator();

            if (res != null && res.result != null && res.result.length > 0) {
                var geocoder = new google.maps.Geocoder;
                GetAddressFromLatLang(geocoder, res.result);
            }
            else {
                $scope.NoLocations = true;
                $scope.previousLocations = null;
                setTimeout(function () {
                    helpers.GoToPage('location', null);
                }, fw7.DelayBeforeScopeApply);
            }
        });
    }

    function ClearPreviousLocationSelected() {
        $('.myCheckBox').removeAttr('checked');

        $scope.previousLocation = null;
        $scope.isMapValid = false;
    }



    $(document).ready(function () {
        app.onPageInit('previousLocations', function (page) {
            if ($rootScope.currentOpeningPage != 'previousLocations') return;
            $rootScope.currentOpeningPage = 'previousLocations';

        });

        app.onPageReinit('previousLocations', function (page) {

        });

        app.onPageBeforeAnimation('previousLocations', function (page) {
            if ($rootScope.currentOpeningPage != 'previousLocations') return;
            $rootScope.currentOpeningPage = 'previousLocations';

            $scope.isLoaded = true;

            var lang = localStorage.getItem('Dalal_lang');
            $scope.isArabic = lang == 'AR' || typeof lang == 'undefined' || lang == null ? true : false;

            $scope.previousLocations = null;

            var contract = GetStoredContract();
            $scope.resetForm();

            LoadPreviousLocations(contract.id, function () { });

        });

        app.onPageReinit('previousLocations', function (page) {
            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('previousLocations', function (page) {
            if ($rootScope.currentOpeningPage != 'previousLocations') return;
            $rootScope.currentOpeningPage = 'previousLocations';

        });

        $scope.resetForm = function () {
            $scope.previousLocation = null;
            $scope.NoLocations = true;


            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        }

        $scope.selectLocation = function (location, position) {
            var element = document.getElementById('chkLocation_' + location.id);
            var isChecked = $(element).prop('checked');
            $scope.previousLocation = location;
        };

        $scope.GoToPickLocation = function () {
            if ($scope.previousLocation != 'undefined' && $scope.previousLocation != null) {
                var storedContract = GetStoredContract();

                storedContract.HouseType = $scope.previousLocation.houseType;
                storedContract.HouseNumber = $scope.previousLocation.houseNumber;
                storedContract.FloorNumber = $scope.previousLocation.floorNumber;
                storedContract.AppartmentNumber = $scope.previousLocation.partmentNUmber;
                storedContract.HouseAddress = $scope.previousLocation.houseAddress;
                storedContract.Latitude = $scope.previousLocation.lat;
                storedContract.Longtitude = $scope.previousLocation.lng;
                CookieService.setCookie('StoredContract', JSON.stringify(storedContract));

                helpers.GoToPage('contractDetails', null);
            }
            else {
                var lang = localStorage.getItem('Dalal_lang');

                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    app.confirm('لا يوجد مواقع سابقة , برجاء تحديد الموقع الخاص بك', 'تأكيد', function () {
                        helpers.GoToPage('location', null);
                    }, function () {
                        
                    });
                }
                else {
                    app.confirm('There is no previous locations , Please select your location ', 'Confirm', function () {
                        helpers.GoToPage('location', null);
                    }, function () {
                        
                    });
                }
                
            }

            
        };

        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('intro', null);
        };

        app.init();
    });

}]);

