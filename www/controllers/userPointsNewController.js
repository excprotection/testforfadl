﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('userPointsNewController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';
    var loadingPoints;
    var allPoints = [];
    var methodName;
    var isPull = true;
    var lang = localStorage.getItem('Dalal_lang');

    var IsArabic=(lang == 'AR' ||lang == 'ar' || typeof lang == 'undefined' || lang == null)?true:false;
    var ActiveStatusTxt=IsArabic?'مفعل':'Active';
    var ActiveStatusTxt = IsArabic ?  'مفعلة ' : 'Active';
    var NotActiveStatusTxt = IsArabic ? 'غير مفعلة ' : 'Not Active';

    function ClearListData() {
        allPoints = [];
        loadingPoints = false;
        CookieService.setCookie('points-page-number', 1);
        $scope.points = null;
        app.pullToRefreshDone();
    }

    function LoadUserPoints(callBack) {
        CookieService.setCookie('points-page-number', 1);
        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
        var methodName = 'api/Loyality/PointsSource_M?userId=' + userLoggedIn.crmUserId + '&pageNumber=' +
            parseInt(CookieService.getCookie('points-page-number')) + '&pageSize=' + myApp.fw7.PageSize + '&Status=' + $scope.pointsStatus;
        allPoints = [];
        app.attachInfiniteScroll('#divInfiniteUserPoints');
        app.showIndicator();
        appServices.CallService('userPoints', 'GET', methodName, '', function (result) {
            app.hideIndicator();
            allPoints = [];
            if (result && result.pointsSource.length > 0) {
                if (result.pointsSource.length < myApp.fw7.PageSize) {
                    $scope.pointsInfiniteLoader = false;
                    app.detachInfiniteScroll('#divInfiniteUserPoints');
                }
                else {
                    $scope.pointsInfiniteLoader = true;
                    app.attachInfiniteScroll('#divInfiniteUserPoints');
                }
                
                allPoints = result.pointsSource;

                allPoints.sort(function (a, b) {
                    return new Date(b.validTill).getTime() - new Date(a.validTill).getTime();
                });
                
                angular.forEach(allPoints, function (pointsObj, index) {
                    pointsObj.statusName = pointsObj.status == '100000000' ? NotActiveStatusTxt : ActiveStatusTxt;
                    pointsObj.validTill = pointsObj.validTill.indexOf('T') > -1 ? pointsObj.validTill.split('T')[0] : pointsObj.validTill;
                });

                $scope.userPointsList = allPoints;

                $scope.noPoints = false;

                if (isPull) {
                    app.pullToRefreshDone();
                }

                callBack(true);

                setTimeout(function () {
                    $scope.allPointsLoaded = true;
                    $scope.isLoaded = true;
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);

            }
            else {
                allPoints = [];
                $scope.points = [];
                $scope.noPoints = true;
                $('.advNoResult').show();
                $scope.pointsInfiniteLoader = false;

                callBack(true);

                setTimeout(function () {
                    $scope.isLoaded = true;
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            }

        });
    }

    $(document).ready(function () {
        app.onPageInit('userPoints', function (page) {
            if ($rootScope.currentOpeningPage != 'userPoints') return;
            $rootScope.currentOpeningPage = 'userPoints';

            fromPage = page.fromPage.name;

            $$('#divInfiniteUserPoints').on('ptr:refresh', function (e) {
                isPull = true;

                LoadUserPoints(function (result) {
                    app.pullToRefreshDone();
                });

                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            });


            $$('#divInfiniteUserPoints').on('infinite', function () {
                if (loadingPoints) return;
                loadingPoints = true;

                var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
                CookieService.setCookie('points-page-number', parseInt(CookieService.getCookie('points-page-number')) + 1);
                var methodName = 'api/Loyality/PointsSource_M?userId=' + userLoggedIn.crmUserId + '&pageNumber=' +
                    parseInt(CookieService.getCookie('points-page-number')) + '&pageSize=' + myApp.fw7.PageSize + '&Status=' + $scope.pointsStatus;

                appServices.CallService('userPoints', 'GET', methodName, '', function (response) {

                    if (response && response.pointsSource.length > 0) {
                        loadingPoints = false;

                        angular.forEach(response.pointsSource, function (point, index) {
                            allPoints.push(point);
                        });

                        allPoints.sort(function (a, b) {
                            return new Date(b.validTill).getTime() - new Date(a.validTill).getTime();
                        });

                        angular.forEach(allPoints, function (pointsObj, index) {
                            pointsObj.statusName = pointsObj.status == '100000000' ? NotActiveStatusTxt : ActiveStatusTxt;
                            pointsObj.validTill = pointsObj.validTill.indexOf('T') > -1 ? pointsObj.validTill.split('T')[0] : pointsObj.validTill;
                        });

                        $scope.points = allPoints;

                        if (response && response.length < myApp.fw7.PageSize) {
                            $scope.pointsInfiniteLoader = false;
                            app.detachInfiniteScroll('#divInfiniteUserPoints');
                            return;
                        }
                    }
                    else {
                        $scope.pointsInfiniteLoader = false;
                        app.detachInfiniteScroll('#divInfiniteUserPoints');
                        loadingPoints = false;
                    }

                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                });
            });
        });

        app.onPageBeforeAnimation('userPoints', function (page) {
            if ($rootScope.currentOpeningPage != 'userPoints') return;
            $rootScope.currentOpeningPage = 'userPoints';

            $scope.isLoaded = false;

            fromPage = page.fromPage.name;
            $scope.AllPoints = page.query.allPoints;
            $scope.pointAmount = page.query.PointsAmount;
            $scope.pointsStatus = page.query.status;
            $scope.AllowConvert = $scope.pointsStatus == '100000000' ? true : false;
            $scope.pointsStatusTxt = $scope.pointsStatus == '100000000' ? NotActiveStatusTxt : ActiveStatusTxt;
            isPull = false;

            LoadUserPoints(function (result) {
                var lang = localStorage.getItem('Dalal_lang');

                $scope.isLoaded = true;

                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    $scope.lblContractNumber = 'رقم العقد :';
                    $scope.lblContact = 'العميل :';
                    $scope.lblAmount = 'المبلغ :';
                    $scope.lbldate = 'صالح حتي :';
                    $scope.lblNumberOfPoints = 'عدد النقاط :';

                    $scope.lblContractCurrency = 'ريال';
                }
                else {
                    $scope.lblContractNumber = 'Number:';
                    $scope.lblContactNumber = 'User :';
                    $scope.lbldate = 'Valid To:';
                    $scope.lblAmount = 'Amount :';
                    $scope.lblNumberOfPoints = 'Points :';

                    $scope.lblContractCurrency = 'SAR';
                }
            });

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);

        });

        app.onPageReinit('userPoints', function (page) {
            isPull = false;

            $('.advNoResult').hide();

            fromPage = page.fromPage.name;

            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('userPoints', function (page) {
            if ($rootScope.currentOpeningPage != 'userPoints') return;
            $rootScope.currentOpeningPage = 'userPoints';

            fromPage = page.fromPage.name;

        });


        $scope.GoBack = function () {
                helpers.GoBack();
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('intro', null);
        };

        $scope.ConvertPoints = function (id) {
            app.showIndicator();

            appServices.CallService('userPoints', 'GET', 'api/Loyality/ChangeStatus_M?id=' + id, '', function (result) {
                app.hideIndicator();
                if (result) {
                    if (IsArabic) {
                        language.openFrameworkModal('نجاح', 'تم تحويل نقاطك بنجاح .', 'alert', function () {
                            helpers.GoToPage('neqaty', null);
                        });
                    }
                    else {
                        language.openFrameworkModal('Success', 'Your Points Converted Successfully', 'alert', function () {
                            helpers.GoToPage('neqaty', null);
                        });
                    }

                }
                else {
                    if (IsArabic) {
                        language.openFrameworkModal('خطأ', 'لم تحويل نقاطك  .', 'alert', function () {

                        });
                    }
                    else {
                        language.openFrameworkModal('Error', 'Your Points Not Converted', 'alert', function () { });
                    }
                }

            });
        }
 
        app.init();
    });

}]);

