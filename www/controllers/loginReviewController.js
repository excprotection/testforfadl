﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('loginReviewController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var pageName = '';
    var lang = localStorage.getItem('Dalal_lang');
    $scope.txtAlignValue = "left";

    $http.get('../Data/data.json')
        .then(function (data) {
           $scope.test = data;
})
    function OpenActivationCodePopup(userId, code, mobile, password) {
        var codeText = 'الكود فقط للتجربة :';
        var codePlaceHolder = 'كود التفعيل';
        var resendCodetext = 'إعادة إرسال الكود';
        var activateText = 'تفعيل';
        var cancelText = 'إلغاء';

        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            codeText = 'الكود فقط للتجربة :';
            codePlaceHolder = 'كود التفعيل';
            resendCodetext = 'إعادة إرسال الكود';
            activateText = 'تفعيل';
            cancelText = 'إلغاء';
        }
        else {
            codeText = 'Code For Trial Use Only';
            codePlaceHolder = 'Activation Code';
            resendCodetext = 'Resend Code';
            activateText = 'Activate';
            cancelText = 'Cancel';
        }

        var afterText = '';

        if (serviceURL.indexOf("http://test.EPDemo.sa") > -1)
        {
            afterText= '<div class="list-block">' +
            '<div class="m-auto">' +
            '<ul>' +
            '<li style="text-align:center;">' +
             '<label class="lblCode">' + codeText + code + ' </label>' +
            '</li>' +
            '<li>' +
            '<div class="item-content">' +
            '<div class="item-inner">' +
            '<div class="item-input">' +
            '<input id="txtCode" type="number" name="phone" placeholder="' + codePlaceHolder + '">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</li>' +
            '</ul>' +
            '</div>' +
            '</div>';
        }
        else
        {
            afterText= '<div class="list-block">' +
            '<div class="m-auto">' +
            '<ul>' +
            '<li>' +
            '<div class="item-content">' +
            '<div class="item-inner">' +
            '<div class="item-input">' +
            '<input id="txtCode" type="number" name="phone" placeholder="' + codePlaceHolder + '">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</li>' +
            '</ul>' +
            '</div>' +
            '</div>';
        }

        var activationSignup = app.modal({
            title: codePlaceHolder,
            text: '',
            afterText:afterText,
            buttons: [
                {
                    text: activateText,
                    onClick: function () {
                        var params = {
                            'code': $('#txtCode').val(),
                            'userId': userId,
                            'phoneNumber': mobile,
                            'password': password
                        };

                        app.showIndicator();
                        appServices.CallService('signupUser', "POST", 'api/account/VerifyCode', params, function (res) {
                            app.hideIndicator();

                            if (res != null) {
                                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                                    language.openFrameworkModal('نجاح', 'تم التأكد من الجوال بنجاح .', 'alert', function () { });
                                }
                                else {
                                    language.openFrameworkModal('نجاح', 'Mobile Is Confirmed Successfully', 'alert', function () { });
                                }

                                app.closeModal(activationSignup);

                                CookieService.setCookie('appToken', res.access_token);
                                CookieService.setCookie('USName', res.userName);
                                CookieService.setCookie('Visitor', false);
                                CookieService.setCookie('loginReviewUsingSocial', false);
                                CookieService.setCookie('UserEntersCode', 'true');
                                CookieService.setCookie('userLoggedIn', JSON.stringify(res.user));
                                helpers.GoToPage('introReview', null);

                            }
                            else {
                                language.openFrameworkModal('خطأ', 'كود التفعيل غير صحيح .', 'alert', function () { });
                                OpenActivationCodePopup(userId, code, mobile, password);
                            }
                        });
                    }
                },
                {
                    text: resendCodetext,
                    onClick: function () {
                        var params = {
                            'phoneNumber': mobile,
                            'userId': userId,
                        };

                        app.showIndicator();
                        appServices.CallService('signupUser', "POST", 'api/account/ReGenrateCode?IsSMSEnabled=true', params, function (res) {
                            app.hideIndicator();
                            if (res != null) {
                                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                                    language.openFrameworkModal('نجاح', 'تم إعادة إرسال الكود للجوال بنجاح .', 'alert', function () { });
                                }
                                else {
                                    language.openFrameworkModal('نجاح', 'Code Is Resent To Your Mobile Successfully', 'alert', function () { });
                                }
                                OpenActivationCodePopup(userId, res.code, mobile, password);
                            }
                            else {
                                OpenActivationCodePopup(userId, res.code, mobile, password);
                            }
                        });
                    }
                },
                {
                    text: cancelText,
                    onClick: function () {
                        app.closeModal(activationSignup);
                    }
                }
            ]
        });
    }

    function EnterApplication() {
        if (CookieService.getCookie('userLoggedIn') || CookieService.getCookie('UserID')) {
            if (CookieService.getCookie('UserEntersCode') == "false") {
                if (CookieService.getCookie('loginReviewUsingSocial') == 'true') {
                    helpers.GoToPage('introReview', null);
                } else {
                    OpenActivationCodePopup();
                }
            }
            else {
                helpers.GoToPage('introReview', null);
            }
        }
        else {
            if (CookieService.getCookie('UID')) {
                helpers.GoToPage('signupUserReview', null);
            }
            else {
                helpers.GoToPage('loginReview', null);
            }
        }
    }


    //EnterApplication();


    $(document).ready(function () {

        app.onPageBeforeInit('loginReview', function (page) {
            if ($rootScope.currentOpeningPage != 'loginReview') return;
            $rootScope.currentOpeningPage = 'loginReview';
        });

        app.onPageInit('loginReview', function (page) {
            if ($rootScope.currentOpeningPage != 'loginReview') return;
            $rootScope.currentOpeningPage = 'loginReview';

            $scope.resetForm();
        });

        app.onPageBeforeAnimation('loginReview', function (page) {
            if ($rootScope.currentOpeningPage != 'loginReview') return;
            $rootScope.currentOpeningPage = 'loginReview';

            var lang = localStorage.getItem('Dalal_lang');
            if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                $scope.txtAlignValue = "right";
            } else {
                $scope.txtAlignValue = "left";
            }
            $scope.resetForm();
        });

        app.onPageAfterAnimation('loginReview', function (page) {
            if ($rootScope.currentOpeningPage != 'loginReview') return;
            $rootScope.currentOpeningPage = 'loginReview';
            $scope.resetForm();

            pageName = page.fromPage.name;
        });

        $scope.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        $scope.nameRegex = /^[A-Za-z0-9]*$/;
        $scope.mobileRegex = /^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/;

        app.init();
    });

    $scope.resetForm = function () {
        $scope.loginReset = false;
        $scope.loginReviewForm.mobile = null;
        $scope.loginReviewForm.password = null;

        if (typeof $scope.LoginReviewForm != 'undefined' && $scope.LoginReviewForm != null) {
            $scope.LoginReviewForm.$setPristine(true);
            $scope.LoginReviewForm.$setUntouched();
        }

        setTimeout(function () {
            $scope.$apply();
        }, fw7.DelayBeforeScopeApply);
    }

    function RegisterDevice(callBack) {
        //helpers.RegisterDevice(true, function (result) {
        //    callBack(true);
        //});

       callBack(true);
    }

    $scope.PassToHome = function () {
        RegisterDevice(function (result) {
            app.hideIndicator();
            CookieService.setCookie('Visitor', true);
            CookieService.removeCookie('userLoggedIn');
            SidePanelService.DrawMenu();
            helpers.GoToPage('introReview', null);
        });
    };

    $scope.GoToSignUp = function () {
        helpers.GoToPage('signupUserReview', null);
    };

    $scope.GoToForgetPassword = function () {
        helpers.GoToPage('forgetPassReview', null);
    };

    $scope.form = {};
    $scope.loginReviewForm = {};
    $scope.submitForm = function (isValid) {
        $scope.loginReset = true;

        if (isValid) {
            var loginEmail = $scope.loginReviewForm.mobile, loginPass = $scope.loginReviewForm.password;

            loginEmail = helpers.parseArabic(loginEmail);

            app.showIndicator();

            RegisterDevice(function (result) {
                var deviceId = CookieService.getCookie('deviceId');

                var params = {
                    'userName': loginEmail,
                    'password': loginPass
                };

                appServices.CallService('loginReview', "POST", "api/account/login", params, function (result,user,code) {
                    app.hideIndicator();
                    if (result != null && result != 'notActive') {
                        var token = JSON.parse(result.token);
                        CookieService.setCookie('appToken', token.access_token);
                        CookieService.setCookie('Visitor', false);
                        CookieService.setCookie('loginReviewUsingSocial', false);
                        CookieService.setCookie('UserEntersCode', 'true');
                        CookieService.setCookie('userLoggedIn', JSON.stringify(result.user));
                        SidePanelService.DrawMenu();
                        if (typeof result.image != 'undefined' && result.image != null && result.image != '' && result.image != ' ') {
                            CookieService.setCookie('usrPhoto', result.image);
                        }
                        else {
                            CookieService.removeCookie('usrPhoto');
                        }

                        helpers.GoToPage('introReview', null);
                    }
                    else {
                        if (result == 'notActive') {
                            OpenActivationCodePopup(user.id, code, user.phoneNumber);
                        }
                    }
                });
            });
        }
    };

    $scope.loginWithFacebook = function () {
        var fbLoginFailed = function (error) {
            
        }

        var fbLoginSuccess = function (userData) {
            if (userData.authResponse) {
                var deviceId = CookieService.getCookie('deviceId');
                var accessToken = userData.authResponse.accessToken;
                var userFullName = '';
                var userEmail = '';

                facebookConnectPlugin.api(userData.authResponse.userID + "/?fields=id,email,first_name,last_name", ["public_profile"],
                    function (result) {
                        userFullName = result.first_name + ' ' + result.last_name;

                        CookieService.setCookie('socialEmail', result.email);
                        var registerObj = {
                            "social_provider": "Facebook",
                            "social_key": userData.authResponse.userID,
                            "name": userFullName,
                            'email': result.email,
                            'image': 'https://graph.facebook.com/' + userData.authResponse.userID + '/picture?type=large',
                            'username': userFullName
                        };

                        RegisterDevice(function (result) {
                            var deviceId = CookieService.getCookie('deviceId');
                            registerObj.deviceId = deviceId;
                            appServices.CallService('loginReview', "POST", "api/v1/social/login?playerId="+deviceId, registerObj, function (res) {
                                app.hideIndicator();
                                if (res != null) {
                                    CookieService.setCookie('USName', res.username);
                                    CookieService.setCookie('appToken', res.api_token);
                                    CookieService.setCookie('Visitor', false);
                                    CookieService.setCookie('loginReviewUsingSocial', true);
                                    CookieService.setCookie('userLoggedIn', JSON.stringify(res));
                                    if (typeof res.image != 'undefined' && res.image != null && res.image != '' && res.image != ' ') {
                                        CookieService.setCookie('usrPhoto', res.image);
                                    }
                                    else {
                                        CookieService.removeCookie('usrPhoto');
                                    }
                                    SidePanelService.DrawMenu();
                                    helpers.GoToPage('introReview', null);
                                }
                            });
                        });
                    },
                    function (error) {
                        console.log('ERROR:' + error);
                    });
            }
        }

        facebookConnectPlugin.login(["public_profile", "email"], fbLoginSuccess, fbLoginFailed);

    };

    $scope.loginWithTwitter = function () {
        TwitterConnect.login(function (data) {
            var accessToken = data.token;
            var userFullName = '';

            TwitterConnect.showUser(function (result) {
                var userFullName = result.name;
                var deviceId = CookieService.getCookie('deviceId');

                var registerObj = {
                    "social_provider": "Twitter",
                    "social_key": data.userId,
                    "name": userFullName,
                    'email': '',
                    'image': result.profile_image_url,
                    'username': userFullName,
                };

                RegisterDevice(function (result) {
                    var deviceId = CookieService.getCookie('deviceId');
                    registerObj.deviceId = deviceId;
                    appServices.CallService('loginReview', "POST", "api/v1/social/login?playerId="+deviceId, registerObj, function (res) {
                        app.hideIndicator();
                        if (res != null) {
                            CookieService.setCookie('USName', res.userName);
                            CookieService.setCookie('appToken', res.api_token);
                            CookieService.setCookie('Visitor', false);
                            CookieService.setCookie('loginReviewUsingSocial', true);
                            CookieService.setCookie('userLoggedIn', JSON.stringify(res));
                            if (typeof res.image != 'undefined' && res.image != null && res.image != '' && res.image != ' ') {
                                CookieService.setCookie('usrPhoto', res.image);
                            }
                            else {
                                CookieService.removeCookie('usrPhoto');
                            }
                            SidePanelService.DrawMenu();
                            helpers.GoToPage('introReview', null);
                        }
                    });
                });

            }, function (error) {
                language.openFrameworkModal('خطأ', 'خطأ في إسترجاع بيانات المستخدم', 'alert', function () { });
            });



        }, function (error) {
            console.log('Error in Login');
        });

    };

    $scope.loginWithGooglePlus = function () {
        window.plugins.googleplus.login(
                {
                    'scopes': 'profile email',
                    'webClientId': '1026744162651-eel8p51gvstv2uh12cn5h4oeuceop414.apps.googleusercontent.com',
                    'offline': false,
                },
                function (obj) {
                    var userName = obj.displayName;
                    var userEmail = obj.email;
                    var userId = obj.userId;
                    var userToken = obj.idToken;
                    var userPicture = obj.imageUrl;

                    var deviceId = CookieService.getCookie('deviceId');

                    CookieService.setCookie('socialEmail', userEmail);

                    var registerObj = {
                        "social_provider": "Google",
                        "social_key": userId,
                        "name": userName,
                        "email": userEmail,
                        'image': userPicture,
                        'username': userName,
                    };

                    RegisterDevice(function (result) {
                        var deviceId = CookieService.getCookie('deviceId');
                        registerObj.deviceId = deviceId;
                        appServices.CallService('loginReview', "POST", "api/v1/social/login?playerId="+deviceId, registerObj, function (res) {
                            app.hideIndicator();
                            if (res != null) {
                                CookieService.setCookie('USName', res.username);
                                CookieService.setCookie('appToken', res.api_token);
                                CookieService.setCookie('Visitor', false);
                                CookieService.setCookie('loginReviewUsingSocial', true);
                                CookieService.setCookie('userLoggedIn', JSON.stringify(res));
                                if (typeof res.image != 'undefined' && res.image != null && res.image != '' && res.image != ' ') {
                                    CookieService.setCookie('usrPhoto', res.image);
                                }
                                else {
                                    CookieService.removeCookie('usrPhoto');
                                }
                                SidePanelService.DrawMenu();
                                helpers.GoToPage('introReview', null);
                            }
                        });
                    });
                },
                function (msg) {
                    console.log('error: ' + msg);
                });
    };

}]);

