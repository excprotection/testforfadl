﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('userInvoicesController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';
    var loadinginvoices;
    var allinvoices = [];
    var methodName;
    var isPull = true;

    function ClearListData() {
        allinvoices = [];
        loadinginvoices = false;
        CookieService.setCookie('invoices-page-number', 1);
        $scope.invoices = null;
        app.pullToRefreshDone();
    }

    function LoadUserInvoices(callBack) {
        CookieService.setCookie('invoices-page-number', 1);
        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

        var methodName = 'api/DomesticInvoice/GetUserInvoices_M/' + userLoggedIn.crmUserId;

        allinvoices = [];
        app.attachInfiniteScroll('#divInfiniteinvoices');

        app.showIndicator();

        appServices.CallService('userInvoices', 'GET', methodName, '', function (result) {

            app.hideIndicator();

            allinvoices = [];

            if (result && result.invoices && result.invoices.length > 0) {
                
                    $scope.invoicesInfiniteLoader = false;
                    app.detachInfiniteScroll('#divInfiniteinvoices');
               

                allinvoices = result.invoices;

                allinvoices.sort(function (a, b) {
                    return new Date(b.dueDate).getTime() - new Date(a.dueDate).getTime();
                });

                angular.forEach(allinvoices, function (invoice, index) {
                    invoice.dueDate = invoice.dueDate.split('T')[0];
                });

                $scope.invoices = allinvoices;

                $scope.noinvoices = false;

                if (isPull) {
                    app.pullToRefreshDone();
                }

                callBack(true);

                setTimeout(function () {
                    $scope.allinvoicesLoaded = true;
                    $scope.isLoaded = true;
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);

            }
            else {
                allinvoices = [];
                $scope.invoices = [];
                $scope.noinvoices = true;
                $('.advNoResult').show();
                $scope.invoicesInfiniteLoader = false;

                callBack(true);

                setTimeout(function () {
                    $scope.isLoaded = true;
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            }

        });
    }

    $(document).ready(function () {
        app.onPageInit('userInvoices', function (page) {
            if ($rootScope.currentOpeningPage != 'userInvoices') return;
            $rootScope.currentOpeningPage = 'userInvoices';

            fromPage = page.fromPage.name;

            $$('#divInfiniteinvoices').on('ptr:refresh', function (e) {
                isPull = true;

                LoadUserInvoices(function (result) {
                    app.pullToRefreshDone();
                });

                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            });


            //$$('#divInfiniteinvoices').on('infinite', function () {
            //    if (loadinginvoices) return;
            //    loadinginvoices = true;

            //    var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

            //    CookieService.setCookie('invoices-page-number', parseInt(CookieService.getCookie('invoices-page-number')) + 1);


            //    var methodName = 'api/DomesticInvoice/GetUserInvoices_M/' + userLoggedIn.crmUserId+ '&pageNumber=' +
            //        parseInt(CookieService.getCookie('invoices-page-number'));

            //    appServices.CallService('userInvoices', 'GET', methodName, '', function (response) {

            //        if (response && response.invoices.length > 0) {
            //            loadinginvoices = false;

            //            angular.forEach(response.invoices, function (contract, index) {
            //                allinvoices.push(contract);
            //            });

            //            allinvoices.sort(function (a, b) {
            //                return new Date(b.dueDate).getTime() - new Date(a.dueDate).getTime();
            //            });

            //            angular.forEach(allinvoices, function (contract, index) {
            //                contract.dueDate = contract.createOn.split(' ')[0];
            //            });

            //            $scope.invoices = allinvoices;

                        
            //                $scope.invoicesInfiniteLoader = false;
            //                app.detachInfiniteScroll('#divInfiniteinvoices');
            //                return;
                        
            //        }
            //        else {
            //            $scope.invoicesInfiniteLoader = false;
            //            app.detachInfiniteScroll('#divInfiniteinvoices');
            //            loadinginvoices = false;
            //        }

            //        setTimeout(function () {
            //            $scope.$apply();
            //        }, fw7.DelayBeforeScopeApply);
            //    });
            //});
        });

        app.onPageBeforeAnimation('userInvoices', function (page) {
            if ($rootScope.currentOpeningPage != 'userInvoices') return;
            $rootScope.currentOpeningPage = 'userInvoices';

            $scope.isLoaded = false;

            fromPage = page.fromPage.name;

            ClearListData();

            isPull = false;

            LoadUserInvoices(function (result) {
                var lang = localStorage.getItem('Dalal_lang');
                $scope.lang = lang;
                $scope.isLoaded = true;

                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    $scope.lblComplaintTicketNumber = 'رقم الفاتورة :';
                    $scope.lblComplaintContractNumber = 'العقد :';
                    $scope.lblComplaintTicketDesc = 'التاريخ :';
                    $scope.lblInvoiceValue = 'المبلغ : ';
                    $scope.lblContractCurrency = 'ريال';
                }
                else {
                    $scope.lblComplaintTicketNumber = 'Invoice Number : ';
                    $scope.lblComplaintContractNumber = 'Contract : ';
                    $scope.lblComplaintTicketDesc = 'Date : ';
                    $scope.lblInvoiceValue = 'Cost :';
                    $scope.lblContractCurrency = 'SAR';
                }
            });

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);

        });

        app.onPageReinit('userInvoices', function (page) {
            isPull = false;

            $('.advNoResult').hide();

            fromPage = page.fromPage.name;

            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('userInvoices', function (page) {
            if ($rootScope.currentOpeningPage != 'userInvoices') return;
            $rootScope.currentOpeningPage = 'userInvoices';

            fromPage = page.fromPage.name;

        });

        //$scope.GoToinvoiceDetails = function (invoice) {
        //    helpers.GoToPage('invoiceInfo', { invoice: invoice });
        //};


        $scope.GoBack = function () {
            if (fromPage == 'paymentMethod') {
                helpers.GoToPage('intro', null);
            }
            else {
                helpers.GoBack();
            }
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('intro', null);
        };

      
        app.init();
    });

}]);

