﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('locationController', ['$document', '$scope', '$q', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope,$q, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';
    var map;
    var latitude;
    var longitiude;
    var lang = localStorage.getItem('Dalal_lang');

    function GetStoredContract() {
        var contract = JSON.parse(CookieService.getCookie('StoredContract'));
        return contract;
    }

    function LoadHouseTypes(callBack) {
        var houseTypes = [];

        app.showIndicator();

        lang = localStorage.getItem('Dalal_lang');
        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            $('#linkLocationHouseType .item-after').html('نوع المنزل');
        }
        else {
            $('#linkLocationHouseType .item-after').html('House Type');
        }

        appServices.CallService('location', "GET", "api/HourlyContract/options/HousingTypes", '', function (res) {
            if (res != null && res.length > 0) {
                $scope.houseTypes = res;
                $$('#linkLocationHouseType select').html('');


                lang = localStorage.getItem('Dalal_lang');
                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    app.smartSelectAddOption('#linkLocationHouseType select', '<option value="" selected>نوع المنزل</option>');
                }
                else {
                    app.smartSelectAddOption('#linkLocationHouseType select', '<option value="" selected>House Type</option>');
                }
                angular.forEach($scope.houseTypes, function (houseType) {
                    app.smartSelectAddOption('#linkLocationHouseType select', '<option value="' + houseType.key + '">' + houseType.value + '</option>');
                });
            }

            callBack(true);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    function LoadFloorNumbers(callBack) {
        var floorNumbers = [];


        lang = localStorage.getItem('Dalal_lang');
        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            $('#linkLocationFloorNumber .item-after').html('رقم الطابق');
        }
        else {
            $('#linkLocationFloorNumber .item-after').html('Floor Number');
        }

        appServices.CallService('location', "GET", "api/HourlyContract/options/HousingFloors", '', function (res) {
            app.hideIndicator();
            if (res != null && res.length > 0) {
                $scope.floorNumbers = res;
                $$('#linkLocationFloorNumber select').html('');


                lang = localStorage.getItem('Dalal_lang');
                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    app.smartSelectAddOption('#linkLocationFloorNumber select', '<option value="" selected>رقم الطابق</option>');
                }
                else {
                    app.smartSelectAddOption('#linkLocationFloorNumber select', '<option value="" selected>Floor Number</option>');
                }
                angular.forEach($scope.floorNumbers, function (floorNumber) {
                    app.smartSelectAddOption('#linkLocationFloorNumber select', '<option value="' + floorNumber.key + '">' + floorNumber.value + '</option>');
                });
            }

            callBack(true);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    function geocodeLatLng(geocoder, map, latitude, longitude) {
        var latlng = { lat: parseFloat(latitude), lng: parseFloat(longitude) };
        geocoder.geocode({ 'location': latlng }, function (results, status) {
            if (status === 'OK') {
                if (results[0]) {
                    var input = document.getElementById('pac-input');
                    input.value = results[0].formatted_address;
                } 
            } else {

                lang = localStorage.getItem('Dalal_lang');
                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    language.openFrameworkModal('خطأ', 'لا يمكن تحديد الموقع من خلال الإحداثيات المطلوبة', 'alert', function () { });
                }
                else {
                    language.openFrameworkModal('خطأ', 'Address Cannot Be Generated For The Given Latitude And Longtitude', 'alert', function () { });
                }
            }
        });
    }

    function InitRequestMap() {
        var mapDiv = document.getElementById('tripMap');
        var input = document.getElementById('pac-input');

        if (input != null) {
            input.remove();
        }

        input = document.createElement('input');
        var divMapContainer = document.getElementById('divMapContainer');

        input.setAttribute('id', 'pac-input');

        lang = localStorage.getItem('Dalal_lang');
        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            input.setAttribute('placeholder', 'أدخل العنوان');
        }
        else {
            input.setAttribute('placeholder', 'Type Address Here');
        }
        input.setAttribute('type', 'text');
        input.className += 'form-control';
        divMapContainer.insertBefore(input, mapDiv);

        var projectMarkers = [];

        var onSuccess = function (position) {
            projectMarkers = [{ "title": 'myMap', "lat": position.coords.latitude, "lng": position.coords.longitude, "description": "currentLocation" }];
            CookieService.setCookie('StoredLatitude', position.coords.latitude);
            CookieService.setCookie('StoredLongtitude', position.coords.longitude);
            helpers.initAutocomplete('tripMap', projectMarkers, true);
            var geocoder = new google.maps.Geocoder;
            geocodeLatLng(geocoder, map, position.coords.latitude, position.coords.longitude);
            app.hideIndicator();
        }

        function onError(error) {

            lang = localStorage.getItem('Dalal_lang');
            if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                language.openFrameworkModal('خطأ', 'تحديد الموقع مغلق لديك علي الهاتف , لا يمكن تحديد الموقع الحالي', 'alert', function () { });
            }
            else {
                language.openFrameworkModal('خطأ', 'Please Turn On Location From Settings On Your Device , Cannot Select Your Current Location', 'alert', function () { });
            }
            projectMarkers = [{ "title": 'myMap', "lat": '24.713552', "lng": '46.675296', "description": "currentLocation" }];
            //if (CookieService.getCookie('StoredLatitude')) CookieService.removeCookie('StoredLatitude');
            //if (CookieService.getCookie('StoredLongtitude')) CookieService.removeCookie('StoredLongtitude');

            CookieService.setCookie('StoredLatitude', '24.713552');
            CookieService.setCookie('StoredLongtitude', '46.675296');

            helpers.initAutocomplete('tripMap', projectMarkers, true);
            app.hideIndicator();
        }

        var devicePlatform = device.platform;

        var lang = localStorage.getItem('Dalal_lang');
        var confirmTitle = 'Confirm';
        var confirmText = 'Do you want to detect your current location ?';

        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            confirmText = 'هل تريد تحديد الموقع الحالي ؟';
            confirmTitle = 'تأكيد';
        }

        if (devicePlatform == 'Android' || devicePlatform == 'android') {
            app.confirm(confirmText, confirmTitle, function () {
                //navigator.geolocation.getCurrentPosition(onSuccess, onError, { enableHighAccuracy: true, timeout: 2 * 1000, maximumAge: 0 });

                cordova.plugins.diagnostic.isGpsLocationEnabled(function (enabled) {

                    if (enabled) {
                        app.hideIndicator();
                        navigator.geolocation.getCurrentPosition(onSuccess, onError, { enableHighAccuracy: true, timeout: 2 * 1000, maximumAge: 0 });
                    }
                    else {

                        lang = localStorage.getItem('Dalal_lang');
                        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                            language.openFrameworkModal('خطأ', 'تحديد الموقع مغلق لديك علي الهاتف , لا يمكن تحديد الموقع الحالي', 'alert', function () { });
                        }
                        else {
                            language.openFrameworkModal('خطأ', 'Please Turn On Location From Settings On Your Device , Cannot Select Your Current Location', 'alert', function () { });
                        }
                        projectMarkers = [{ "title": 'myMap', "lat": '24.713552', "lng": '46.675296', "description": "currentLocation" }];
                        //if (CookieService.getCookie('StoredLatitude')) CookieService.removeCookie('StoredLatitude');
                        //if (CookieService.getCookie('StoredLongtitude')) CookieService.removeCookie('StoredLongtitude');

                        CookieService.setCookie('StoredLatitude', '24.713552');
                        CookieService.setCookie('StoredLongtitude', '46.675296');

                        helpers.initAutocomplete('tripMap', projectMarkers, true);
                        app.hideIndicator();
                    }

                }, function (error) {

                    lang = localStorage.getItem('Dalal_lang');

                    if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                        language.openFrameworkModal('خطأ', 'تحديد الموقع مغلق لديك علي الهاتف , لا يمكن تحديد الموقع الحالي', 'alert', function () { });
                    }
                    else {
                        language.openFrameworkModal('خطأ', 'Please Turn On Location From Settings On Your Device , Cannot Select Your Current Location', 'alert', function () { });
                    }

                    projectMarkers = [{ "title": 'myMap', "lat": '24.713552', "lng": '46.675296', "description": "currentLocation" }];
                    CookieService.setCookie('StoredLatitude', '24.713552');
                    CookieService.setCookie('StoredLongtitude', '46.675296');

                    helpers.initAutocomplete('tripMap', projectMarkers, true);
                    app.hideIndicator();
                });
            }, function () {
                        var projectMarkers = [{ "title": 'myMap', "lat": '24.713552', "lng": '46.675296', "description": "currentLocation" }];
                        CookieService.setCookie('StoredLatitude', '24.713552');
                        CookieService.setCookie('StoredLongtitude', '46.675296');

                        helpers.initAutocomplete('tripMap', projectMarkers, true);
                        var geocoder = new google.maps.Geocoder;
                        geocodeLatLng(geocoder, map, 24.713552, 46.675296);
                        app.hideIndicator();
            });
        }
        else {
            app.confirm(confirmText, confirmTitle, function () {
                navigator.geolocation.getCurrentPosition(onSuccess, onError, { enableHighAccuracy: true, timeout: 2 * 1000, maximumAge: 0 });
            }, function () {
                var projectMarkers = [{ "title": 'myMap', "lat": '24.713552', "lng": '46.675296', "description": "currentLocation" }];
                CookieService.setCookie('StoredLatitude', '24.713552');
                CookieService.setCookie('StoredLongtitude', '46.675296');

                helpers.initAutocomplete('tripMap', projectMarkers, true);
                var geocoder = new google.maps.Geocoder;
                geocodeLatLng(geocoder, map, 24.713552, 46.675296);
                app.hideIndicator();
            });
            
        }

    }

    $(document).ready(function () {
        app.onPageInit('location', function (page) {
            if ($rootScope.currentOpeningPage != 'location') return;
            $rootScope.currentOpeningPage = 'location';

        });

        app.onPageReinit('location', function (page) {
            
        });

        app.onPageBeforeAnimation('location', function (page) {
            if ($rootScope.currentOpeningPage != 'location') return;
            $rootScope.currentOpeningPage = 'location';

            $scope.isLoaded = true;

            var lang = localStorage.getItem('Dalal_lang');
            $scope.isArabic = lang == 'AR' || typeof lang == 'undefined' || lang == null ? true : false;

            var contract = GetStoredContract();
            if (typeof contract.HouseType == 'undefined') {
                $scope.resetForm();


                InitRequestMap();

                LoadHouseTypes(function (result) {
                    $scope.isHouseVilla = true;
                    LoadFloorNumbers(function (result) {
                    });
                });

                $$('#linkLocationHouseType select').on('change', function () {
                    var houseTypeId = $(this).val();
                    var houseType = $scope.houseTypes.filter(function (obj) {
                        return obj.key == houseTypeId;
                    })[0];

                    $scope.formFields.houseType = houseType;

                    $scope.isHouseVilla = houseType.key == 0 ? true : false;

                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                });

                $$('#linkLocationFloorNumber select').on('change', function () {
                    var floorNumberId = $(this).val();
                    var floorNumber = $scope.floorNumbers.filter(function (obj) {
                        return obj.key == floorNumberId;
                    })[0];

                    $scope.formFields.floorNumber = floorNumber;
                });

            }

        });

        app.onPageReinit('location', function (page) {
            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('location', function (page) {
            if ($rootScope.currentOpeningPage != 'location') return;
            $rootScope.currentOpeningPage = 'location';

        });

        $scope.form = {};
        $scope.locationForm = {};
        $scope.formFields = {};

        $scope.resetForm = function () {
            $scope.locationReset = false;
            $scope.submittedLocation = false;

            $scope.isHouseVilla = true;

            $scope.isHouseTypeValid = true;
            $scope.isFloorNumberValid = true;
            $scope.isHouseNumberValid = true;
            $scope.isAppratmentNumberValid = true;
            $scope.isMapValid = true;

            $scope.isHouseNumberFormatCorrect = false;
            $scope.isAppartmentNumberFormatCorrect = false;

            $scope.locationForm.houseType = null;
            $scope.locationForm.houseNumber = null;
            $scope.locationForm.floorNumber = null;
            $scope.locationForm.appartmentNumber = null;
            $scope.locationForm.houseAddress = null;

            if (typeof $scope.LocationForm != 'undefined' && $scope.LocationForm != null) {
                $scope.LocationForm.$setPristine(true);
                $scope.LocationForm.$setUntouched();
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        }

        $scope.submitForm = function (isValid) {
            $scope.locationReset = true;
            $scope.submittedLocation = true;

            var houseTypeId = $('#linkLocationHouseType select').val();
            var floorNumberId = $('#linkLocationFloorNumber select').val();
            var houseNumber = $('#txtHouseNumber').val();
            var appartmentNumber = $('#txtAppartmentNumber').val();
            var mapLatitude = CookieService.getCookie('StoredLatitude');
            var mapLongtitude = CookieService.getCookie('StoredLongtitude');

            $scope.isHouseTypeValid = $scope.locationReset && $scope.submittedLocation && houseTypeId != null && houseTypeId != '' && houseTypeId != ' ' ? true : false;
            $scope.isFloorNumberValid = $scope.locationReset && $scope.submittedLocation && floorNumberId != null && floorNumberId != '' && floorNumberId != ' ' ? true : false;
            $scope.isMapValid = $scope.locationReset && $scope.submittedLocation && mapLatitude != null && mapLatitude != '' && mapLatitude != ' ' ? true : false;
            $scope.isHouseNumberValid = $scope.locationReset && $scope.submittedLocation && houseNumber != null && houseNumber != '' && houseNumber != ' ' ? true : false;
            $scope.isAppratmentNumberValid = $scope.locationReset && $scope.submittedLocation && appartmentNumber != null && appartmentNumber != '' && appartmentNumber != ' ' ? true : false;

            if ($scope.isHouseNumberValid) {
                var patt = new RegExp(/^(?!\s*$)(\d|\w|[\u0621-\u064A\u0660-\u0669 ])+$/);
                $scope.isHouseNumberFormatCorrect = patt.test(houseNumber);
            }
            if (houseTypeId == 1 && $scope.isAppratmentNumberValid) {
                var patt = new RegExp(/^(?!\s*$)(\d|\w|[\u0621-\u064A\u0660-\u0669 ])+$/);
                $scope.isAppartmentNumberFormatCorrect = patt.test(appartmentNumber);
            }

            if ($scope.isHouseTypeValid && $scope.isFloorNumberValid && $scope.isMapValid && $scope.isHouseNumberValid && $scope.isHouseNumberFormatCorrect &&
                (houseTypeId == 0 || (houseTypeId == 1 && $scope.isAppratmentNumberValid && $scope.isAppartmentNumberFormatCorrect))) {
                var storedContract = GetStoredContract();

                storedContract.HouseType = $scope.formFields.houseType;
                storedContract.HouseNumber = $scope.locationForm.houseNumber;
                storedContract.FloorNumber = $scope.formFields.floorNumber;
                storedContract.AppartmentNumber = $scope.locationForm.appartmentNumber;
                storedContract.HouseAddress = $scope.locationForm.houseAddress;

                storedContract.Latitude = mapLatitude;
                storedContract.Longtitude = mapLongtitude;
                

                CookieService.setCookie('StoredContract', JSON.stringify(storedContract));

                helpers.GoToPage('contractDetails', null);

            }
        };

        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('intro', null);
        };

        app.init();
    });

}]);

