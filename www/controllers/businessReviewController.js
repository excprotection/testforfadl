﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('businessReviewController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';

    $(document).ready(function () {
        app.onPageInit('businessReview', function (page) {
            if ($rootScope.currentOpeningPage != 'businessReview') return;
            $rootScope.currentOpeningPage = 'businessReview';

        });

        app.onPageBeforeAnimation('businessReview', function (page) {
            if ($rootScope.currentOpeningPage != 'businessReview') return;
            $rootScope.currentOpeningPage = 'businessReview';

            $scope.isLoaded = true;

        });

        app.onPageReinit('businessReview', function (page) {
            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('businessReview', function (page) {
            if ($rootScope.currentOpeningPage != 'businessReview') return;
            $rootScope.currentOpeningPage = 'businessReview';

        });


        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('introReview', null);
        };

        $scope.CallUs = function () {
            var contactPhone = '+966920033660';

            window.plugins.CallNumber.callNumber(
              function onSuccess(successResult) {

              }, function onError(errorResult) {
                  
                  console.log("Error:" + errorResult);
              }, contactPhone, true);
        };

        app.init();
    });

}]);

