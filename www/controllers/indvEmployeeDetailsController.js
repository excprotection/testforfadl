﻿/// <reference path='../js/angular.js' />

myApp.angular.controller('indvEmployeeDetailsController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
	'use strict';

	var fw7 = myApp.fw7;
	var app = myApp.fw7.app;
	var lang = localStorage.getItem('Dalal_lang');
	var vat = 5;
	function LoadEmployeeDetails() {
	  
		app.showIndicator();
		appServices.CallService('indvEmployeeDetails', 'Post', '', dataVariables, function (res) {
			app.hideIndicator();
			if (res) {
				$scope.Employee = res;
			}
		});
	}

	$(document).ready(function () {
		app.onPageInit('indvEmployeeDetails', function (page) {
			if ($rootScope.currentOpeningPage != 'indvEmployeeDetails') return;
			$rootScope.currentOpeningPage = 'indvEmployeeDetails';
		});

		app.onPageReinit('indvEmployeeDetails', function (page) {

			$rootScope.RemoveEditPagesFromHistory();
		});

		app.onPageBeforeAnimation('indvEmployeeDetails', function (page) {
			if ($rootScope.currentOpeningPage != 'indvEmployeeDetails') return;
			$rootScope.currentOpeningPage = 'indvEmployeeDetails';

			var lang = localStorage.getItem('Dalal_lang');
			$scope.isArabic = lang == 'AR' || typeof lang == 'undefined' || lang == null ? true : false;

			LoadEmployeeDetails();
			setTimeout(function () {
				$scope.$apply();
			}, fw7.DelayBeforeScopeApply);
		});

		app.onPageAfterAnimation('indvEmployeeDetails', function (page) {
			if ($rootScope.currentOpeningPage != 'indvEmployeeDetails') return;
			$rootScope.currentOpeningPage = 'indvEmployeeDetails';

		});

		$scope.GoToSuccess = function () {
			var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

		};
		$scope.GoBack = function () {
			helpers.GoBack();
		};

		$scope.GoToLogin = function () {
			helpers.GoToPage('login', null);
		};

		$scope.GetReligion = function (religionId) {
			var religionTxt = 'مسلم';
			switch (religionId) {
				case 1:
					religionTxt = 'مسلم'
					break;
				case 2:
					religionTxt = 'مسيحي'
					break;
				case 5:
					religionTxt = 'ديانةاخري'
					break;
				default:
					religionTxt = 'مسلم'

			}
			return religionTxt;
		}

		$scope.GetMarital_Status = function (statusId) {
			var statusTxt = '';
			switch (statusId) {
				case 1:
					statusTxt = 'أعزب/عزباء'
					break;
				case 2:
					statusTxt = 'متزوج/متزوجة'
					break;
				case 3:
					statusTxt = 'مطلق/مطلقة'
					break;
				default:
					statusTxt = 'غير محدد'
			}
			return statusTxt;
		}

		$scope.GetGender = function (genderId) {
			var genderTxt = '';
			switch (genderId) {
				case 1:
					genderTxt = 'ذكر'
					break;
				case 2:
					genderTxt = 'أنثى'
					break;
				default:
					genderTxt = 'غير محدد'

			}
			return genderTxt;
		}

		app.init();

	});
}]);

