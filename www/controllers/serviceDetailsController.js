﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('serviceDetailsController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
	'use strict';

	var fw7 = myApp.fw7;
	var app = myApp.fw7.app;
	var lang = localStorage.getItem('Dalal_lang');
	var serviceName;
	var IsArabic = true;

	function IsShowEmployeePhoto() {

		var ShowImage = false;
		app.showIndicator();
		appServices.CallService('serviceDetails', "GET", "api/IndivContract/GetShowEmploteePhoto_M", '', function (res) {
			app.hideIndicator();
			if (res != null && res.toLowerCase() == 'true') {
				ShowImage = true;
			}
			ShowImage = false;
		});

		CookieService.setCookie('IsShowEmployeePhoto', ShowImage);
	}

	function GetEmployeeSettings(callBack) {
		app.showIndicator()
		appServices.CallService('serviceDetails', "GET", "api/IndivContract/GetEmployeeSettings_M", '', function (res) {
			if (res) {
				var setting = {
					isShowEmployeePhoto: res.isShowEmployeePhoto,
					isShowEmployeeData: res.isShowEmployeeData

				}
				CookieService.setCookie('indvEmployeeSettings', JSON.stringify(setting));
				callBack(true);
			}
			else {
			    var setting = {
			        isShowEmployeePhoto: true,
			        isShowEmployeeData: true

			    }
			    CookieService.setCookie('indvEmployeeSettings', JSON.stringify(setting));
				callBack(false);
			}

			console.log(res);
		});
	}

	function CheckLastContractStatus(userId, requiredProfessionId) {
		var lastContract = null;
		appServices.CallService('serviceDetails', "GET", "api/IndividualContractRequest/GetLastRequestId_M?userId=" + userId, '', function (res) {
			app.hideIndicator()
			if (res!=null &&res =="") {
			    helpers.GoToPage('indvNationalities', { requiredProfessionId: requiredProfessionId });
			}
			else {
			    var contract = res.split(";");
			    if (contract && contract.length > 0) {
			        var requestId = contract[0];
			        var status = contract[1];
			        var message = '';
			        var title = '';

			        if (status == '1')
			        {
			            if (IsArabic) {
			                title = 'خطأ';
			                message = 'برجاء سداد قيمة الطلب السابق أو إلغاؤه , قبل انشاء طلب جديد';
			            }
			            else {
			                title = 'Error';
			                message = 'Please pay the value of the previous request or cancel it before creating a new request';
			            }
			        }

			        if (status == '100000006')
			        {
			            if (IsArabic) {
			                title = 'خطأ';
			                message = 'برجاء الإنتظار حتى يتم مراجعة صورة التحويل البنكي بواسطة قسم المالية';
			            }
			            else {
			                title = 'Error';
			                message = 'Please wait until your bank transfer image is reviewed by finance departement';
			            }
			        }

			        language.openFrameworkModal(title, message, 'alert', function () {
			            helpers.GoToPage('requestInfo', { requestId: requestId });
			        });
			    }
			}

		});
	}

	$(document).ready(function () {
		app.onPageInit('serviceDetails', function (page) {
			if ($rootScope.currentOpeningPage != 'serviceDetails') return;
			$rootScope.currentOpeningPage = 'serviceDetails';

		});

		app.onPageBeforeAnimation('serviceDetails', function (page) {
			if ($rootScope.currentOpeningPage != 'serviceDetails') return;
			$rootScope.currentOpeningPage = 'serviceDetails';

			$scope.isLoaded = true;

			serviceName = page.query.serviceName;

			lang = localStorage.getItem('Dalal_lang');
			IsArabic = lang == 'AR' || typeof lang == 'undefined' || lang == null ? true : false;
			if (serviceName == 'Dalal') {
				$scope.ServiceImage = 'img/home_imgs/dallal.png';
				if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
					$scope.ServiceTitle = 'دلال';
					$scope.ServiceDescription = 'خدمـة لسـاعات محـددة وأيـام محـددة حسـب الاتفـاق مـع العميـل ، لا تتطلـب إقامـة العمالـة لـدى العميـل,يتم خلالهـا إيصـال العمالـة للعميـل علـى أن يبـدأ حسـاب وقـت الخدمـة منـذ وصـول العمالـة الـى منـزل العميـل.';
				}
				else {
					$scope.ServiceTitle = 'Dalal';
					$scope.ServiceDescription = "Service for specific hours and specific days according to the agreement with the customer, Does not require the stayment of employees at the client, And Employee is delivered to the customer to start calculating the time of service since the arrival of employee to the client's home.";
				}
			}
			else if (serviceName == 'Tadbeer') {
				$scope.ServiceImage = 'img/home_imgs/tdbeer.png';
				if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
					$scope.ServiceTitle = 'تدبير';
					$scope.ServiceDescription = 'المـوارد البشـرية للعمـل فـي منـازل عائـلات المواطنـين والمقيمين، لمهـن متعـددة مـن جنسـيات مختلفـة.مع الحرص علــى توفـيـر موظفـيـن متعاقديــن مدربين ً تدريبــا ً جيــدا، يتفهمــون الثقافــة المحليــة ومتطلبــات العمــل المنزلــي. وتتحمــل الشــركة مســؤولية إدارة الموظفــن المتعاقديــن للعمــل.';
				}
				else {
					$scope.ServiceTitle = 'Tadbeer';
					$scope.ServiceDescription = 'Human resources to work in the homes of families of citizens and residents, for various professions of different nationalities. With the provision of well-trained contracting personnel, they understand the local culture and the requirements of domestic work. The company is responsible for the management of contracted employees.';
				}
			}
			else if (serviceName == 'mosa3ed') {
				$scope.ServiceImage = 'img/home_imgs/mosa3ed.png';
				if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
					$scope.ServiceTitle = 'عناية';
					$scope.ServiceDescription = 'خدمـة الرعايـة المنزليـة لكبـار السـن وذوي الأحتياجات الخاصـة وخدمـة المرافـق الصحـي الخـاص أو المرافقـة الصحيـة الخاصـة.';
				}
				else {
					$scope.ServiceTitle = 'Enayah';
					$scope.ServiceDescription = 'Home care for the elderly, people with special needs, and the service of private health facilities or private health care.';
				}
			}
			else {
				$scope.ServiceImage = 'img/home_imgs/gahez.png';
				if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
					$scope.ServiceTitle = 'جاهز';
					$scope.ServiceDescription = 'تقدم "إيبي ديمو" الموارد البشرية التي تحتاجها المنشآت المختلفة في القطاعين العام والخاص خلال مدة زمنية وجيزة وذلك عبر نظام التعاقد التشغيلي قصير المدي "مؤقت", من أجل تحقيق أهدافهم التشغيلية , ما يساعد علي منع تعطل المشاريع الناتج عن نقص العمالة.';
				}
				else {
					$scope.ServiceTitle = 'Gahez';
					$scope.ServiceDescription = '"EPDemo" provides the human resources needed by the various establishments in the public and private sectors within a short period of time through the short-term "temporary" operational contracting system, in order to achieve their operational objectives, which helps to prevent the disruption of projects resulting from the shortage of labor.';
				}
			}

			setTimeout(function () {
				$rootScope.$apply();
			}, fw7.DelayBeforeScopeApply);
		});

		app.onPageReinit('serviceDetails', function (page) {

		});

		app.onPageAfterAnimation('serviceDetails', function (page) {
			if ($rootScope.currentOpeningPage != 'serviceDetails') return;
			$rootScope.currentOpeningPage = 'serviceDetails';


		});

		$scope.GoBack = function () {
			helpers.GoBack();
		};

		$scope.ContinueToNextPage = function (requiredProfessionId) {
			if (serviceName == 'Dalal') {
				helpers.GoToPage('terms', null);
			}
			else if (serviceName == 'Tadbeer') {
				var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
				//if (userLoggedIn) {
				//	GetEmployeeSettings(function (result) {
				//		CheckLastContractStatus(userLoggedIn.crmUserId, requiredProfessionId);
				//	});
				//}
				//else {
				//	helpers.GoToPage('login', null);
				//}
				if (userLoggedIn) {
				    helpers.GoToPage('individualSector', { requiredProfessionId: requiredProfessionId, InReview: false });
				}
				else {
				    helpers.GoToPage('login', null);
				}

			}
			else if (serviceName == 'mosa3ed') {
				var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

				if (userLoggedIn) {
					helpers.GoToPage('individualSector', { requiredProfessionId: requiredProfessionId });
				}
				else {
					helpers.GoToPage('login', null);
				}
			}
			else {

			}
		};

		$scope.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		$scope.mobileRegex = /^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/;

		app.init();
	});

}]);
