﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('userConsumedPointsNewController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';
    var loadingPoints;
    var allPoints = [];
    var methodName;
    var isPull = true;
    var lang = localStorage.getItem('Dalal_lang');

    var IsArabic=(lang == 'AR' ||lang == 'ar' || typeof lang == 'undefined' || lang == null)?true:false;
    //var ActiveStatusTxt=IsArabic?'مفعل':'Active';
    //var ActiveStatusTxt = IsArabic ?  'مفعلة ' : 'Active';
    //var NotActiveStatusTxt = IsArabic ? 'غير مفعلة ' : 'Not Active';

    function ClearListData() {
        allPoints = [];
        loadingPoints = false;
        CookieService.setCookie('consumed-points-page-number', 1);
        $scope.points = null;
        app.pullToRefreshDone();
    }

    function LoaduserConsumedPoints(callBack) {
     
        CookieService.setCookie('consumed-points-page-number', 1);
        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

        var methodName = 'api/Loyality/PointsConsuming_M?userId=' + userLoggedIn.crmUserId + '&pageNumber=' +
            parseInt(CookieService.getCookie('consumed-points-page-number')) + '&pageSize=' + myApp.fw7.PageSize ;

        allPoints = [];
        app.attachInfiniteScroll('#divInfiniteuserConsumedPoints');

        app.showIndicator();

        appServices.CallService('userConsumedPoints', 'GET', methodName, '', function (result) {
            
            app.hideIndicator();

            allPoints = [];

            if (result && result.pointsConsuming.length > 0) {
                if (result.pointsConsuming.length < myApp.fw7.PageSize) {
                    $scope.pointsInfiniteLoader = false;
                    app.detachInfiniteScroll('#divInfiniteuserConsumedPoints');
                }
                else {
                    $scope.pointsInfiniteLoader = true;
                    app.attachInfiniteScroll('#divInfiniteuserConsumedPoints');
                }
                
                allPoints = result.pointsConsuming;

                allPoints.sort(function (a, b) {
                    return new Date(b.creationDate).getTime() - new Date(a.creationDate).getTime();
                });

                $scope.userConsumedPointsList = allPoints;

                $scope.noPoints = false;

                if (isPull) {
                    app.pullToRefreshDone();
                }

                callBack(true);

                setTimeout(function () {
                    $scope.allPointsLoaded = true;
                    $scope.isLoaded = true;
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);

            }
            else {
                allPoints = [];
                $scope.userConsumedPointsList = [];
                $scope.noPoints = true;
                $('.advNoResult').show();
                $scope.pointsInfiniteLoader = false;

                callBack(true);

                setTimeout(function () {
                    $scope.isLoaded = true;
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            }

        });
    }

    $(document).ready(function () {
        app.onPageInit('userConsumedPoints', function (page) {
            if ($rootScope.currentOpeningPage != 'userConsumedPoints') return;
            $rootScope.currentOpeningPage = 'userConsumedPoints';

            fromPage = page.fromPage.name;

            $$('#divInfiniteuserConsumedPoints').on('ptr:refresh', function (e) {
                isPull = true;

                LoaduserConsumedPoints(function (result) {
                    app.pullToRefreshDone();
                });

                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            });


            $$('#divInfiniteuserConsumedPoints').on('infinite', function () {
                if (loadingPoints) return;
                loadingPoints = true;
                var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
                CookieService.setCookie('consumed-points-page-number', parseInt(CookieService.getCookie('consumed-points-page-number')) + 1);
                var methodName = 'api/Loyality/PointsSource_M?userId=' + userLoggedIn.crmUserId + '&pageNumber=' +
                    parseInt(CookieService.getCookie('consumed-points-page-number')) + '&pageSize=' + myApp.fw7.PageSize ;

                appServices.CallService('userConsumedPoints', 'GET', methodName, '', function (response) {
                    if (result && result.pointsConsuming.length > 0) {
                        loadingPoints = false;

                        angular.forEach(result.pointsConsuming, function (point, index) {
                            allPoints.push(point);
                        });

                        allPoints.sort(function (a, b) {
                            return new Date(b.creationDate).getTime() - new Date(a.creationDate).getTime();
                        });

                       
                        $scope.points = allPoints;

                        if (response && result.pointsConsuming < myApp.fw7.PageSize) {
                            $scope.pointsInfiniteLoader = false;
                            app.detachInfiniteScroll('#divInfiniteuserConsumedPoints');
                            return;
                        }
                    }
                    else {
                        $scope.pointsInfiniteLoader = false;
                        app.detachInfiniteScroll('#divInfiniteuserConsumedPoints');
                        loadingPoints = false;
                    }

                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                });
            });
        });

        app.onPageBeforeAnimation('userConsumedPoints', function (page) {
            if ($rootScope.currentOpeningPage != 'userConsumedPoints') return;
            $rootScope.currentOpeningPage = 'userConsumedPoints';

            $scope.isLoaded = false;

            fromPage = page.fromPage.name;
            $scope.AllPoints = page.query.allPoints;
            $scope.pointAmount = page.query.PointsAmount;
            
            isPull = false;

            LoaduserConsumedPoints(function (result) {
                var lang = localStorage.getItem('Dalal_lang');

                $scope.isLoaded = true;

                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    $scope.lblContractNumber = 'رقم العقد :';
                    $scope.lblAmount = 'المبلغ :';
                    $scope.lbldate = 'التاريخ :';
                    $scope.lblNumberOfPoints = 'عدد النقاط :';
                    $scope.lblContractCurrency = ' نقطة ';
                }
                else {
                    $scope.lblContractNumber = 'Number:';
                    $scope.lbldate = 'Date:';
                    $scope.lblAmount = 'Amount :';
                    $scope.lblNumberOfPoints = 'Points :';
                    $scope.lblContractCurrency = ' Points ';
                }
            });

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);

        });

        app.onPageReinit('userConsumedPoints', function (page) {
            isPull = false;

            $('.advNoResult').hide();

            fromPage = page.fromPage.name;

            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('userConsumedPoints', function (page) {
            if ($rootScope.currentOpeningPage != 'userConsumedPoints') return;
            $rootScope.currentOpeningPage = 'userConsumedPoints';

            fromPage = page.fromPage.name;

        });


        $scope.GoBack = function () {
                helpers.GoBack();
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('intro', null);
        };
    
        app.init();
    });

}]);

