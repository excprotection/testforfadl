﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('userIndvContractsController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';
    var loadingContracts;
    var allContracts = [];
    var methodName;
    var isPull = true;

    function ClearListData() {
        allContracts = [];
        loadingContracts = false;
        CookieService.setCookie('user-indv-contracts-page-number', 1);
        $scope.contracts = null;
        app.pullToRefreshDone();
    }

    function LoadUserIndvContracts(callBack) {
        CookieService.setCookie('user-indv-contracts-page-number', 1);
        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

        var methodName = 'api/Profile/IndividualContract/All_M?userId=' + userLoggedIn.crmUserId + '&pageSize=' + myApp.fw7.PageSize + '&pageNumber=' +
            parseInt(CookieService.getCookie('user-indv-contracts-page-number')) ;

        allContracts = [];
        app.attachInfiniteScroll('#divInfiniteIndvContracts');

        app.showIndicator();

        appServices.CallService('userIndvContracts', 'GET', methodName, '', function (result) {

            app.hideIndicator();

            allContracts = [];

            if (result && result.contracts.length > 0) {
                if (result.contracts.length < myApp.fw7.PageSize) {
                    $scope.contractsInfiniteLoader = false;
                    app.detachInfiniteScroll('#divInfiniteIndvContracts');
                }
                else {
                    $scope.contractsInfiniteLoader = true;
                    app.attachInfiniteScroll('#divInfiniteIndvContracts');
                }

                allContracts = result.contracts;

                allContracts.sort(function (a, b) {
                    return new Date(b.contractDate).getTime() - new Date(a.contractDate).getTime();
                });

                angular.forEach(allContracts, function (contract, index) {
                    contract.contractDate = contract.contractDate.split(' ')[0];
                });

                $scope.contracts = allContracts;

                $scope.noContracts = false;

                if (isPull) {
                    app.pullToRefreshDone();
                }

                callBack(true);

                setTimeout(function () {
                    $scope.allContractsLoaded = true;
                    $scope.isLoaded = true;
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);

            }
            else {
                allContracts = [];
                $scope.contracts = [];
                $scope.noContracts = true;
                $('.advNoResult').show();
                $scope.contractsInfiniteLoader = false;

                callBack(true);

                setTimeout(function () {
                    $scope.isLoaded = true;
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            }

        });
    }

    $(document).ready(function () {
        app.onPageInit('userIndvContracts', function (page) {
            if ($rootScope.currentOpeningPage != 'userIndvContracts') return;
            $rootScope.currentOpeningPage = 'userIndvContracts';

            fromPage = page.fromPage.name;

            $$('#divInfiniteIndvContracts').on('ptr:refresh', function (e) {
                isPull = true;

                LoadUserIndvContracts(function (result) {
                    app.pullToRefreshDone();
                });

                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            });


            $$('#divInfiniteIndvContracts').on('infinite', function () {
                if (loadingContracts) return;
                loadingContracts = true;

                var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

                CookieService.setCookie('user-indv-contracts-page-number', parseInt(CookieService.getCookie('user-indv-contracts-page-number')) + 1);


                var methodName = 'api/Profile/IndividualContract/All_M?userId=' + userLoggedIn.crmUserId + '&pageSize=' + myApp.fw7.PageSize + '&pageNumber=' +
                    parseInt(CookieService.getCookie('user-indv-contracts-page-number')) ;

                appServices.CallService('userIndvContracts', 'GET', methodName, '', function (response) {

                    if (response && response.contracts.length > 0) {
                        loadingContracts = false;

                        angular.forEach(response.contracts, function (contract, index) {
                            allContracts.push(contract);
                        });

                        allContracts.sort(function (a, b) {
                            return new Date(b.contractDate).getTime() - new Date(a.contractDate).getTime();
                        });

                        angular.forEach(allContracts, function (contract, index) {
                            contract.contractDate = contract.createOn.split(' ')[0];
                        });

                        $scope.contracts = allContracts;

                        if (response && response.length < myApp.fw7.PageSize) {
                            $scope.contractsInfiniteLoader = false;
                            app.detachInfiniteScroll('#divInfiniteIndvContracts');
                            return;
                        }
                    }
                    else {
                        $scope.contractsInfiniteLoader = false;
                        app.detachInfiniteScroll('#divInfiniteIndvContracts');
                        loadingContracts = false;
                    }

                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                });
            });
        });

        app.onPageBeforeAnimation('userIndvContracts', function (page) {
            if ($rootScope.currentOpeningPage != 'userIndvContracts') return;
            $rootScope.currentOpeningPage = 'userIndvContracts';

            $scope.isLoaded = false;

            fromPage = page.fromPage.name;

            ClearListData();

            isPull = false;

            LoadUserIndvContracts(function (result) {
                var lang = localStorage.getItem('Dalal_lang');
                $scope.lang = lang;
                $scope.isLoaded = true;

                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    $scope.lblComplaintTicketNumber = 'رقم العقد :';
                    $scope.lblComplaintContractNumber = 'المبلغ :';
                    $scope.lblComplaintTicketDesc = 'التاريخ :';
                    $scope.lblContractCurrency = 'ريال';
                }
                else {
                    $scope.lblComplaintTicketNumber = 'Number : ';
                    $scope.lblComplaintContractNumber = 'Cost : ';
                    $scope.lblComplaintTicketDesc = 'Date : ';
                    $scope.lblContractCurrency = 'SAR';
                }
            });

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);

        });

        app.onPageReinit('userIndvContracts', function (page) {
            isPull = false;

            $('.advNoResult').hide();

            fromPage = page.fromPage.name;

            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('userIndvContracts', function (page) {
            if ($rootScope.currentOpeningPage != 'userIndvContracts') return;
            $rootScope.currentOpeningPage = 'userIndvContracts';

            fromPage = page.fromPage.name;

        });

        $scope.GoToContractDetails = function (contract) {
            helpers.GoToPage('indivContractInfo', { contractId: contract.contractId });
        };


        $scope.GoBack = function () {
            if (fromPage == 'paymentMethod') {
                helpers.GoToPage('intro', null);
            }
            else {
                helpers.GoBack();
            }
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('intro', null);
        };

        $scope.AddNewContract = function () {
            helpers.GoToPage('serviceDetails', { serviceName: 'Tadbeer' });
        };

        app.init();
    });

}]);

