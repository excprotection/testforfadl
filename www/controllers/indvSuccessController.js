﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('indvSuccessController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';

    function GetStoredContract() {
        var contract = JSON.parse(CookieService.getCookie('indvContract'));
        return contract;
    }

    

    $(document).ready(function () {
        app.onPageInit('indvSuccess', function (page) {
            if ($rootScope.currentOpeningPage != 'indvSuccess') return;
            $rootScope.currentOpeningPage = 'indvSuccess';

        });

        app.onPageBeforeAnimation('indvSuccess', function (page) {
            if ($rootScope.currentOpeningPage != 'indvSuccess') return;
            $rootScope.currentOpeningPage = 'indvSuccess';

            $scope.isLoaded = true;

            var contract = GetStoredContract();
           // contract.availableDaysNumber = contract.availableDays != null && contract.availableDays != '' ? contract.availableDays.split(',').length : 0;

            $scope.contract = contract;

        });

        app.onPageReinit('indvSuccess', function (page) {
            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('indvSuccess', function (page) {
            if ($rootScope.currentOpeningPage != 'indvSuccess') return;
            $rootScope.currentOpeningPage = 'indvSuccess';

        });


        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.PayNow = function () {
        	helpers.GoToPage('indvPaymentMethod',{ contract:$scope.contract  });
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('intro', null);
        };

        app.init();
    });

}]);

