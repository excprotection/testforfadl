﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('landingController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'helpers', '$interval', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, helpers, $interval) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;

    function EnterApplication() {
        if (CookieService.getCookie('userLoggedIn') || CookieService.getCookie('UserID')) {
            CookieService.setCookie('Visitor', false);
        }
        else {
            CookieService.removeCookie('deviceId');
            CookieService.setCookie('Visitor', true);
        }

        helpers.GoToPage('intro', null);
    }
    
    EnterApplication();
    
    
    $(document).ready(function () {

        app.onPageInit('landing', function (page) {
            if ($rootScope.currentOpeningPage != 'landing') return;
        });

        app.onPageAfterAnimation('landing', function (page) {
            if ($rootScope.currentOpeningPage != 'landing') return;
        });

        app.init();
    });

}]);