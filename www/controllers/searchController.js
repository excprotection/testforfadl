﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('searchController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';
    var loadingAdvertisements;
    var allAdvertisements = [];
    var isPull = true;
    var methodName;

    function ClearListData() {
        allAdvertisements = [];
        loadingAdvertisements = false;
        CookieService.setCookie('search-page-number', 1);
        $scope.advertisements = null;
        app.pullToRefreshDone();
    }

    function LoadAdvertisements(callBack) {
        CookieService.setCookie('search-page-number', 1);
        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

        allAdvertisements = [];
        app.attachInfiniteScroll('#divInfiniteSearch');

        if (!isPull) {
            //SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            app.showIndicator();
        }
        appServices.CallService('search', 'GET', 'api/v1/ads/list?page=' + parseInt(CookieService.getCookie('search-page-number')) + '&&pageSize=1000000', '', function (result) {
            if (!isPull) {
                app.hideIndicator();
            }

            var response = result && result.data ? result.data : null;

            allAdvertisements = [];

            if (response && response.length > 0) {
                $$('#divNoAdvertisements').removeClass('partialNoResult');

                if (response.length < myApp.fw7.PageSize) {
                    $scope.searchInfiniteLoader = false;
                    app.detachInfiniteScroll('#divInfiniteSearch');
                }
                else {
                    $scope.searchInfiniteLoader = true;
                    app.attachInfiniteScroll('#divInfiniteSearch');
                }

                angular.forEach(response, function (advertisement) {
                    advertisement.day = advertisement.created_at.split(' ')[0];
                    if (advertisement.is_sold == 0) {
                        allAdvertisements.push(advertisement);
                    }
                });

                $scope.advertisements = allAdvertisements;
                $scope.noAdvertisements = allAdvertisements.length > 0 ? false : true;
            }
            else {
                $$('#divNoAdvertisements').addClass('partialNoResult');
                allAdvertisements = [];
                $scope.advertisements = [];
                $scope.noAdvertisements = true;
                $scope.searchInfiniteLoader = false;
            }

            callBack(true);

            setTimeout(function () {
                $scope.isLoaded = true;
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    $('#SearchNotificationsLoaded').hide();
    $('#SearchNotificationsNotLoaded').show();

    $(document).ready(function () {
        app.onPageInit('search', function (page) {
            if ($rootScope.currentOpeningPage != 'search') return;
            $rootScope.currentOpeningPage = 'search';

            $scope.resetForm();

            $$('#divInfiniteSearch').on('ptr:refresh', function (e) {
                isPull = true;

                var IsVisitor = $rootScope.isVisitor();
                if (IsVisitor == true) {
                    $scope.IsVisitor = true;
                    $('.spanNotifications').hide();
                }
                else {
                    $scope.IsVisitor = false;
                    $('#SearchNotificationsLoaded').hide();
                    $('#SearchNotificationsNotLoaded').show();
                    $rootScope.CheckNotifications(function (notificationLength) {
                        $('#SearchNotificationsLoaded').show();
                        $('#SearchNotificationsNotLoaded').hide();
                    });
                }

                LoadAdvertisements(function (result) {
                    app.pullToRefreshDone();
                });

                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            });

            $$('#divInfiniteSearch').on('infinite', function () {
                if (loadingAdvertisements) return;
                loadingAdvertisements = true;

                var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

                CookieService.setCookie('search-page-number', parseInt(CookieService.getCookie('search-page-number')) + 1);

                appServices.CallService('search', 'GET', 'api/v1/ads/list?page=' + parseInt(CookieService.getCookie('search-page-number')) + '&&pageSize=1000000', '', function (result) {
                    var response = result.data;

                    if (response && response.length > 0) {
                        loadingAdvertisements = false;

                        angular.forEach(response, function (advertisement) {
                            advertisement.day = advertisement.created_at.split(' ')[0];
                            if (advertisement.is_sold == 0) {
                                allAdvertisements.push(advertisement);
                            }
                        });

                        $scope.advertisements = allAdvertisements;

                        if (response && response.length < myApp.fw7.PageSize) {
                            $scope.searchInfiniteLoader = false;
                            app.detachInfiniteScroll('#divInfiniteSearch');
                            return;
                        }
                    }
                    else {
                        $scope.searchInfiniteLoader = false;
                        app.detachInfiniteScroll('#divInfiniteSearch');
                        loadingAdvertisements = false;
                    }

                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                });
            });
        });

        app.onPageBeforeAnimation('search', function (page) {
            if ($rootScope.currentOpeningPage != 'search') return;
            $rootScope.currentOpeningPage = 'search';

            $scope.resetForm();

            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageReinit('search', function (page) {
            isPull = false;

            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('search', function (page) {
            if ($rootScope.currentOpeningPage != 'search') return;
            $rootScope.currentOpeningPage = 'search';

            var methodName = page.query.methodName;
            var categoryId = page.query.categoryId;

            $scope.pageName = 'البحث';

            $scope.resetForm();

            isPull = false;

            app.initPullToRefresh('#divInfiniteSearch');

            $rootScope.CheckNewMessages(function (notifications) {
                $scope.NewNotificationsNumber = notifications;
                setTimeout(function () {
                    $scope.$apply();
                }, 10);
            });

            LoadAdvertisements(function (result) { });

            var IsVisitor = $rootScope.isVisitor();
            if (IsVisitor == true) {
                $scope.IsVisitor = true;
                $('.spanNotifications').hide();
            }
            else {
                $scope.IsVisitor = false;
                $rootScope.CheckNotifications(function (notificationLength) {
                    $('#SearchNotificationsLoaded').show();
                    $('#SearchNotificationsNotLoaded').hide();
                });
            }

            $scope.isLoaded = false;

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        $scope.resetForm = function () {
            $scope.searchForm.name = null;

            if (typeof $scope.SearchForm != 'undefined' && $scope.SearchForm != null) {
                $scope.SearchForm.$setPristine(true);
                $scope.SearchForm.$setUntouched();
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        }

        $scope.form = {};
        $scope.searchForm = {};

        $scope.GoToIntro = function () {
            helpers.GoToPage('intro', null);
        };

        $scope.GoToNotifications = function () {
            helpers.GoToPage('userNotification', null);
        };

        $scope.GoToAdvertisementDetails = function (advertisement) {
            helpers.GoToPage('advertisementsDetails', { advertisement: advertisement });
        };

        $scope.FilterResults = function (filterText) {
            setTimeout(function () {

                if (filterText == '' || filterText == ' ') {
                    $scope.advertisements = allAdvertisements;
                    $scope.noAdvertisements = $scope.advertisements.length > 0 ? false : true;

                    if ($scope.advertisements.length > 0) {
                        $$('#divNoAdvertisements').removeClass('partialNoResult');
                    }
                    else {
                        $$('#divNoAdvertisements').addClass('partialNoResult');
                    }
                }
                else {
                    $scope.advertisements = allAdvertisements.filter(function (item) {
                        return item.name.toLowerCase().indexOf(filterText.toLowerCase()) > -1;
                    });

                    $scope.noAdvertisements = $scope.advertisements.length > 0 ? false : true;

                    if ($scope.advertisements.length > 0) {
                        $$('#divNoAdvertisements').removeClass('partialNoResult');
                    }
                    else {
                        $$('#divNoAdvertisements').addClass('partialNoResult');
                    }
                }

                $scope.$apply();
            }, 500);
        };

        app.init();
    });

}]);

