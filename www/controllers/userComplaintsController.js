﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('userComplaintsController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';
    var loadingComplaints;
    var allComplaints = [];
    var methodName;
    var isPull = true;

    function ClearListData() {
        allComplaints = [];
        loadingComplaints = false;
        CookieService.setCookie('complaints-page-number', 1);
        $scope.complaints = null;
        app.pullToRefreshDone();
    }

    function LoadUserComplaints(callBack) {
        CookieService.setCookie('complaints-page-number', 1);
        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

        var methodName = 'api/CustomerTicket/Dalal/GetTickets_M?sectorId=4&userId=' + userLoggedIn.crmUserId + '&pageNumber=' +
            parseInt(CookieService.getCookie('complaints-page-number')) + '&pageSize=' + myApp.fw7.PageSize;

        allComplaints = [];
        app.attachInfiniteScroll('#divInfiniteComplaints');

        app.showIndicator();

        appServices.CallService('userComplaints', 'GET', methodName, '', function (result) {

            allComplaints = [];

            app.hideIndicator();

            if (result && result.tickets.length > 0) {
                if (result.tickets.length < myApp.fw7.PageSize) {
                    $scope.complaintsInfiniteLoader = false;
                    app.detachInfiniteScroll('#divInfiniteComplaints');
                }
                else {
                    $scope.complaintsInfiniteLoader = true;
                    app.attachInfiniteScroll('#divInfiniteComplaints');
                }

                angular.forEach(result.tickets, function (complain, index) {
                    complain.IsClosed = complain.statusId == 100000000 ? false : true;
                    complain.isContractFound = complain.contract != 'undefined' && complain.contract != null && complain.contract != '' && complain.contract != ' ' ? true : false;
                });

                allComplaints = result.tickets;

                $scope.complaints = allComplaints;
                $scope.noComplaints = false;

                if (isPull) {
                    app.pullToRefreshDone();
                }

                callBack(true);

                setTimeout(function () {
                    $scope.allComplaintsLoaded = true;
                    $scope.isLoaded = true;
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);

            }
            else {
                allComplaints = [];
                $scope.complaints = [];
                $scope.noComplaints = true;
                $('.advNoResult').show();
                $scope.complaintsInfiniteLoader = false;

                callBack(true);

                setTimeout(function () {
                    $scope.isLoaded = true;
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            }

        });
    }

    $(document).ready(function () {
        app.onPageInit('userComplaints', function (page) {
            if ($rootScope.currentOpeningPage != 'userComplaints') return;
            $rootScope.currentOpeningPage = 'userComplaints';

            $$('#divInfiniteComplaints').on('ptr:refresh', function (e) {
                isPull = true;

                LoadUserComplaints(function (result) {
                    app.pullToRefreshDone();
                });

                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            });


            $$('#divInfiniteComplaints').on('infinite', function () {
                if (loadingComplaints) return;
                loadingComplaints = true;

                var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

                CookieService.setCookie('complaints-page-number', parseInt(CookieService.getCookie('complaints-page-number')) + 1);


                var methodName = 'api/CustomerTicket/Dalal/GetTickets_M?sectorId=4&userId=' + userLoggedIn.crmUserId + '&pageNumber=' +
                    parseInt(CookieService.getCookie('complaints-page-number')) + '&pageSize=' + myApp.fw7.PageSize;

                appServices.CallService('userComplaints', 'GET', methodName, '', function (result) {

                    if (result && result.tickets.length > 0) {
                        loadingComplaints = false;

                        angular.forEach(result.tickets, function (ticket, index) {
                            allComplaints.push(ticket);
                        });

                        //allComplaints.sort(function (a, b) {
                        //    return new Date(b.createdOn).getTime() - new Date(a.createdOn).getTime();
                        //});

                        $scope.complaints = allComplaints;

                        setTimeout(function () {
                            $scope.$apply();
                        }, fw7.DelayBeforeScopeApply);

                        if (result.tickets && result.tickets.length < myApp.fw7.PageSize) {
                            $scope.complaintsInfiniteLoader = false;
                            app.detachInfiniteScroll('#divInfiniteComplaints');
                            return;
                        }
                    }
                    else {
                        $scope.complaintsInfiniteLoader = false;
                        app.detachInfiniteScroll('#divInfiniteComplaints');
                        loadingComplaints = false;
                    }

                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                });
            });
        });

        app.onPageBeforeAnimation('userComplaints', function (page) {
            if ($rootScope.currentOpeningPage != 'userComplaints') return;
            $rootScope.currentOpeningPage = 'userComplaints';

            $scope.isLoaded = false;

            ClearListData();

            isPull = false;

            app.attachInfiniteScroll('#divInfiniteComplaints');

            LoadUserComplaints(function (result) {
                var lang = localStorage.getItem('Dalal_lang');

                $scope.isLoaded = true;

                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    $scope.lblComplaintTicketNumber = 'رقم الشكوى :';
                    $scope.lblComplaintContractNumber = 'رقم العقد :';
                    $scope.lblComplaintTicketDesc = 'تفاصيل الشكوي :';
                    $scope.lblComplaintTicketStatus = 'حالة الشكوي :';
                }
                else {
                    $scope.lblComplaintTicketNumber = 'Number : ';
                    $scope.lblComplaintContractNumber = 'Contract Number : ';
                    $scope.lblComplaintTicketDesc = 'Description : ';
                    $scope.lblComplaintTicketStatus = 'Status :';
                }
            });

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);

        });

        app.onPageReinit('userComplaints', function (page) {
            isPull = false;

            $('.advNoResult').hide();

            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('userComplaints', function (page) {
            if ($rootScope.currentOpeningPage != 'userComplaints') return;
            $rootScope.currentOpeningPage = 'userComplaints';

        });


        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('intro', null);
        };

        $scope.AddNewComplaint = function () {
            helpers.GoToPage('addComplaint', null);
        };

        app.init();
    });

}]);

