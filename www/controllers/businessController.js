﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('businessController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';

    $(document).ready(function () {
        app.onPageInit('business', function (page) {
            if ($rootScope.currentOpeningPage != 'business') return;
            $rootScope.currentOpeningPage = 'business';

        });

        app.onPageBeforeAnimation('business', function (page) {
            if ($rootScope.currentOpeningPage != 'business') return;
            $rootScope.currentOpeningPage = 'business';

            $scope.isLoaded = true;

        });

        app.onPageReinit('business', function (page) {
            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('business', function (page) {
            if ($rootScope.currentOpeningPage != 'business') return;
            $rootScope.currentOpeningPage = 'business';

        });


        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('intro', null);
        };

        $scope.CallUs = function () {
            var contactPhone = '+966920033660';

            window.plugins.CallNumber.callNumber(
              function onSuccess(successResult) {

              }, function onError(errorResult) {
                  
                  console.log("Error:" + errorResult);
              }, contactPhone, true);
        };

        app.init();
    });

}]);

