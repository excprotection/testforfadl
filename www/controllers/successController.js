﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('successController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';

    function GetStoredContract() {
        var contract = JSON.parse(CookieService.getCookie('StoredContract'));
        return contract;
    }

    

    $(document).ready(function () {
        app.onPageInit('success', function (page) {
            if ($rootScope.currentOpeningPage != 'success') return;
            $rootScope.currentOpeningPage = 'success';

        });

        app.onPageBeforeAnimation('success', function (page) {
            if ($rootScope.currentOpeningPage != 'success') return;
            $rootScope.currentOpeningPage = 'success';

            $scope.isLoaded = true;

            var contract = GetStoredContract();
            contract.availableDaysNumber = contract.availableDays != null && contract.availableDays != '' ? contract.availableDays.split(',').length : 0;

            $scope.contract = contract;

        });

        app.onPageReinit('success', function (page) {
            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('success', function (page) {
            if ($rootScope.currentOpeningPage != 'success') return;
            $rootScope.currentOpeningPage = 'success';

        });


        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.PayNow = function () {
            helpers.GoToPage('paymentMethod', null);
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('intro', null);
        };

        app.init();
    });

}]);

