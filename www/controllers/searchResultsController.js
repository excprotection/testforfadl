﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('searchResultsController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';
    var loadingMyQuestions;
    var allQuestions = [];

    function LoadAdvertisments() {
        if (typeof $rootScope.swiper != 'undefined' && $rootScope.swiper != null) {
            $rootScope.swiper.destroy(true, true);
        }

        $scope.Advertisements = myApp.fw7.Advertisements;
        $rootScope.initalSwiper('#SearchResultsSwiper', myApp.fw7.Advertisements);
        setTimeout(function () {
            $scope.$apply();
        }, fw7.DelayBeforeScopeApply);
    }

    function ClearListData() {
        allQuestions = [];
        loadingMyQuestions = false;
        CookieService.setCookie('searchResults-page-number', 1);
        $scope.questions = null;
    }

    function LoadQuestions(name, category, city, country, callBack) {
        CookieService.setCookie('searchResults-page-number', 1);
        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
        //SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
        app.showIndicator();

        if (category == "" && city == "" && country == "") {
            appServices.CallService('searchResults', 'GET', "api/Questions/GetAll", '', function (response) {
                app.hideIndicator();

                allQuestions = [];

                if (response && response.length > 0) {
                    $$('.page[data-page="searchResults"]').removeClass('noResult');
                    angular.forEach(response, function (question, index) {
                        question.creationDate = question.creationDate.split('T')[0];
                        allQuestions.push(question);
                    });

                    $scope.questions = allQuestions;
                    $scope.noQuestions = allQuestions.length > 0 ? false : true;
                }
                else {
                    $$('.page[data-page="searchResults"]').addClass('noResult');
                    allQuestions = [];
                    $scope.questions = [];
                    $scope.noQuestions = true;
                }

                setTimeout(function () {
                    $scope.isLoaded = true;
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            });
        }
        else {


            var queryStringData = {
                'page': parseInt(CookieService.getCookie('searchResults-page-number')),
                'show': myApp.fw7.PageSize
            };

            var params = {
                name: name,
                categoryId: category,
                cityId: city,
                countryId: country
            };

            allQuestions = [];
            app.attachInfiniteScroll('#divInfiniteSearchResults');

            //SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            app.showIndicator();
            appServices.CallService('searchResults', 'POST', "api/Questions/Search" + '?page=' + queryStringData.page + '&show=' + queryStringData.show, params, function (response) {
                app.hideIndicator();

                allQuestions = [];

                if (response && response.length > 0) {
                    $$('.page[data-page="searchResults"]').removeClass('noResult');

                    if (response.length < myApp.fw7.PageSize) {
                        $scope.searchResultsInfiniteLoader = false;
                        app.detachInfiniteScroll('#divInfiniteSearchResults');
                    }
                    else {
                        $scope.searchResultsInfiniteLoader = true;
                        app.attachInfiniteScroll('#divInfiniteSearchResults');
                    }

                    angular.forEach(response, function (question) {
                        question.creationDate = question.creationDate.split('T')[0];
                        allQuestions.push(question);
                    });

                    $scope.questions = allQuestions;
                    $scope.noQuestions = allQuestions.length > 0 ? false : true;
                }
                else {
                    $$('.page[data-page="searchResults"]').addClass('noResult');
                    allQuestions = [];
                    $scope.questions = [];
                    $scope.noQuestions = true;
                    $scope.searchResultsInfiniteLoader = false;
                }

                callBack(true);

                setTimeout(function () {
                    $scope.isLoaded = true;
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            });
        }
    }

    $('#SearchResultsNotificationsLoaded').hide();
    $('#SearchResultsNotificationsNotLoaded').show();

    $(document).ready(function () {
        app.onPageInit('searchResults', function (page) {
            if ($rootScope.currentOpeningPage != 'searchResults') return;
            $rootScope.currentOpeningPage = 'searchResults';

            var name = page.query.name;
            var city = page.query.city;
            var category = page.query.category;
            var country = page.query.country;

            $$('#divInfiniteSearchResults').on('infinite', function () {
                if (loadingMyQuestions) return;
                loadingMyQuestions = true;

                CookieService.setCookie('searchResults-page-number', parseInt(CookieService.getCookie('searchResults-page-number')) + 1);

                var queryStringData = {
                    'page': parseInt(CookieService.getCookie('searchResults-page-number')),
                    'show': myApp.fw7.PageSize
                };

                var params = {
                    name: name,
                    categoryId: category,
                    cityId: city,
                    countryId: country
                };

                appServices.CallService('searchResults', 'POST', "api/Questions/Search" + '?page=' + queryStringData.page + '&show=' + queryStringData.show, params, function (response) {
                    if (response && response.length > 0) {
                        loadingMyQuestions = false;

                        angular.forEach(response, function (question) {
                            allQuestions.push(question);
                        });

                        $scope.questions = allQuestions;

                        if (response && response.length < myApp.fw7.PageSize) {
                            $scope.searchResultsInfiniteLoader = false;
                            app.detachInfiniteScroll('#divInfiniteSearchResults');
                            return;
                        }
                    }
                    else {
                        $scope.searchResultsInfiniteLoader = false;
                        app.detachInfiniteScroll('#divInfiniteSearchResults');
                        loadingMyQuestions = false;
                    }

                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                });
            });
        });

        app.onPageBeforeAnimation('searchResults', function (page) {
            if ($rootScope.currentOpeningPage != 'searchResults') return;
            $rootScope.currentOpeningPage = 'searchResults';

            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageReinit('searchResults', function (page) {

            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('searchResults', function (page) {
            if ($rootScope.currentOpeningPage != 'searchResults') return;
            $rootScope.currentOpeningPage = 'searchResults';

            var name = page.query.name;
            var city = page.query.city;
            var category = page.query.category;
            var country = page.query.country;

            $scope.isLoaded = false;

            $rootScope.CheckNewMessages(function (notifications) {
                $scope.NewNotificationsNumber = notifications;
                setTimeout(function () {
                    $scope.$apply();
                }, 10);
            });

            LoadQuestions(name, category, city, country, function (result) { });

            var IsVisitor = $rootScope.isVisitor();
            if (IsVisitor == true) {
                $scope.IsVisitor = true;
                $('.spanNotifications').hide();
            }
            else {
                $scope.IsVisitor = false;
                $rootScope.CheckNotifications(function (notificationLength) {
                    $('#SearchResultsNotificationsLoaded').show();
                    $('#SearchResultsNotificationsNotLoaded').hide();
                });
            }

            LoadAdvertisments();

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });


        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('intro', null);
        };

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', null);
        };

        $scope.GoToNotifications = function () {
            helpers.GoToPage('userNotification', null);
        };

        $scope.GoToAddQuestion = function () {
            helpers.GoToPage('addQuestion', null);
        };

        $scope.GoToQuestionDetails = function (question) {
            helpers.GoToPage('question', { question: question });
        };

        app.init();
    });

}]);

