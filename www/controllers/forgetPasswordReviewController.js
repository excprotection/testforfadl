﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('forgetPasswordReviewController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    $scope.txtAlignValue = "left";

    $(document).ready(function () {
        app.onPageInit('forgetPassReview', function (page) {
            if ($rootScope.currentOpeningPage != 'forgetPassReview') return;
            $rootScope.currentOpeningPage = 'forgetPassReview';

        });

        app.onPageBeforeAnimation('forgetPassReview', function (page) {
            if ($rootScope.currentOpeningPage != 'forgetPassReview') return;
            $rootScope.currentOpeningPage = 'forgetPassReview';
            var lang = localStorage.getItem('Dalal_lang');
            if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                $scope.txtAlignValue = "right";
            } else {
                $scope.txtAlignValue = "left";
            }
            $scope.resetForm();
        });

        app.onPageReinit('forgetPassReview', function (page) {
            $scope.resetForm();
        });

        app.onPageAfterAnimation('forgetPassReview', function (page) {
            if ($rootScope.currentOpeningPage != 'forgetPassReview') return;
            $rootScope.currentOpeningPage = 'forgetPassReview';

            $scope.resetForm();
        });

        $scope.resetForm = function () {
            $scope.forgetPassReset = false;
            $scope.forgetPassForm.mobile = null;
            if (typeof $scope.ForgetPasswordForm != 'undefined' && $scope.ForgetPasswordForm != null) {
                $scope.ForgetPasswordForm.$setPristine(true);
                $scope.ForgetPasswordForm.$setUntouched();
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        }

        $scope.form = {};
        $scope.forgetPassForm = {};

        $scope.submitForm = function (isValid) {
            $scope.forgetPassReset = true;
            if (isValid) {
                var mobile = $scope.forgetPassForm.mobile;

                var params = {
                    'phoneNumber': mobile
                };

                app.showIndicator();
                appServices.CallService('forgetPassReview', "POST", "api/account/ForgotPassword?IsSMSEnabled=true", params, function (res) {
                    app.hideIndicator();
                    if (res != null) {
                        CookieService.setCookie('confirmationMobile', $scope.forgetPassForm.mobile);
                        CookieService.setCookie('confirmationCode', res.code);
                        language.openFrameworkModal('نجاح', 'تم إرسال الكود لجوالك بنجاح .', 'alert', function () { });
                        helpers.GoToPage('resetPasswordReview', null);
                    }
                });
            }
        };

        $scope.GoBack = function () {
            helpers.GoBack();
        }

        $scope.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        $scope.mobileRegex = /^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/;

        app.init();
    });

}]);

