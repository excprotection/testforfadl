﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('requestInfoNewController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';

    function GetRequestDetails(requestId, callBack) {

        app.showIndicator();

        appServices.CallService('requestInfo', "GET", "api/IndividualContractRequest/Details_M/" + requestId, '', function (res) {
            app.hideIndicator();
            if (res != null) {
                callBack(res.request);
            }
            else {
                callBack(null);
            }
        });
    }

    function OpenActivationCodePopup(requestId, code, mobile) {
        var codeText = 'الكود فقط للتجربة :';
        var codePlaceHolder = 'كود الإلغاء';
        var resendCodetext = 'إعادة إرسال';
        var activateText = 'تأكيد';
        var cancelText = 'تراجع';
        
        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            codeText = 'الكود فقط للتجربة :';
            codePlaceHolder = 'كود الإلغاء';
            resendCodetext = 'إعادة إرسال';
            activateText = 'تأكيد';
            cancelText = 'تراجع';
        }
        else {
            codeText = 'Code For Trial Use Only';
            codePlaceHolder = 'Cancellation Code';
            resendCodetext = 'Resend';
            activateText = 'Confirm';
            cancelText = 'Back';
        }

        var afterText = '';

        if (serviceURL.indexOf("http://test.EPDemo.sa") > -1) {
            afterText = '<div class="list-block">' +
            '<div class="m-auto">' +
            '<ul>' +
            '<li style="text-align:center;">' +
             '<label class="lblCode">' + codeText + code + ' </label>' +
            '</li>' +
            '<li>' +
            '<div class="item-content">' +
            '<div class="item-inner">' +
            '<div class="item-input">' +
            '<input id="txtIndvCode" type="text" name="phone" placeholder="' + codePlaceHolder + '">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</li>' +
            '</ul>' +
            '</div>' +
            '</div>';
        }
        else {
            afterText = '<div class="list-block">' +
         '<div class="m-auto">' +
         '<ul>' +
         '<li>' +
         '<div class="item-content">' +
         '<div class="item-inner">' +
         '<div class="item-input">' +
         '<input id="txtIndvCode" type="text" name="phone" placeholder="' + codePlaceHolder + '">' +
         '</div>' +
         '</div>' +
         '</div>' +
         '</li>' +
         '</ul>' +
         '</div>' +
         '</div>';
        };


        var activationCancelRequest = app.modal({
            title: codePlaceHolder,
            text: '',
            afterText: afterText,
            buttons: [
                {
                    text: activateText,
                    onClick: function () {
                        var codeEntered = $('#txtIndvCode').val();
						
                        if (codeEntered == code) {
                            app.showIndicator();

                            appServices.CallService('requestInfo', "GET", "api/IndividualContractRequest/CancelRequest_M?requestId=" + requestId + "&cancelCode=" + codeEntered, '', function (res) {
                                app.hideIndicator();
                                if (res != null) {
                                    if (lang == 'AR' || lang == null || typeof lang == 'undefined') {
                                    	language.openFrameworkModal('نجاح', 'تم إلغاء العقد بنجاح', 'alert', function () {
                                    		app.closeModal(activationCancelRequest);
                                    		//helpers.GoToPage('userIndvContractRequests', null);
                                    	});
                                    }
                                    else {
                                    	language.openFrameworkModal('Success', 'Request has been cancelled successfully.', 'alert', function () {

                                    		app.closeModal(activationCancelRequest);
                                    		//helpers.GoToPage('userIndvContractRequests', null);
                                    });
                                    }
                                  
                                    //app.closeModal(activationCancelRequest);
                                    //helpers.GoToPage('userIndvContractRequests', null);
                                }
                                else {
                                    if (lang == 'AR' || lang == null || typeof lang == 'undefined') {
                                        language.openFrameworkModal('خطأ', 'حدث خطأ في إلغاء العقد .. برجاء المحاولة مرة أخري', 'alert', function () { });
                                    }
                                    else {
                                        language.openFrameworkModal('Error', 'Error in cancelling request .. Please try again.', 'alert', function () { });
                                    }

                                    OpenActivationCodePopup(requestId, code, mobile);
                                }
                            });
                        }
                        else {
                            OpenActivationCodePopup(requestId, code, mobile);
                        }
                    }
                },
                {
                    text: resendCodetext,
                    onClick: function () {
                        var params = {
                            'requestId': requestId,
                            'mobile': mobile
                        };

                        app.showIndicator();
                        appServices.CallService('requestInfo', "GET", 'api/IndividualContractRequest/SendCancelCode_M?requestId=' + requestId + '&&mobile=' + mobile + '&&IsSMSEnabled=true', params, function (res) {
                            app.hideIndicator();
                            if (res != null) {

                                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                                    language.openFrameworkModal('نجاح', 'تم إعادة إرسال الكود للجوال بنجاح .', 'alert', function () { });
                                }
                                else {
                                    language.openFrameworkModal('نجاح', 'Code Is Resent To Your Mobile Successfully', 'alert', function () { });
                                }
                                OpenActivationCodePopup(requestId, res.code, mobile);
                            }
                            else {
                                OpenActivationCodePopup(requestId, res.code, mobile);
                            }
                        });
                    }
                },
                {
                    text: cancelText,
                    onClick: function () {
                        app.closeModal(activationCancelRequest);
                    }
                }
            ]
        });
    }

    function CancelRequest(request, callBack) {
        var lang = CookieService.getCookie('lang');
        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
    
		   app.showIndicator();
		   appServices.CallService('requestInfo', "GET", 'api/IndividualContractRequest/SendCancelCode_M?requestId=' + request.individualContractRequestId + '&&mobile=' + userLoggedIn.phoneNumber + '&&IsSMSEnabled=true', '', function (res) {
            app.hideIndicator();
            if (res != null) {
            	OpenActivationCodePopup(request.individualContractRequestId, res.code , userLoggedIn.phoneNumber);
				}
			});


    }

    $(document).ready(function () {
        app.onPageInit('requestInfo', function (page) {
            if ($rootScope.currentOpeningPage != 'requestInfo') return;
            $rootScope.currentOpeningPage = 'requestInfo';

        });

        app.onPageBeforeAnimation('requestInfo', function (page) {
            if ($rootScope.currentOpeningPage != 'requestInfo') return;
            $rootScope.currentOpeningPage = 'requestInfo';

            $scope.isLoaded = true;

            var requestId = page.query.requestId;

            GetRequestDetails(requestId, function (result) {
                if (result != null) {
                    var request = result;
                    //request.availableDaysNumber = request.selectedDays != null && request.selectedDays != '' ? request.selectedDays.split(',').length : 0;
                    //request.startDay = request.startDay.split('T')[0];
                    //request.selectedPackage = request.hourlyPricingCost;

                    $scope.IsNotPaid = request.statusCode == 1 ? true : false;
                    var lang = CookieService.getCookie('lang');
                    $scope.lang = lang;
                    $scope.request = request;

                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                }
                else {
                    language.openFrameworkModal('نجاح', 'لا يوجد تفاصيل لهذا العقد.', 'alert', function () { });
                    helpers.GoBack();
                }
            });
        });

        app.onPageReinit('requestInfo', function (page) {
            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('requestInfo', function (page) {
            if ($rootScope.currentOpeningPage != 'requestInfo') return;
            $rootScope.currentOpeningPage = 'requestInfo';

        });


        $scope.GoBack = function () {

            helpers.GoBack();
        };

        $scope.PayNow = function () {
        	//   helpers.GoToPage('paymentMethod', { request: $scope.request });
        	helpers.GoToPage('indvPaymentMethod', { contract: $scope.request });
        };

        $scope.Cancel = function () {
        	CancelRequest($scope.request);
        	$scope.GoToIntro();
        };

        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('intro', null);
        };

        app.init();
    });

}]);

