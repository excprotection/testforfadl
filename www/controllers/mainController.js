﻿/// <reference path="../js/jquery-2.1.0.js" />
/// <reference path="../js/framework7.min.js" />
/// <reference path="../js/angular.js" />

myApp.angular.controller('mainController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var mainView = fw7.views['.view-main'];
    var app = myApp.fw7.app;
    $rootScope.currentOpeningPage = 'home';


    $scope.GoToIntro = function () {
        helpers.GoToPage('intro', null);
    };

    $scope.GoToHome = function () {
        helpers.GoToPage('home', null);
    };

    $scope.GoToProfile = function () {
        helpers.GoToPage('userProfile', null);
    };

    $scope.GoToMyContracts = function () {
        //helpers.GoToPage('userContractsTypes', null);
        helpers.GoToPage('userContracts', null);
    };

    $scope.GoToMyComplaints = function () {
        helpers.GoToPage('userComplaints', null);
    };

    $scope.GoToIndividuals = function () {
        helpers.GoToPage('terms', null);
    };

    $scope.GoToMedical = function () {
        helpers.GoToPage('medical', null);
    };

    $scope.GoToBusiness = function () {
        helpers.GoToPage('business', null);
    };

    $scope.GoToAboutUs = function () {
        helpers.GoToPage('about', null);
    };

    $scope.GoToMission = function () {
        helpers.GoToPage('mission', null);
    };

    $scope.GoToMembers = function () {
        helpers.GoToPage('members', null);
    };

    $scope.GoToContact = function () {
        helpers.GoToPage('branches', null);
    };
    $scope.GoToMyRequests = function () {
        helpers.GoToPage('userIndvContractRequests', null);
    };
    $scope.GoToMyInvoices = function () {
        helpers.GoToPage('userInvoices', null);
    };
    $scope.GoToMyPoints = function () {
        helpers.GoToPage('neqaty', null);
    };
    $scope.GoToChangeLanguage = function () {
        var lang = localStorage.getItem('Dalal_lang');
        var IsVisitor = CookieService.getCookie('Visitor') ? CookieService.getCookie('Visitor') : false;
        var userLoggedIn = CookieService.getCookie('userLoggedIn') ? JSON.parse(CookieService.getCookie('userLoggedIn')) : null;

        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            $('#lblMenuLoginText').html('Logout');
            localStorage.setItem('Dalal_lang', 'EN');
        }
        else {
            $('#lblMenuLoginText').html('تسجيل خروج');
            localStorage.setItem('Dalal_lang', 'AR');
        }


        app.closePanel();
        language.ChangeLanguage();
        helpers.GoToPage('changeLanguage', null);
    };

    $scope.GoToChangePassword = function () {
        helpers.GoToPage('changePass', null);
    };

    $scope.GoToLogin = function () {
        CookieService.removeCookie('appToken');
        CookieService.removeCookie('USName');
        CookieService.removeCookie('userLoggedIn');
        CookieService.removeCookie('deviceId');
        CookieService.setCookie('Visitor', true);
        helpers.GoToPage('login', null);

        //var deviceId = CookieService.getCookie('deviceId');
        //var token = CookieService.getCookie('appToken');
        //if (typeof token != 'undefined' && token != null && typeof deviceId != 'undefined' && device != null) {
        //    app.showIndicator();
        //    appServices.CallService('resetPassword', "POST", "api/v1/devices/delete?playerId=" + deviceId, '', function (res) {
        //        app.hideIndicator();
        //        if (res != null) {
        //            $rootScope.pusher.disconnect();
        //            cordova.plugins.notification.badge.clear();
        //            CookieService.removeCookie('appToken');
        //            CookieService.removeCookie('USName');
        //            CookieService.removeCookie('userLoggedIn');
        //            CookieService.removeCookie('deviceId');
        //            CookieService.setCookie('Visitor', false);
        //            helpers.GoToPage('login', null);
        //        }
        //        else {
        //            $rootScope.pusher.disconnect();
        //            cordova.plugins.notification.badge.clear();
        //            CookieService.removeCookie('appToken');
        //            CookieService.removeCookie('USName');
        //            CookieService.removeCookie('userLoggedIn');
        //            CookieService.removeCookie('deviceId');
        //            CookieService.setCookie('Visitor', false);
        //            helpers.GoToPage('login', null);
        //        }
        //    });
        //}
        //else {
        //    cordova.plugins.notification.badge.clear();
        //    CookieService.removeCookie('appToken');
        //    CookieService.removeCookie('USName');
        //    CookieService.removeCookie('userLoggedIn');
        //    CookieService.removeCookie('deviceId');
        //    CookieService.setCookie('Visitor', false);
        //    helpers.GoToPage('login', null);
        //}
    };


    $scope.GoToIntroReview = function () {
        helpers.GoToPage('introReview', null);
    };

    $scope.GoToProfileReview = function () {
        helpers.GoToPage('userProfileReview', null);
    };

    $scope.GoToMedicalReview = function () {
        helpers.GoToPage('medicalReview', null);
    };

    $scope.GoToBranchesReview = function () {
        helpers.GoToPage('branchesReview', null);
    };

    $scope.GoToAboutUsReview = function () {
        helpers.GoToPage('aboutReview', null);
    };
    $scope.GoToMyOrdersReview = function () {
        helpers.GoToPage('myOrdersReview', null);
    };
    $scope.GoToMissionReview = function () {
        helpers.GoToPage('missionReview', null);
    };

    $scope.GoToMembersReview = function () {
        helpers.GoToPage('membersReview', null);
    };

    $scope.GoToContactUs = function () {
        helpers.GoToPage('individualSectorReview', null);
    };

    $scope.GoToChangeLanguageReview = function () {
        var lang = localStorage.getItem('Dalal_lang');
        var IsVisitor = CookieService.getCookie('Visitor') ? CookieService.getCookie('Visitor') : false;
        var userLoggedIn = CookieService.getCookie('userLoggedIn') ? JSON.parse(CookieService.getCookie('userLoggedIn')) : null;

        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            $('#lblMenuLoginText').html('Logout');
            localStorage.setItem('Dalal_lang', 'EN');
        }
        else {
            $('#lblMenuLoginText').html('تسجيل خروج');
            localStorage.setItem('Dalal_lang', 'AR');
        }


        app.closePanel();
        language.ChangeLanguage();
        helpers.GoToPage('changeLanguageReview', null);
    };

    $scope.GoToChangePasswordReview = function () {
        helpers.GoToPage('changePassReview', null);
    };

    $scope.GoToLoginReview = function () {
        CookieService.removeCookie('appToken');
        CookieService.removeCookie('USName');
        CookieService.removeCookie('userLoggedIn');
        CookieService.removeCookie('deviceId');
        CookieService.setCookie('Visitor', true);
        helpers.GoToPage('loginReview', null);
    };

    SidePanelService.DrawMenu();

    $(document).ready(function () {

    });

}]);


