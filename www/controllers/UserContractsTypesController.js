﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('userContractsTypesController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var lang = localStorage.getItem('Dalal_lang');


    $(document).ready(function () {
       
        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToLogin = function () {
            helpers.GoToPage('login', null);
        };

        $scope.GoToContractsPage = function (type) {
            if (type == "HourlyContract") {
                helpers.GoToPage('userContracts', null);
            }
            else if (type == "IndividualContract") {
                helpers.GoToPage('userIndvContracts', null);
            }
        }

        $scope.GoToOnline = function () {
        	var url = 'http://test.EPDemo.sa/ar/PaymentForMobile/IndividualOnlinePayment/f3c2a4f7-8cda-e811-80f3-000d3ab19e01';

        	var ref = cordova.InAppBrowser.open(url, '_blank', 'location=no,toolbar=yes,zoom=no');
        	ref.addEventListener('loadstart', function () { alert(event.url); });
        }
        app.init();

    });
}]);

