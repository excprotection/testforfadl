﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('aboutReviewController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';

    $(document).ready(function () {
        app.onPageInit('aboutReview', function (page) {
            if ($rootScope.currentOpeningPage != 'aboutReview') return;
            $rootScope.currentOpeningPage = 'aboutReview';

        });

        app.onPageBeforeAnimation('aboutReview', function (page) {
            if ($rootScope.currentOpeningPage != 'aboutReview') return;
            $rootScope.currentOpeningPage = 'aboutReview';

            $scope.isLoaded = true;

        });

        app.onPageReinit('aboutReview', function (page) {
            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('aboutReview', function (page) {
            if ($rootScope.currentOpeningPage != 'aboutReview') return;
            $rootScope.currentOpeningPage = 'aboutReview';

        });


        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('introReview', null);
        };

        app.init();
    });

}]);

