﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('aboutController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';

    $(document).ready(function () {
        app.onPageInit('about', function (page) {
            if ($rootScope.currentOpeningPage != 'about') return;
            $rootScope.currentOpeningPage = 'about';

        });

        app.onPageBeforeAnimation('about', function (page) {
            if ($rootScope.currentOpeningPage != 'about') return;
            $rootScope.currentOpeningPage = 'about';

            $scope.isLoaded = true;

        });

        app.onPageReinit('about', function (page) {
            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('about', function (page) {
            if ($rootScope.currentOpeningPage != 'about') return;
            $rootScope.currentOpeningPage = 'about';

        });


        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('intro', null);
        };

        app.init();
    });

}]);

