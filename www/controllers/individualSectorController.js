﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('individualSectorController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';
    var availableDaysSelected = [];
    var disabledDays = [];
    var selectedPackage = {};
    var lang = localStorage.getItem('Dalal_lang');
    var InReview = false;

    function LoadCities(callBack) {
        var cities = [];
     
        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            $('#linkTown .item-after').html('المدينة');
        }
        else {
            $('#linkTown .item-after').html('City');
        }

        appServices.CallService('create', "GET", "api/city/QuickAll", '', function (res) {
          
            if (res != null && res.length > 0) {
                $scope.cities = res;
                $$('#linkTown select').html('');

                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    app.smartSelectAddOption('#linkTown select', '<option value="" selected>المدينة</option>');
                }
                else {
                    app.smartSelectAddOption('#linkTown select', '<option value="" selected>City</option>');
                }
                angular.forEach($scope.cities, function (city) {
                    app.smartSelectAddOption('#linkTown select', '<option value="' + city.key + '">' + city.value + '</option>');
                });
            }

            callBack(true);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    function LoadIndividualNationalities(callBack) {
        var cities = [];

     
        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            $('#linkNationality .item-after').html('الجنسية');
        }
        else {
            $('#linkNationality .item-after').html('Nationality');
        }

        appServices.CallService('individualSector', "GET", "api/Nationality/Individual", '', function (res) {

            if (res != null && res.length > 0) {
                $scope.cities = res;
                $$('#linkNationality select').html('');

                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    app.smartSelectAddOption('#linkNationality select', '<option value="" selected>الجنسية</option>');
                }
                else {
                    app.smartSelectAddOption('#linkNationality select', '<option value="" selected>Nationality</option>');
                }
                angular.forEach($scope.cities, function (city) {
                    app.smartSelectAddOption('#linkNationality select', '<option value="' + city.key + '">' + city.value + '</option>');
                });
            }

            callBack(true);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    function LoadIndividualProfessions(callBack) {
        var cities = [];

     
        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            $('#linkRequiredProfession .item-after').html('المهنة المطلوبة');
        }
        else {
            $('#linkRequiredProfession .item-after').html('Required Profession');
        }

        appServices.CallService('individualSector', "GET", "api/Professions/Individual", '', function (res) {
           
            if (res != null && res.length > 0) {
                $scope.cities = res;
                $$('#linkRequiredProfession select').html('');

                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    app.smartSelectAddOption('#linkRequiredProfession select', '<option value="">المهنة المطلوبة</option>');
                }
                else {
                    app.smartSelectAddOption('#linkRequiredProfession select', '<option value="">Desired Profession</option>');
                }
                angular.forEach($scope.cities, function (city) {
                    if (city.value == 'عاملة منزلية') {
                        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                            $('#linkRequiredProfession .item-after').html(city.value);
                        }
                        else {
                            $('#linkRequiredProfession .item-after').html(city.value);
                        }
                        app.smartSelectAddOption('#linkRequiredProfession select', '<option value="' + city.key + '" selected>' + city.value + '</option>');
                    }
                    else {
                        app.smartSelectAddOption('#linkRequiredProfession select', '<option value="' + city.key + '">' + city.value + '</option>');
                    }
                });
            }

            callBack(true);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    function LoadGenders(callBack) {
        var genders = [];

        lang = localStorage.getItem('Dalal_lang');
        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            $('#linkIndividualSectorGender .item-after').html('النوع');
        }
        else {
            $('#linkIndividualSectorGender .item-after').html('Gender');
        }

        appServices.CallService('individualSector', "GET", "api/contact/Genders", '', function (res) {
            if (res != null && res.length > 0) {
                $scope.genders = res;
                $$('#linkIndividualSectorGender select').html('');

                lang = localStorage.getItem('Dalal_lang');
                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    app.smartSelectAddOption('#linkIndividualSectorGender select', '<option value="" selected>النوع</option>');
                }
                else {
                    app.smartSelectAddOption('#linkIndividualSectorGender select', '<option value="" selected>Gender</option>');
                }
                angular.forEach($scope.genders, function (gender) {
                    app.smartSelectAddOption('#linkIndividualSectorGender select', '<option value="' + gender.key + '">' + gender.value + '</option>');
                });
            }

            callBack(true);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    function LoadCountries(callBack) {
        var countries = [];

        lang = localStorage.getItem('Dalal_lang');
        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            $('#linkCountry .item-after').html('الجنسية');
        }
        else {
            $('#linkCountry .item-after').html('Nationality');
        }

        appServices.CallService('individualSector', "GET", "api/Nationality/QuickAll", '', function (res) {
            if (res != null && res.length > 0) {
                $scope.countries = res;
                $$('#linkCountry select').html('');

                lang = localStorage.getItem('Dalal_lang');
                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    app.smartSelectAddOption('#linkCountry select', '<option value="" selected>الجنسية</option>');
                }
                else {
                    app.smartSelectAddOption('#linkCountry select', '<option value="" selected>Nationality</option>');
                }
                angular.forEach($scope.countries, function (country) {
                    app.smartSelectAddOption('#linkCountry select', '<option value="' + country.key + '">' + country.value + '</option>');
                });
            }

            callBack(true);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    function LoadALL() {
        app.showIndicator();
        LoadCities(function (result) {
            LoadIndividualNationalities(function (result) {
                LoadIndividualProfessions(function (result) {
                    LoadGenders(function (result) {
                        LoadCountries(function (result) {
                            $('#linkTown').each(function (index) {
                                if (lang == 'AR' || lang == null || typeof lang == 'undefined') {
                                    $(this).attr('data-picker-close-text', 'تم');
                                    $(this).attr('data-searchbar-placeholder', 'بحث عن مدينة');
                                }
                                else {
                                    $(this).attr('data-picker-close-text', 'Close');
                                    $(this).attr('data-searchbar-placeholder', 'Search Cities');
                                }
                            });

                            $scope.userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

                            if ($scope.userLoggedIn.email) {
                                $scope.showEmail = false;
                            }
                            else {
                                $scope.showEmail = true;
                            }


                            lang = localStorage.getItem('Dalal_lang');
                            $scope.isArabic = lang == 'AR' || typeof lang == 'undefined' || lang == null ? true : false;


                            appServices.CallService('userProfile', 'GET', "api/contact/GetDetails_M/" + $scope.userLoggedIn.crmUserId, '', function (userData) {
                                app.hideIndicator();
                                if (userData) {
                                    $scope.userLoggedIn.nationalityId = userData.contact.nationalityId;
                                    $scope.userLoggedIn.townId = userData.contact.townId;
                                    $scope.userLoggedIn.genderId = userData.contact.genderId;
                                    $scope.userLoggedIn.idNumber = userData.contact.idNumber;


                                    if ($scope.userLoggedIn.email) {
                                        $scope.showEmail = false;
                                    }
                                    else {
                                        $scope.showEmail = true;
                                    }

                                    if ($scope.userLoggedIn.nationalityId) {
                                        $scope.showCountry = false;
                                    }
                                    else {
                                        $scope.showCountry = true;
                                    }

                                    if ($scope.userLoggedIn.genderId) {
                                        $scope.showGender = false;
                                    }
                                    else {
                                        $scope.showGender = true;
                                    }

                                    if ($scope.userLoggedIn.idNumber) {
                                        $scope.showId = false;
                                    }
                                    else {
                                        $scope.showId = true;
                                    }
                                }
                            });

                            $('#linkNationality select').val('');

                            $('#linkTown select').val('');
                            $('#divItemAfterCreateTown').text($('#linkTown select option:selected').text());

                            $('#linkIndividualSectorGender select').val('');
                            $('#divItemAfterCreateIndividualSectorGender').text($('#linkIndividualSectorGender select option:selected').text());

                            $("#linkCountry select").val('');
                            $('#divItemAfterCreateIndividualSectorCountry').text($('#linkCountry select option:selected').text());

                            $("#txtServiceDescription").val('');
                            $("#txtEmail").val('');
                            $("#txtIdNumber").val('');
                            $("#txtJobTitle").val('');

                            $('#divItemAfterCreateIndividualSectorNationality').text($('#linkNationality select option:selected').text());
                        });
                    });
                });
            });
        });
    }

    function LoadExceptNationalityAndProfession() {
        app.showIndicator();
        LoadCities(function (result) {
            LoadIndividualNationalities(function (result) {
                LoadIndividualProfessions(function (result) {
                    LoadGenders(function (result) {
                        LoadCountries(function (result) {
                            $('#linkTown').each(function (index) {
                                if (lang == 'AR' || lang == null || typeof lang == 'undefined') {
                                    $(this).attr('data-picker-close-text', 'تم');
                                    $(this).attr('data-searchbar-placeholder', 'بحث عن مدينة');
                                }
                                else {
                                    $(this).attr('data-picker-close-text', 'Close');
                                    $(this).attr('data-searchbar-placeholder', 'Search Cities');
                                }
                            });

                            $scope.userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

                            if ($scope.userLoggedIn.email) {
                                $scope.showEmail = false;
                            }
                            else {
                                $scope.showEmail = true;
                            }


                            lang = localStorage.getItem('Dalal_lang');
                            $scope.isArabic = lang == 'AR' || typeof lang == 'undefined' || lang == null ? true : false;


                            appServices.CallService('userProfile', 'GET', "api/contact/GetDetails_M/" + $scope.userLoggedIn.crmUserId, '', function (userData) {
                                app.hideIndicator();
                                if (userData) {
                                    $scope.userLoggedIn.nationalityId = userData.contact.nationalityId;
                                    $scope.userLoggedIn.townId = userData.contact.townId;
                                    $scope.userLoggedIn.genderId = userData.contact.genderId;
                                    $scope.userLoggedIn.idNumber = userData.contact.idNumber;


                                    if ($scope.userLoggedIn.email) {
                                        $scope.showEmail = false;
                                    }
                                    else {
                                        $scope.showEmail = true;
                                    }

                                    if ($scope.userLoggedIn.nationalityId) {
                                        $scope.showCountry = false;
                                    }
                                    else {
                                        $scope.showCountry = true;
                                    }

                                    if ($scope.userLoggedIn.genderId) {
                                        $scope.showGender = false;
                                    }
                                    else {
                                        $scope.showGender = true;
                                    }

                                    if ($scope.userLoggedIn.idNumber) {
                                        $scope.showId = false;
                                    }
                                    else {
                                        $scope.showId = true;
                                    }
                                }
                            });

                            $('#linkNationality select').val('');

                            $('#linkTown select').val('');
                            $('#divItemAfterCreateTown').text($('#linkTown select option:selected').text());

                            $('#linkIndividualSectorGender select').val('');
                            $('#divItemAfterCreateIndividualSectorGender').text($('#linkIndividualSectorGender select option:selected').text());

                            $("#linkCountry select").val('');
                            $('#divItemAfterCreateIndividualSectorCountry').text($('#linkCountry select option:selected').text());

                            $("#txtServiceDescription").val('');
                            $("#txtEmail").val('');
                            $("#txtIdNumber").val('');
                            $("#txtJobTitle").val('');

                            $('#divItemAfterCreateIndividualSectorNationality').text($('#linkNationality select option:selected').text());
                        });
                    });
                });
            });
        });
    }


    $(document).ready(function () {

        app.onPageInit('individualSector', function (page) {
            var xxxx = 1;
        });

        app.onPageReinit('individualSector', function (page) {
            $scope.resetForm();
        });

        app.onPageBeforeAnimation('individualSector', function (page) {
            $scope.isLoaded = true;
            $scope.submittedCreate = false;

            $scope.resetForm();

            InReview = page.query.InReview;

            $scope.InReview = InReview;

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);

            if (InReview == true) {
                LoadExceptNationalityAndProfession();
            }
            else {
                LoadALL();
            }
        });

        $scope.form = {};
        $scope.createForm = {};

        $scope.resetForm = function () {
            $scope.createReset = false;
            $scope.submittedCreate = false;

            $scope.createForm.nationality = null;
            $scope.createForm.requiredProfession = null;
            $scope.createForm.serviceDescription = null;
            //$scope.createForm.email = null;
            $scope.createForm.country = null;
            $scope.createForm.gender = null;
            $scope.createForm.idNumber = null;
            $scope.createForm.jobTitle = null;
            $scope.createForm.town = null;

            if (typeof $scope.CreateForm != 'undefined' && $scope.CreateForm != null) {
                $scope.CreateForm.$setPristine(true);
                $scope.CreateForm.$setUntouched();
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        };

        $scope.submitForm = function (isValid) {
            var lang = localStorage.getItem('Dalal_lang');
            $scope.createReset = true;
            $scope.submittedCreate = true;
            var nationalityId = $('#linkNationality select').val();
            var requiredProfessionId = $('#linkRequiredProfession select').val();
            var townId = $('#linkTown select').val();
            var genderId = $('#linkIndividualSectorGender select').val();
            var countryId = $("#linkCountry select").val();
            var serviceDescription = $("#txtServiceDescription").val();
            var email = $("#txtEmail").val();
            var idNumber = $("#txtIdNumber").val();
            var jobTitle = $("#txtJobTitle").val();

            $scope.isNationalityValid = $scope.createReset && $scope.submittedCreate && nationalityId != null && nationalityId != '' && nationalityId != ' ' ? true : false;
            $scope.isRequiredProfessionValid = $scope.createReset && $scope.submittedCreate && requiredProfessionId != null && requiredProfessionId != '' && requiredProfessionId != ' ' ? true : false;
            $scope.isJobTitleValid = ($scope.createReset && $scope.submittedCreate && jobTitle != null && jobTitle != '' && jobTitle != ' ' ? true : false);
            $scope.isTownValid = $scope.createReset && $scope.submittedCreate && townId != null && townId != '' && townId != ' ' ? true : false;
            $scope.isEmailValid = ($scope.createReset && $scope.submittedCreate && email != null && email != '' && email != ' ' ? true : false) || !$scope.showEmail;
            $scope.isIdNumberValid = ($scope.createReset && $scope.submittedCreate && idNumber != null && idNumber != '' && idNumber != ' ' && idNumber.length==10 ? true : false) || !$scope.showId;
            $scope.isGenderValid = ($scope.createReset && $scope.submittedCreate && genderId != null && genderId != '' && genderId != ' ' ? true : false) || !$scope.showGender;
            $scope.isCountryValid = ($scope.createReset && $scope.submittedCreate && countryId != null && countryId != '' && countryId != ' ' ? true : false) || !$scope.showGender;

            var isIdNumberCorrect = false;

            countryId = typeof countryId != 'undefined' && countryId != null && countryId != '' && countryId != ' ' ? countryId : $scope.userLoggedIn.nationalityId;
            idNumber = typeof idNumber != 'undefined' && idNumber != null && idNumber != '' && idNumber != ' ' ? idNumber : $scope.userLoggedIn.idNumber;

            if (countryId == '1e0ff838-292f-e311-b3fd-00155d010303') {
                isIdNumberCorrect = idNumber != null && idNumber != '' && idNumber != ' ' && idNumber.startsWith('1') || idNumber.startsWith('١') == true ? true : false;
            }
            else {
                isIdNumberCorrect = idNumber != null && idNumber != '' && idNumber != ' ' && idNumber.startsWith('2') || idNumber.startsWith('٢') == true ? true : false;
            }

            $scope.isIdNumberCorrect = isIdNumberCorrect && $scope.isIdNumberValid;

            if (InReview) {
                $scope.isNationalityValid = true;
                $scope.isRequiredProfessionValid = true;

                nationalityId = 'be0ef838-292f-e311-b3fd-00155d010303';
                requiredProfessionId = '39c7f260-292f-e311-b3fd-00155d010303';
            }

            if ($scope.isNationalityValid && $scope.isRequiredProfessionValid && $scope.isJobTitleValid && $scope.isTownValid && $scope.isEmailValid && $scope.isIdNumberCorrect && $scope.isGenderValid && $scope.isCountryValid) {
                var params = {
                    "id": "",
                    "requiredNationality": nationalityId,
                    "requiredNationalityName": $('#linkNationality select option:selected').text(),
                    "requiredProfession": requiredProfessionId,
                    "requiredProfessionName": $('#linkRequiredProfession select option:selected').text(),
                    "cityId": townId,
                    "cityName": $('#linkTown select option:selected').text(),
                    "regionId": "",
                    "regionName": "",
                    "districtId": "",
                    "districtName": "",
                    "name": $scope.userLoggedIn.name,
                    "mobile": $scope.userLoggedIn.phoneNumber,
                    "jobTitle": jobTitle,
                    "homePhone": "",
                    "idNumber": $scope.showId ? idNumber : $scope.userLoggedIn.idNumber,
                    "sectorId": "",
                    "salesPersonName": "",
                    "salesPersonId": "",
                    "description": "",
                    "email": $scope.showEmail ? email : $scope.userLoggedIn.email,
                    "nationalityId": $scope.showCountry ? countryId : $scope.userLoggedIn.nationalityId,
                    "genderId": $scope.showGender ? genderId : $scope.userLoggedIn.genderId,
                    "contactId": $scope.userLoggedIn.crmUserId
                }

                app.showIndicator();
                appServices.CallService('individualSector', "POST", "api/Lead/Individual/CreateAndCompleteProfile", params, function (res) {
                    app.hideIndicator();
                    if (res != null) {
                        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                            var message = '  تم إرسال طلبك بنجاح .. رقم الطلب الخاص بك هو   ';
                            message += ' [ ' + res.user.code + ' ] ';
                            myApp.fw7.app.alert(message, 'نجاح', function () {
                                helpers.GoToPage('intro', null);
                            });
                        }
                        else {
                            var message = ' Request has been sent successfully .. Your request number is [ ' + res.user.code + ' ]';
                            myApp.fw7.app.alert(message, 'Success', function () {
                                helpers.GoToPage('intro', null);
                            });
                        }
                    }
                });
            }
            else {
                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            }
        };

        $scope.GoBack = function () {
            helpers.GoBack();
        };

        

        function isEmpty(obj) {
            for (var prop in obj) {
                if (obj.hasOwnProperty(prop))
                    return false;
            }

            return JSON.stringify(obj) === JSON.stringify({});
        }

        app.init();
    });

}]);

