﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('termsController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';

    function geocodeLatLng(geocoder, latitude, longitude,callBack) {
        var latlng = { lat: parseFloat(latitude), lng: parseFloat(longitude) };
        geocoder.geocode({ 'location': latlng }, function (results, status) {
            if (status === 'OK') {
                if (results[0]) {
                    for (var i = 0; i < results.length; i++) {
                        if (results[i].types[0] === "locality") {
                            var city = results[i].address_components[0].short_name;
                            var state = results[i].address_components[2].short_name;

                            CookieService.setCookie('StoredCity', city);
                            CookieService.setCookie('StoredState', state);

                            callBack(true);
                        }
                    }
                }
                else {
                    callBack(false);
                }
            }
            else {
                callBack(false);
            }
        });
    }
  

    function GetCurrentLocation(callBack) {

        var onSuccess = function (position) {
            var geocoder = new google.maps.Geocoder;
            geocodeLatLng(geocoder, position.coords.latitude, position.coords.longitude, function (Result) {
                callBack(Result);
            });
        }

        function onError(error) {
            callBack(null);
        }

        var devicePlatform = device.platform;

        cordova.getAppVersion.getVersionNumber(function (version) {
            if (typeof version != 'undefined' && version != null) {
                if (devicePlatform == 'Android' || devicePlatform == 'android') {
                    cordova.plugins.diagnostic.isGpsLocationEnabled(function (enabled) {
                        if (enabled) {
                            navigator.geolocation.getCurrentPosition(onSuccess, onError, { enableHighAccuracy: true, timeout: 2 * 1000, maximumAge: 0 });
                        }
                        else {
                            callBack(null);
                        }

                    }, function (error) {
                        callBack(null);
                    });
                }
                else {
                    navigator.geolocation.getCurrentPosition(onSuccess, onError, { enableHighAccuracy: true, timeout: 2 * 1000, maximumAge: 0 });
                }
            }
            else {
                navigator.geolocation.getCurrentPosition(onSuccess, onError, { enableHighAccuracy: true, timeout: 2 * 1000, maximumAge: 0 });
            }
        });

    }

    $(document).ready(function () {
        app.onPageInit('terms', function (page) {
            if ($rootScope.currentOpeningPage != 'terms') return;
            $rootScope.currentOpeningPage = 'terms';

        });

        app.onPageBeforeAnimation('terms', function (page) {
            if ($rootScope.currentOpeningPage != 'terms') return;
            $rootScope.currentOpeningPage = 'terms';
            $scope.isUserApprovedTerms = false;
          
            var IsVisitor = $rootScope.isVisitor();
            if (IsVisitor == true) {
                $scope.IsVisitor = true;
            }
            else {
                $scope.IsVisitor = false;
            }

            $scope.isLoaded = true;

            $('#iosIcon_AcceptTerms').removeClass('iosIcon');
            $scope.isUserApprovedTerms = false;

            //$scope.LoadPoints();

            $rootScope.RemoveEditPagesFromHistory();

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageReinit('terms', function (page) {
            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('terms', function (page) {
            if ($rootScope.currentOpeningPage != 'terms') return;
            $rootScope.currentOpeningPage = 'terms';

            $$('.popup-terms').on('popup:opened', function () {
                if (!$scope.terms) {
                    app.showIndicator();

                    appServices.CallService('terms', "GET", "api/Translator/GetTermsForMobile", '', function (res) {
                        app.hideIndicator();
                        if (res != null && res.length > 0) {
                            $scope.terms = res;
                        }

                        setTimeout(function () {
                            $scope.$apply();
                        }, fw7.DelayBeforeScopeApply);
                    });
                }
            });
        });

        $scope.LoadPoints = function () {
            var IsVisitor = $rootScope.isVisitor();
            
            if (IsVisitor == false || IsVisitor == 'false') {
                $scope.ShowTerms = true;
                var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

                $scope.AllPoints = ' --- ';
                $scope.pointAmount = ' --- ';

                app.showIndicator();

                appServices.CallService('userPoints', 'GET', 'api/Loyality/LoyalityBalance?id=' + userLoggedIn.crmUserId, '', function (result) {
                    app.hideIndicator();
                    if (result) {
                        $scope.AllPoints = result.loyaltyPoints;
                        $scope.pointAmount = result.loyaltyBalance;
                    }
                });
            }
            else {
                $scope.ShowTerms = false;
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        };

        $scope.GoToMyPoints = function () {
            helpers.GoToPage('neqaty', null);
        };


        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('intro', null);
        };

        $scope.GoToContractDetails = function (contract) {
            helpers.GoToPage('contractInfo', { contract: contract });
        };

        $scope.selectAcceptTerms = function (isUserApprovedTerms) {
            var element = document.getElementById('chkAcceptTerms');
            var isChecked = $(element).prop('checked');
            var hasClassChecked = $('#iosIcon_AcceptTerms').hasClass('iosIcon');

            if (isChecked == false && !hasClassChecked) {
                $('#iosIcon_AcceptTerms').addClass('iosIcon');
                $scope.isUserApprovedTerms = true;
            }
            else {
                $('#iosIcon_AcceptTerms').removeClass('iosIcon');
                $scope.isUserApprovedTerms = false;
            }

            setTimeout(function () {
                $scope.$apply();
                app.hideIndicator();
            }, 500);
        };

        $scope.AcceptAgreement = function () {
            var IsVisitor = $rootScope.isVisitor();
            if (IsVisitor == false || IsVisitor == 'false') {
                app.showIndicator();
                
                GetCurrentLocation(function (result) {
                    $scope.userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

                    appServices.CallService('terms', "GET", "api/HourlyContract/GetLatestNotPaidContract?crmUserId=" + $scope.userLoggedIn.crmUserId, '', function (res) {
                        app.hideIndicator();
                        if (res == '' || res == null || res == undefined) {
                            helpers.GoToPage('create', null);
                        }
                        else {

                            if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                                language.openFrameworkModal('خطأ', 'برجاء سداد قيمة العقد السابق أو إلغاؤه , قبل انشاء عقد جديد', 'alert', function () { });
                            }
                            else {
                                language.openFrameworkModal('خطأ', 'Please pay the value of the previous contract or cancel it before creating a new contract', 'alert', function () { });
                            }

                            helpers.GoToPage('contractInfo', {
                                contract:
                                    {
                                        contractId: res
                                    }
                            });
                        }
                    });
                });
            }
            else { helpers.GoToPage('login', null); }
        };

        $scope.RejectAgreement = function () {
            helpers.GoBack();
        };

        app.init();
    });

}]);

