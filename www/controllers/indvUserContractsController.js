﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('indvUserContractsController', ['$document', 'indvUserContracts', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';
    var loadingContracts;
    var allContracts = [];
    var methodName;
    var isPull = true;

    function ClearListData() {
        allContracts = [];
        loadingContracts = false;
        CookieService.setCookie('indv-contracts-page-number', 1);
        $scope.contracts = null;
        app.pullToRefreshDone();
    }

    function LoadUserContracts(callBack) {
        CookieService.setCookie('indv-contracts-page-number', 1);
        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

        var methodName = 'api/Profile/IndividualContract/All_M?userId=' + userLoggedIn.crmUserId + '&pageNumber=' +
            parseInt(CookieService.getCookie('indv-contracts-page-number')) + '&pageSize=' + myApp.fw7.PageSize;

        allContracts = [];
        app.attachInfiniteScroll('#divInfiniteIndvContracts');

        app.showIndicator();

        appServices.CallService('indvUserContracts', 'GET', methodName, '', function (result) {
            
            app.hideIndicator();

            allContracts = [];

            if (result && result.contracts.length > 0) {
                if (result.contracts.length < myApp.fw7.PageSize) {
                    $scope.contractsInfiniteLoader = false;
                    app.detachInfiniteScroll('#divInfiniteIndvContracts');
                }
                else {
                    $scope.contractsInfiniteLoader = true;
                    app.attachInfiniteScroll('#divInfiniteIndvContracts');
                }

                allContracts = result.contracts;

                allContracts.sort(function (a, b) {
                    return new Date(b.createdOn).getTime() - new Date(a.createdOn).getTime();
                });

                angular.forEach(allContracts, function (contract, index) {
                    contract.createdOn = contract.createdOn.split(' ')[0];
                });

                $scope.contracts = allContracts;

                $scope.noContracts = false;

                if (isPull) {
                    app.pullToRefreshDone();
                }

                callBack(true);

                setTimeout(function () {
                    $scope.allContractsLoaded = true;
                    $scope.isLoaded = true;
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);

            }
            else {
                allContracts = [];
                $scope.contracts = [];
                $scope.noContracts = true;
                $('.advNoResult').show();
                $scope.contractsInfiniteLoader = false;

                callBack(true);

                setTimeout(function () {
                    $scope.isLoaded = true;
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            }

        });
    }

    $(document).ready(function () {
        app.onPageInit('indvUserContracts', function (page) {
            if ($rootScope.currentOpeningPage != 'indvUserContracts') return;
            $rootScope.currentOpeningPage = 'indvUserContracts';

            fromPage = page.fromPage.name;

            $$('#divInfiniteIndvContracts').on('ptr:refresh', function (e) {
                isPull = true;

                LoadUserContracts(function (result) {
                    app.pullToRefreshDone();
                });

                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            });


            $$('#divInfiniteIndvContracts').on('infinite', function () {
                if (loadingContracts) return;
                loadingContracts = true;

                var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

                CookieService.setCookie('indv-contracts-page-number', parseInt(CookieService.getCookie('indv-contracts-page-number')) + 1);


                var methodName = 'api/Profile/HourlyContract/All_M?userId=' + userLoggedIn.crmUserId + '&pageNumber=' +
                    parseInt(CookieService.getCookie('indv-contracts-page-number')) + '&pageSize=' + myApp.fw7.PageSize;

                appServices.CallService('indvUserContracts', 'GET', methodName, '', function (response) {

                    if (response && response.contracts.length > 0) {
                        loadingContracts = false;

                        angular.forEach(response.contracts, function (contract, index) {
                            allContracts.push(contract);
                        });

                        allContracts.sort(function (a, b) {
                            return new Date(b.createdOn).getTime() - new Date(a.createdOn).getTime();
                        });

                        angular.forEach(allContracts, function (contract, index) {
                            contract.createdOn = contract.createOn.split(' ')[0];
                        });

                        $scope.contracts = allContracts;

                        if (response && response.length < myApp.fw7.PageSize) {
                            $scope.contractsInfiniteLoader = false;
                            app.detachInfiniteScroll('#divInfiniteIndvContracts');
                            return;
                        }
                    }
                    else {
                        $scope.contractsInfiniteLoader = false;
                        app.detachInfiniteScroll('#divInfiniteIndvContracts');
                        loadingContracts = false;
                    }

                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                });
            });
        });

        app.onPageBeforeAnimation('indvUserContracts', function (page) {
            if ($rootScope.currentOpeningPage != 'indvUserContracts') return;
            $rootScope.currentOpeningPage = 'indvUserContracts';

            $scope.isLoaded = false;

            fromPage = page.fromPage.name;

            ClearListData();

            isPull = false;

            LoadUserContracts(function (result) {
                var lang = localStorage.getItem('Dalal_lang');

                $scope.isLoaded = true;

                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    $scope.lblComplaintTicketNumber = 'رقم العقد :';
                    $scope.lblComplaintContractNumber = 'المبلغ :';
                    $scope.lblComplaintTicketDesc = 'التاريخ :';
                    $scope.lblContractCurrency = 'ريال';
                }
                else {
                    $scope.lblComplaintTicketNumber = 'Number : ';
                    $scope.lblComplaintContractNumber = 'Cost : ';
                    $scope.lblComplaintTicketDesc = 'Date : ';
                    $scope.lblContractCurrency = 'SAR';
                }
            });

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);

        });

        app.onPageReinit('indvUserContracts', function (page) {
            isPull = false;

            $('.advNoResult').hide();

            fromPage = page.fromPage.name;

            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('indvUserContracts', function (page) {
            if ($rootScope.currentOpeningPage != 'indvUserContracts') return;
            $rootScope.currentOpeningPage = 'indvUserContracts';

            fromPage = page.fromPage.name;

        });

        $scope.GoToContractDetails = function (contract) {
            helpers.GoToPage('contractInfo', { contract: contract });
        };


        $scope.GoBack = function () {
            if (fromPage == 'paymentMethod') {
                helpers.GoToPage('intro', null);
            }
            else {
                helpers.GoBack();
            }
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('intro', null);
        };

        $scope.AddNewContract = function () {
            helpers.GoToPage('terms', null);
        };

        app.init();
    });

}]);

