﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('resetPasswordController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;

    $(document).ready(function () {
        app.onPageInit('resetPassword', function (page) {
            if ($rootScope.currentOpeningPage != 'resetPassword') return;
            $rootScope.currentOpeningPage = 'resetPassword';

        });

        app.onPageBeforeAnimation('resetPassword', function (page) {
            if ($rootScope.currentOpeningPage != 'resetPassword') return;
            $rootScope.currentOpeningPage = 'resetPassword';

            var code = CookieService.getCookie('confirmationCode');

            if (serviceURL.indexOf("http://test.EPDemo.sa") > -1)
            {
                $scope.showCode = true;
            }
            else
            {
                $scope.showCode = false;
            }

           

            $scope.code = code;

            $scope.resetForm();

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageReinit('resetPassword', function (page) {
            $scope.resetForm();
        });

        app.onPageAfterAnimation('resetPassword', function (page) {
            if ($rootScope.currentOpeningPage != 'resetPassword') return;
            $rootScope.currentOpeningPage = 'resetPassword';

            $scope.resetForm();
        });

        $scope.resetForm = function () {
            $scope.resetPasswordReset = false;
            $scope.resetPassForm.code = null;
            $scope.resetPassForm.newPassword = null;
            $scope.resetPassForm.confrmNewPassword = null;
            if (typeof $scope.ResetPasswordForm != 'undefined' && $scope.ResetPasswordForm != null) {
                $scope.ResetPasswordForm.$setPristine(true);
                $scope.ResetPasswordForm.$setUntouched();
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        }

        $scope.form = {};
        $scope.resetPassForm = {};

        $scope.submitForm = function (isValid) {
            var mobile = CookieService.getCookie('confirmationMobile');

            $scope.resetPasswordReset = true;
            if (isValid) {

                var params = {
                    'code': $scope.resetPassForm.code,
                    'password': $scope.resetPassForm.newPassword,
                    'confirmPassword': $scope.resetPassForm.confrmNewPassword,
                    'phoneNumber': mobile
                };

                app.showIndicator();
                appServices.CallService('resetPassword', "POST", "api/account/ResetPassword", params, function (res) {
                    app.hideIndicator();
                    if (res != null) {
                        language.openFrameworkModal('نجاح', 'تم تغيير كلمة المرور القديمة بنجاح .', 'alert', function () { });
                        helpers.GoToPage('login', null);
                    }
                });

            }
        };

        $scope.GoBack = function () {
            helpers.GoBack();
        }

        app.init();
    });

}]);

