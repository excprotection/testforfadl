﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('changePasswordController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;

    $(document).ready(function () {
        app.onPageInit('changePass', function (page) {
            if ($rootScope.currentOpeningPage != 'changePass') return;
            $rootScope.currentOpeningPage = 'changePass';

        });

        app.onPageReinit('changePass', function (page) {
            $scope.resetForm();
        });

        app.onPageBeforeAnimation('changePass', function (page) {
            if ($rootScope.currentOpeningPage != 'changePass') return;
            $rootScope.currentOpeningPage = 'changePass';

            $scope.resetForm();
        });

        app.onPageAfterAnimation('changePass', function (page) {
            if ($rootScope.currentOpeningPage != 'changePass') return;
            $rootScope.currentOpeningPage = 'changePass';

            
        });

        $scope.resetForm = function () {
            $scope.changePasswordReset = false;
            $scope.changePassForm.oldPassword = null;
            $scope.changePassForm.newPassword = null;
            $scope.changePassForm.confrmNewPassword = null;
            if (typeof $scope.$parent.ChangePasswordForm != 'undefined' && $scope.$parent.ChangePasswordForm != null) {
                $scope.$parent.ChangePasswordForm.$setPristine(true);
                $scope.$parent.ChangePasswordForm.$setUntouched();
            }
        }

        $scope.form = {};
        $scope.changePassForm = {};

        $scope.submitForm = function (isValid) {
            var userLoggedIn=JSON.parse(CookieService.getCookie('userLoggedIn'));
            
            $scope.changePasswordReset = true;
            if (isValid) {
                var params = {
                    'userId': userLoggedIn.id,
                    'oldPassword': $scope.changePassForm.oldPassword,
                    'newPassword': $scope.changePassForm.newPassword,
                    'confirmPassword': $scope.changePassForm.confrmNewPassword
                }

                app.showIndicator();
                appServices.CallService('changePass', "POST", "api/account/ChangePassword", params, function (res) {
                    app.hideIndicator();
                    if (res != null || res == true) {
                        language.openFrameworkModal('نجاح', 'تم تعديل كلمة السر بنجاح .', 'alert', function () {});
                        $scope.resetForm();
                        helpers.GoToPage('intro', null);
                    }
                });

            }
        };

        $scope.GoBack = function () {
            helpers.GoBack();
        }

        $scope.passRegex = /^[A-Za-z0-9]*$/;

        app.init();
    });

}]);

