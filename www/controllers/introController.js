﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('introController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';

    var IntroSwiper;

    function InitiateSwiper(id) {
        IntroSwiper = new Swiper(id, {
            loop: false,
            autoplayDisableOnInteraction: false,
            speed: 2000,
            slidesPerView: 1,
            centeredSlides: true,
            autoplay: 2000,
            pagination: '.swiper-pagination',
        });

        setTimeout(function () {
            IntroSwiper.update();
        }, fw7.DelayBeforeScopeApply);
    }

    function StartSwiper() {
        if (IntroSwiper) IntroSwiper.destroy(true, true);
        InitiateSwiper('#IntroSwiper');
    }


    function GetOffers() {

        appServices.CallService('intro', "GET", "api/General/GetSliderImages?ImageType=2", '', function (res) {
            if (res != null) {
                $scope.Offers = res;

                angular.forEach($scope.Offers, function (offer, index) {
                    offer.imageName = typeof offer.imageName != 'undefined' && offer.imageName != null ? hostUrl + "SliderImages/" + offer.imageName : 'img/noImage.png';
                });

                $scope.IsOffersLoaded = true;

                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);

                setTimeout(function () {
                    StartSwiper();
                }, 50);
            }
            else {
                $scope.IsOffersLoaded = false;
                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            }
        });
    }

    $scope.isLoaded = true;

    if (!CookieService.getCookie('Visitor') && !CookieService.getCookie('userLoggedIn')) {
        console.log('first time to load cookie');
        CookieService.setCookie('Visitor', true);
    }

    setTimeout(function () {
        $scope.$apply();
    }, fw7.DelayBeforeScopeApply);

    function DrawMenu() {
        var IsVisitor = CookieService.getCookie('Visitor') ? CookieService.getCookie('Visitor') : false;
        var loginUsingSocial = CookieService.getCookie('loginUsingSocial') ? CookieService.getCookie('loginUsingSocial') : false;
        var userLoggedIn = CookieService.getCookie('userLoggedIn') ? JSON.parse(CookieService.getCookie('userLoggedIn')) : null;
        $rootScope.IsVisitor = IsVisitor;

        var fw7 = myApp.fw7;

        if (IsVisitor == 'true') {
            $('#linkMenuToHome').css('display', 'block ');
            $('#linkMenuToMyContracts').css('display', 'none');
            $('#linkMenuToMyComplaints').css('display', 'none');
            $('#linkMenuToIndividuals').css('display', 'none');
            $('#linkMenuToMyRequests').css('display', 'none');
            $('#linkMenuToMyInvoices').css('display', 'none');
            $('#linkMenuToMedical').css('display', 'block');
            $('#linkMenuToBusiness').css('display', 'block');
            $('#linkMenuToMission').css('display', 'block');
            $('#linkMenuToMembers').css('display', 'block');
            $('#linkMenuToChangePassword').css('display', 'none');
            $('#linkMenuToContact').css('display', 'block');
            $('#linkMenuToAbout').css('display', 'block');
            $('#linkMenuToChangeLanguage').css('display', 'block');
            $('#linkMenuToLogin').css('display', 'block');
            $('#linkMenuToProfile').css('display', 'none');
            $('#linkMenuToMyPoints').css('display', 'none');
            if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                $('#lblMenuLoginText').html('تسجيل الدخول');
            }
            else {
                $('#lblMenuLoginText').html('Login');
            }
           


            $$('#imgSideMenu').attr('src', 'img/logo.png');
        }
        else {
            $('#linkMenuToHome').css('display', 'block');
            $('#linkMenuToMyContracts').css('display', 'block');
            $('#linkMenuToMyComplaints').css('display', 'block');
            $('#linkMenuToIndividuals').css('display', 'block');
            $('#linkMenuToMyRequests').css('display', 'block');
            $('#linkMenuToMyInvoices').css('display', 'block');
            $('#linkMenuToMedical').css('display', 'block');
            $('#linkMenuToBusiness').css('display', 'block');
            $('#linkMenuToMission').css('display', 'block');
            $('#linkMenuToMembers').css('display', 'block');
            $('#linkMenuToContact').css('display', 'block');
            $('#linkMenuToAbout').css('display', 'block');
            $('#linkMenuToChangeLanguage').css('display', 'block');
            $('#linkMenuToLogin').css('display', 'block');
            $('#linkMenuToMyPoints').css('display', 'block');
            if (typeof userLoggedIn != 'undefined' && userLoggedIn != null) {
                $('#linkMenuToProfile').css('display', 'block');
                if (loginUsingSocial == 'true') {
                    $('#linkMenuToChangePassword').css('display', 'none');
                }
                else {
                    $('#linkMenuToChangePassword').css('display', 'block');
                }
                

                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    $('#lblMenuLoginText').html('تسجيل خروج');
                }
                else {
                    $('#lblMenuLoginText').html('Logout');
                }
            }
            else {
                $('#linkMenuToProfile').css('display', 'none');
                $('#linkMenuToChangePassword').css('display', 'none');
                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    $('#lblMenuLoginText').html('تسجيل الدخول');
                }
                else {
                    $('#lblMenuLoginText').html('Login');
                }
            }

            if (typeof userLoggedIn != 'undefined' && userLoggedIn != null) {
                if (typeof userLoggedIn.image != 'undefined' && userLoggedIn.image != null && userLoggedIn.image != '' && userLoggedIn.image != ' ') {
                    if (userLoggedIn.image.indexOf('http://') > -1 || userLoggedIn.image.indexOf('https://') > -1 || userLoggedIn.image.indexOf('http://placehold.it') > -1 ||
                        userLoggedIn.image.indexOf('https://placehold.it') > -1) {
                        $$('#imgSideMenu').attr('src', userLoggedIn.image);
                    }
                    else {
                        if (userLoggedIn.image.indexOf('placehold.it') > -1) {
                            $$('#imgSideMenu').attr('src', 'http:' + userLoggedIn.image);
                        }
                        else {
                            $$('#imgSideMenu').attr('src', hostUrl + userLoggedIn.image);
                        }
                    }
                }
                else {
                    $$('#imgSideMenu').attr('src', 'img/logo.png');
                }
            }
            else {
                $$('#imgSideMenu').attr('src', 'img/logo.png');
            }
        }

        setTimeout(function () {
            //$scope.$apply();
            // $rootScope.$apply();
        }, fw7.DelayBeforeScopeApply);
    }

    $(document).ready(function () {

        DrawMenu();

        $scope.IsOffersLoaded = false;

        document.addEventListener("deviceready", onDeviceReady, false);
        function onDeviceReady() {
            setTimeout(function () {
                GetOffers();
            }, 100);
        }

        app.onPageInit('intro', function (page) {
            if ($rootScope.currentOpeningPage != 'intro') return;
            $rootScope.currentOpeningPage = 'intro';

            $scope.isLoaded = true;

            DrawMenu();

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageBeforeAnimation('intro', function (page) {
            if ($rootScope.currentOpeningPage != 'intro') return;
            $rootScope.currentOpeningPage = 'intro';

            $scope.isLoaded = true;

            var IsVisitor = CookieService.getCookie('Visitor') ? CookieService.getCookie('Visitor') : false;

            if (IsVisitor != 'true' && IsVisitor != true) {
                var deviceId = CookieService.getCookie('deviceId');

                if (typeof deviceId != 'undefined' && deviceId != null) {
                    appServices.CallService('home', "POST", "api/notification/AddDevice/" + deviceId, '', function (result) {
                        app.hideIndicator();
                        if (result != null) {
                        }

                    });
                }
            }

            DrawMenu();

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);

        });

        app.onPageReinit('intro', function (page) {
            DrawMenu();
        });

        app.onPageAfterAnimation('intro', function (page) {
            if ($rootScope.currentOpeningPage != 'intro') return;
            $rootScope.currentOpeningPage = 'intro';

        });

        $rootScope.RemoveEditPagesFromHistory = function () {
            for (var i = 0; i < fw7.views[0].history.length; i++) {
                if (fw7.views[0].history[i] === '#connect') fw7.views[0].history.splice(i, 1);
            }
        };

        $rootScope.isVisitor = function () {
            var IsVisitor = CookieService.getCookie('Visitor') ? CookieService.getCookie('Visitor') : false;
            if (IsVisitor == 'true' || IsVisitor == true) {
                return true;
            }
            else {
                return false;
            }
        };

        $rootScope.CheckAppInReview = function (callBack) {
            app.showIndicator();
            appServices.CallService('home', "GET", "api/General/InReview", '', function (res) {
                app.hideIndicator();
                if (res != null && res.inReview !=null) {
                    callBack(res.inReview);
                }
                else {
                    callBack(false);
                }
            });
        };

        $scope.GoToServiceDetails = function (serviceName) {
            $rootScope.CheckAppInReview(function (InReview) {
                if (InReview == true) {
                    var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
                    if (userLoggedIn) {
                        helpers.GoToPage('individualSector', { requiredProfessionId: '5bc7f260-292f-e311-b3fd-00155d010303', InReview: InReview });
                    }
                    else {
                        helpers.GoToPage('login', null);
                    }
                }
                else {
                    helpers.GoToPage('serviceDetails', { serviceName: serviceName });
                }
            });          
        };

        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToIndividual = function () {
            helpers.GoToPage('home', { type: 'Individual' });
        };

        $scope.GoToAgreement = function () {
            helpers.GoToPage('terms', null);
        };

        $scope.GoToContact = function () {
            helpers.GoToPage('branches', null);
        };

        $scope.GoToMedical = function () {
            //helpers.GoToPage('home', { type: 'Medical' });
            helpers.GoToPage('medical', null);
        };

        $scope.GoToBusiness = function () {
            //helpers.GoToPage('home', { type: 'Business' });
            helpers.GoToPage('business', null);
        };

        app.init();
    });

}]);

