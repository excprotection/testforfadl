﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('membersController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';

    $(document).ready(function () {
        app.onPageInit('members', function (page) {
            if ($rootScope.currentOpeningPage != 'members') return;
            $rootScope.currentOpeningPage = 'members';

        });

        app.onPageBeforeAnimation('members', function (page) {
            if ($rootScope.currentOpeningPage != 'members') return;
            $rootScope.currentOpeningPage = 'members';

            $scope.isLoaded = true;

        });

        app.onPageReinit('members', function (page) {
            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('members', function (page) {
            if ($rootScope.currentOpeningPage != 'members') return;
            $rootScope.currentOpeningPage = 'members';

        });


        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('intro', null);
        };

        app.init();
    });

}]);

