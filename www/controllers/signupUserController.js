﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('signupUserController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var lang = localStorage.getItem('Dalal_lang');

    function LoadCities(callBack) {
        var cities = [];

        app.showIndicator();

        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            $('#linkSignupCity .item-after').html('المدينة');
        }
        else {
            $('#linkSignupCity .item-after').html('City');
        }
        $scope.userForm.city = null;

        appServices.CallService('signupUser', "GET", "api/v1/cities", '', function (res) {
            app.hideIndicator();
            if (res != null && res.length > 0) {
                $scope.cities = res;
                myApp.fw7.Cities = res;
            }

            callBack(true);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    function OpenActivationCodePopup(userId, code, mobile, password) {
        var codeText = 'الكود فقط للتجربة :';
        var codePlaceHolder = 'كود التفعيل';
        var resendCodetext = 'إعادة إرسال الكود';
        var activateText = 'تفعيل';
        var cancelText = 'إلغاء';

        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            codeText = 'الكود فقط للتجربة :';
            codePlaceHolder = 'كود التفعيل';
            resendCodetext = 'إعادة إرسال الكود';
            activateText = 'تفعيل';
            cancelText = 'إلغاء';
        }
        else {
            codeText = 'Code For Trial Use Only';
            codePlaceHolder = 'Activation Code';
            resendCodetext = 'Resend Code';
            activateText = 'Activate';
            cancelText = 'Cancel';
        }

        var afterText = '';

        if (serviceURL.indexOf("http://test.EPDemo.sa") > -1) {
            afterText = '<div class="list-block">' +
            '<div class="m-auto">' +
            '<ul>' +
            '<li style="text-align:center;">' +
             '<label class="lblCode">' + codeText + code + ' </label>' +
            '</li>' +
            '<li>' +
            '<div class="item-content">' +
            '<div class="item-inner">' +
            '<div class="item-input">' +
            '<input id="txtCode" type="number" name="phone" placeholder="' + codePlaceHolder + '">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</li>' +
            '</ul>' +
            '</div>' +
            '</div>';
        }
        else {
            afterText = '<div class="list-block">' +
         '<div class="m-auto">' +
         '<ul>' +
         '<li>' +
         '<div class="item-content">' +
         '<div class="item-inner">' +
         '<div class="item-input">' +
         '<input id="txtCode" type="number" name="phone" placeholder="' + codePlaceHolder + '">' +
         '</div>' +
         '</div>' +
         '</div>' +
         '</li>' +
         '</ul>' +
         '</div>' +
         '</div>';
        };


        var activationSignup = app.modal({
            title: codePlaceHolder,
            text: '',
            afterText: afterText,
            buttons: [
                {
                    text: activateText,
                    onClick: function () {
                        var params = {
                            'code': $('#txtCode').val(),
                            'userId': userId,
                            'phoneNumber': mobile,
                            'password': password
                        };

                        app.showIndicator();
                        appServices.CallService('signupUser', "POST", 'api/account/VerifyCode', params, function (res) {
                            app.hideIndicator();

                            if (res != null) {
                                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                                    language.openFrameworkModal('نجاح', 'تم التأكد من الجوال بنجاح .', 'alert', function () { });
                                }
                                else {
                                    language.openFrameworkModal('نجاح', 'Mobile Is Confirmed Successfully', 'alert', function () { });
                                }

                                app.closeModal(activationSignup);

                                var result = JSON.parse(res.code);

                                CookieService.setCookie('appToken', result.access_token);
                                CookieService.setCookie('USName', result.userName);
                                CookieService.setCookie('Visitor', false);
                                CookieService.setCookie('loginUsingSocial', false);
                                CookieService.setCookie('UserEntersCode', 'true');
                                CookieService.setCookie('userLoggedIn', JSON.stringify(res.user));
                                helpers.GoToPage('intro', null);

                            }
                            else {
                                OpenActivationCodePopup(userId, code, mobile,password);
                            }
                        });
                    }
                },
                {
                    text: resendCodetext,
                    onClick: function () {
                        var params = {
                            'phoneNumber': mobile,
                            'userId': userId,
                        };

                        app.showIndicator();
                        appServices.CallService('signupUser', "POST", 'api/account/ReGenrateCode?IsSMSEnabled=true', params, function (res) {
                            app.hideIndicator();
                            if (res != null) {
                                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                                    language.openFrameworkModal('نجاح', 'تم إعادة إرسال الكود للجوال بنجاح .', 'alert', function () { });
                                }
                                else {
                                    language.openFrameworkModal('نجاح', 'Code Is Resent To Your Mobile Successfully', 'alert', function () { });
                                }
                                OpenActivationCodePopup(userId, res.code, mobile, password);
                            }
                            else {
                                OpenActivationCodePopup(userId, res.code, mobile, password);
                            }
                        });
                    }
                },
                {
                    text: cancelText,
                    onClick: function () {
                        app.closeModal(activationSignup);
                    }
                }
            ]
        });
    }

    $(document).ready(function () {
        app.onPageInit('signupUser', function (page) {
            if ($rootScope.currentOpeningPage != 'signupUser') return;
            $rootScope.currentOpeningPage = 'signupUser';
        });

        app.onPageReinit('signupUser', function (page) {
            $scope.resetForm();
            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageBeforeAnimation('signupUser', function (page) {
            if ($rootScope.currentOpeningPage != 'signupUser') return;
            $rootScope.currentOpeningPage = 'signupUser';
            $scope.resetForm();

            $rootScope.RemoveEditPagesFromHistory();

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageAfterAnimation('signupUser', function (page) {
            if ($rootScope.currentOpeningPage != 'signupUser') return;
            $rootScope.currentOpeningPage = 'signupUser';

        });

        

        $scope.resetForm = function () {
            $scope.signUpUserReset = false;
            $scope.submittedUserSignup = false;
            $scope.isCityValid = true;
            $scope.userForm.firstname = null;
            $scope.userForm.middlename = null;
            $scope.userForm.lastname = null;
            $scope.userForm.email = null;
            $scope.userForm.mobile = null;
            $scope.userForm.newPassword = null;
            $scope.userForm.confrmNewPassword = null;

            $scope.user = {};
            if (typeof $scope.SignupUserForm != 'undefined' && $scope.SignupUserForm != null) {
                $scope.SignupUserForm.$setPristine(true);
                $scope.SignupUserForm.$setUntouched();
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        }

        $scope.submitForm = function (signUpForm) {
            $scope.signUpUserReset = true;
            $scope.submittedUserSignup = true;

            var mobile = $scope.userForm.mobile;

            mobile = helpers.parseArabic(mobile);

            if (signUpForm.$valid) {
                $('#userSignUp').prop('disabled', 'disabled');
                var user = {
                    "name": $scope.userForm.firstname + ' ' + $scope.userForm.middlename + ' ' + $scope.userForm.lastname,
                    "userName": mobile,
                    "firstName": $scope.userForm.firstname,
                    "middleName": $scope.userForm.middlename,
                    "lastName": $scope.userForm.lastname,
                    "email": $scope.userForm.email,
                    "password": $scope.userForm.newPassword,
                    "confirmPassword": $scope.userForm.confrmNewPassword
                }

                app.showIndicator();
                appServices.CallService('signupUser', "POST", "api/account/register", user, function (res2) {
                    $('#userSignUp').prop('disabled', '');
                    app.hideIndicator();

                    if (res2 != null) {
                        CookieService.setCookie('UserId', res2.userId);
                        CookieService.setCookie('UserEntersCode', false);
                        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                            language.openFrameworkModal('نجاح', 'تم تسجيل بياناتك بنجاح .', 'alert', function () { });
                        }
                        else {
                            language.openFrameworkModal('نجاح', 'You Are Registered Successfully', 'alert', function () { });
                        }
                        OpenActivationCodePopup(res2.userId, res2.code, $scope.userForm.mobile, $scope.userForm.newPassword);
                    }
                });
            }
        };

        $scope.form = {};
        $scope.userForm = {};

        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToLogin = function () {
            helpers.GoToPage('login', null);
        };

        $scope.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        $scope.nameRegex = /^[A-Za-z0-9]*$/;
        $scope.mobileRegex = /^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/;

        app.init();

    });
}]);

