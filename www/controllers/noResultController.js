﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('noResultController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';

    $(document).ready(function () {
        app.onPageInit('noResult', function (page) {
            if ($rootScope.currentOpeningPage != 'noResult') return;
            $rootScope.currentOpeningPage = 'noResult';

        });

        app.onPageBeforeAnimation('noResult', function (page) {
            if ($rootScope.currentOpeningPage != 'noResult') return;
            $rootScope.currentOpeningPage = 'noResult';

            $scope.resetForm();
        });

        app.onPageReinit('noResult', function (page) {
            if ($rootScope.currentOpeningPage != 'noResult') return;
            $rootScope.currentOpeningPage = 'noResult';

        });

        app.onPageAfterAnimation('noResult', function (page) {
            if ($rootScope.currentOpeningPage != 'noResult') return;
            $rootScope.currentOpeningPage = 'noResult';

        });


        $scope.GoBack = function () {
            helpers.GoBack();
        };

        app.init();
    });

}]);

