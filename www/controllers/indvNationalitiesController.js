﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('indvNationalitiesController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
	'use strict';

	var fw7 = myApp.fw7;
	var app = myApp.fw7.app;
	var lang = localStorage.getItem('Dalal_lang');

	function LoadNationalities() {
		app.showIndicator();
		appServices.CallService('indvNationalities', "GET", 'api/IndivContract/GetAvailableNumbers_M?id=CIT', '', function (res) {
		    app.hideIndicator();
		    if (res != null) {

		        angular.forEach(res, function (nationality, index) {
		            angular.forEach(nationality.professionCounts, function (profession, index) {
		                profession.LastOne = parseInt(index + 1) == nationality.professionCounts.length ? true : false;
		            });

		            var nationalityImgSrc='';
		            if(nationality.nationality == "إندونيسيا" || nationality.nationality=="Indonesia"){
		                nationalityImgSrc = 'url(img/countries/indonisia.png) no-repeat center';
		            }
		            else if (nationality.nationality == "الفلبين" || nationality.nationality == "Philippines") {
		                nationalityImgSrc = 'url(img/countries/Philippines.png) no-repeat center';
		            }
		            else{
		                nationalityImgSrc = 'url(img/countries/bangladesh.png) no-repeat center';
		            }

		            nationality.imgSrc = nationalityImgSrc;
		        });

		        $scope.Nationalities = res;
		    }
		});
	}

	$(document).ready(function () {
		app.onPageInit('indvNationalities', function (page) {
			if ($rootScope.currentOpeningPage != 'indvNationalities') return;
			$rootScope.currentOpeningPage = 'indvNationalities';

		});

		app.onPageReinit('indvNationalities', function (page) {
			
		});

		app.onPageBeforeAnimation('indvNationalities', function (page) {
			if ($rootScope.currentOpeningPage != 'indvNationalities') return;
			$rootScope.currentOpeningPage = 'indvNationalities';

			LoadNationalities();

			setTimeout(function () {
				$scope.$apply();
			}, fw7.DelayBeforeScopeApply);
		});

		app.onPageAfterAnimation('indvNationalities', function (page) {
			if ($rootScope.currentOpeningPage != 'indvNationalities') return;
			$rootScope.currentOpeningPage = 'indvNationalities';

		});


		$scope.GoBack = function () {
			helpers.GoBack();
		};

		$scope.GoToLogin = function () {
			helpers.GoToPage('login', null);
		};

		$scope.ShowNationalityDetails = function (nationality) {
			helpers.GoToPage("indvJobs",{ nationality: nationality });
		}
		app.init();

	});
}]);

