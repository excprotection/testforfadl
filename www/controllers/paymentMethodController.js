﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('paymentMethodController', ['$document', '$scope', '$rootScope', '$http', '$interval', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, $interval, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';
    var lang = localStorage.getItem('Dalal_lang');
    var ft;
    var tempUserImage;

    function PayOnline(callBack) {
        var lang = localStorage.getItem('Dalal_lang');
        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
        var contract = $scope.contract;
        var contractId = '';

        if (typeof contract == 'undefined' || contract == null) {
            contract = JSON.parse(CookieService.getCookie('StoredContract'));
            contractId = typeof contract.ContractId == 'undefined' || contract.ContractId == null ? contract.contractId : contract.ContractId;
        }
        else {
            contractId = contract.contractId;
        }

        lang = lang == 'AR' || lang == null || typeof lang == 'undefined' ? "ar" : "en";

        if (serviceURL.indexOf("http://test.EPDemo.sa") > -1)
        {
            var url = 'https://test.EPDemo.sa/' + lang + '/PaymentForMobile/DalalShopperResult/' + contractId + '?userId=' + userLoggedIn.crmUserId + '&contractId=' + contractId;
        }
        else
        {
            var url = 'https://EPDemo.sa/' + lang + '/PaymentForMobile/DalalShopperResult/' + contractId + '?userId=' + userLoggedIn.crmUserId + '&contractId=' + contractId;
        }

        var ref = cordova.InAppBrowser.open(url, '_blank', 'location=no,toolbar=yes,zoom=no');

        ref.addEventListener('loadstart', function (e) {
            var url = e.url;

            if (url.indexOf('Failure') > -1 || url.indexOf('failure') > -1) {
                ref.close();
                if (lang == 'ar' || typeof lang == 'undefined' || lang == null) {
                    language.openFrameworkModal('خطأ', 'حدث خطأ في الدفع , برجاء المحاولة مرة أخري .', 'alert', function () { });
                }
                else {
                    language.openFrameworkModal('Error', 'Error In Payment , Please Try Again.', 'alert', function () { });
                }
                
            }

            var paymentId = null;
            var token = null;
            var PayerID = null;

            if (url.indexOf('PaymentForMobile/Success') > -1 || url.indexOf('paymentformobile/success') > -1 || url.indexOf('hyperpay.docs.oppwa.com') > -1) {
                ref.close();
                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    language.openFrameworkModal('نجاح', 'تمت عملية الدفع بنجاح , شكرا لك .', 'alert', function () { });
                }
                else {
                    language.openFrameworkModal('Success', 'Payment Process Successeded , Thank You.', 'alert', function () { });
                }

                setTimeout(function () {
                    CookieService.removeCookie('StoredContract');
                    helpers.GoToPage('userContracts', null);
                }, 1000);

            }
        });

        ref.addEventListener('exit', function (e) {
            //if (pageName == 'addProduct') {
            //    //GetBackInHistory();
            //}
            //else {
            //    //ref.close();
            //}
        });
    }

    $(document).ready(function () {
        app.onPageInit('paymentMethod', function (page) {
            if ($rootScope.currentOpeningPage != 'paymentMethod') return;
            $rootScope.currentOpeningPage = 'paymentMethod';

        });

        app.onPageBeforeAnimation('paymentMethod', function (page) {
            if ($rootScope.currentOpeningPage != 'paymentMethod') return;
            $rootScope.currentOpeningPage = 'paymentMethod';

            $scope.isLoaded = true;

            var contract = page.query.contract;
            $scope.contract = contract;

            $scope.userImage = 'img/noImage.png';

            $scope.userImageElement = {
                id: 1,
                loading: false,
                src: $scope.userImage
            };

            tempUserImage = $scope.userImageElement.src;

            var totalMessage = '';

            lang = localStorage.getItem('Dalal_lang');

            if (lang == 'AR' || lang == null || typeof lang == 'undefined') {
                totalMessage += '<b><label>يمكن استخدام بطاقة الصراف الآلي "مدى" في عمليات التسوق الالكتروني:</label></b>';
                totalMessage += '<br /><br />';
                totalMessage += '<label>-	بنك  الراجحي:  ادخل على مباشر للافراد .. الحسابات .. إدارة بطاقة الصراف الالي .. تفعيل الشراء عبر الانترنت</label>';
                totalMessage += '<br />';
                totalMessage += '<label>-	بنك البلاد : تم تفعيلها</label>';
                totalMessage += '<br />';
                totalMessage += '<label>-	بنك الانماء: تم تفعيلها</label>';
            }
            else {
                totalMessage += '<b><label>The "Mada" ATM card can be used in e-shopping operations:</label></b>';
                totalMessage += '<br /><br />';
                totalMessage += '<label>- Al Rajhi Bank: Enter "Mubasher Service" for individuals .. Accounts .. Manage the ATM card .. Activate the purchase through the Internet</label>';
                totalMessage += '<br />';
                totalMessage += '<label>- Bank Albilad: activated</label>';
                totalMessage += '<br />';
                totalMessage += '<label>- Bank Alinmaa : activated</label>';
            }

            $scope.payMessage = totalMessage;

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);

        });

        app.onPageReinit('paymentMethod', function (page) {
            $rootScope.RemoveEditPagesFromHistory();

            
        });

        app.onPageAfterAnimation('paymentMethod', function (page) {
            if ($rootScope.currentOpeningPage != 'paymentMethod') return;
            $rootScope.currentOpeningPage = 'paymentMethod';

        });

        $scope.GoToBankTransfer = function () {
            helpers.GoToPage('bankTransfer' , { contract: $scope.contract });
        };

        $scope.PayOnline = function () {
            PayOnline(function (result) { });
        };

        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('intro', null);
        };

       

        app.init();
    });

}]);

