﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('indvSearchController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
	'use strict';

	var fw7 = myApp.fw7;
	var app = myApp.fw7.app;
	var lang = localStorage.getItem('Dalal_lang');
	
	var minAge = 18;
	var maxAge = 60;
	var minStart = 25;
	var maxStart = 50;
	var experiences = [
		{key:"لا فرق",value:""},
		{key:"لم يسبق له العمل",value:"1"},
		{key:"سبق له العمل",value:"2"}]
	function InitiateRangeSliders(rangeSliderId,min,max) {
	 var slider = document.getElementById(rangeSliderId);

	    noUiSlider.create(slider, {
	        range: {
	            'min': min,
	            'max': max
	        },
	        step: 1,
	        start: [minStart, maxStart],
	        connect: true,
	        direction: 'rtl',
	        orientation: 'horizontal',
	        behaviour: 'tap-drag',
	        tooltips: true,
	    	//tooltips: [true, wNumb({ decimals: 0 })],
	        format: wNumb({//formate with wNumb Liberary.
	        	decimals: 0,
	        	thousand: '.'
	        })
	    	/// Manual formate:
	        //format: {
	        //	to: function (value) {
	        //		return parseInt(value);
	        //	},
	        //	from: function (value) {
	        //		return parseInt(value);
	        //	}
	        //}
	    });

	    if (rangeSliderId == 'slider-connect-age') {
	        slider.noUiSlider.on('update', function (values, handle) {
	        	//$scope.age = values[1] + ',' + values[0];
	        	$scope.employee.age = values[1] + ',' + values[0];
	        });
	    }
	}

	function updateSliderRange(rangeSliderId, min, max) {
		var slider = document.getElementById(rangeSliderId);
		slider.noUiSlider.updateOptions({
			range: {
				'min': min,
				'max': max
			}
		});
	}
	
	function ResetSliderRange() {
		var slider = document.getElementById('slider-connect-age');
		slider.noUiSlider.reset();
	}
	$(document).ready(function () {
		app.onPageInit('indvSearch', function (page) {
			if ($rootScope.currentOpeningPage != 'indvSearch') return;
			$rootScope.currentOpeningPage = 'indvSearch';
		});

		app.onPageReinit('indvSearch', function (page) {
			$rootScope.RemoveEditPagesFromHistory();
			$scope.ResetForm();
		});

		app.onPageBeforeAnimation('indvSearch', function (page) {
			if ($rootScope.currentOpeningPage != 'indvSearch') return;
			$rootScope.currentOpeningPage = 'indvSearch';
			setTimeout(function () {
				$scope.$apply();
			}, fw7.DelayBeforeScopeApply);
			$scope.ResetForm();
		});

		app.onPageAfterAnimation('indvSearch', function (page) {
			if ($rootScope.currentOpeningPage != 'indvSearch') return;
			$rootScope.currentOpeningPage = 'indvSearch';

			$scope.nationalityId = page.query.nationalityId;
			$scope.professionId = page.query.professionId;
			$scope.packageId = page.query.packageId;
			//console.log($scope.employee);
		});
		$scope.employee = {};
		$scope.minAge = minAge;
		$scope.maxAge = maxAge;


		$scope.ResetForm = function () {
			console.log('call reset');
			$scope.employee = new Object();
			$scope.employee.religion = "";
			$scope.employee.experience = "";
			$$('#indvLinkExperience .item-after').html('لا فرق');
			$$('#indvLinkReligion .item-after').html('غير محدد');

			
			$scope.employee.sex = "";
			$scope.employee.canCook = "";
			$scope.employee.treatChildren = "";
			$scope.employee.treatOld = "";
			$scope.employee.speakEnglish = "";
			$$('#indvLinkSex .item-after').html('غير محدد');
			$$('#indvLinkCanCook .item-after').html('غير محدد');
			$$('#indvLinkTreatChildren .item-after').html('غير محدد');
			$$('#indvLinkTreatOld .item-after').html('غير محدد');
			$$('#indvLinkSpeakEnglish .item-after').html('غير محدد');

			//updateSliderRange('slider-connect-age', minStart, maxStart);
			ResetSliderRange();
		}

		$scope.GoToSearchResults = function (employee) {
			helpers.GoToPage('indvSearchResults', { employee: $scope.employee, nationalityId: $scope.nationalityId, professionId: $scope.professionId, packageId: $scope.packageId });
		}

		$scope.GoBack = function () {
			helpers.GoBack();
		};

		$scope.GoToLogin = function () {
			helpers.GoToPage('login', null);
		};

		app.init();
		InitiateRangeSliders('slider-connect-age', minAge, maxAge);

	});
}]);

