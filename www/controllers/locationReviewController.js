﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('locationReviewController', ['$document', '$scope', '$q', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $q, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';
    var map;
    var latitude;
    var longitiude;
    var lang = localStorage.getItem('Dalal_lang');
    $scope.txtAlignValue = "left";

    function geocodeLatLng(geocoder, map, latitude, longitude) {
        lang = localStorage.getItem('Dalal_lang');
        var latlng = { lat: parseFloat(latitude), lng: parseFloat(longitude) };
        geocoder.geocode({ 'location': latlng }, function (results, status) {
            if (status === 'OK') {
                if (results[0]) {
                    var input = document.getElementById('pac-input');
                    input.value = results[0].formatted_address;
                }
            } else {

                lang = localStorage.getItem('Dalal_lang');
                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    language.openFrameworkModal('خطأ', 'لا يمكن تحديد الموقع من خلال الإحداثيات المطلوبة', 'alert', function () { });
                }
                else {
                    language.openFrameworkModal('error', 'Address Cannot Be Generated For The Given Latitude And Longtitude', 'alert', function () { });
                }
            }
        });
    }

    function InitRequestMap() {

        var mapDiv = document.getElementById('tripMapRev');
        var input = document.getElementById('pac-input');

        if (input != null) {
            input.remove();
        }

        input = document.createElement('input');
        var divMapRevContainer = document.getElementById('divMapRevContainer');

        input.setAttribute('id', 'pac-input');

        lang = localStorage.getItem('Dalal_lang');
        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            input.setAttribute('placeholder', 'أدخل العنوان');
        }
        else {
            input.setAttribute('placeholder', 'Type Address Here');
        }
        input.setAttribute('type', 'text');
        input.className += 'form-control';
        divMapRevContainer.insertBefore(input, mapDiv);

        var projectMarkers = [];

        var onSuccess = function (position) {
            projectMarkers = [{ "title": 'myMap', "lat": position.coords.latitude, "lng": position.coords.longitude, "description": "currentLocation" }];
            CookieService.setCookie('StoredLatitude', position.coords.latitude);
            CookieService.setCookie('StoredLongtitude', position.coords.longitude);
            helpers.initAutocomplete('tripMapRev', projectMarkers, true);
            var geocoder = new google.maps.Geocoder;
            geocodeLatLng(geocoder, map, position.coords.latitude, position.coords.longitude);
            app.hideIndicator();
        }

        function onError(error) {

            lang = localStorage.getItem('Dalal_lang');
            if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                language.openFrameworkModal('خطأ', 'تحديد الموقع مغلق لديك علي الهاتف , لا يمكن تحديد الموقع الحالي', 'alert', function () { });
            }
            else {
                language.openFrameworkModal('error', 'Please Turn On Location From Settings On Your Device , Cannot Select Your Current Location', 'alert', function () { });
            }
            projectMarkers = [{ "title": 'myMap', "lat": '24.713552', "lng": '46.675296', "description": "currentLocation" }];

            CookieService.setCookie('StoredLatitude', '24.713552');
            CookieService.setCookie('StoredLongtitude', '46.675296');

            helpers.initAutocomplete('tripMapRev', projectMarkers, true);
            app.hideIndicator();
        }

        var devicePlatform = device.platform;

        var lang = localStorage.getItem('Dalal_lang');
        var confirmTitle = 'Confirm';
        var confirmText = 'Do you want to detect your current location ?';

        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            confirmText = 'هل تريد تحديد الموقع الحالي ؟';
            confirmTitle = 'تأكيد';
        }

        if (devicePlatform == 'Android' || devicePlatform == 'android') {
            app.confirm(confirmText, confirmTitle, function () {
                     cordova.plugins.diagnostic.isGpsLocationEnabled(function (enabled) {

                    if (enabled) {
                        app.hideIndicator();
                        navigator.geolocation.getCurrentPosition(onSuccess, onError, { enableHighAccuracy: true, timeout: 2 * 1000, maximumAge: 0 });
                    }
                    else {

                        lang = localStorage.getItem('Dalal_lang');
                        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                            language.openFrameworkModal('خطأ', 'تحديد الموقع مغلق لديك علي الهاتف , لا يمكن تحديد الموقع الحالي', 'alert', function () { });
                        }
                        else {
                            language.openFrameworkModal('error', 'Please Turn On Location From Settings On Your Device , Cannot Select Your Current Location', 'alert', function () { });
                        }
                        projectMarkers = [{ "title": 'myMap', "lat": '24.713552', "lng": '46.675296', "description": "currentLocation" }];

                        CookieService.setCookie('StoredLatitude', '24.713552');
                        CookieService.setCookie('StoredLongtitude', '46.675296');

                        helpers.initAutocomplete('tripMapRev', projectMarkers, true);
                        app.hideIndicator();
                    }

                }, function (error) {

                    lang = localStorage.getItem('Dalal_lang');

                    if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                        language.openFrameworkModal('خطأ', 'تحديد الموقع مغلق لديك علي الهاتف , لا يمكن تحديد الموقع الحالي', 'alert', function () { });
                    }
                    else {
                        language.openFrameworkModal('error', 'Please Turn On Location From Settings On Your Device , Cannot Select Your Current Location', 'alert', function () { });
                    }

                    projectMarkers = [{ "title": 'myMap', "lat": '24.713552', "lng": '46.675296', "description": "currentLocation" }];
                    CookieService.setCookie('StoredLatitude', '24.713552');
                    CookieService.setCookie('StoredLongtitude', '46.675296');

                    helpers.initAutocomplete('tripMapRev', projectMarkers, true);
                    app.hideIndicator();
                });
            }, function () {
                var projectMarkers = [{ "title": 'myMap', "lat": '24.713552', "lng": '46.675296', "description": "currentLocation" }];
                CookieService.setCookie('StoredLatitude', '24.713552');
                CookieService.setCookie('StoredLongtitude', '46.675296');

                helpers.initAutocomplete('tripMapRev', projectMarkers, true);
                var geocoder = new google.maps.Geocoder;
                geocodeLatLng(geocoder, map, 24.713552, 46.675296);
                app.hideIndicator();
            });
        }
        else {
            app.confirm(confirmText, confirmTitle, function () {
                navigator.geolocation.getCurrentPosition(onSuccess, onError, { enableHighAccuracy: true, timeout: 2 * 1000, maximumAge: 0 });
            }, function () {
                var projectMarkers = [{ "title": 'myMap', "lat": '24.713552', "lng": '46.675296', "description": "currentLocation" }];
                CookieService.setCookie('StoredLatitude', '24.713552');
                CookieService.setCookie('StoredLongtitude', '46.675296');

                helpers.initAutocomplete('tripMapRev', projectMarkers, true);
                var geocoder = new google.maps.Geocoder;
                geocodeLatLng(geocoder, map, 24.713552, 46.675296);
                app.hideIndicator();
            });

        }

    }

    $(document).ready(function () {
        app.onPageInit('locationReview', function (page) {
            if ($rootScope.currentOpeningPage != 'locationReview') return;
            $rootScope.currentOpeningPage = 'locationReview';

        });

        app.onPageReinit('locationReview', function (page) {

        });

        app.onPageBeforeAnimation('locationReview', function (page) {
            if ($rootScope.currentOpeningPage != 'locationReview') return;
            $rootScope.currentOpeningPage = 'locationReview';

            $scope.isLoaded = true;
            var lang = localStorage.getItem('Dalal_lang');
            if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                $scope.txtAlignValue = "right";
            } else {
                $scope.txtAlignValue = "left";
            }
            var user = page.query.user;
            $scope.userDetails = user;

            $scope.isArabic = lang == 'AR' || typeof lang == 'undefined' || lang == null ? true : false;
            $scope.resetForm();  
            InitRequestMap();

        });

        app.onPageReinit('locationReview', function (page) {
            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('locationReview', function (page) {
            if ($rootScope.currentOpeningPage != 'locationReview') return;
            $rootScope.currentOpeningPage = 'locationReview';

        });

        $scope.form = {};
        $scope.LocationRevForm = {};
        $scope.formFields = {};

        $scope.resetForm = function () {
            $scope.locationReset = false;
            $scope.submittedLocation = false;

            $scope.isHouseNumberValid = true;
            $scope.isareaNameValid = true;
            $scope.isStreetNameValid = true;
            $scope.isAppratmentNumberValid = true;
            $scope.isMapValid = true;
            $scope.isHouseNumberFormatCorrect = false;
            $scope.isAppartmentNumberFormatCorrect = false;

            $scope.LocationRevForm.houseNumber = null;
            $scope.LocationRevForm.floorNumber = null;
            $scope.LocationRevForm.appartmentNumber = null;
            $scope.LocationRevForm.houseAddress = null;
            $scope.LocationRevForm.streetName = null;
            $scope.LocationRevForm.areaName = null;
            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        }

        $scope.submitForm = function (isValid) {
            $scope.locationReset = true;
            $scope.submittedLocation = true;
            var houseNumber = $('#txtHouseNumberRev').val();
            var appartmentNumber = $('#txtAppartmentNumberRev').val();
            var areaName = $('#txtAreaNameRev').val();
            var streetName = $('#txtStreetNameRev').val();
            var mapLatitude = CookieService.getCookie('StoredLatitude');
            var mapLongtitude = CookieService.getCookie('StoredLongtitude');
            var houseAddress = $('#txtHouseRevAddress').val();

            $scope.isMapValid = $scope.locationReset && $scope.submittedLocation && mapLatitude != null && mapLatitude != '' && mapLatitude != ' ' ? true : false;
            $scope.isareaNameValid = $scope.locationReset && $scope.submittedLocation && areaName != null && areaName != '' && areaName != ' ' ? true : false;
            $scope.isStreetNameValid = $scope.locationReset && $scope.submittedLocation && streetName != null && streetName != '' && streetName != ' ' ? true : false;

            $scope.isHouseNumberValid = $scope.locationReset && $scope.submittedLocation && houseNumber != null && houseNumber != '' && houseNumber != ' ' ? true : false;
            $scope.isAppratmentNumberValid = $scope.locationReset && $scope.submittedLocation && appartmentNumber != null && appartmentNumber != '' && appartmentNumber != ' ' ? true : false;

            if ($scope.isHouseNumberValid) {
                var patt = new RegExp(/^(?!\s*$)(\d|\w|[\u0621-\u064A\u0660-\u0669 ])+$/);
                $scope.isHouseNumberFormatCorrect = patt.test(houseNumber);
            }
            if ($scope.isAppratmentNumberValid) {
                var patt = new RegExp(/^(?!\s*$)(\d|\w|[\u0621-\u064A\u0660-\u0669 ])+$/);
                $scope.isAppartmentNumberFormatCorrect = patt.test(appartmentNumber);
            }

            if ( $scope.isMapValid && $scope.isHouseNumberValid && $scope.isHouseNumberFormatCorrect &&
                ($scope.isAppratmentNumberValid && $scope.isAppartmentNumberFormatCorrect && $scope.isareaNameValid && $scope.isStreetNameValid)) {
               
                var orderDetails = {
                    "fullName": $scope.userDetails.fullName,
                    "email": $scope.userDetails.email,
                   "orderType": $scope.userDetails.orderType,
                    "mobile": $scope.userDetails.mobile,
                    "details": $scope.userDetails.details,
                    "houseNumber" : houseNumber,
                    "appartmentNumber" : appartmentNumber,
                    "areaName" : areaName,
                    "streetName": streetName,
                    "houseAddress": houseAddress,
                }     
                helpers.GoToPage('showOrderDetailsReview', { orderDetails: orderDetails });
            }
        };

        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('introReview', null);
        };

        app.init();
    });

}]);

