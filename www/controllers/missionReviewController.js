﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('missionReviewController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';

    $(document).ready(function () {
        app.onPageInit('missionReview', function (page) {
            if ($rootScope.currentOpeningPage != 'missionReview') return;
            $rootScope.currentOpeningPage = 'missionReview';

        });

        app.onPageBeforeAnimation('missionReview', function (page) {
            if ($rootScope.currentOpeningPage != 'missionReview') return;
            $rootScope.currentOpeningPage = 'missionReview';

            $scope.isLoaded = true;

        });

        app.onPageReinit('missionReview', function (page) {
            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('missionReview', function (page) {
            if ($rootScope.currentOpeningPage != 'missionReview') return;
            $rootScope.currentOpeningPage = 'missionReview';

        });


        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('introReview', null);
        };

        app.init();
    });

}]);

