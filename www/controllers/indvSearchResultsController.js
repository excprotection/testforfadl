﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('indvSearchResultsController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
	'use strict';

	var fw7 = myApp.fw7;
	var app = myApp.fw7.app;
	var lang = localStorage.getItem('Dalal_lang');
	var isPull = true;
	var pageSize = fw7.PageSize;

	var loadingResults = false;


	function Initialvalues(employee) {
		employee.nationalityId = $scope.nationalityId;
		employee.professionId = $scope.professionId;
		employee.PageIndex = 1;
		employee.PageSize = pageSize;
		employee.AgeOperand = "range";
		$scope.employee = employee;
	}

	function SearchByPage(employee) {

		app.showIndicator();
		appServices.CallService('indvSearchResults', "Post", 'api/IndivContract/GetAvailableEmployeesFiltered_M', employee, function (res) {
			app.hideIndicator();
			if (res != null) {
				if (res.count > 0) {
					$.each(res.employees, function (key, value) {
						if (value.skills) {
							value.skills = value.skills.replace(' ,', ',').replace(', ', ',');
						}
						var skills = value.skills.split(',');
						for (var i = 0; i < skills.length; i++) {
							skills[i] = skills[i].trim();
						}
						res.employees[key].skills = skills;
					});
					if (res.employees.length > 0) {

						if ($scope.employee.PageIndex > 1 || $scope.Employees) {
							$scope.Employees = $scope.Employees.concat(res.employees);
							$scope.totalCount = res.count;
						}
						else {
							$scope.Employees = res.employees;
							$scope.totalCount = res.count;
						}
						$scope.noEmployees = false;
						$scope.SearchResultsLoader = false;
					}
					$scope.professionName = res.employees[0].jobTitle;
				}
				else {
					$scope.noEmployees = true;
					$scope.EmployeesInfiniteLoader = false;
				}
				// update page index to get next page.
				$scope.employee.PageIndex++
				loadingResults = false;//
			} else {

				$scope.SearchResultsLoader = false;
				$scope.noEmployees = true;
			}
			$scope.isLoaded = true;
		});

	}


	function AddBootstrapIfNotExist() {
		var bootstrapCssElement = document.getElementById('boostrapFile');
		var fontsCssElement = document.getElementById('fontsFile');
		var head = document.getElementsByTagName('head')[0];

		if (!bootstrapCssElement) {
			var boostrapLink = document.createElement('link');
			boostrapLink.id = 'boostrapFile';
			boostrapLink.rel = 'stylesheet';
			boostrapLink.type = 'text/css';
			boostrapLink.href = 'css/bootstrap.min.css';
			head.appendChild(boostrapLink);

			var fontsLink = document.createElement('link');
			fontsLink.id = 'fontsFile';
			fontsLink.rel = 'stylesheet';
			fontsLink.type = 'text/css';
			fontsLink.href = 'fonts/fonts.css';
			head.appendChild(fontsLink);
		}

	}
	$(document).ready(function () {
		app.onPageInit('indvSearchResults', function (page) {
			if ($rootScope.currentOpeningPage != 'indvSearchResults') return;
			$rootScope.currentOpeningPage = 'indvSearchResults';

			$$('#divInfiniteIndvSearchResults').on('ptr:refresh', function (e) {
				isPull = true;

				$scope.employee = {};
				Initialvalues($scope.employee)
				SearchByPage($scope.employee);

				setTimeout(function () {
					$scope.$apply();
					app.pullToRefreshDone();
				}, fw7.DelayBeforeScopeApply);

			});
			$$('#divInfiniteIndvSearchResults').on('infinite', function () {
				if (loadingResults) return;
				loadingResults = true;

				setTimeout(function () {
				
				if ($scope.Employees.length >= parseInt($scope.totalCount)) {

					app.detachInfiniteScroll($$('#divInfiniteIndvSearchResults'));

					$$('.infinite-scroll-preloader').remove();
					return;
				     }
                 }, 1000);

				SearchByPage($scope.employee);

			});

		});

		app.onPageReinit('indvSearchResults', function (page) {
			AddBootstrapIfNotExist();
		});

		app.onPageBeforeAnimation('indvSearchResults', function (page) {
			if ($rootScope.currentOpeningPage != 'indvSearchResults') return;
			$rootScope.currentOpeningPage = 'indvSearchResults';

			$scope.nationalityId = page.query.nationalityId;
			$scope.professionId = page.query.professionId;
			$scope.packageId = page.query.packageId;

			$scope.professionName = page.query.professionName;
			$scope.FilteredEmployee = page.query.employee;
			$scope.totalCount = 0;
			$scope.isLoaded = false;

			var setting = JSON.parse(CookieService.getCookie('indvEmployeeSettings'));
			$scope.ShowEmployeePhoto = setting.isShowEmployeePhoto;

			$scope.Employees = null;
			Initialvalues($scope.FilteredEmployee);
			SearchByPage($scope.FilteredEmployee);

			setTimeout(function () {
				$scope.$apply();
			}, fw7.DelayBeforeScopeApply);
		});

		app.onPageAfterAnimation('indvSearchResults', function (page) {
			if ($rootScope.currentOpeningPage != 'indvSearchResults') return;
			$rootScope.currentOpeningPage = 'indvSearchResults';

		});

		$scope.SortBy = 'birthdate';
		isPull = false;
		$scope.noEmployees = true;
		$scope.SearchResultsLoader = true;

		$scope.SearchResults = function () {
			helpers.GoToPage('indvSearch', { nationalityId: $scope.nationalityId, professionId: $scope.professionId, packageId: $scope.packageId });
		}

		$scope.GetReligion = function (religionId) {
			var religionTxt = 'مسلم';
			switch (religionId) {
				case 1:
					religionTxt = 'مسلم'
					break;
				case 2:
					religionTxt = 'مسيحي'
					break;
				case 5:
					religionTxt = 'ديانةاخري'
					break;
				default:
					religionTxt = 'مسلم'

			}
			return religionTxt;
		}


		$scope.GoBack = function () {
			helpers.GoBack();
		};

		$scope.GoToLogin = function () {
			helpers.GoToPage('login', null);
		};
		$scope.CreateIndvContract = function (employeeId) {

			var indvContract = {
				packageId: $scope.packageId,
				nationalityId: $scope.nationalityId,
				professionId: $scope.professionId,
				EmployeeId: employeeId,
				Latitude: null,
				Longitude: null,
				HouseType: null,
				HouseNo: null,
				FloorNo: null,
				PartmentNo: null
			}

			CookieService.setCookie('indvContract', JSON.stringify(indvContract));
			helpers.GoToPage('indvPrevLocations', null);
		}

		app.init();

	});
}]);

