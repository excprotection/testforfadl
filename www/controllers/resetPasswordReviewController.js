﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('resetPasswordReviewController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    $scope.txtAlignValue = "left";


    $(document).ready(function () {
        app.onPageInit('resetPasswordReview', function (page) {
            if ($rootScope.currentOpeningPage != 'resetPasswordReview') return;
            $rootScope.currentOpeningPage = 'resetPasswordReview';

        });

        app.onPageBeforeAnimation('resetPasswordReview', function (page) {
            if ($rootScope.currentOpeningPage != 'resetPasswordReview') return;
            $rootScope.currentOpeningPage = 'resetPasswordReview';

            var code = CookieService.getCookie('confirmationCode');

            if (serviceURL.indexOf("http://test.EPDemo.sa") > -1)
            {
                $scope.showCode = true;
            }
            else
            {
                $scope.showCode = false;
            }

        
            var lang = localStorage.getItem('Dalal_lang');
            if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                $scope.txtAlignValue = "right";
            } else {
                $scope.txtAlignValue = "left";
            }
            $scope.code = code;

            $scope.resetForm();

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageReinit('resetPasswordReview', function (page) {
            $scope.resetForm();
        });

        app.onPageAfterAnimation('resetPasswordReview', function (page) {
            if ($rootScope.currentOpeningPage != 'resetPasswordReview') return;
            $rootScope.currentOpeningPage = 'resetPasswordReview';

            $scope.resetForm();
        });

        $scope.resetForm = function () {
            $scope.resetPasswordReset = false;
            $scope.resetPassForm.code = null;
            $scope.resetPassForm.newPassword = null;
            $scope.resetPassForm.confrmNewPassword = null;
            if (typeof $scope.ResetPasswordForm != 'undefined' && $scope.ResetPasswordForm != null) {
                $scope.ResetPasswordForm.$setPristine(true);
                $scope.ResetPasswordForm.$setUntouched();
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        }

        $scope.form = {};
        $scope.resetPassForm = {};

        $scope.submitForm = function (isValid) {
            var mobile = CookieService.getCookie('confirmationMobile');

            $scope.resetPasswordReset = true;
            if (isValid) {

                var params = {
                    'code': $scope.resetPassForm.code,
                    'password': $scope.resetPassForm.newPassword,
                    'confirmPassword': $scope.resetPassForm.confrmNewPassword,
                    'phoneNumber': mobile
                };

                app.showIndicator();
                appServices.CallService('resetPasswordReview', "POST", "api/account/ResetPassword", params, function (res) {
                    app.hideIndicator();
                    if (res != null) {
                        language.openFrameworkModal('نجاح', 'تم تغيير كلمة المرور القديمة بنجاح .', 'alert', function () { });
                        helpers.GoToPage('loginReview', null);
                    }
                });

            }
        };

        $scope.GoBack = function () {
            helpers.GoBack();
        }

        app.init();
    });

}]);

