﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('forgetPasswordController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;

    $(document).ready(function () {
        app.onPageInit('forgetPass', function (page) {
            if ($rootScope.currentOpeningPage != 'forgetPass') return;
            $rootScope.currentOpeningPage = 'forgetPass';

        });

        app.onPageBeforeAnimation('forgetPass', function (page) {
            if ($rootScope.currentOpeningPage != 'forgetPass') return;
            $rootScope.currentOpeningPage = 'forgetPass';

            $scope.resetForm();
        });

        app.onPageReinit('forgetPass', function (page) {
            $scope.resetForm();
        });

        app.onPageAfterAnimation('forgetPass', function (page) {
            if ($rootScope.currentOpeningPage != 'forgetPass') return;
            $rootScope.currentOpeningPage = 'forgetPass';

            $scope.resetForm();
        });

        $scope.resetForm = function () {
            $scope.forgetPassReset = false;
            $scope.forgetPassForm.mobile = null;
            if (typeof $scope.ForgetPasswordForm != 'undefined' && $scope.ForgetPasswordForm != null) {
                $scope.ForgetPasswordForm.$setPristine(true);
                $scope.ForgetPasswordForm.$setUntouched();
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        }

        $scope.form = {};
        $scope.forgetPassForm = {};

        $scope.submitForm = function (isValid) {
            $scope.forgetPassReset = true;
            if (isValid) {
                var mobile = $scope.forgetPassForm.mobile;

                var params = {
                    'phoneNumber': mobile
                };

                app.showIndicator();
                appServices.CallService('forgetPass', "POST", "api/account/ForgotPassword?IsSMSEnabled=true", params, function (res) {
                    app.hideIndicator();
                    if (res != null) {
                        CookieService.setCookie('confirmationMobile', $scope.forgetPassForm.mobile);
                        CookieService.setCookie('confirmationCode', res.code);
                        language.openFrameworkModal('نجاح', 'تم إرسال الكود لجوالك بنجاح .', 'alert', function () { });
                        helpers.GoToPage('resetPassword', null);
                    }
                });
            }
        };

        $scope.GoBack = function () {
            helpers.GoBack();
        }

        $scope.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        $scope.mobileRegex = /^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/;

        app.init();
    });

}]);

