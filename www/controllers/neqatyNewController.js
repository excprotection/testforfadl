﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('neqatyNewController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var loadingHome;
    var allQuestions = [];
    var isPull = true;
   

    $(document).ready(function () {

        app.onPageInit('neqaty', function (page) {
            if ($rootScope.currentOpeningPage != 'neqaty') return;

        });

        app.onPageReinit('neqaty', function (page) {
            if ($rootScope.currentOpeningPage != 'neqaty') return;
            $rootScope.currentOpeningPage = 'neqaty';
        });

        app.onPageBeforeAnimation('neqaty', function (page) {
            if ($rootScope.currentOpeningPage != 'neqaty') return;
            $rootScope.currentOpeningPage = 'neqaty';

            $scope.isLoaded = true;

            $scope.LoadPoints();

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageAfterAnimation('neqaty', function (page) {
            if ($rootScope.currentOpeningPage != 'neqaty') return;
            $rootScope.currentOpeningPage = 'neqaty';

        });

        $scope.LoadPoints = function () {
            var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

            $scope.AllPoints = ' --- ';
            $scope.pointAmount = ' --- ';

            app.showIndicator();

            appServices.CallService('userPoints', 'GET', 'api/Loyality/LoyalityBalance?id=' + userLoggedIn.crmUserId, '', function (result) {
                app.hideIndicator();
                if (result) {
                    $scope.AllPoints = result.loyaltyPoints;
                    $scope.pointAmount = result.loyaltyBalance;
                }
            });
        };

        $scope.PointsDetails= function (status) {
            helpers.GoToPage('userPoints', { allPoints: $scope.AllPoints, PointsAmount: $scope.pointAmount, status: status });
        }

        $scope.ConsumedPointsDetails = function (status) {
            helpers.GoToPage('userConsumedPoints', { allPoints: $scope.AllPoints, PointsAmount: $scope.pointAmount });
        }

        app.init();
    });

}]);

