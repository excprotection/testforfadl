﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('indvJobsController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var lang = localStorage.getItem('Dalal_lang');

    function SetTotalCount(profession) {
    	CookieService.setCookie('professionTotalCount', profession.count);
    }
    $(document).ready(function () {
        app.onPageInit('indvJobs', function (page) {
            if ($rootScope.currentOpeningPage != 'indvJobs') return;
            $rootScope.currentOpeningPage = 'indvJobs';

        });

        app.onPageReinit('indvJobs', function (page) {

        });

        app.onPageBeforeAnimation('indvJobs', function (page) {
            if ($rootScope.currentOpeningPage != 'indvJobs') return;
            $rootScope.currentOpeningPage = 'indvJobs';

            var nationality = page.query.nationality;

            angular.forEach(nationality.professionCounts, function (profession, index) {
                if (profession.professionId == '49c7f260-292f-e311-b3fd-00155d010303') {
                    profession.professionImage = 'img/job-icons/driver.png';
                }
                else if (profession.professionId == 'a73a000c-426b-e511-80e9-00505691216d') {
                    profession.professionImage = 'img/job-icons/maid.png';
                }
                else if (profession.professionId == '39C7F260-292F-E311-B3FD-00155D010303') {
                    profession.professionImage = 'img/job-icons/home-maid.png';
                }
                else if (profession.professionId == '37C7F260-292F-E311-B3FD-00155D010303') {
                    profession.professionImage = 'img/job-icons/servant.png';
                }
                else if (profession.professionId == '47C7F260-292F-E311-B3FD-00155D010303') {
                    profession.professionImage = 'img/job-icons/tailor.png';
                }
                else if (profession.professionId == '4BC7F260-292F-E311-B3FD-00155D010303') {
                    profession.professionImage = 'img/job-icons/chef-man.png';
                }
                else if (profession.professionId == '5BC7F260-292F-E311-B3FD-00155D010303') {
                    profession.professionImage = 'img/job-icons/nurse.png';
                }
            });

            $scope.nationality = page.query.nationality;

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageAfterAnimation('indvJobs', function (page) {
            if ($rootScope.currentOpeningPage != 'indvJobs') return;
            $rootScope.currentOpeningPage = 'indvJobs';

        });


        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToLogin = function () {
            helpers.GoToPage('login', null);
        };

        $scope.ShowPackages = function (nationalityId, profession) {
        	SetTotalCount(profession);
            helpers.GoToPage('indvPackages', { nationalityId: nationalityId, professionId: profession.professionId, professionName: profession.profession });
        }
      
        app.init();

    });
}]);

