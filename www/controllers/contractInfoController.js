﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('contractInfoController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';

    function GetContractDetails(contractId, callBack) {

        app.showIndicator();

        appServices.CallService('contractInfo', "GET", "api/HourlyContract/GetDetails/" + contractId, '', function (res) {
            app.hideIndicator();
            if (res != null) {
                callBack(res.contract);
            }
            else {
                callBack(null);
            }
        });
    }

    function OpenActivationCodePopup(contractId, code, mobile) {
        var codeText = 'الكود فقط للتجربة :';
        var codePlaceHolder = 'كود الإلغاء';
        var resendCodetext = 'إعادة إرسال';
        var activateText = 'تأكيد';
        var cancelText = 'تراجع';

        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            codeText = 'الكود فقط للتجربة :';
            codePlaceHolder = 'كود الإلغاء';
            resendCodetext = 'إعادة إرسال';
            activateText = 'تأكيد';
            cancelText = 'تراجع';
        }
        else {
            codeText = 'Code For Trial Use Only';
            codePlaceHolder = 'Cancellation Code';
            resendCodetext = 'Resend';
            activateText = 'Confirm';
            cancelText = 'Back';
        }

        var afterText = '';

        if (serviceURL.indexOf("http://test.EPDemo.sa") > -1) {
            afterText = '<div class="list-block">' +
            '<div class="m-auto">' +
            '<ul>' +
            '<li style="text-align:center;">' +
             '<label class="lblCode">' + codeText + code + ' </label>' +
            '</li>' +
            '<li>' +
            '<div class="item-content">' +
            '<div class="item-inner">' +
            '<div class="item-input">' +
            '<input id="txtCode" type="text" name="phone" placeholder="' + codePlaceHolder + '">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</li>' +
            '</ul>' +
            '</div>' +
            '</div>';
        }
        else {
            afterText = '<div class="list-block">' +
         '<div class="m-auto">' +
         '<ul>' +
         '<li>' +
         '<div class="item-content">' +
         '<div class="item-inner">' +
         '<div class="item-input">' +
         '<input id="txtCode" type="text" name="phone" placeholder="' + codePlaceHolder + '">' +
         '</div>' +
         '</div>' +
         '</div>' +
         '</li>' +
         '</ul>' +
         '</div>' +
         '</div>';
        };


        var activationCancelContract = app.modal({
            title: codePlaceHolder,
            text: '',
            afterText: afterText,
            buttons: [
                {
                    text: activateText,
                    onClick: function () {
                        var codeEntered = $('#txtCode').val();
                        if (codeEntered == code) {
                            app.showIndicator();

                            appServices.CallService('contractInfo', "GET", "api/HourlyContract/CancelContractForMobile/" + contractId, '', function (res) {
                                app.hideIndicator();
                                if (res != null) {
                                    if (lang == 'AR' || lang == null || typeof lang == 'undefined') {
                                        language.openFrameworkModal('نجاح', 'تم إلغاء العقد بنجاح', 'alert', function () { });
                                    }
                                    else {
                                        language.openFrameworkModal('Success', 'Contract has been cancelled successfully.', 'alert', function () { });
                                    }

                                    app.closeModal(activationCancelContract);
                                    helpers.GoToPage('userContracts', null);
                                }
                                else {
                                    if (lang == 'AR' || lang == null || typeof lang == 'undefined') {
                                        language.openFrameworkModal('خطأ', 'حدث خطأ في إلغاء العقد .. برجاء المحاولة مرة أخري', 'alert', function () { });
                                    }
                                    else {
                                        language.openFrameworkModal('Error', 'Error in cancelling contract .. Please try again.', 'alert', function () { });
                                    }

                                    OpenActivationCodePopup(contractId, code, mobile);
                                }
                            });
                        }
                        else {
                            OpenActivationCodePopup(contractId, code, mobile);
                        }
                    }
                },
                {
                    text: resendCodetext,
                    onClick: function () {
                        var params = {
                            'contractNum': code,
                            'mobile': mobile
                        };

                        app.showIndicator();
                        appServices.CallService('contractInfo', "GET", 'api/HourlyContract/SendCancelCode?IsSMSEnabled=true&&contractNum=' + code + '&&mobile=' + mobile, params, function (res) {
                            app.hideIndicator();
                            if (res != null) {
                                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                                    language.openFrameworkModal('نجاح', 'تم إعادة إرسال الكود للجوال بنجاح .', 'alert', function () { });
                                }
                                else {
                                    language.openFrameworkModal('نجاح', 'Code Is Resent To Your Mobile Successfully', 'alert', function () { });
                                }
                                OpenActivationCodePopup(contractId, res.code, mobile);
                            }
                            else {
                                OpenActivationCodePopup(contractId, res.code, mobile);
                            }
                        });
                    }
                },
                {
                    text: cancelText,
                    onClick: function () {
                        app.closeModal(activationCancelContract);
                    }
                }
            ]
        });
    }

    function CancelContract(contract, callBack) {
        var lang = CookieService.getCookie('lang');

        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

        OpenActivationCodePopup(contract.contractId, contract.contractNum, userLoggedIn.phoneNumber);
    }

    $(document).ready(function () {
        app.onPageInit('contractInfo', function (page) {
            if ($rootScope.currentOpeningPage != 'contractInfo') return;
            $rootScope.currentOpeningPage = 'contractInfo';

        });

        app.onPageBeforeAnimation('contractInfo', function (page) {
            if ($rootScope.currentOpeningPage != 'contractInfo') return;
            $rootScope.currentOpeningPage = 'contractInfo';

            $scope.isLoaded = true;

            var contract = page.query.contract;

            var contractId = typeof contract.contractId != 'undefined' && contract.contractId != null ? contract.contractId : contract.ContractId;

            GetContractDetails(contractId, function (result) {
                if (result != null) {
                    var contract = result;
                    contract.availableDaysNumber = contract.selectedDays != null && contract.selectedDays != '' ? contract.selectedDays.split(',').length : 0;
                    contract.startDay = contract.startDay.split('T')[0];
                    contract.selectedPackage = contract.hourlyPricingCost;

                    $scope.IsNotPaid = contract.statusCode == 100000006 ? true : false;
                    $scope.IsCancelled = contract.contractType == "True" || contract.contractType == true || contract.statusCode == 100000007 || !$scope.IsNotPaid ? true : false;

                    $scope.contract = contract;

                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                }
                else {
                    language.openFrameworkModal('نجاح', 'لا يوجد تفاصيل لهذا العقد.', 'alert', function () { });
                    helpers.GoBack();
                }
            });
        });

        app.onPageReinit('contractInfo', function (page) {
            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('contractInfo', function (page) {
            if ($rootScope.currentOpeningPage != 'contractInfo') return;
            $rootScope.currentOpeningPage = 'contractInfo';

        });


        $scope.GoBack = function () {

            helpers.GoBack();
        };

        $scope.PayNow = function () {
            helpers.GoToPage('paymentMethod', { contract: $scope.contract });
        };

        $scope.Cancel = function () {
            CancelContract($scope.contract);
        };

        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('intro', null);
        };

        app.init();
    });

}]);

