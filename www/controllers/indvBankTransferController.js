﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('indvBankTransferController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
	'use strict';

	var fw7 = myApp.fw7;
	var app = myApp.fw7.app;
	var fromPage = '';
	var lang = localStorage.getItem('Dalal_lang');
	var ft;
	var tempUserImage;

	$(document).ready(function () {
		app.onPageInit('indvBankTransfer', function (page) {
			if ($rootScope.currentOpeningPage != 'indvBankTransfer') return;
			$rootScope.currentOpeningPage = 'indvBankTransfer';

		});

		app.onPageBeforeAnimation('indvBankTransfer', function (page) {
			if ($rootScope.currentOpeningPage != 'indvBankTransfer') return;
			$rootScope.currentOpeningPage = 'indvBankTransfer';

			$scope.isLoaded = true;

			var contract = page.query.contract;
			if (!contract) {
				if (CookieService.getCookie('indvContract')) {
					contract = JSON.parse(CookieService.getCookie('indvContract'));
				}
			}
			var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

			$scope.userName = userLoggedIn.name;

			$scope.contract = contract;
			console.log('contract');
			console.log($scope.contract);

			$scope.advertisementMainImage = { id: 1, loading: false, src: 'img/noImage.png', baseString: '', progress: 0, newImage: false }

			setTimeout(function () {
				$scope.$apply();
			}, fw7.DelayBeforeScopeApply);

		});

		app.onPageReinit('indvBankTransfer', function (page) {
			$rootScope.RemoveEditPagesFromHistory();
		});

		app.onPageAfterAnimation('indvBankTransfer', function (page) {
			if ($rootScope.currentOpeningPage != 'indvBankTransfer') return;
			$rootScope.currentOpeningPage = 'indvBankTransfer';

		});


		$scope.CopyNumber = function (number) {
			cordova.plugins.clipboard.copy(number, function (result) {
				lang = localStorage.getItem('Dalal_lang');

				if (lang == 'AR' || lang == null || typeof lang == 'undefined') {
					language.openFrameworkModal('نجاح', 'تم نسخ رقم الحساب بنجاح', 'alert', function () { });
				}
				else {
					language.openFrameworkModal('Success', 'Account Number Is Copied Successfully', 'alert', function () { });
				}
			}, function (error) {
				if (lang == 'AR' || lang == null || typeof lang == 'undefined') {
					language.openFrameworkModal('خطأ', 'خطأ في نسخ رقم الحساب', 'alert', function () { });
				}
				else {
					language.openFrameworkModal('Error', 'Cannot Copy Account Number', 'alert', function () { });
				}
			});
		};


		$scope.GoBack = function () {
			helpers.GoBack();
		};

		$scope.GoToIntro = function () {
			helpers.GoToPage('intro', null);
		};

		function ConvertImgToBase64Url(url, outputFormat, callback) {
			var img = new Image();
			img.crossOrigin = 'Anonymous';
			img.onload = function () {
				var canvas = document.createElement('CANVAS');
				var ctx = canvas.getContext('2d');
				canvas.height = this.height;
				canvas.width = this.width;
				ctx.drawImage(this, 0, 0, canvas.width, canvas.height);
				var dataURL = canvas.toDataURL(outputFormat);
				callback(dataURL, url);
				canvas = null;
			};

			img.src = url;
		}

		function getImage() {

			window.imagePicker.getPictures(function (results) {
				if (results && results.length > 0) {

					angular.forEach(results, function (image) {
						var selectedImage = image;

						$scope.userImageElement = {
							id: 1,
							loading: false,
							src: $scope.userImage
						};

						ConvertImgToBase64Url(selectedImage, 'jpg', function (imgBase64, selectedImage) {
							$scope.userImage = imgBase64.split(',')[1];
							$scope.uploadImage($scope.userImage, selectedImage);
						});

						setTimeout(function () {
							$scope.$apply();
						}, 10);


					});

				}
			},
            function (error) {
            	console.log('Error: ' + error);
            }, {
            	maximumImagesCount: 1,
            	outputType: 0,
            	width: 1024,
            	height: 768
            });
		}

		$scope.AddUserImage = function () {
			lang = localStorage.getItem('Dalal_lang');

			if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
				app.confirm('برجاء التأكد من تحويل المبلغ وعمل التحويل البنكي بنجاح ثم رفع صورة التحويل', 'تأكيد', function () {
					var permissions = cordova.plugins.permissions;

					if (ft)
						ft.abort();

					$scope.userImage = tempUserImage;


					var list = [
                        permissions.READ_EXTERNAL_STORAGE,
                        permissions.WRITE_EXTERNAL_STORAGE
					];

					permissions.checkPermission(list, function (status) {
						if (status.hasPermission) {
							getImage();
						}
						else {
							permissions.requestPermissions(list, function (status) {
								if (!status.hasPermission) {

								}
								else {
									getImage();
								}
							}, function () {
							});
						}
					});
				}, function () {

				});
			}
			else {
				app.confirm('Please Ensure Payment Is Transferred Successfully And Bank Transfer Is Done Then Upload Bank Transfer Image', 'Confirm', function () {
					var permissions = cordova.plugins.permissions;

					if (ft)
						ft.abort();

					$scope.userImage = tempUserImage;


					var list = [
                        permissions.READ_EXTERNAL_STORAGE,
                        permissions.WRITE_EXTERNAL_STORAGE
					];

					permissions.checkPermission(list, function (status) {
						if (status.hasPermission) {
							getImage();
						}
						else {
							permissions.requestPermissions(list, function (status) {
								if (!status.hasPermission) {

								}
								else {
									getImage();
								}
							}, function () {
							});
						}
					});
				}, function () {

				});
			}

		};

		$scope.uploadImage = function (imageSrc, imageFilePath) {

			var langPrefix = lang == 'AR' || lang == null || typeof lang == 'undefined' ? 'ar' : 'en';
			var contract = $scope.contract;
			var contractId = '';

			if (typeof contract == 'undefined' || contract == null) {
				contract = JSON.parse(CookieService.getCookie('indvContract'));
				contractId = typeof contract.ContractId == 'undefined' || contract.ContractId == null ? contract.contractId : contract.ContractId;
			}
			else {
				if (contract.contractId == "") {
					contract.contractId = contract.individualContractRequestId;
					console.log('contractId: ' + contractId);
					$log.log('ContractRequestId: ' + contract.individualContractRequestId);
				}
				//individualContractRequestId = "bf146167-addd-e811-80f4-000d3ab19e01"
				contractId = typeof contract.ContractId == 'undefined' || contract.ContractId == null ? contract.contractId : contract.ContractId;
			}

			var params = {
				'ContractId': contractId,
				'Name': imageFilePath.substr(imageFilePath.lastIndexOf('/') + 1),
				'ImageBase': imageSrc
			};

			$scope.userImageElement.loading = true;
			setTimeout(function () {
				$scope.$apply();
			}, 10);

			//oldurl = 'api/Payment/SystemicBankTransfer'             
			appServices.CallService('indvBankTransfer', "POST", 'api/Payment/UploadInvoiceBankFileRequest_M', params, function (res) {

				if (res != null) {
					if (langPrefix == 'ar') {
						language.openFrameworkModal('نجاح', 'تم رفع ملف التحويل البنكي بنجاح', 'alert', function () { });
					}
					else {
						language.openFrameworkModal('Success', 'Bank Transfer Statement was uploaded successfully!', 'alert', function () { });
					}
					$scope.userImageElement.loading = false;
					$scope.userImageElement.src = $scope.userImage;
					tempUserImage = $scope.userImage;
					setTimeout(function () {
						$scope.$apply();
					}, 10);


					setTimeout(function () {
						CookieService.removeCookie('indvContract');
						helpers.GoToPage('userIndvContractRequests', null);
					}, 2000);
				}
				else {
					if (langPrefix == 'ar') {
						language.openFrameworkModal('خطأ', 'لم يتم رفع ملف التحويل البنكي', 'alert', function () { });
					}
					else {
						language.openFrameworkModal('Error', 'Bank Transfer Statement was not uploaded!', 'alert', function () { });
					}
					$scope.userImageElement.loading = false;
					setTimeout(function () {
						$scope.$apply();
					}, 10);
				}
			});
		}

		app.init();
	});

}]);

