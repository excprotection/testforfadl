﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('indvLocationsController', ['$document', '$scope', '$q', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $q, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
	'use strict';

	var fw7 = myApp.fw7;
	var app = myApp.fw7.app;
	var fromPage = '';
	var map;
	var latitude;
	var longitiude;
	var lang = localStorage.getItem('Dalal_lang');

	function GetStoredContract() {
		var contract = JSON.parse(CookieService.getCookie('indvContract'));
		return contract;
	}

	//function LoadCities(callBack) {
	//	var cities = [];

	//	lang = localStorage.getItem('Dalal_lang');
	//	if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
	//		$('#linkindvLocationCity .item-after').html('المدينة');
	//	}
	//	else {
	//		$('#linkindvLocationCity .item-after').html('City');
	//	}

	//	appServices.CallService('indvLocations', "GET", "api/city/QuickAll", '', function (res) {
	//		if (res != null && res.length > 0) {
	//			$scope.cities = res;
	//			$$('#linkindvLocationCity select').html('');
	//			lang = localStorage.getItem('Dalal_lang');
	//			if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
	//				app.smartSelectAddOption('#linkindvLocationCity select', '<option value="" selected>المدينة</option>');
	//			}
	//			else {
	//				app.smartSelectAddOption('#linkindvLocationCity select', '<option value="" selected>City</option>');
	//			}
	//			angular.forEach($scope.cities, function (city) {
	//				app.smartSelectAddOption('#linkindvLocationCity select', '<option value="' + city.key + '">' + city.value + '</option>');
	//			});
	//		}
	//		callBack(true);
	//		setTimeout(function () {
	//			$scope.$apply();
	//		}, fw7.DelayBeforeScopeApply);
	//	});
	//}

	function LoadHouseTypes(callBack) {
		var houseTypes = [];

		app.showIndicator();

		lang = localStorage.getItem('Dalal_lang');
		if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
			$('#indvLinkLocationHouseType .item-after').html('نوع المنزل');
		}
		else {
			$('#indvLinkLocationHouseType .item-after').html('House Type');
		}

		appServices.CallService('indvLocations', "GET", "api/HourlyContract/options/HousingTypes", '', function (res) {
			if (res != null && res.length > 0) {
				$scope.houseTypes = res;
				$$('#indvLinkLocationHouseType select').html('');


				lang = localStorage.getItem('Dalal_lang');
				if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
					app.smartSelectAddOption('#indvLinkLocationHouseType select', '<option value="" selected>نوع المنزل</option>');
				}
				else {
					app.smartSelectAddOption('#indvLinkLocationHouseType select', '<option value="" selected>House Type</option>');
				}
				angular.forEach($scope.houseTypes, function (houseType) {
					app.smartSelectAddOption('#indvLinkLocationHouseType select', '<option value="' + houseType.key + '">' + houseType.value + '</option>');
				});
			}

			callBack(true);

			setTimeout(function () {
				$scope.$apply();
			}, fw7.DelayBeforeScopeApply);
		});
	}

	function LoadFloorNumbers(callBack) {
		var floorNumbers = [];


		lang = localStorage.getItem('Dalal_lang');
		if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
			$('#indvLinkLocationFloorNumber .item-after').html('رقم الطابق');
		}
		else {
			$('#indvLinkLocationFloorNumber .item-after').html('Floor Number');
		}

		appServices.CallService('indvLocations', "GET", "api/HourlyContract/options/HousingFloors", '', function (res) {
			app.hideIndicator();
			if (res != null && res.length > 0) {
				$scope.floorNumbers = res;
				$$('#indvLinkLocationFloorNumber select').html('');


				lang = localStorage.getItem('Dalal_lang');
				if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
					app.smartSelectAddOption('#indvLinkLocationFloorNumber select', '<option value="" selected>رقم الطابق</option>');
				}
				else {
					app.smartSelectAddOption('#indvLinkLocationFloorNumber select', '<option value="" selected>Floor Number</option>');
				}
				angular.forEach($scope.floorNumbers, function (floorNumber) {
					app.smartSelectAddOption('#indvLinkLocationFloorNumber select', '<option value="' + floorNumber.key + '">' + floorNumber.value + '</option>');
				});
			}

			callBack(true);

			setTimeout(function () {
				$scope.$apply();
			}, fw7.DelayBeforeScopeApply);
		});
	}

	function geocodeLatLng(geocoder, map, latitude, longitude) {
		var latlng = { lat: parseFloat(latitude), lng: parseFloat(longitude) };
		geocoder.geocode({ 'location': latlng }, function (results, status) {
			if (status === 'OK') {
				if (results[0]) {
					var input = document.getElementById('indv-pac-input');
					input.value = results[0].formatted_address;
				}
			} else {

				lang = localStorage.getItem('Dalal_lang');
				if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
					language.openFrameworkModal('خطأ', 'لا يمكن تحديد الموقع من خلال الإحداثيات المطلوبة', 'alert', function () { });
				}
				else {
					language.openFrameworkModal('خطأ', 'Address Cannot Be Generated For The Given Latitude And Longtitude', 'alert', function () { });
				}
			}
		});
	}
	function GetLocationError(error, lang) {
		var errorText = '';
		if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
			switch (error.code) {
				case error.PERMISSION_DENIED:
					errorText = "تحديد الموقع مغلق لديك علي الهاتف , لا يمكن تحديد الموقع الحالي"
					break;
				case error.POSITION_UNAVAILABLE:
					errorText = "معلومات الموقع غير متوفرة."
					break;
				case error.TIMEOUT:
					errorText = "انتهت مهلة طلب الحصول على موقع المستخدم."
					break;
				case error.UNKNOWN_ERROR:
					errorText = "حدث خطأ غير معروف."
					break;
			}
		}
		else {
			switch (error.code) {
				case error.PERMISSION_DENIED:
					errorText = "Please Turn On Location From Settings On Your Device , Cannot Select Your Current Location."
					break;
				case error.POSITION_UNAVAILABLE:
					errorText = "Location information is unavailable."
					break;
				case error.TIMEOUT:
					errorText = "The request to get user location timed out."
					break;
				case error.UNKNOWN_ERROR:
					errorText = "An unknown error occurred."
					break;
			}
		}
		return errorText;
	}


	function InitRequestMap() {
		var mapDiv = document.getElementById('indvtripMap');
		var input = document.getElementById('indv-pac-input');

		if (input != null) {
			input.remove();
		}

		input = document.createElement('input');
		var divMapContainer = document.getElementById('divindvMapContainer');

		input.setAttribute('id', 'indv-pac-input');

		lang = localStorage.getItem('Dalal_lang');
		if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
			input.setAttribute('placeholder', 'أدخل العنوان');
		}
		else {
			input.setAttribute('placeholder', 'Type Address Here');
		}
		input.setAttribute('type', 'text');
		input.className += 'form-control';
		divMapContainer.insertBefore(input, mapDiv);

		var projectMarkers = [];
		var onSuccess = function (position) {
			projectMarkers = [{ "title": 'myMap', "lat": position.coords.latitude, "lng": position.coords.longitude, "description": "currentLocation" }];
			CookieService.setCookie('StoredLatitude', position.coords.latitude);
			CookieService.setCookie('StoredLongitude', position.coords.longitude);
			//
			helpers.initAutocomplete('indvtripMap', projectMarkers, true, 'indv-pac-input');
			//helpers.initAutocomplete('indvtripMap', projectMarkers, true);
			var geocoder = new google.maps.Geocoder;
			geocodeLatLng(geocoder, map, position.coords.latitude, position.coords.longitude);
			app.hideIndicator();
		}

		function onError(error) {
			lang = localStorage.getItem('Dalal_lang');
			var errorText = GetLocationError(error, lang);
			if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
				language.openFrameworkModal('خطأ', errorText, 'alert', function () { });
			}
			else {
				language.openFrameworkModal('Error', errorText, 'alert', function () { });
			}
			projectMarkers = [{ "title": 'myMap', "lat": '24.713552', "lng": '46.675296', "description": "currentLocation" }];
			//if (CookieService.getCookie('StoredLatitude')) CookieService.removeCookie('StoredLatitude');
			//if (CookieService.getCookie('StoredLongitude')) CookieService.removeCookie('StoredLongitude');

			CookieService.setCookie('StoredLatitude', '24.713552');
			CookieService.setCookie('StoredLongitude', '46.675296');
			helpers.initAutocomplete('indvtripMap', projectMarkers, true, 'indv-pac-input');
			//helpers.initAutocomplete('indvtripMap', projectMarkers, true);
			app.hideIndicator();
		}

		var devicePlatform = device.platform;
		var confirmTitle = 'Confirm';
		var confirmText = 'Do you want to detect your current location ?';

		if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
			confirmText = 'هل تريد تحديد الموقع الحالي ؟';
			confirmTitle = 'تأكيد';
		}

		if (devicePlatform == 'Android' || devicePlatform == 'android') {
			app.confirm(confirmText, confirmTitle, function () {
				//navigator.geolocation.getCurrentPosition(onSuccess, onError, { enableHighAccuracy: true, timeout: 2 * 1000, maximumAge: 0 });

				$('#currentLoc').prop("disabled", true);
				cordova.plugins.diagnostic.isGpsLocationEnabled(
					function (enabled) {
						if (enabled) {
							app.hideIndicator();
							navigator.geolocation.getCurrentPosition(onSuccess, onError, { enableHighAccuracy: true, timeout: 2 * 1000, maximumAge: 0 });
						}
						else {
							lang = localStorage.getItem('Dalal_lang');
							if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
								language.openFrameworkModal('خطأ', ' لا يمكن تحديد الموقع الحالي', 'alert', function () { });

								//language.openFrameworkModal('خطأ', 'تحديد الموقع مغلق لديك علي الهاتف , لا يمكن تحديد الموقع الحالي', 'alert', function () { });
							}
							else {
								//language.openFrameworkModal('خطأ', 'Please Turn On Location From Settings On Your Device , Cannot Select Your Current Location', 'alert', function () { });

								language.openFrameworkModal('خطأ', 'Cannot Select Your Current Location', 'alert', function () { });
							}
							projectMarkers = [{ "title": 'myMap', "lat": '24.713552', "lng": '46.675296', "description": "currentLocation" }];
							//if (CookieService.getCookie('StoredLatitude')) CookieService.removeCookie('StoredLatitude');
							//if (CookieService.getCookie('StoredLongitude')) CookieService.removeCookie('StoredLongitude');

							CookieService.setCookie('StoredLatitude', '24.713552');
							CookieService.setCookie('StoredLongitude', '46.675296');
							helpers.initAutocomplete('indvtripMap', projectMarkers, true, 'indv-pac-input');
							//  helpers.initAutocomplete('indvtripMap', projectMarkers, true);
							app.hideIndicator();
						}
					},
					function (error) {
						console.log('diagnostic.isGpsLocationEnabled: ');
						console.log(error);
						lang = localStorage.getItem('Dalal_lang');

						if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
							language.openFrameworkModal('خطأ', 'تحديد الموقع مغلق لديك علي الهاتف , لا يمكن تحديد الموقع الحالي', 'alert', function () { });
						}
						else {
							language.openFrameworkModal('خطأ', 'Please Turn On Location From Settings On Your Device , Cannot Select Your Current Location', 'alert', function () { });
						}

						projectMarkers = [{ "title": 'myMap', "lat": '24.713552', "lng": '46.675296', "description": "currentLocation" }];
						CookieService.setCookie('StoredLatitude', '24.713552');
						CookieService.setCookie('StoredLongitude', '46.675296');

						//helpers.initAutocomplete('indvtripMap', projectMarkers, true);
						helpers.initAutocomplete('indvtripMap', projectMarkers, true, 'indv-pac-input');
						app.hideIndicator();
					});

			}, function () {
				var projectMarkers = [{ "title": 'myMap', "lat": '24.713552', "lng": '46.675296', "description": "currentLocation" }];
				CookieService.setCookie('StoredLatitude', '24.713552');
				CookieService.setCookie('StoredLongtitude', '46.675296');

				//helpers.initAutocomplete('indvtripMap', projectMarkers, true);
				helpers.initAutocomplete('indvtripMap', projectMarkers, true, 'indv-pac-input');
				var geocoder = new google.maps.Geocoder;
				geocodeLatLng(geocoder, map, 24.713552, 46.675296);
				app.hideIndicator();
			});
			$('#currentLoc').prop("disabled", false);
		}
		else {
			app.confirm(confirmText, confirmTitle, function () {
				navigator.geolocation.getCurrentPosition(onSuccess, onError, { enableHighAccuracy: true, timeout: 10 * 1000, maximumAge: Infinity });
			}, function () {
				var projectMarkers = [{ "title": 'myMap', "lat": '24.713552', "lng": '46.675296', "description": "currentLocation" }];
				CookieService.setCookie('StoredLatitude', '24.713552');
				CookieService.setCookie('StoredLongtitude', '46.675296');
				helpers.initAutocomplete('indvtripMap', projectMarkers, true, 'indv-pac-input');
				//helpers.initAutocomplete('indvtripMap', projectMarkers, true);
				var geocoder = new google.maps.Geocoder;
				geocodeLatLng(geocoder, map, 24.713552, 46.675296);
				app.hideIndicator();
			});
			//navigator.geolocation.getCurrentPosition(onSuccess, onError, { enableHighAccuracy: true, timeout: 2 * 1000, maximumAge: 0 });
		}

	}

	$(document).ready(function () {
		app.onPageInit('indvLocations', function (page) {
			if ($rootScope.currentOpeningPage != 'indvLocations') return;
			$rootScope.currentOpeningPage = 'indvLocations';
			$scope.resetForm();
			InitRequestMap();
			LoadHouseTypes(function (result) {
				$scope.isHouseVilla = true;
				LoadFloorNumbers(function (result) {
					//LoadCities(function (result) { });
				});
			});
			//$$('#linkindvLocationCity select').on('change', function () {
			//	var cityId = $(this).val();
			//	var city = $scope.cities.filter(function (obj) {
			//		return obj.key == cityId;
			//	})[0];
			//	$scope.formFields.city = city;
			//});
			$$('#indvLinkLocationHouseType select').on('change', function () {
				var houseTypeId = $(this).val();
				var houseType = $scope.houseTypes.filter(function (obj) {
					return obj.key == houseTypeId;
				})[0];

				$scope.formFields.houseType = houseType;

				$scope.isHouseVilla = houseType.key == 0 ? true : false;

				setTimeout(function () {
					$scope.$apply();
				}, fw7.DelayBeforeScopeApply);
			});

			$$('#indvLinkLocationFloorNumber select').on('change', function () {
				var floorNumberId = $(this).val();
				var floorNumber = $scope.floorNumbers.filter(function (obj) {
					return obj.key == floorNumberId;
				})[0];

				$scope.formFields.floorNumber = floorNumber;
			});
		});

		app.onPageBeforeAnimation('indvLocations', function (page) {
			if ($rootScope.currentOpeningPage != 'indvLocations') return;
			$rootScope.currentOpeningPage = 'indvLocations';

			$scope.isLoaded = true;

			var lang = localStorage.getItem('Dalal_lang');
			$scope.isArabic = lang == 'AR' || typeof lang == 'undefined' || lang == null ? true : false;

			var contract = GetStoredContract();
			if (!contract)// no contract get from new flow;
				$scope.resetForm();
			
		});

		app.onPageReinit('indvLocations', function (page) {
			$rootScope.RemoveEditPagesFromHistory();
			console.log('re init onPageReinit-indvLocations');
		});

		app.onPageAfterAnimation('indvLocations', function (page) {
			if ($rootScope.currentOpeningPage != 'indvLocations') return;
			$rootScope.currentOpeningPage = 'indvLocations';

		});

		$scope.form = {};
		$scope.locationForm = {};
		$scope.formFields = {};

		$scope.resetForm = function () {
			$scope.locationReset = false;
			$scope.submittedLocation = false;

			$scope.isHouseVilla = true;

			$scope.isCityValid = true;
			$scope.isHouseTypeValid = true;
			$scope.isFloorNumberValid = true;
			$scope.isHouseNumberValid = true;
			$scope.isAppratmentNumberValid = true;
			$scope.isMapValid = true;

			$scope.locationForm.houseType = null;
			$scope.locationForm.houseNumber = null;
			$scope.locationForm.floorNumber = null;
			$scope.locationForm.appartmentNumber = null;
			$scope.locationForm.houseAddress = null;

			if (typeof $scope.LocationForm != 'undefined' && $scope.LocationForm != null) {
				$scope.LocationForm.$setPristine(true);
				$scope.LocationForm.$setUntouched();
			}

			setTimeout(function () {
				$scope.$apply();
			}, fw7.DelayBeforeScopeApply);
		}

		$scope.submitForm = function (isValid) {
			$scope.locationReset = true;
			$scope.submittedLocation = true;

			//var cityId = $('#linkindvLocationCity select').val();
			var houseTypeId = $('#indvLinkLocationHouseType select').val();
			var floorNumberId = $('#indvLinkLocationFloorNumber select').val();
			var houseNumber = $('#txtIndvHouseNumber').val();
			var appartmentNumber = $('#txtIndvAppartmentNumber').val();
			var mapLatitude = CookieService.getCookie('StoredLatitude');
			var mapLongitude = CookieService.getCookie('StoredLongitude');

			//$scope.isCityValid = $scope.locationReset && $scope.submittedLocation && cityId != null && cityId != '' && cityId != ' ' ? true : false;
			$scope.isHouseTypeValid = $scope.locationReset && $scope.submittedLocation && houseTypeId != null && houseTypeId != '' && houseTypeId != ' ' ? true : false;
			$scope.isFloorNumberValid = $scope.locationReset && $scope.submittedLocation && floorNumberId != null && floorNumberId != '' && floorNumberId != ' ' ? true : false;
			$scope.isMapValid = $scope.locationReset && $scope.submittedLocation && mapLatitude != null && mapLatitude != '' && mapLatitude != ' ' ? true : false;
			$scope.isHouseNumberValid = $scope.locationReset && $scope.submittedLocation && houseNumber != null && houseNumber != '' && houseNumber != ' ' ? true : false;
			$scope.isAppratmentNumberValid = $scope.locationReset && $scope.submittedLocation && appartmentNumber != null && appartmentNumber != '' && appartmentNumber != ' ' ? true : false;


			if ( $scope.isHouseTypeValid && $scope.isFloorNumberValid && $scope.isMapValid && $scope.isHouseNumberValid &&
                (houseTypeId == 0 || (houseTypeId == 1 && $scope.isAppratmentNumberValid))) {
				var storedContract = GetStoredContract();
				storedContract = (storedContract == null) ? {} : storedContract;
				storedContract.HouseType = $scope.formFields.houseType;
				storedContract.HouseNumber = $scope.locationForm.houseNumber;
				storedContract.FloorNumber = $scope.formFields.floorNumber;
				storedContract.AppartmentNumber = $scope.locationForm.appartmentNumber;
				storedContract.HouseAddress = $scope.locationForm.houseAddress;
				storedContract.Latitude = mapLatitude;
				storedContract.Longitude = mapLongitude;
				//storedContract.city = $scope.formFields.city;

				CookieService.setCookie('indvContract', JSON.stringify(storedContract));
				helpers.GoToPage('indvCustomerWork', null);

			}
		};

		$scope.GoBack = function () {
			helpers.GoBack();
		};

		$scope.GoToIntro = function () {
			helpers.GoToPage('intro', null);
		};

		app.init();
	});

}]);



