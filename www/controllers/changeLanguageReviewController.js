﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('changeLanguageReviewController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';
    var ftMainImage;

    $(document).ready(function () {

        app.onPageInit('changeLanguageReview', function (page) {
            if ($rootScope.currentOpeningPage != 'changeLanguageReview') return;
            $rootScope.currentOpeningPage = 'changeLanguageReview';

        });

        app.onPageBeforeAnimation('changeLanguageReview', function (page) {
            if ($rootScope.currentOpeningPage != 'changeLanguageReview') return;
            $rootScope.currentOpeningPage = 'changeLanguageReview';

        });

        app.onPageReinit('changeLanguageReview', function (page) {
            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('changeLanguageReview', function (page) {
            if ($rootScope.currentOpeningPage != 'changeLanguageReview') return;
            $rootScope.currentOpeningPage = 'changeLanguageReview';

            var lang = localStorage.getItem('Dalal_lang');

            if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                myApp.fw7.app = new Framework7({
                    swipeBackPage: false,
                    swipePanel: false,
                    panelsCloseByOutside: true,
                    animateNavBackIcon: true,
                    material: isAndroid ? true : false,
                    materialRipple: false,
                    modalButtonOk: 'موافق',
                    modalButtonCancel: 'إلغاء'
                });

                $('body').css('direction', 'rtl');
                $('html').css('direction', 'rtl !important');
                $('html').attr('dir', 'rtl');

            }
            else {             
                myApp.fw7.app = new Framework7({
                    swipeBackPage: false,
                    swipePanel: false,
                    panelsCloseByOutside: true,
                    animateNavBackIcon: true,
                    material: isAndroid ? true : false,
                    materialRipple: false,
                    modalButtonOk: 'Ok',
                    modalButtonCancel: 'Cancel'
                });

                $('body').css('direction', 'ltr');
                $('html').css('direction', 'ltr !important');
                $('html').attr('dir', 'ltr');
            }
         
            setTimeout(function () {
                helpers.GoToPage('introReview', null);
            }, 3000);
        });

        function ConvertImgToBase64Url(url, outputFormat, callback) {
            var img = new Image();
            img.crossOrigin = 'Anonymous';
            img.onload = function () {
                var canvas = document.createElement('CANVAS');
                var ctx = canvas.getContext('2d');
                canvas.height = this.height;
                canvas.width = this.width;
                ctx.drawImage(this, 0, 0, canvas.width, canvas.height);
                var dataURL = canvas.toDataURL(outputFormat);
                callback(dataURL, url);
                canvas = null;
            };

            img.src = url;
        }

        $scope.uploadMainImage = function (image) {
            var options = new FileUploadOptions();
            options.fileKey = "image";
            options.chunkedMode = false;
            options.trustAllHosts = true;
            options.httpMethod = "POST";

            var params = {};
            params.BankFileString = image.src;
            params.BankFileName = "newImage";

            options.params = params;

            if (ftMainImage)
                ftMainImage.abort();

            ftMainImage = new FileTransfer();

            $scope.advertisementMainImage.loading = true;

            ftMainImage.onprogress = function (progressEvent) {
                if (progressEvent.lengthComputable) {
                    var percent = Math.ceil(progressEvent.loaded * 100 / progressEvent.total);
                    $scope.advertisementMainImage.progress = percent;
                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                }
            };

            var contract = JSON.parse(CookieService.getCookie('StoredContract'));

            ftMainImage.upload(image.src, encodeURI(serviceURL + "/api/Payment/SystemicBankTransfer/" + contract.ContractId), function (res) {
                var result = JSON.parse(res.response);
                $scope.advertisementMainImage.loading = false;
                $scope.advertisementMainImage.newImage = false;
                if (result.status == true || result.status == "") {
                    $scope.advertisementMainImage.id = 11111;
                    $scope.advertisementMainImage.src = result.data.image;
                    $scope.advertisementMainImageFile = result.data.image;
                    $scope.advertisementMainImageFileThumb = result.data.thumb;
                }
                else {
                    language.openFrameworkModal('خطأ', 'يوجد خطأ في خدمة رفع الصور .', 'alert', function () { });
                }
                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);

            }, function (error) {
                $scope.advertisementMainImage = { id: 0, loading: false, src: 'img/noImage.png', baseString: 'img/noImage.png', progress: 0, newImage: false }
                if (error.code != 4) {
                    language.openFrameworkModal('خطأ', 'يوجد خطأ في رفع الصورة .', 'alert', function () { });
                }
                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);

            }, options);
        }

        function getMainImage() {
            window.imagePicker.getPictures(function (results) {
                if (results && results.length > 0) {

                    angular.forEach(results, function (image) {
                        $scope.advertisementMainImage.loading = false;
                        $scope.advertisementMainImage.src = image;
                        $scope.advertisementMainImage.newImage = true;
                        $scope.advertisementMainImage.baseString = image;
                        $scope.advertisementMainImage.progress = 0;

                        ConvertImgToBase64Url(image, 'jpg', function (imgBase64, image) {
                            $scope.advertisementMainImageBase64 = imgBase64.split(',')[1];
                        });

                        $scope.uploadMainImage($scope.advertisementMainImage);
                    });

                    setTimeout(function () {
                        $scope.$apply();
                    }, 10);

                }
            },
            function (error) {
                console.log('Error: ' + error);
            }, {
                maximumImagesCount: 1,
                outputType: 0,
                width: 1280,
                height: 800
            });
        }

        $scope.AddAdvertisementMainImage = function () {
            var permissions = cordova.plugins.permissions;

            var list = [
                permissions.READ_EXTERNAL_STORAGE,
                permissions.WRITE_EXTERNAL_STORAGE
            ];

            function error() {
                console.warn('Storage permissions is not turned on');
            }

            permissions.checkPermission(list, function (status) {
                if (status.hasPermission) {
                    getMainImage();
                }
                else {
                    permissions.requestPermissions(list, function (status) {
                        if (!status.hasPermission) error();
                        getMainImage();
                    }, error);
                }
            });
        };


        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('introReview', null);
        };

        app.init();
    });

}]);

