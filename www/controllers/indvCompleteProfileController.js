﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('indvCompleteProfileController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';
    var map;
    var latitude;
    var longitiude;
    var contact;
    var lang = localStorage.getItem('Dalal_lang');
    var ft;
    var tempUserImage;


    function GetStoredContract() {
        var contract = JSON.parse(CookieService.getCookie('indvContract'));
        return contract;
    }

    function LoadCountries(callBack) {
        var countries = [];

        app.showIndicator();

        lang = localStorage.getItem('Dalal_lang');
        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            $('#linkindvCompleteProfileCountry .item-after').html('الدولة');
        }
        else {
            $('#linkindvCompleteProfileCountry .item-after').html('Country');
        }

        appServices.CallService('indvCompleteProfile', "GET", "api/Nationality/QuickAll", '', function (res) {
            if (res != null && res.length > 0) {
                $scope.countries = res;
                $$('#linkindvCompleteProfileCountry select').html('');

                lang = localStorage.getItem('Dalal_lang');
                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    app.smartSelectAddOption('#linkindvCompleteProfileCountry select', '<option value="" selected>الدولة</option>');
                }
                else {
                    app.smartSelectAddOption('#linkindvCompleteProfileCountry select', '<option value="" selected>Country</option>');
                }
                angular.forEach($scope.countries, function (country) {
                    app.smartSelectAddOption('#linkindvCompleteProfileCountry select', '<option value="' + country.key + '">' + country.value + '</option>');
                });
            }

            callBack(true);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    function LoadCities(callBack) {
        var cities = [];

        lang = localStorage.getItem('Dalal_lang');
        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            $('#linkindvCompleteProfileCity .item-after').html('المدينة');
        }
        else {
            $('#linkindvCompleteProfileCity .item-after').html('City');
        }

        appServices.CallService('indvCompleteProfile', "GET", "api/city/QuickAll", '', function (res) {
            if (res != null && res.length > 0) {
                $scope.cities = res;
                $$('#linkindvCompleteProfileCity select').html('');

                lang = localStorage.getItem('Dalal_lang');
                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    app.smartSelectAddOption('#linkindvCompleteProfileCity select', '<option value="" selected>المدينة</option>');
                }
                else {
                    app.smartSelectAddOption('#linkindvCompleteProfileCity select', '<option value="" selected>City</option>');
                }
                angular.forEach($scope.cities, function (city) {
                    app.smartSelectAddOption('#linkindvCompleteProfileCity select', '<option value="' + city.key + '">' + city.value + '</option>');
                });
            }

            callBack(true);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    function LoadGenders(callBack) {
        var genders = [];

        lang = localStorage.getItem('Dalal_lang');
        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            $('#linkindvCompleteProfileGender .item-after').html('النوع');
        }
        else {
            $('#linkindvCompleteProfileGender .item-after').html('Gender');
        }

        appServices.CallService('indvCompleteProfile', "GET", "api/contact/Genders", '', function (res) {
            if (res != null && res.length > 0) {
                $scope.genders = res;
                $$('#linkindvCompleteProfileGender select').html('');

                lang = localStorage.getItem('Dalal_lang');
                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    app.smartSelectAddOption('#linkindvCompleteProfileGender select', '<option value="" selected>النوع</option>');
                }
                else {
                    app.smartSelectAddOption('#linkindvCompleteProfileGender select', '<option value="" selected>Gender</option>');
                }
                angular.forEach($scope.genders, function (gender) {
                    app.smartSelectAddOption('#linkindvCompleteProfileGender select', '<option value="' + gender.key + '">' + gender.value + '</option>');
                });
            }

            callBack(true);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    function LoadUserDetails(callBack) {
        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

        appServices.CallService('indvCompleteProfile', "POST", "api/account/CreateUserInCRMIFNotCreated?UserId="+userLoggedIn.id , '', function (res) {
            app.hideIndicator();
            appServices.CallService('indvCompleteProfile', "GET", "api/contact/GetDetails/" + userLoggedIn.crmUserId, '', function (res) {
                app.hideIndicator();
                if (res != null) {
                    contact = res.contact;
                    $scope.indvCompleteProfileForm.fullName = contact.fullName;

                    setTimeout(function () {
                        $scope.$apply();
                        callBack(true);
                    }, fw7.DelayBeforeScopeApply);
                }
                else {
                    callBack(null);
                }

            });
        });

        
    }


    $(document).ready(function () {
        app.onPageInit('indvCompleteProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'indvCompleteProfile') return;
            $rootScope.currentOpeningPage = 'indvCompleteProfile';

        });

        app.onPageReinit('indvCompleteProfile', function (page) {
            $scope.resetForm();
        });

        app.onPageBeforeAnimation('indvCompleteProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'indvCompleteProfile') return;
            $rootScope.currentOpeningPage = 'indvCompleteProfile';

            $scope.isLoaded = true;

            var lang = localStorage.getItem('Dalal_lang');
            $scope.isArabic = lang == 'AR' || typeof lang == 'undefined' || lang == null ? true : false;

            $scope.resetForm();
            
            LoadCountries(function (result) {
                LoadCities(function (result1) {
                    LoadGenders(function (result2) {
                        LoadUserDetails(function (result3) { });
                    });
                });
            });

            $$('#linkindvCompleteProfileCountry select').on('change', function () {
                var countryId = $(this).val();
                var country = $scope.countries.filter(function (obj) {
                    return obj.key == countryId;
                })[0];

                $scope.formFields.country = country;
            });

            $$('#linkindvCompleteProfileCity select').on('change', function () {
                var cityId = $(this).val();
                var city = $scope.cities.filter(function (obj) {
                    return obj.key == cityId;
                })[0];

                $scope.formFields.city = city;
            });

            $$('#linkindvCompleteProfileGender select').on('change', function () {
                var genderId = $(this).val();
                var gender = $scope.genders.filter(function (obj) {
                    return obj.key == genderId;
                })[0];

                $scope.formFields.gender = gender;
            });

        });

        app.onPageReinit('indvCompleteProfile', function (page) {
            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('indvCompleteProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'indvCompleteProfile') return;
            $rootScope.currentOpeningPage = 'indvCompleteProfile';

        });

        $scope.form = {};
        $scope.indvCompleteProfileForm = {};
        $scope.formFields = {};
       var isImageUploaded = false;
        $scope.resetForm = function () {
            $scope.indvCompleteProfileReset = false;
            $scope.submittedindvCompleteProfile = false;

            $scope.isCountryValid = true;
            $scope.isCityValid = true;
            $scope.isGenderValid = true;
            $scope.isIdNumberCorrect = true;

            $scope.indvCompleteProfileForm.country = null;
            $scope.indvCompleteProfileForm.fullName = null;
            $scope.indvCompleteProfileForm.city = null;
            $scope.indvCompleteProfileForm.gender = null;
            $scope.indvCompleteProfileForm.idNumber = null;

            if (typeof $scope.IndvCompleteProfileForm != 'undefined' && $scope.IndvCompleteProfileForm != null) {
            	$scope.IndvCompleteProfileForm.$setPristine(true);
            	$scope.IndvCompleteProfileForm.$setUntouched();
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        }

        $scope.submitForm = function (IndvCompleteProfileForm) {
            $scope.indvCompleteProfileReset = true;
            $scope.submittedindvCompleteProfile = true;

            var countryId = $('#linkindvCompleteProfileCountry select').val();
            var cityId = $('#linkindvCompleteProfileCity select').val();
            var GenderId = $('#linkindvCompleteProfileGender select').val();
            var idNumber = $scope.indvCompleteProfileForm.idNumber;0



            $scope.isCountryValid = $scope.indvCompleteProfileReset && $scope.submittedindvCompleteProfile && countryId != null && countryId != '' && countryId != ' ' ? true : false;
            $scope.isCityValid = $scope.indvCompleteProfileReset && $scope.submittedindvCompleteProfile && cityId != null && cityId != '' && cityId != ' ' ? true : false;
            $scope.isGenderValid = $scope.indvCompleteProfileReset && $scope.submittedindvCompleteProfile && GenderId != null && GenderId != '' && GenderId != ' ' ? true : false;
            $scope.isUserImageEmpty = !isImageUploaded;
            var isIdNumberCorrect = false;

            if (countryId == '1e0ff838-292f-e311-b3fd-00155d010303') {
                isIdNumberCorrect = idNumber != null && idNumber != '' && idNumber != ' ' && idNumber.startsWith('1') == true ? true : false;
            }
            else {
                isIdNumberCorrect = idNumber != null && idNumber != '' && idNumber != ' ' && idNumber.startsWith('2') == true ? true : false;
            }

            $scope.isIdNumberCorrect = isIdNumberCorrect;

            if ($scope.isCountryValid && $scope.isCityValid && $scope.isGenderValid && !IndvCompleteProfileForm.idNumber.$invalid && isIdNumberCorrect && !$scope.isUserImageEmpty) {
                

                var params = {
                    'ContactId': contact.contactId,
                    'MobilePhone': contact.mobilePhone,
                    'FullName': contact.fullName,
                    'CityId': cityId,
                    'NationalityId': countryId,
                    'GenderId': GenderId,
                    'IdNumber': idNumber
                };

                app.showIndicator();
                appServices.CallService('forgetPass', "POST", "api/contact/UpdateProfile_M", params, function (res) {
                    app.hideIndicator();
                    if (res != null) {
                        var storedContract = GetStoredContract();

                        storedContract.user = contact;
                        storedContract.user.country = $scope.formFields.country;
                        storedContract.user.city = $scope.formFields.city;
                        storedContract.user.gender = $scope.formFields.gender;
                        storedContract.user.idNumber = $scope.indvCompleteProfileForm.idNumber;

                        CookieService.setCookie('indvContract', JSON.stringify(storedContract));
                    	// helpers.GoToPage('indvContractDetails', null);
                       
                        var createParams = {
                        	'EmployeeId': storedContract.EmployeeId,
                        	'packageId': storedContract.packageId,
                        	'nationalityId': storedContract.nationalityId,
                        	'professionId': storedContract.professionId,
                        	'WorkingAddress': storedContract.sectorAddress,
                        	'WorkingPlace': storedContract.workingPlace,
                        	'WorkingSector': storedContract.sectorType,
                        	'userName': storedContract.user.fullName,
                        	//'CityId': storedContract.city.key,
                        	'Latitude': storedContract.Latitude,
                        	'Longitude': storedContract.Longitude,
                        	'HouseNo': storedContract.HouseNumber,
                        	'HouseType': storedContract.HouseType.key,
                        	'FloorNo': storedContract.FloorNumber.key,
                        	'PartmentNo': storedContract.AppartmentNumber,
                        	'CustomerId': storedContract.user.contactId
                        };

                        app.showIndicator();
                        appServices.CallService('indvContract', 'POST', 'api/IndivContract/IndividualContractRequest_M', createParams, function (res1) {
                        	app.hideIndicator();
                        	if (res1 != null) {

                        		var newContract = storedContract;
                        		newContract.ContractId = res1.contractId;
                        		newContract.ContractNum = res1.contractNum;
                        		//newContract.cityId = res1.cityId;
                        		$scope.contract = newContract;
                        		CookieService.setCookie('indvContract', JSON.stringify($scope.contract));

                        		language.openFrameworkModal('نجاح', 'تم إنشاء العقد بنجاح .', 'alert', function () { });
                        		helpers.GoToPage('indvSuccess', null);
                        	}
                        });





                        //language.openFrameworkModal('نجاح', 'تم إنشاء العقد بنجاح .', 'alert', function () { });
                        //helpers.GoToPage('indvSuccess', null);
                    }
                });

            }
        };

        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('intro', null);
        };

        function ConvertImgToBase64Url(url, outputFormat, callback) {
            var img = new Image();
            img.crossOrigin = 'Anonymous';
            img.onload = function () {
                var canvas = document.createElement('CANVAS');
                var ctx = canvas.getContext('2d');
                canvas.height = this.height;
                canvas.width = this.width;
                ctx.drawImage(this, 0, 0, canvas.width, canvas.height);
                var dataURL = canvas.toDataURL(outputFormat);
                callback(dataURL, url);
                canvas = null;
            };

            img.src = url;
        }

        function getImage() {

        	window.imagePicker.getPictures(function (results) {
        		if (results && results.length > 0) {

        			angular.forEach(results, function (image) {
        				var selectedImage = image;

        				$scope.userImageElement = {
        					id: 1,
        					loading: false,
        					src: $scope.userImage
        				};

        				ConvertImgToBase64Url(selectedImage, 'jpg', function (imgBase64, selectedImage) {
        					$scope.userImage = imgBase64.split(',')[1];
        					$scope.uploadImage($scope.userImage, selectedImage);
        				});

        				setTimeout(function () {
        					$scope.$apply();
        				}, 10);


        			});

        		}
        	},
            function (error) {
            	console.log('Error: ' + error);
            }, {
            	maximumImagesCount: 1,
            	outputType: 0,
            	width: 1024,
            	height: 768
            });
        }

        $scope.AddUserImage = function () {
        	lang = localStorage.getItem('Dalal_lang');

        	var permissions = cordova.plugins.permissions;

        	if (ft)
        		ft.abort();
        	$scope.userImage = tempUserImage;
        	var list = [
                permissions.READ_EXTERNAL_STORAGE,
                permissions.WRITE_EXTERNAL_STORAGE
        	];
        	permissions.checkPermission(list, function (status) {
        		if (status.hasPermission) {
        			getImage();
        		}
        		else {
        			permissions.requestPermissions(list, function (status) {
        				if (!status.hasPermission) {

        				}
        				else {
        					getImage();
        				}
        			}, function () {
        			});
        		}
        	});
        }

        $scope.uploadImage = function (imageSrc, imageFilePath) {
            var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

        	var langPrefix = lang == 'AR' || lang == null || typeof lang == 'undefined' ? 'ar' : 'en';

        	var params = {
        		'ContactId': userLoggedIn.crmUserId,
        		'Name': imageFilePath.substr(imageFilePath.lastIndexOf('/') + 1),
        		'ImageBase': imageSrc
        	};

        	$scope.userImageElement.loading = true;

        	setTimeout(function () {
        		$scope.$apply();
        	}, 10);

        	appServices.CallService('indvCompleteProfile', "POST", 'api/IndividualContractRequest/UploadIqamaFile_M', params, function (res) {

        		if (res != null) {
        			if (langPrefix == 'ar') {
        				language.openFrameworkModal('نجاح', 'تم رفع صوره الإقامة بنجاح', 'alert', function () { });
        			}
        			else {
        				language.openFrameworkModal('Success', 'Iqama image was uploaded successfully!', 'alert', function () { });
        			}
        			$scope.userImageElement.loading = false;
        			$scope.userImageElement.src = $scope.userImage;
        			tempUserImage = $scope.userImage;
        			isImageUploaded = true;
        			setTimeout(function () {
        				$scope.$apply();
        			}, 10);


        			//setTimeout(function () {
        			//	CookieService.removeCookie('indvContract');
        			//	helpers.GoToPage('userIndvContractRequests', null);
        			//}, 2000);
        		}
        		else {
        			if (langPrefix == 'ar') {
        				language.openFrameworkModal('خطأ', 'لم يتم رفع صوره الإقامة', 'alert', function () { });
        			}
        			else {
        				language.openFrameworkModal('Error', 'Residence was not uploaded!', 'alert', function () { });
        			}
        			$scope.userImageElement.loading = false;
        			isImageUploaded = false;
        			setTimeout(function () {
        				$scope.$apply();
        			}, 10);
        		}
        	});
        }

       // $scope.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        $scope.nameRegex = /^[A-Za-z0-9]*$/;
        $scope.mobileRegex = /^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/;

        app.init();
    });

}]);

