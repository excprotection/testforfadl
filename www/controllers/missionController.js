﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('missionController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';

    $(document).ready(function () {
        app.onPageInit('mission', function (page) {
            if ($rootScope.currentOpeningPage != 'mission') return;
            $rootScope.currentOpeningPage = 'mission';

        });

        app.onPageBeforeAnimation('mission', function (page) {
            if ($rootScope.currentOpeningPage != 'mission') return;
            $rootScope.currentOpeningPage = 'mission';

            $scope.isLoaded = true;

        });

        app.onPageReinit('mission', function (page) {
            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('mission', function (page) {
            if ($rootScope.currentOpeningPage != 'mission') return;
            $rootScope.currentOpeningPage = 'mission';

        });


        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('intro', null);
        };

        app.init();
    });

}]);

