﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('userIndvContractRequestsController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';
    var loadingContracts;
    var allContracts = [];
    var methodName;
    var isPull = true;

    function ClearListData() {
        allContracts = [];
        loadingContracts = false;
        CookieService.setCookie('user-indv-contract-requests-page-number', 1);
        $scope.contracts = null;
        app.pullToRefreshDone();
    }

    function LoadUserIndvContractRequests(callBack) {
        CookieService.setCookie('user-indv-contract-requests-page-number', 1);
        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

        var methodName = 'api/IndividualContractRequest/GetRequests/All_M?userId=' + userLoggedIn.crmUserId + '&pageSize=' + myApp.fw7.PageSize + '&pageNumber=' +
            parseInt(CookieService.getCookie('user-indv-contract-requests-page-number'));

        allContracts = [];
        app.attachInfiniteScroll('#divInfiniteIndvContractRequests');

        app.showIndicator();

        appServices.CallService('userIndvContractRequests', 'GET', methodName, '', function (result) {

            app.hideIndicator();

            allContracts = [];

            if (result && result.contracts.length > 0) {
                if (result.contracts.length < myApp.fw7.PageSize) {
                    $scope.contractsInfiniteLoader = false;
                    app.detachInfiniteScroll('#divInfiniteIndvContractRequests');
                }
                else {
                    $scope.contractsInfiniteLoader = true;
                    app.attachInfiniteScroll('#divInfiniteIndvContractRequests');
                }

                allContracts = result.contracts;

                allContracts.sort(function (a, b) {
                    return new Date(b.requestDate).getTime() - new Date(a.requestDate).getTime();
                });

                angular.forEach(allContracts, function (contract, index) {
                    contract.requestDate = contract.requestDate.split(' ')[0];
                });

                $scope.contracts = allContracts;

                $scope.noContracts = false;

                if (isPull) {
                    app.pullToRefreshDone();
                }

                callBack(true);

                setTimeout(function () {
                    $scope.allContractsLoaded = true;
                    $scope.isLoaded = true;
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);

            }
            else {
                allContracts = [];
                $scope.contracts = [];
                $scope.noContracts = true;
                $('.advNoResult').show();
                $scope.contractsInfiniteLoader = false;

                callBack(true);

                setTimeout(function () {
                    $scope.isLoaded = true;
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            }

        });
    }

    $(document).ready(function () {
        app.onPageInit('userIndvContractRequests', function (page) {
            if ($rootScope.currentOpeningPage != 'userIndvContractRequests') return;
            $rootScope.currentOpeningPage = 'userIndvContractRequests';

            fromPage = page.fromPage.name;

            $$('#divInfiniteIndvContractRequests').on('ptr:refresh', function (e) {
                isPull = true;

                LoadUserIndvContractRequests(function (result) {
                    app.pullToRefreshDone();
                });

                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            });


            $$('#divInfiniteIndvContractRequests').on('infinite', function () {
                if (loadingContracts) return;
                loadingContracts = true;

                var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

                CookieService.setCookie('user-indv-contract-requests-page-number', parseInt(CookieService.getCookie('user-indv-contract-requests-page-number')) + 1);


                var methodName = 'api/IndividualContractRequest/GetRequests/All_M?userId=' + userLoggedIn.crmUserId + '&pageSize=' + myApp.fw7.PageSize + '&pageNumber=' +
                    parseInt(CookieService.getCookie('user-indv-contract-requests-page-number'));

                appServices.CallService('userIndvContractRequests', 'GET', methodName, '', function (response) {

                    if (response && response.contracts.length > 0) {
                        loadingContracts = false;

                        angular.forEach(response.contracts, function (contract, index) {
                            allContracts.push(contract);
                        });

                        allContracts.sort(function (a, b) {
                            return new Date(b.requestDate).getTime() - new Date(a.requestDate).getTime();
                        });

                        angular.forEach(allContracts, function (contract, index) {
                            contract.requestDate = contract.createOn.split(' ')[0];
                        });

                        $scope.contracts = allContracts;

                        if (response && response.length < myApp.fw7.PageSize) {
                            $scope.contractsInfiniteLoader = false;
                            app.detachInfiniteScroll('#divInfiniteIndvContractRequests');
                            return;
                        }
                    }
                    else {
                        $scope.contractsInfiniteLoader = false;
                        app.detachInfiniteScroll('#divInfiniteIndvContractRequests');
                        loadingContracts = false;
                    }

                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                });
            });
        });

        app.onPageBeforeAnimation('userIndvContractRequests', function (page) {
            if ($rootScope.currentOpeningPage != 'userIndvContractRequests') return;
            $rootScope.currentOpeningPage = 'userIndvContractRequests';

            $scope.isLoaded = false;

            fromPage = page.fromPage.name;

            ClearListData();

            isPull = false;

            LoadUserIndvContractRequests(function (result) {
                var lang = localStorage.getItem('Dalal_lang');
                $scope.lang = lang;
                $scope.isLoaded = true;

                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    $scope.lblComplaintTicketNumber = 'رقم الطلب :';
                    $scope.lblComplaintContractNumber = 'المبلغ :';
                    $scope.lblComplaintTicketDesc = 'التاريخ :';
                    $scope.lblContractCurrency = 'ريال';
                }
                else {
                    $scope.lblComplaintTicketNumber = 'Number : ';
                    $scope.lblComplaintContractNumber = 'Cost : ';
                    $scope.lblComplaintTicketDesc = 'Date : ';
                    $scope.lblContractCurrency = 'SAR';
                }
            });

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);

        });

        app.onPageReinit('userIndvContractRequests', function (page) {
            isPull = false;

            $('.advNoResult').hide();

            fromPage = page.fromPage.name;

            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('userIndvContractRequests', function (page) {
            if ($rootScope.currentOpeningPage != 'userIndvContractRequests') return;
            $rootScope.currentOpeningPage = 'userIndvContractRequests';

            fromPage = page.fromPage.name;

        });

        $scope.GoToRequestDetails = function (contract) {
            helpers.GoToPage('requestInfo', { requestId: contract.individualContractRequestId });
        };


        $scope.GoBack = function () {
            if (fromPage == 'indvPaymentMethod') {
                helpers.GoToPage('intro', null);
            }
            else {
                helpers.GoBack();
            }
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('intro', null);
        };

        $scope.AddNewContract = function () {
            $rootScope.CheckAppInReview(function (InReview) {
                if (InReview == true) {
                    var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
                    if (userLoggedIn) {
                        helpers.GoToPage('individualSector', { requiredProfessionId: '5bc7f260-292f-e311-b3fd-00155d010303', InReview: InReview });
                    }
                    else {
                        helpers.GoToPage('login', null);
                    }
                }
                else {
                    helpers.GoToPage('serviceDetails', { serviceName: 'Tadbeer' });
                }
            });
        };

        app.init();
    });

}]);

