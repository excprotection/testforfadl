﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('homeController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var loadingHome;
    var allQuestions = [];
    var isPull = true;
    var categoryId = 0;
    var homeSwiper;
    var indexToGenerateAdv = 4;
    var isMessageChannelNBinded = false;
    var isCommentChannelNBinded = false;

    $(document).ready(function () {

        app.onPageInit('home', function (page) {
            if ($rootScope.currentOpeningPage != 'home') return;

        });

        app.onPageReinit('home', function (page) {
            if ($rootScope.currentOpeningPage != 'home') return;
            $rootScope.currentOpeningPage = 'home';
        });

        app.onPageBeforeAnimation('home', function (page) {
            if ($rootScope.currentOpeningPage != 'home') return;
            $rootScope.currentOpeningPage = 'home';

            var type = page.query.type;

            if (type == 'Individual') {
                $scope.isIndividual = true;
                $scope.isMedical = false;
                $scope.isBusiness = false;
            }
            else if (type == 'Medical') {
                $scope.isIndividual = false;
                $scope.isMedical = true;
                $scope.isBusiness = false;
            }
            else {
                $scope.isIndividual = false;
                $scope.isMedical = false;
                $scope.isBusiness = true;
            }

            $scope.isLoaded = true;

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageAfterAnimation('home', function (page) {
            if ($rootScope.currentOpeningPage != 'home') return;
            $rootScope.currentOpeningPage = 'home';

        });

        $scope.GoToServiceDetails = function (serviceName) {
            helpers.GoToPage('serviceDetails', { serviceName: serviceName });
        };

        $scope.CallUs = function () {
            var contactPhone = '+966920033660';

            window.plugins.CallNumber.callNumber(
              function onSuccess(successResult) {

              }, function onError(errorResult) {

                  console.log("Error:" + errorResult);
              }, contactPhone, true);
        };

        app.init();
    });

}]);

