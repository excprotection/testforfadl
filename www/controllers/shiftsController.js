﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('shiftsController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';

    function GetStoredContract() {
        var contract = JSON.parse(CookieService.getCookie('StoredContract'));
        return contract;
    }

    function LoadShifts(callBack) {
        var params = '';
        var selectedDays = '';

        var storedContract = GetStoredContract();
        var duration = storedContract.duration != null ? parseInt(storedContract.duration.key) : 0;
        var monthsNumber = duration + parseInt(storedContract.promotionExtraVisits);

        var allDays = storedContract.englishAvailableDays;

        angular.forEach(allDays, function (day, index) {

            if (day.name == 'السبت') day.name = 'Saturday';
            else if (day.name == 'الأحد') day.name = 'Sunday';
            else if (day.name == 'الإثنين') day.name = 'Monday';
            else if (day.name == 'الثلاثاء') day.name = 'Tuesday';
            else if (day.name == 'الأربعاء') day.name = 'Wednesday';
            else if (day.name == 'الخميس') day.name = 'Thursday';
            else if (day.name == 'الجمعة') day.name = 'Friday';

        });

        angular.forEach(allDays, function (day, index) {
            if (index == 0) {
                selectedDays += day.name;
            }
            else {
                selectedDays += ',' + day.name;
            }
        });

        //storedContract.englishAvailableDays = selectedDays;
        //CookieService.setCookie('StoredContract', JSON.stringify(storedContract));

        params = 'ContractStartDate=' + storedContract.firstVisit + '&NoOfMonths=' + monthsNumber + '&Days=' + selectedDays + '&hours=' +
            storedContract.hour.key + '&shift=' + storedContract.isMorningShift;

        app.showIndicator();

        appServices.CallService('create', "GET", "api/HourlyPricing/ContractShiftDays?" + params, '', function (res) {
            app.hideIndicator();
            if (res != null && res.length > 0) {
                callBack(res);
            }
            else {
                callBack(null);
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    $(document).ready(function () {
        app.onPageInit('shifts', function (page) {
            if ($rootScope.currentOpeningPage != 'shifts') return;
            $rootScope.currentOpeningPage = 'shifts';

        });

        app.onPageBeforeAnimation('shifts', function (page) {
            if ($rootScope.currentOpeningPage != 'shifts') return;
            $rootScope.currentOpeningPage = 'shifts';

            $scope.isLoaded = true;

            var lang = localStorage.getItem('Dalal_lang');
            $scope.isArabic = lang == 'AR' || typeof lang == 'undefined' || lang == null ? true : false;

            LoadShifts(function (result) {
                if (result != null) {
                    var storedContract = GetStoredContract();
                    var postponedDays = [];
                    var daysToPostPoned = [];

                    postponedDays = result.filter(function (el) {
                        return el.isPostpondedDay == true;
                    }).map(function (el) {
                        return el.dateStr;
                    });

                    daysToPostPoned = result.filter(function (el) {
                        return el.isDayToPostpond == true;
                    }).map(function (el) {
                        return el.dateStr;
                    });

                    storedContract.PostpondedDays = postponedDays.join(',');
                    storedContract.DaysToPostpond = daysToPostPoned.join(',');
                    CookieService.setCookie('StoredContract', JSON.stringify(storedContract));

                    $scope.shifts = result;
                }
                else {
                    $scope.shifts = [];
                }
            });

        });

        app.onPageReinit('shifts', function (page) {
            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('shifts', function (page) {
            if ($rootScope.currentOpeningPage != 'shifts') return;
            $rootScope.currentOpeningPage = 'shifts';

        });

        $scope.OpenReason = function (shift) {
            if (shift.isDayToPostpond == true) {
                var reasonTitle = 'Postpone Reason';
                var cancelText = 'Ok';

                var lang = localStorage.getItem('Dalal_lang');
                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    reasonTitle = 'سبب التأجيل';
                    cancelText = 'تم';
                }

                var activationSignup = app.modal({
                    title: reasonTitle,
                    text: shift.status,
                    afterText: '',
                    buttons: [
                         {
                             text: cancelText,
                             onClick: function () {
                                 app.closeModal(activationSignup);
                             }
                         }
                    ]
                });
            }
        };


        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToPickLocation = function () {
            helpers.GoToPage('previousLocations', null);
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('intro', null);
        };

        app.init();
    });

}]);

