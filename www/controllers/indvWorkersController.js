﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('indvWorkersController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
	'use strict';

	var fw7 = myApp.fw7;
	var app = myApp.fw7.app;
	var lang = localStorage.getItem('Dalal_lang');
	var isPull = true;
	var pageSize =fw7.PageSize;
	var pageIndex = 0;
	var loadingEmployees = false;

	function GetEmployeeCount() {
		return CookieService.getCookie('professionTotalCount');
	}
	function Initialvalues(employee) {
		employee.nationalityId = $scope.nationalityId;
		employee.professionId = $scope.professionId;
		employee.PageIndex = 1;
		//just for test
		employee.PageSize = pageSize;
		//employee.AgeOperand = "range";
		employee.AgeOperand = "range";

	}

	//function SearchByPage(employee) {
	//	app.attachInfiniteScroll('#divInfiniteEmployees');
	//	app.showIndicator();
	//	appServices.CallService('indvWorkers', "Post", 'api/IndivContract/GetAvailableEmployeesFiltered_M', employee, function (res) {
	//		app.hideIndicator();
	//		if (res != null) {
	//			if (res.length > 0) {
	//				$.each(res, function (key, value) {
	//					if (value.skills) {
	//						value.skills = value.skills.replace(' ,', ',').replace(', ', ',');
	//					}
	//					var skills = value.skills.split(',');
	//					for (var i = 0; i < skills.length; i++) {
	//						skills[i] = skills[i].trim();
	//					}
	//					res[key].skills = skills;
	//				});
	//				if (res.length > 0) {
	//					if (pageIndex > 1 || $scope.Employees) {
	//						$scope.Employees = $scope.Employees.concat(res);
	//					}
	//					else {
	//						$scope.Employees = res;
	//					}
	//					$scope.noEmployees = false;
	//					$scope.EmployeesInfiniteLoader = false;
	//				}
	//				$scope.professionName = res[0].jobTitle;
	//			}
	//			else {
	//				$scope.noEmployees = true;
	//				$scope.EmployeesInfiniteLoader = false;
	//			}
	//			// update page index to get next page.
	//			pageIndex++;
	//		} else {
	//			$scope.EmployeesInfiniteLoader = false;
	//			$scope.noEmployees = true;
	//		}
	//		$scope.isLoaded = true;
	//	});
	//}

	function LoadAvailableEmployees(nationalityId, professionId) {
		app.showIndicator();
		appServices.CallService('indvWorkers', "GET", "api/IndivContract/GetAvailableEmployees_M?nationalityId=" + nationalityId + "&professionId=" + professionId + "&pageIndex=" + pageIndex + "&pageSize=" + pageSize, '', function (res) {
			app.hideIndicator();
			if (res != null) {

				$.each(res, function (key, value) {
					if (value.skills != '' && value.skills != ' ') {
						var skills = value.skills.split(',');
						for (var i = 0; i < skills.length; i++) {
							skills[i] = skills[i].trim();
						}
						res[key].skills = skills;
					}
				});

				angular.forEach(res, function (employee, index) {
					employee.imgSrc = 'img/img.JPG';
				});

				if (res.length > 0) {
					if (pageIndex > 1 || $scope.Employees) {
						$scope.Employees = $scope.Employees.concat(res);
						loadingEmployees = false;
					}
					else {
						$scope.Employees = res;
					}

					$scope.noEmployees = false;
					$scope.EmployeesInfiniteLoader = false;
				}
				else {
					$scope.noEmployees = true;
					$scope.EmployeesInfiniteLoader = true;
				}

				$scope.isLoaded = true;

			}
			pageIndex++;
		});


	}

	//function IsShowEmployeePhoto () {

	//	var ShowImage=false;
	//	appServices.CallService('indvWorkers', "GET", "api/IndivContract/GetShowEmploteePhoto_M", '', function (res) {
	//		if (res != null && res.toLowerCase() == 'true') {
	//			ShowImage= true;
	//		}
	//		ShowImage = false;
	//	});
	//	CookieService.setCookie('IsShowEmployeePhoto', ShowImage);
	//}
	function AddBootstrapIfNotExist() {
		var bootstrapCssElement = document.getElementById('boostrapFile');
		var fontsCssElement = document.getElementById('fontsFile');
		var head = document.getElementsByTagName('head')[0];

		if (!bootstrapCssElement) {
			var boostrapLink = document.createElement('link');
			boostrapLink.id = 'boostrapFile';
			boostrapLink.rel = 'stylesheet';
			boostrapLink.type = 'text/css';
			boostrapLink.href = 'css/bootstrap.min.css';
			head.appendChild(boostrapLink);

			var fontsLink = document.createElement('link');
			fontsLink.id = 'fontsFile';
			fontsLink.rel = 'stylesheet';
			fontsLink.type = 'text/css';
			fontsLink.href = 'fonts/fonts.css';
			head.appendChild(fontsLink);
		}

	}
	$(document).ready(function () {
		app.onPageInit('indvWorkers', function (page) {
			if ($rootScope.currentOpeningPage != 'indvWorkers') return;
			$rootScope.currentOpeningPage = 'indvWorkers';

			$$('#divInfiniteEmployees').on('ptr:refresh', function (e) {
				isPull = true;
				if ($scope.Employees)
					if ($scope.Employees.length > pageSize)
						LoadAvailableEmployees($scope.nationalityId, $scope.professionId);



				setTimeout(function () {
					$scope.$apply();
					app.pullToRefreshDone();
				}, fw7.DelayBeforeScopeApply);

			});

			$$('#divInfiniteEmployees').on('infinite', function () {
				if (loadingEmployees) return;
				loadingEmployees = true;

				setTimeout(function () {
					if ($scope.Employees.length >= parseInt($scope.totalCount)) {
						// Nothing more to load, detach infinite scroll events to prevent unnecessary loadings
						app.detachInfiniteScroll($$('.infinite-scroll'));
						// Remove preloader
						$$('.infinite-scroll-preloader').remove();
						return;
					}
					LoadAvailableEmployees($scope.nationalityId, $scope.professionId);

				}, 1000);
			});

		});

		app.onPageReinit('indvWorkers', function (page) {
			pageIndex = 1;
			AddBootstrapIfNotExist();
		});

		app.onPageBeforeAnimation('indvWorkers', function (page) {
			if ($rootScope.currentOpeningPage != 'indvWorkers') return;
			$rootScope.currentOpeningPage = 'indvWorkers';

			$scope.nationalityId = page.query.nationalityId;
			$scope.professionId = page.query.professionId;
			$scope.professionName = page.query.professionName;
			$scope.packageId = page.query.packageId;
			$scope.FilteredEmployee = page.query.employee;
			$scope.totalCount = GetEmployeeCount();
			$scope.isLoaded = false;
			//            helpers.CheckUseBoostrapOrNot('indvWorkers');
			var setting = JSON.parse(CookieService.getCookie('indvEmployeeSettings'));
			$scope.ShowEmployeePhoto = setting.isShowEmployeePhoto;


			app.attachInfiniteScroll('#divInfiniteEmployees');
			LoadAvailableEmployees($scope.nationalityId, $scope.professionId);


			setTimeout(function () {
				$scope.$apply();
			}, fw7.DelayBeforeScopeApply);
		});

		app.onPageAfterAnimation('indvWorkers', function (page) {
			if ($rootScope.currentOpeningPage != 'indvWorkers') return;
			$rootScope.currentOpeningPage = 'indvWorkers';

		});

		$scope.SortBy = 'birthdate';
		isPull = false;
		$scope.noEmployees = true;
		$scope.EmployeesInfiniteLoader = true;

		$scope.SearchWorkers = function () {
			helpers.GoToPage('indvSearch', { nationalityId: $scope.nationalityId, professionId: $scope.professionId, packageId: $scope.packageId });
		}

		$scope.GetReligion = function (religionId) {
			var religionTxt = 'مسلم';
			switch (religionId) {
				case 1:
					religionTxt = 'مسلم'
					break;
				case 2:
					religionTxt = 'مسيحي'
					break;
				case 5:
					religionTxt = 'ديانةاخري'
					break;
				default:
					religionTxt = 'مسلم'

			}
			return religionTxt;
		}

		$scope.ContractDetails = function (employeeId) {

			var indvContract = {
				packageId: $scope.packageId,
				nationalityId: $scope.nationalityId,
				professionId: $scope.professionId,
				EmployeeId: employeeId,
				Latitude: null,
				Longitude: null,
				HouseType: null,
				HouseNo: null,
				FloorNo: null,
				PartmentNo: null
			}

			CookieService.setCookie('indvContract', JSON.stringify(indvContract));
			helpers.GoToPage('indvPrevLocations', null);

		}

		$scope.GoBack = function () {
			helpers.GoBack();
		};

		$scope.GoToLogin = function () {
			helpers.GoToPage('login', null);
		};


		app.init();

	});
}]);

