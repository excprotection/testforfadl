﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('introReviewController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';
    var test=  {"data": "test7777777777777777777"}

    var IntroSwiper;

    function InitiateSwiper(id) {
        IntroSwiper = new Swiper(id, {
            loop: false,
            autoplayDisableOnInteraction: false,
            speed: 2000,
            slidesPerView: 1,
            centeredSlides: true,
            autoplay: 2000,
            pagination: '.swiper-pagination',
        });

        setTimeout(function () {
            IntroSwiper.update();
        }, fw7.DelayBeforeScopeApply);
    }

    function StartSwiper() {
        if (IntroSwiper) IntroSwiper.destroy(true, true);
        InitiateSwiper('#IntroSwiper');
    }

    function GetOffers() {

        var Offers = [{ id: 1, 'imageName': 'img/business/shutterstock.png', description: '' }];
        $scope.Offers = Offers;
        $scope.IsOffersLoaded = true;
        setTimeout(function () {
            $scope.$apply();
        }, fw7.DelayBeforeScopeApply);
    }

    $scope.isLoaded = true;

    if (!CookieService.getCookie('Visitor') && !CookieService.getCookie('userLoggedIn')) {
        console.log('first time to load cookie');
        CookieService.setCookie('Visitor', true);
    }

    setTimeout(function () {
        $scope.$apply();
    }, fw7.DelayBeforeScopeApply);

    function DrawMenu() {
        var IsVisitor = CookieService.getCookie('Visitor') ? CookieService.getCookie('Visitor') : false;
        var loginUsingSocial = CookieService.getCookie('loginUsingSocial') ? CookieService.getCookie('loginUsingSocial') : false;
        var userLoggedIn = CookieService.getCookie('userLoggedIn') ? JSON.parse(CookieService.getCookie('userLoggedIn')) : null;
        $rootScope.IsVisitor = IsVisitor;
        var lang = localStorage.getItem('Dalal_lang');

        var fw7 = myApp.fw7;

        if (IsVisitor == 'true' || IsVisitor == true) {
            $('#linkMenuToHome').css('display', 'block ');
            $('#linkMenuToMyContracts').css('display', 'none');
            $('#linkMenuToMyComplaints').css('display', 'none');
            $('#linkMenuToIndividuals').css('display', 'none');
            $('#linkMenuToMyRequests').css('display', 'none');
            $('#linkMenuToMyInvoices').css('display', 'none');
            $('#linkMenuToMedical').css('display', 'block');
            $('#linkMenuToBusiness').css('display', 'block');
            $('#linkMenuToMission').css('display', 'block');
            $('#linkMenuToMembers').css('display', 'block');
            $('#linkMenuToChangePassword').css('display', 'none');
            $('#linkMenuToContact').css('display', 'block');
            $('#linkMenuToAbout').css('display', 'block');
            $('#linkMenuToChangeLanguage').css('display', 'block');
            $('#linkMenuToLogin').css('display', 'block');
            $('#linkMenuToProfile').css('display', 'none');
            $('#linkMenuToMyPoints').css('display', 'none');

            $('#linkMenuToHomeReview').css('display', 'block ');
            $('#linkMenuToMedicalReview').css('display', 'block');
            $('#linkMenuToOrdersReview').css('display', 'block');
            $('#linkMenuToMissionReview').css('display', 'block');
            $('#linkMenuToMembersReview').css('display', 'block');
            $('#linkMenuToBranchesReview').css('display', 'block');
            $('#linkMenuToAboutReview').css('display', 'block');

            $('#linkMenuToChangeLanguageReview').css('display', 'block');

            $('#linkMenuToChangePasswordReview').css('display', 'none');
            $('#linkMenuToLoginReview').css('display', 'block');
            $('#linkMenuToProfileReview').css('display', 'none');
            $('#linkMenuToContactUsReview').css('display', 'none');

            $('#divContactUs').css('display', 'block');

            if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                $('#lblMenuLoginText').html('تسجيل الدخول');
                $('#lblMenuLoginReviewText').html('تسجيل الدخول');
            }
            else {
                $('#lblMenuLoginText').html('Login');
                $('#lblMenuLoginReviewText').html('Login');
            }
            $$('#imgSideMenu').attr('src', 'img/logo.png');
        }
        else {
            $('#linkMenuToHome').css('display', 'block');
            $('#linkMenuToMyContracts').css('display', 'block');
            $('#linkMenuToMyComplaints').css('display', 'block');
            $('#linkMenuToIndividuals').css('display', 'block');
            $('#linkMenuToMyRequests').css('display', 'block');
            $('#linkMenuToMyInvoices').css('display', 'block');
            $('#linkMenuToMedical').css('display', 'block');
            $('#linkMenuToBusiness').css('display', 'block');
            $('#linkMenuToMission').css('display', 'block');
            $('#linkMenuToMembers').css('display', 'block');
            $('#linkMenuToContact').css('display', 'block');
            $('#linkMenuToAbout').css('display', 'block');
            $('#linkMenuToChangeLanguage').css('display', 'block');
            $('#linkMenuToLogin').css('display', 'block');
            $('#linkMenuToMyPoints').css('display', 'block');

            $('#linkMenuToHomeReview').css('display', 'block ');
            $('#linkMenuToMedicalReview').css('display', 'block'); 
            $('#linkMenuToOrdersReview').css('display', 'block');
            $('#linkMenuToMissionReview').css('display', 'block');
            $('#linkMenuToMembersReview').css('display', 'block');
            $('#linkMenuToBranchesReview').css('display', 'block');
            $('#linkMenuToAboutReview').css('display', 'block');
            $('#linkMenuToChangeLanguageReview').css('display', 'block');
            $('#linkMenuToLoginReview').css('display', 'block');
            $('#linkMenuToContactUsReview').css('display', 'block');

            $('#divContactUs').css('display', 'block');

            if (typeof userLoggedIn != 'undefined' && userLoggedIn != null) {
                $('#linkMenuToProfileReview').css('display', 'block');
                if (loginUsingSocial == 'true') {
                    $('#linkMenuToChangePassword').css('display', 'none');
                    $('#linkMenuToChangePasswordReview').css('display', 'none');
                }
                else {
                    $('#linkMenuToChangePassword').css('display', 'block');
                    $('#linkMenuToChangePasswordReview').css('display', 'none');
                }
                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    $('#lblMenuLoginText').html('تسجيل خروج');
                    $('#lblMenuLoginReviewText').html('تسجيل خروج');
                }
                else {
                    $('#lblMenuLoginText').html('Logout');
                    $('#lblMenuLoginReviewText').html('Logout');
                }
            }
            else {
                $('#linkMenuToProfileReview').css('display', 'none');
                $('#linkMenuToChangePassword').css('display', 'none');
                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    $('#lblMenuLoginText').html('تسجيل الدخول');
                    $('#lblMenuLoginReviewText').html('تسجيل الدخول');
                }
                else {
                    $('#lblMenuLoginText').html('Login');
                    $('#lblMenuLoginReviewText').html('Login');
                }
            }

            if (typeof userLoggedIn != 'undefined' && userLoggedIn != null) {
                if (typeof userLoggedIn.image != 'undefined' && userLoggedIn.image != null && userLoggedIn.image != '' && userLoggedIn.image != ' ') {
                    if (userLoggedIn.image.indexOf('http://') > -1 || userLoggedIn.image.indexOf('https://') > -1 || userLoggedIn.image.indexOf('http://placehold.it') > -1 ||
                        userLoggedIn.image.indexOf('https://placehold.it') > -1) {
                        $$('#imgSideMenu').attr('src', userLoggedIn.image);
                    }
                    else {
                        if (userLoggedIn.image.indexOf('placehold.it') > -1) {
                            $$('#imgSideMenu').attr('src', 'http:' + userLoggedIn.image);
                        }
                        else {
                            $$('#imgSideMenu').attr('src', hostUrl + userLoggedIn.image);
                        }
                    }
                }
                else {
                    $$('#imgSideMenu').attr('src', 'img/logo.png');
                }
            }
            else {
                $$('#imgSideMenu').attr('src', 'img/logo.png');
            }
        }

        setTimeout(function () {
            //$scope.$apply();
            // $rootScope.$apply();
        }, fw7.DelayBeforeScopeApply);
    }

    $(document).ready(function () {

        DrawMenu();

        $scope.IsOffersLoaded = false;

        document.addEventListener("deviceready", onDeviceReady, false);
        function onDeviceReady() {
            setTimeout(function () {
                GetOffers();
            }, 100);
        }

        app.onPageInit('introReview', function (page) {
            if ($rootScope.currentOpeningPage != 'introReview') return;
            $rootScope.currentOpeningPage = 'introReview';

            $scope.isLoaded = true;

            DrawMenu();

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageBeforeAnimation('introReview', function (page) {
            if ($rootScope.currentOpeningPage != 'introReview') return;
            $rootScope.currentOpeningPage = 'introReview';

            $scope.isLoaded = true;

            var IsVisitor = CookieService.getCookie('Visitor') ? CookieService.getCookie('Visitor') : false;

            if (IsVisitor != 'true' && IsVisitor != true) {
                var deviceId = CookieService.getCookie('deviceId');

                if (typeof deviceId != 'undefined' && deviceId != null) {
                    appServices.CallService('home', "POST", "api/notification/AddDevice/" + deviceId, '', function (result) {
                        app.hideIndicator();
                        if (result != null) {
                        }

                    });
                }
            }

            DrawMenu();

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);

        });

        app.onPageReinit('introReview', function (page) {
            DrawMenu();
        });

        app.onPageAfterAnimation('introReview', function (page) {
            if ($rootScope.currentOpeningPage != 'introReview') return;
            $rootScope.currentOpeningPage = 'introReview';

        });

        $rootScope.RemoveEditPagesFromHistory = function () {
            for (var i = 0; i < fw7.views[0].history.length; i++) {
                if (fw7.views[0].history[i] === '#connect') fw7.views[0].history.splice(i, 1);
            }
        };

        $rootScope.isVisitor = function () {
            var IsVisitor = CookieService.getCookie('Visitor') ? CookieService.getCookie('Visitor') : false;
            if (IsVisitor == 'true' || IsVisitor == true) {
                return true;
            }
            else {
                return false;
            }
        };

        $scope.GoToServiceDetails = function (serviceName) {
            if (serviceName == 'Members') {
                helpers.GoToPage('membersReview', null);
            }
            else if (serviceName == 'Medical') {
                helpers.GoToPage('medicalReview', null);
            }
            else if (serviceName == 'Branches') {
                helpers.GoToPage('branchesReview', null);
            }
            else if (serviceName == 'ContactUs') {
                var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
                var lang = localStorage.getItem('Dalal_lang');
                if (userLoggedIn) {
                    helpers.GoToPage('individualSectorReview', null);
                } else {
                    var confirmTitle = 'Warning';
                    var confirmText = 'To Proceed And Be Able To Use Our Service , You Must Login First';

                    if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                        confirmText = 'للمتابعة والتمتع بخدماتنا يجب عليك تسجيل دخولك أولا';
                        confirmTitle = 'تحذير';
                    }
                    app.confirm(confirmText, confirmTitle, function () {
                        helpers.GoToPage('loginReview', null);
                    }, function () { });
                }
            }
            else if (serviceName == 'request') {
                helpers.GoToPage('addRequestReview', null);
            }
            else {
                helpers.GoToPage('missionReview', null);
            }
        };

        $scope.GoBack = function () {
            helpers.GoBack();
        };

        app.init();
    });

}]);

