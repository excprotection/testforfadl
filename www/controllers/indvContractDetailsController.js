﻿/// <reference path='../js/angular.js' />

myApp.angular.controller('indvContractDetailsController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
	'use strict';

	var fw7 = myApp.fw7;
	var app = myApp.fw7.app;
	var lang = localStorage.getItem('Dalal_lang');
	var vat = 5;
	function LoadContractDetails() {
	    var dataVariables =
			{
			    'employeeId': $scope.Storedcontract.EmployeeId,
			    'packageId': $scope.Storedcontract.packageId,
			    'Latitude': $scope.Storedcontract.Latitude,
			    'Longitude': $scope.Storedcontract.Longitude
			};

		app.showIndicator();
		appServices.CallService('indvContractDetails', 'Post', 'api/IndivContract/GetContractDetails_M', dataVariables, function (res) {
			app.hideIndicator();
			if (res) {
				$scope.contract = res;

				var totalBeforeVate = parseFloat((($scope.contract.monthly_Paid * $scope.contract.months) + $scope.contract.advancedPayment));
				var vatAmount = parseFloat(((totalBeforeVate * vat) / 100));
				var totalWithVat = parseFloat((totalBeforeVate + vatAmount));
				$scope.contract.hideEmployeeData = $scope.contract.name == null;

				$scope.contract.totalBeforeVat = totalBeforeVate;
				$scope.contract.vatAmount = vatAmount;
				$scope.contract.vatRate = vat;
				$scope.contract.totalPriceWithVat = totalWithVat

			}
		});
	}

	$(document).ready(function () {
		app.onPageInit('indvContractDetails', function (page) {
			if ($rootScope.currentOpeningPage != 'indvContractDetails') return;
			$rootScope.currentOpeningPage = 'indvContractDetails';
		});

		app.onPageReinit('indvContractDetails', function (page) {

			$rootScope.RemoveEditPagesFromHistory();
		});

		app.onPageBeforeAnimation('indvContractDetails', function (page) {
			if ($rootScope.currentOpeningPage != 'indvContractDetails') return;
			$rootScope.currentOpeningPage = 'indvContractDetails';

			var lang = localStorage.getItem('Dalal_lang');

			$scope.Storedcontract = JSON.parse(CookieService.getCookie('indvContract'));
			$scope.isArabic = lang == 'AR' || typeof lang == 'undefined' || lang == null ? true : false;

			LoadContractDetails();

			setTimeout(function () {
				$scope.$apply();
			}, fw7.DelayBeforeScopeApply);
		});

		app.onPageAfterAnimation('indvContractDetails', function (page) {
			if ($rootScope.currentOpeningPage != 'indvContractDetails') return;
			$rootScope.currentOpeningPage = 'indvContractDetails';

		});

		$scope.GoToSuccess = function () {
			var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

			if (userLoggedIn != null) {
				app.showIndicator();                                                             
				appServices.CallService('indvContractDetails', 'GET', 'api/IndividualContractRequest/IsContactProfileCompleted_M/' + userLoggedIn.crmUserId, '', function (res) {
					app.hideIndicator();
					if (res != null && res.completed == true) {
						console.log(' $scope.Storedcontract.professionId,: '+ $scope.Storedcontract.professionId);
						var params = {
							'EmployeeId': $scope.Storedcontract.EmployeeId,
							'packageId': $scope.Storedcontract.packageId,
							'nationalityId': $scope.Storedcontract.nationalityId,
							'professionId': $scope.Storedcontract.professionId,
							'WorkingAddress': $scope.Storedcontract.sectorAddress,
							'WorkingPlace': $scope.Storedcontract.workingPlace,
							'WorkingSector': $scope.Storedcontract.sectorType,
							'userName':userLoggedIn.userName,
							//'CityId': $scope.Storedcontract.city.key,
							'Latitude': $scope.Storedcontract.Latitude,
							'Longitude': $scope.Storedcontract.Longitude,
							'HouseNo': $scope.Storedcontract.HouseNumber,
							'HouseType': $scope.Storedcontract.HouseType.key,
							'FloorNo': $scope.Storedcontract.FloorNumber.key,
							'PartmentNo': $scope.Storedcontract.AppartmentNumber,
							'CustomerId': userLoggedIn.crmUserId
						};

						app.showIndicator();
						appServices.CallService('contractDetails', 'POST', 'api/IndivContract/IndividualContractRequest_M', params, function (res) {
							app.hideIndicator();
							if (res != null) {

								var newContract = $scope.contract;
								newContract.ContractId = res.contractId;
								newContract.ContractNum = res.contractNum;
								//newContract.cityId = res.cityId;
								$scope.contract = newContract;
								CookieService.setCookie('indvContract', JSON.stringify($scope.contract));

								language.openFrameworkModal('نجاح', 'تم إنشاء العقد بنجاح .', 'alert', function () { });
								helpers.GoToPage('indvSuccess', null);
							}
						});
					}
					else {
						helpers.GoToPage('indvCompleteProfile', null);
					}
				});
			}
			else {
				helpers.GoToPage('indvcompleteProfile', null);
			}

		};
		$scope.GoBack = function () {
			helpers.GoBack();
		};

		$scope.GoToLogin = function () {
			helpers.GoToPage('login', null);
		};

		$scope.GetReligion = function (religionId) {
			var religionTxt = 'مسلم';
			switch (religionId) {
				case 1:
					religionTxt = 'مسلم'
					break;
				case 2:
					religionTxt = 'مسيحي'
					break;
				case 5:
					religionTxt = 'ديانةاخري'
					break;
				default:
					religionTxt = 'مسلم'

			}
			return religionTxt;
		}

		$scope.GetMarital_Status = function (statusId) {
			var statusTxt = '';
			switch (statusId) {
				case 1:
					statusTxt = 'أعزب/عزباء'
					break;
				case 2:
					statusTxt = 'متزوج/متزوجة'
					break;
				case 3:
					statusTxt = 'مطلق/مطلقة'
					break;
				default:
					statusTxt = 'غير محدد'
			}
			return statusTxt;
		}

		$scope.GetGender = function (genderId) {
			var genderTxt = '';
			switch (genderId) {
				case 1:
					genderTxt = 'ذكر'
					break;
				case 2:
					genderTxt = 'أنثى'
					break;
				default:
					genderTxt = 'غير محدد'

			}
			return genderTxt;
		}

		app.init();

	});
}]);

