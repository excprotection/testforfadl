﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('AddRequestReviewController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var lang = localStorage.getItem('Dalal_lang');
    var newPassword;
    var newPasswordConfirmed;
    $scope.txtAlignValue = "left";

    $(document).ready(function () {
        app.onPageInit('addRequestReview', function (page) {
            if ($rootScope.currentOpeningPage != 'addRequestReview') return;
            $rootScope.currentOpeningPage = 'addRequestReview';
        });

        app.onPageReinit('addRequestReview', function (page) {
            $scope.resetForm();
            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageBeforeAnimation('addRequestReview', function (page) {
            if ($rootScope.currentOpeningPage != 'addRequestReview') return;
            $rootScope.currentOpeningPage = 'addRequestReview';
            var lang = localStorage.getItem('Dalal_lang');
            if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                $scope.txtAlignValue = "right";
            } else {
                $scope.txtAlignValue = "left";
            }
            $scope.resetForm();
            $rootScope.RemoveEditPagesFromHistory();
            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageAfterAnimation('addRequestReview', function (page) {
            if ($rootScope.currentOpeningPage != 'addRequestReview') return;
            $rootScope.currentOpeningPage = 'addRequestReview';
        });

        $scope.resetForm = function () {
            $scope.addRequestRevReset = false;
            $scope.submittedAddReq = false;
            $scope.userForm.firstname = null;
            $scope.userForm.middlename = null;
            $scope.userForm.email = null; 
            $scope.userForm.mobile = null;
            $scope.userForm.orderType = null;
            $scope.userForm.detials = null;

            $scope.user = {};

            if (typeof $scope.addRequestReview != 'undefined' && $scope.addRequestReview != null) {
                $scope.addRequestReview.$setPristine(true);
                $scope.addRequestReview.$setUntouched();
            }
            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        }

        $scope.submitForm = function (addReqForm) {
            $scope.addRequestRevReset = true;
            $scope.submittedAddReq = true;

            if (addReqForm) {
                var user = {
                    "fullName" : $scope.userForm.firstname + ' ' + $scope.userForm.middlename ,
                    "email": $scope.userForm.email,
                    "orderType": $scope.userForm.orderType,
                    "mobile": $scope.userForm.mobile,
                    "details": $scope.userForm.detials,
                }

                helpers.GoToPage('locationReview', { user: user });
            }
        };

        $scope.form = {};
        $scope.userForm = {};

        $scope.GoBack = function () {
            helpers.GoBack();
        };
        app.init();

    });

}]);

