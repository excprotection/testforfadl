﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('showOrderDetailsReviewController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';
    var lang = localStorage.getItem('Dalal_lang');
    $scope.showOrderType = true;
    $scope.showUserEmail = true;
    $scope.showHouseAddress = true;
    $(document).ready(function () {
        app.onPageInit('showOrderDetailsReview', function (page) {
            if ($rootScope.currentOpeningPage != 'showOrderDetailsReview') return;
            $rootScope.currentOpeningPage = 'showOrderDetailsReview';

        });

        app.onPageBeforeAnimation('showOrderDetailsReview', function (page) {
            if ($rootScope.currentOpeningPage != 'showOrderDetailsReview') return;
            $rootScope.currentOpeningPage = 'showOrderDetailsReview';

            $scope.isLoaded = true;

            var orderDetails = page.query.orderDetails;
            $scope.orderDetails = orderDetails;
            if ($scope.orderDetails.houseAddress == '' || $scope.orderDetails.houseAddress == undefined || $scope.orderDetails.houseAddress == null) {
                $scope.showHouseAddress = false;
            }

            if ($scope.orderDetails.email == '' || $scope.orderDetails.email == undefined || $scope.orderDetails.email == null) {
                $scope.showUserEmail = false;
            }

            if ($scope.orderDetails.orderType == '' || $scope.orderDetails.orderType == undefined || $scope.orderDetails.orderType == null) {
                $scope.showOrderType = false;
            }
            var lang = localStorage.getItem('Dalal_lang');
            $scope.isArabic = lang == 'AR' || typeof lang == 'undefined' || lang == null ? true : false;

            if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {

                $scope.lblUserFullName = 'الاسم بالكامل';
                $scope.lblUserEmail = 'البريد الالكتروني';
                $scope.lblUserMobile = ' رقم الجوال';
                $scope.lblOrderType = ' نوع الطلب';
                $scope.lblOrderDetails = ' تفاصيل الطلب';
                $scope.lblLocationAreaName = 'اسم المنطقة';
                $scope.lblLocationStreetName = 'اسم الشارع';
                $scope.lblLocationHouseNumber = 'رقم المنزل';
                $scope.lblLocationAppartmentNumber = 'رقم الشقه';
                $scope.lblLocationHouseAddress = 'معلم قريب من المنزل';
            }
            else {
        
                $scope.lblUserFullName = 'Full Name';
                $scope.lblUserEmail = 'Email';
                $scope.lblUserMobile = 'Mobile Number';
                $scope.lblOrderType = 'Order Type ';
                $scope.lblOrderDetails = 'Order Details ';
                $scope.lblLocationAreaName = 'Area Name ';
                $scope.lblLocationStreetName = 'Street Name ';
                $scope.lblLocationHouseNumber = 'House Number';
                $scope.lblLocationAppartmentNumber = 'Appartment Number ';
                $scope.lblLocationHouseAddress = 'House Address';
            }
        });

        app.onPageReinit('showOrderDetailsReview', function (page) {
            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('showOrderDetailsReview', function (page) {
            if ($rootScope.currentOpeningPage != 'showOrderDetailsReview') return;
            $rootScope.currentOpeningPage = 'showOrderDetailsReview';

        });


        $scope.GoBack = function () {
            helpers.GoBack();
        };

        var btnCreateOrderRev = document.getElementById("btnCreateOrderRev");
        btnCreateOrderRev.addEventListener("touchend", btnIsTouched, false);

        function btnIsTouched(event) {
            $('#btnCreateOrderRev').prop("disabled", true);
            $scope.GoToSuccess();
            event.preventDefault();
            return false;
        }

        $scope.GoToSuccess = function () {
            var lang = localStorage.getItem('Dalal_lang');
                $scope.myOrder = {
                    "fullName": $scope.orderDetails.fullName,
                    "email": $scope.orderDetails.email,
                    "orderType": $scope.orderDetails.orderType,
                    "mobile": $scope.orderDetails.mobile,
                    "details": $scope.orderDetails.details,
                    "houseNumber" : $scope.orderDetails.houseNumber,
                    "appartmentNumber" : $scope.orderDetails.appartmentNumber,
                    "areaName" : $scope.orderDetails.areaName,
                    "streetName": $scope.orderDetails.streetName,
                    "houseAddress": $scope.orderDetails.houseAddress,
                };
                    var jsonData = [];
                    if (localStorage.getItem("data")) {
                        jsonData = JSON.parse(localStorage.getItem("data"));
                    }
                    jsonData.push({
                        order: $scope.myOrder
                    });
                    localStorage.setItem("data", JSON.stringify(jsonData));

            if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                var message = 'تم إرسال طلبك بنجاح';
                myApp.fw7.app.alert(message, 'نجاح', function () {
                    helpers.GoToPage('introReview', null);
                });
            }
            else {
                var message = 'Request has been sent successfully';
                myApp.fw7.app.alert(message, 'Success', function () {
                    helpers.GoToPage('introReview', null);
                });
            }
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('introReview', null);
        };

        app.init();
    });

}]);

