﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('membersReviewController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';

    $(document).ready(function () {
        app.onPageInit('membersReview', function (page) {
            if ($rootScope.currentOpeningPage != 'membersReview') return;
            $rootScope.currentOpeningPage = 'membersReview';

        });

        app.onPageBeforeAnimation('membersReview', function (page) {
            if ($rootScope.currentOpeningPage != 'membersReview') return;
            $rootScope.currentOpeningPage = 'membersReview';

            $scope.isLoaded = true;

        });

        app.onPageReinit('membersReview', function (page) {
            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('membersReview', function (page) {
            if ($rootScope.currentOpeningPage != 'membersReview') return;
            $rootScope.currentOpeningPage = 'membersReview';

        });


        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('introReview', null);
        };

        app.init();
    });

}]);

