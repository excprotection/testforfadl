﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('completeProfileController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', '$sce', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, $sce, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';
    var map;
    var latitude;
    var longitiude;
    var contact;
    var lang = localStorage.getItem('Dalal_lang');

    function GetStoredContract() {
        var contract = JSON.parse(CookieService.getCookie('StoredContract'));
        return contract;
    }

    function LoadCountries(callBack) {
        var countries = [];

        app.showIndicator();

        lang = localStorage.getItem('Dalal_lang');
        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            $('#linkCompleteProfileCountry .item-after').html('الدولة');
        }
        else {
            $('#linkCompleteProfileCountry .item-after').html('Country');
        }

        appServices.CallService('completeProfile', "GET", "api/Nationality/QuickAll", '', function (res) {
            if (res != null && res.length > 0) {
                $scope.countries = res;
                $$('#linkCompleteProfileCountry select').html('');

                lang = localStorage.getItem('Dalal_lang');
                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    app.smartSelectAddOption('#linkCompleteProfileCountry select', '<option value="" selected>الدولة</option>');
                }
                else {
                    app.smartSelectAddOption('#linkCompleteProfileCountry select', '<option value="" selected>Country</option>');
                }
                angular.forEach($scope.countries, function (country) {
                    app.smartSelectAddOption('#linkCompleteProfileCountry select', '<option value="' + country.key + '">' + country.value + '</option>');
                });
            }

            callBack(true);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    function LoadCities(callBack) {
        var cities = [];

        lang = localStorage.getItem('Dalal_lang');
        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            $('#linkCompleteProfileCity .item-after').html('المدينة');
        }
        else {
            $('#linkCompleteProfileCity .item-after').html('City');
        }

        appServices.CallService('completeProfile', "GET", "api/city/QuickAll", '', function (res) {
            if (res != null && res.length > 0) {
                $scope.cities = res;
                $$('#linkCompleteProfileCity select').html('');

                lang = localStorage.getItem('Dalal_lang');
                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    app.smartSelectAddOption('#linkCompleteProfileCity select', '<option value="" selected>المدينة</option>');
                }
                else {
                    app.smartSelectAddOption('#linkCompleteProfileCity select', '<option value="" selected>City</option>');
                }
                angular.forEach($scope.cities, function (city) {
                    app.smartSelectAddOption('#linkCompleteProfileCity select', '<option value="' + city.key + '">' + city.value + '</option>');
                });
            }

            callBack(true);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    function LoadGenders(callBack) {
        var genders = [];

        lang = localStorage.getItem('Dalal_lang');
        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
            $('#linkCompleteProfileGender .item-after').html('النوع');
        }
        else {
            $('#linkCompleteProfileGender .item-after').html('Gender');
        }

        appServices.CallService('completeProfile', "GET", "api/contact/Genders", '', function (res) {
            if (res != null && res.length > 0) {
                $scope.genders = res;
                $$('#linkCompleteProfileGender select').html('');

                lang = localStorage.getItem('Dalal_lang');
                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    app.smartSelectAddOption('#linkCompleteProfileGender select', '<option value="" selected>النوع</option>');
                }
                else {
                    app.smartSelectAddOption('#linkCompleteProfileGender select', '<option value="" selected>Gender</option>');
                }
                angular.forEach($scope.genders, function (gender) {
                    app.smartSelectAddOption('#linkCompleteProfileGender select', '<option value="' + gender.key + '">' + gender.value + '</option>');
                });
            }

            callBack(true);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    function LoadUserDetails(callBack) {
        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

        appServices.CallService('completeProfile', "POST", "api/account/CreateUserInCRMIFNotCreated?UserId="+userLoggedIn.id , '', function (res) {
            app.hideIndicator();
            appServices.CallService('completeProfile', "GET", "api/contact/GetDetails/" + userLoggedIn.crmUserId, '', function (res) {
                app.hideIndicator();
                if (res != null) {
                    contact = res.contact;
                    $scope.completeProfileForm.fullName = contact.fullName;
                    $scope.completeProfileForm.email = contact.email;

                    setTimeout(function () {
                        $scope.$apply();
                        callBack(true);
                    }, fw7.DelayBeforeScopeApply);
                }
                else {
                    callBack(null);
                }

            });
        });

        
    }


    $(document).ready(function () {
        app.onPageInit('completeProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'completeProfile') return;
            $rootScope.currentOpeningPage = 'completeProfile';

        });

        app.onPageReinit('completeProfile', function (page) {
            //$scope.resetForm();
        });

        app.onPageBeforeAnimation('completeProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'completeProfile') return;
            $rootScope.currentOpeningPage = 'completeProfile';

            $scope.isLoaded = true;

            var lang = localStorage.getItem('Dalal_lang');
            $scope.isArabic = lang == 'AR' || typeof lang == 'undefined' || lang == null ? true : false;

            $scope.resetForm();
            
            LoadCountries(function (result) {
                LoadCities(function (result1) {
                    LoadGenders(function (result2) {
                        LoadUserDetails(function (result3) { });
                    });
                });
            });

            $$('#linkCompleteProfileCountry select').on('change', function () {
                var countryId = $(this).val();
                var country = $scope.countries.filter(function (obj) {
                    return obj.key == countryId;
                })[0];

                $scope.formFields.country = country;
            });

            $$('#linkCompleteProfileCity select').on('change', function () {
                var cityId = $(this).val();
                var city = $scope.cities.filter(function (obj) {
                    return obj.key == cityId;
                })[0];

                $scope.formFields.city = city;
            });

            $$('#linkCompleteProfileGender select').on('change', function () {
                var genderId = $(this).val();
                var gender = $scope.genders.filter(function (obj) {
                    return obj.key == genderId;
                })[0];

                $scope.formFields.gender = gender;
            });

        });

        app.onPageReinit('completeProfile', function (page) {
            $rootScope.RemoveEditPagesFromHistory();
        });

        app.onPageAfterAnimation('completeProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'completeProfile') return;
            $rootScope.currentOpeningPage = 'completeProfile';

        });

        $scope.form = {};
        $scope.completeProfileForm = {};
        $scope.formFields = {};

        $scope.resetForm = function () {
            $scope.completeProfileReset = false;
            $scope.submittedCompleteProfile = false;

            $scope.isCountryValid = true;
            $scope.isCityValid = true;
            $scope.isGenderValid = true;
            $scope.isIdNumberCorrect = true;

            $scope.completeProfileForm.country = null;
            $scope.completeProfileForm.fullName = null;
            $scope.completeProfileForm.city = null;
            $scope.completeProfileForm.gender = null;
            $scope.completeProfileForm.email = null;
            $scope.completeProfileForm.idNumber = null;

            if (typeof $scope.CompleteProfileForm != 'undefined' && $scope.CompleteProfileForm != null) {
                $scope.CompleteProfileForm.$setPristine(true);
                $scope.CompleteProfileForm.$setUntouched();
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        }

        $scope.submitForm = function (CompleteProfileForm) {
            $scope.completeProfileReset = true;
            $scope.submittedCompleteProfile = true;

            var countryId = $('#linkCompleteProfileCountry select').val();
            var cityId = $('#linkCompleteProfileCity select').val();
            var GenderId = $('#linkCompleteProfileGender select').val();
            var email = $scope.completeProfileForm.email;
            var idNumber = $scope.completeProfileForm.idNumber;

            idNumber = helpers.parseArabic(idNumber);

            $scope.isCountryValid = $scope.completeProfileReset && $scope.submittedCompleteProfile && countryId != null && countryId != '' && countryId != ' ' ? true : false;
            $scope.isCityValid = $scope.completeProfileReset && $scope.submittedCompleteProfile && cityId != null && cityId != '' && cityId != ' ' ? true : false;
            $scope.isGenderValid = $scope.completeProfileReset && $scope.submittedCompleteProfile && GenderId != null && GenderId != '' && GenderId != ' ' ? true : false;

            var isIdNumberCorrect = false;

            if (countryId == '1e0ff838-292f-e311-b3fd-00155d010303') {
            	isIdNumberCorrect = idNumber != null && idNumber != '' && idNumber != ' ' && idNumber.startsWith('1') || idNumber.startsWith('١') == true ? true : false;
            }
            else {
            	isIdNumberCorrect = idNumber != null && idNumber != '' && idNumber != ' ' && idNumber.startsWith('2') || idNumber.startsWith('٢') == true ? true : false;
            }

            $scope.isIdNumberCorrect = isIdNumberCorrect;

            if ($scope.isCountryValid && $scope.isCityValid && $scope.isGenderValid && !CompleteProfileForm.email.$invalid && !CompleteProfileForm.idNumber.$invalid && isIdNumberCorrect) {
                

                var params = {
                    'ContactId': contact.contactId,
                    'MobilePhone': contact.mobilePhone,
                    'Email': contact.email,
                    'FullName': contact.fullName,
                    'CityId': cityId,
                    'NationalityId': countryId,
                    'GenderId': GenderId,
                    'IdNumber': idNumber
                };

                app.showIndicator();
                appServices.CallService('forgetPass', "POST", "api/contact/UpdateProfile_M", params, function (res) {
                    app.hideIndicator();
                    if (res != null) {
                        var storedContract = GetStoredContract();

                        storedContract.user = contact;
                        storedContract.user.country = $scope.formFields.country;
                        storedContract.user.city = $scope.formFields.city;
                        storedContract.user.gender = $scope.formFields.gender;
                        storedContract.user.email = $scope.completeProfileForm.email;
                        storedContract.user.idNumber = $scope.completeProfileForm.idNumber;

                        CookieService.setCookie('StoredContract', JSON.stringify(storedContract));
                        helpers.GoToPage('contractDetails', null);
                    }
                });

            }
        };



        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.GoToIntro = function () {
            helpers.GoToPage('intro', null);
        };

        $scope.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        $scope.nameRegex = /^[A-Za-z0-9]*$/;
        $scope.mobileRegex = /^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/;

        app.init();
    });

}]);

