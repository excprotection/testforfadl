﻿/// <reference path="../js/angular.js" />
/// <reference path="../js/framework7.js" />
/// <reference path="../js/jquery-2.1.0.js" />

var isAndroid = Framework7.prototype.device.android === true;
var isIos = Framework7.prototype.device.ios === true;

Template7.global = {
    android: isAndroid,
    ios: isIos
};

var $$ = Dom7;

if (isAndroid) {
    $$('.view.navbar-through').removeClass('navbar-through').addClass('navbar-fixed');
    $$('.view .navbar').prependTo('.view .page');
}

if (isIos) {
    $$('.view.navbar-fixed').removeClass('navbar-fixed').addClass('navbar-through');
    $$('.view .navbar').prependTo('.view .page');
}

var myApp = {};

myApp.config = {
};

var compareTo = function () {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function (scope, element, attributes, ngModel) {

            ngModel.$validators.compareTo = function (modelValue) {
                return modelValue == scope.otherModelValue;
            };

            scope.$watch("otherModelValue", function () {
                ngModel.$validate();
            });
        }
    };
};

myApp.angular = angular.module('myApp', ['ErrorCatcher', 'jkAngularRatingStars', 'ngSanitize']).config(function ($provide) {
    $provide.decorator("$exceptionHandler", function ($delegate, $injector, $log) {
        return function (exception, cause) {
            $log.error(exception)
            $delegate(exception, cause);
        };
    });
}).directive("limitTo", [function () {
    return {
        restrict: "A",
        link: function (scope, elem, attrs) {
            var limit = parseInt(attrs.limitTo);
            angular.element(elem).on("keypress", function (e) {
                if (this.value.length == limit) e.preventDefault();
            });
        }
    }
}]).directive('numbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModelCtrl) {
            function fromUser(text) {
                if (text) {
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    if (transformedInput !== text) {
                        ngModelCtrl.$setViewValue(transformedInput);
                        ngModelCtrl.$render();
                    }
                    return transformedInput;
                }
                return undefined;
            }
            ngModelCtrl.$parsers.push(fromUser);
        }
    };
}).directive('longTouch', function () {
    return {
        scope: {
            longTouch: "&",
            time: "@",
            clickEvent: "&",
        },
        link: function (scope, element, attrs) {
            scope.timepassed = false;
            scope.x = 0;
            scope.y = 0;
            scope.moved = false;
            element.on('touchstart', function (event) {
                scope.moved = false;
                scope.timepassed = false;
                if (event.originalEvent)
                    scope.x = event.originalEvent.touches[0].clientX;
                else
                    scope.x = event.touches[0].clientX;

                if (event.originalEvent)
                    scope.y = event.originalEvent.touches[0].clientY;
                else
                    scope.y = event.touches[0].clientY;
                var time = 600;
                if (isNaN(scope.time) == false) {
                    if (Number.isInteger(scope.time))
                        time = scope.time;
                    else
                        time = Number(scope.time);
                }

                setTimeout(function () {
                    console.log('settitmeout in touch start');
                    scope.timepassed = true;
                    scope.$apply();
                }, time);


            });
            element.on('touchcancel', function (event) {
                scope.timepassed = false;

            });
            element.on('touchmove', function (event) {
                var x = 0;
                var y = 0;
                if (event.originalEvent)
                    x = event.originalEvent.touches[0].clientX;
                else
                    x = event.touches[0].clientX;
                if (event.originalEvent)
                    y = event.originalEvent.touches[0].clientY;
                else
                    y = event.touches[0].clientY;
                if (x - scope.x > 10 || x - scope.x < -10 || y - scope.y < 10 || y - scope.y > 10) {
                    scope.timepassed = false;
                    scope.moved = true;
                }
            });
            element.on('touchend', function (event) {
                if (scope.timepassed && scope.longTouch) {
                    console.log('longTouch');
                    scope.longTouch();
                }
                else if (!scope.timepassed && scope.clickEvent && scope.moved == false) {
                    console.log('clickEvent');
                    scope.clickEvent();
                }

                console.log(' touch end');
                scope.timepassed = false;
            });
        }
    };
}).directive('stringToNumber', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModel) {
            ngModel.$parsers.push(function (value) {
                return '' + value;
            });
            ngModel.$formatters.push(function (value) {
                return parseFloat(value);
            });
        }
    };
}).directive('homePage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'homeController',
        scope: {},
        templateUrl: 'pages/home.html'
    };
}).directive('introPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'introController',
        scope: {},
        templateUrl: 'pages/intro.html'
    };
}).directive('landingPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'landingController',
        scope: {},
        templateUrl: 'pages/landing.html'
    };
}).directive('connectPage', function () {
    return {
        restrict: 'E',
        bindToController: false,
        controller: '',
        scope: {},
        templateUrl: 'pages/connect.html'
    };
}).directive('loginPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'loginController',
        scope: {},
        templateUrl: 'pages/login.html'
    };
}).directive('signupUserPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'signupUserController',
        scope: {},
        templateUrl: 'pages/signupUser.html'
    };
}).directive('forgetPassPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'forgetPasswordController',
        scope: {},
        templateUrl: 'pages/forgetPassword.html'
    };
}).directive('resetPasswordPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'resetPasswordController',
        scope: {},
        templateUrl: 'pages/resetPassword.html'
    };
}).directive('changePassPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'changePasswordController',
        scope: {},
        templateUrl: 'pages/changePassword.html'
    };
}).directive('userProfilePage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'userProfileController',
        scope: {},
        templateUrl: 'pages/userProfile.html'
    };
}).directive('editProfilePage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'editProfileController',
        scope: {},
        templateUrl: 'pages/editProfile.html'
    };
}).directive('noResultPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'noResultController',
        scope: {},
        templateUrl: 'pages/noResult.html'
    };
}).directive('searchPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'searchController',
        scope: {},
        templateUrl: 'pages/search.html'
    };
}).directive('termsPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'termsController',
        scope: {},
        templateUrl: 'pages/terms.html'
    };
}).directive('searchResultsPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'searchResultsController',
        scope: {},
        templateUrl: 'pages/searchResults.html'
    };
}).directive('aboutPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'aboutController',
        scope: {},
        templateUrl: 'pages/about.html'
    };
}).directive('medicalPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'medicalController',
        scope: {},
        templateUrl: 'pages/Medical.html'
    };
}).directive('soonPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'soonController',
        scope: {},
        templateUrl: 'pages/soon.html'
    };
}).directive('businessPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'businessController',
        scope: {},
        templateUrl: 'pages/business.html'
    };
}).directive('missionPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'missionController',
        scope: {},
        templateUrl: 'pages/mission.html'
    };
}).directive('membersPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'membersController',
        scope: {},
        templateUrl: 'pages/members.html'
    };
}).directive('branchesPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'branchesController',
        scope: {},
        templateUrl: 'pages/branches.html'
    };
}).directive('createPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'createController',
        scope: {},
        templateUrl: 'pages/create.html'
    };
}).directive('shiftsPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'shiftsController',
        scope: {},
        templateUrl: 'pages/shifts.html'
    };
}).directive('locationPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'locationController',
        scope: {},
        templateUrl: 'pages/location.html'
    };
}).directive('previousLocationsPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'previousLocationsController',
        scope: {},
        templateUrl: 'pages/previousLocations.html'
    };
}).directive('contractDetailsPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'contractDetailsController',
        scope: {},
        templateUrl: 'pages/contractDetails.html'
    };
}).directive('successPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'successController',
        scope: {},
        templateUrl: 'pages/success.html'
    };
}).directive('completeProfilePage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'completeProfileController',
        scope: {},
        templateUrl: 'pages/completeProfile.html'
    };
}).directive('userComplaintsPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'userComplaintsController',
        scope: {},
        templateUrl: 'pages/userComplaints.html'
    };
}).directive('userContractsPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'userContractsController',
        scope: {},
        templateUrl: 'pages/userContracts.html'
    };
}).directive('contractInfoPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'contractInfoController',
        scope: {},
        templateUrl: 'pages/contractInfo.html'
    };
}).directive('addComplaintPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'addComplaintController',
        scope: {},
        templateUrl: 'pages/addComplaint.html'
    };
}).directive('paymentMethodPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'paymentMethodController',
        scope: {},
        templateUrl: 'pages/paymentMethod.html'
    };
}).directive('bankTransferPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'bankTransferController',
        scope: {},
        templateUrl: 'pages/bankTransfer.html'
    };
}).directive('changeLanguagePage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'changeLanguageController',
        scope: {},
        templateUrl: 'pages/changeLanguage.html'
    };
}).directive('individualSectorPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'individualSectorController',
        scope: {},
        templateUrl: 'pages/individualSector.html'
    };
}).directive('serviceDetailsPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'serviceDetailsController',
        scope: {},
        templateUrl: 'pages/ServiceDetails.html'
    };
}).directive('indvContractDetailsPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'indvContractDetailsController',
        scope: {},
        templateUrl: 'pages/indvContractDetails.html'
    };
}).directive('indvLocationsPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'indvLocationsController',
        scope: {},
        templateUrl: 'pages/indvLocations.html'
    };
}).directive('indvPrevLocationsPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'indvPrevLocationsController',
        scope: {},
        templateUrl: 'pages/indvPrevLocations.html'
    };
}).directive('indvSearchPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'indvSearchController',
        scope: {},
        templateUrl: 'pages/indvSearch.html'
    };
}).directive('indvNationalitiesPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'indvNationalitiesController',
        scope: {},
        templateUrl: 'pages/indvNationalities.html'
    };
}).directive('indvPackagesPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'indvPackagesController',
        scope: {},
        templateUrl: 'pages/indvPackages.html'
    };
}).directive('indvWorkersPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'indvWorkersController',
        scope: {},
        templateUrl: 'pages/indvWorkers.html'
    };
}).directive('indvJobsPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'indvJobsController',
        scope: {},
        templateUrl: 'pages/indvJobs.html'
    };
}).directive('indvCustomerWorkPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'indvCustomerWorkController',
        scope: {},
        templateUrl: 'pages/indvCustomerWork.html'
    };
}).directive('indvCompleteProfilePage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'indvCompleteProfileController',
        scope: {},
        templateUrl: 'pages/indvCompleteProfile.html'
    };

}).directive('indvSuccessPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'indvSuccessController',
        scope: {},
        templateUrl: 'pages/indvSuccess.html'
    };
}).directive('indvBankTransferPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'indvBankTransferController',
        scope: {},
        templateUrl: 'pages/indvBankTransfer.html'
    };
}).directive('indvPaymentMethodPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'indvPaymentMethodController',
        scope: {},
        templateUrl: 'pages/indvPaymentMethod.html'
    };
}).directive('userContractsTypesPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'userContractsTypesController',
        scope: {},
        templateUrl: 'pages/userContractsTypes.html'
    };
}).directive('userIndvContractsPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'userIndvContractsController',
        scope: {},
        templateUrl: 'pages/userIndvContracts.html'
    };
}).directive('userIndvContractRequestsPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'userIndvContractRequestsController',
        scope: {},
        templateUrl: 'pages/userIndvContractRequests.html'
    };
}).directive('userInvoicesPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'userInvoicesController',
        scope: {},
        templateUrl: 'pages/userInvoices.html'
    };
}).directive('requestInfoPage', function () {
    return {
    	restrict: 'E',
        bindToController: true,
        controller: 'requestInfoNewController',
	    scope: {},              
	    templateUrl: 'pages/requestInfoNew.html'
    };
}).directive('indivContractInfoPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'indivContractInfoController',
        scope: {},
        templateUrl: 'pages/indivContractInfo.html'
    };
}).directive('indvEmployeeDetailsPage', function () {
	return {
		restrict: 'E',
		bindToController: true,
		controller: 'indvEmployeeDetailsController',
		scope: {},
		templateUrl: 'pages/indvEmployeeDetails.html'
	};
}).directive('indvSearchResultsPage', function () {
	return {
		restrict: 'E',
		bindToController: true,
		controller: 'indvSearchResultsController',
		scope: {},
		templateUrl: 'pages/indvSearchResults.html'
	};
}).directive('neqatyPage', function () {
	return {
		restrict: 'E',
		bindToController: true,
		controller: 'neqatyNewController',
		scope: {},
		templateUrl: 'pages/neqatyNew.html'
	};
}).directive('userPointsPage', function () {
	return {
		restrict: 'E',
		bindToController: true,
		controller: 'userPointsNewController',
		scope: {},
		templateUrl: 'pages/userPointsNew.html'
	};
}).directive('userConsumedPointsPage', function () {
	return {
		restrict: 'E',
		bindToController: true,
		controller: 'userConsumedPointsNewController',
		scope: {},
		templateUrl: 'pages/userConsumedPointsNew.html'
	};
}).directive('loginReviewPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'loginReviewController',
        scope: {},
        templateUrl: 'pages/loginReview.html'
    };
}).directive('signupUserReviewPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'signupUserReviewController',
        scope: {},
        templateUrl: 'pages/signupUserReview.html'
    };
}).directive('forgetPassReviewPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'forgetPasswordReviewController',
        scope: {},
        templateUrl: 'pages/forgetPasswordReview.html'
    };
}).directive('resetPasswordReviewPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'resetPasswordReviewController',
        scope: {},
        templateUrl: 'pages/resetPasswordReview.html'
    };
}).directive('changePassReviewPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'changePasswordReviewController',
        scope: {},
        templateUrl: 'pages/changePasswordReview.html'
    };
}).directive('introReviewPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'introReviewController',
        scope: {},
        templateUrl: 'pages/introReview.html'
    };
}).directive('aboutReviewPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'aboutReviewController',
        scope: {},
        templateUrl: 'pages/aboutReview.html'
    };
}).directive('medicalReviewPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'medicalReviewController',
        scope: {},
        templateUrl: 'pages/MedicalReview.html'
    };
}).directive('businessReviewPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'businessReviewController',
        scope: {},
        templateUrl: 'pages/businessReview.html'
    };
}).directive('membersReviewPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'membersReviewController',
        scope: {},
        templateUrl: 'pages/membersReview.html'
    };
}).directive('missionReviewPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'missionReviewController',
        scope: {},
        templateUrl: 'pages/missionReview.html'
    };
}).directive('branchesReviewPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'branchesReviewController',
        scope: {},
        templateUrl: 'pages/branchesReview.html'
    };
}).directive('changeLanguageReviewPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'changeLanguageReviewController',
        scope: {},
        templateUrl: 'pages/changeLanguageReview.html'
    };
}).directive('userProfileReviewPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'userProfileReviewController',
        scope: {},
        templateUrl: 'pages/userProfileReview.html'
    };
}).directive('individualSectorReviewPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'individualSectorReviewController',
        scope: {},
        templateUrl: 'pages/individualSectorReview.html'
    };
}).directive('addRequestReviewPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'AddRequestReviewController',
        scope: {},
        templateUrl: 'pages/addRequestReview.html'
    };
}).directive('locationReviewPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'locationReviewController',
        scope: {},
        templateUrl: 'pages/locationReview.html'
    };
}).directive('showOrderDetailsReviewPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'showOrderDetailsReviewController',
        scope: {},
        templateUrl: 'pages/showOrderDetailsReview.html'
    };
}).directive('myOrdersReviewPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'myOrdersReviewController',
        scope: {},
        templateUrl: 'pages/myOrdersReview.html'
    };
});

myApp.angular.directive('fallbackSrc', function () {
    var fallbackSrc = {
        link: function postLink(scope, iElement, iAttrs) {
            iElement.bind('error', function () {
                angular.element(this).attr("src", iAttrs.fallbackSrc);
            });
        }
    }
    return fallbackSrc;
});


myApp.fw7 = {
    app: new Framework7({
        swipeBackPage: false,
        swipePanel: false,
        panelsCloseByOutside: true,
        animateNavBackIcon: true,
        material: isAndroid ? true : false,
        materialRipple: false,
        modalButtonOk: 'ok',
        modalButtonCancel: 'Cancel'
    }),
    options: {
        dynamicNavbar: false,
        domCache: true
    },
    views: [],
    Countries: [],
    Cities: [],
    Categories: [],
    Advertisements: [],
    DelayBeforeScopeApply: 10,
    PageSize:50
};