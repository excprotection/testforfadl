﻿/// <reference path="../js/angular.js" />
/// <reference path="../js/framework7.js" />
/// <reference path="../js/jquery-2.2.4.js" />

//var serviceURL = 'http://test.abdal.sa:8003/';
//var hostUrl = 'http://test.abdal.sa/';

var serviceURL = 'http://crm.abdalrec.com:3325/';
var hostUrl = 'http://crm.abdalrec.com/';

var lang = 'AR';
var appToken = '';
var userId = 0;
userId = localStorage.getItem("UID");
var user;
var allCities = [];
var allCountries = [];
var allUsers = [];
var allAreas = [];
var BackIsClicked = true;
var scrollLoadsBefore = false;
var initLoginPage = true;
var initSignupPage = true;
var initactivationPage = true;
var initForgetPassword = true;
var initResetPassword = true;
var initChangePassword = true;
var initSearch = true;
var initSideMenu = true;
var clientId = 'consoleApp';
var clientSecret = '123@abc';
var initUserBlocked = false;
var  initDeviceback = true;
var messageInterval;
var myPhotoBrowserPopupDark;
var pusherEvents = {};


myApp.angular.factory('InitService', ['$http', '$document', '$log', 'helpers', function ($http, $document, $log, helpers) {
    'use strict';

    var pub = {},
      eventListeners = {
          'ready': []
      };

    pub.addEventListener = function (eventName, listener) {
        eventListeners[eventName].push(listener);
    };

    (function () {
        $(document).ready(function () {
            var fw7 = myApp.fw7;
            var i;

            $('#viewReview').hide();
            $('#viewCustomers').hide();
            $('#menuCustomers').hide();
            $('#menuReview').hide();

            fw7.app.showIndicator();

            $http({
                method: 'GET',
                url: 'https://geoip-db.com/json',
                data: '',
                async: true,
                crossDomain: true,
            }).then(
                function successCallback(response) {
                    fw7.app.hideIndicator();
                    var ip = response.data.IPv4;
                    var country = response.data.country_code;

                    if (country == 'SA' || country == 'sa') {
                        $('#viewCustomers').show();
                        $('#menuCustomers').show();
                        localStorage.setItem('Dalal_InSaudiArabia', true);
                        fw7.views.push(fw7.app.addView('.view-main', fw7.options));
                    }
                    else {
                        $('#viewReview').show();
                        $('#menuReview').show();
                        localStorage.setItem('Dalal_InSaudiArabia', false);
                        fw7.views.push(fw7.app.addView('.view-review', fw7.options));
                    }

                    for (i = 0; i < eventListeners.ready.length; i = i + 1) {
                        eventListeners.ready[i]();
                    }
                },
                function errorCallback(error) {
                    fw7.app.hideIndicator();
                    var error = error;
                    fw7.views.push(fw7.app.addView('.view-main', fw7.options));

                    for (i = 0; i < eventListeners.ready.length; i = i + 1) {
                        eventListeners.ready[i]();
                    }
                });

           
        });
       
        window.document.addEventListener('backbutton', function (event) {
            var fw7 = myApp.fw7;
            var currentPage = fw7.views[0].activePage.name;

            fw7.app.hideIndicator();

            for (var i = 0; i < fw7.views[0].history.length; i++) {
                if (fw7.views[0].history[i] === '#connect' || fw7.views[0].history[i] === '#paymentMethod') fw7.views[0].history.splice(i, 1);
            }

            if (currentPage == 'connect' || currentPage == 'success' ||
                (currentPage == 'userContracts' && fw7.views[0].activePage.fromPage.name == 'paymentMethod')) return false;

            if (currentPage == 'indvSuccess' || (currentPage =='indvPaymentMethod' && fw7.views[0].activePage.fromPage.name =='requestInfo'  )) return false;
            if (currentPage == 'indvPaymentMethod' && fw7.views[0].activePage.fromPage.name == 'indvSuccess') return false;

            if (currentPage == 'home' || currentPage == 'login') {
                fw7.app.closePanel();
                if (BackIsClicked) {
                    BackIsClicked = false;
                    ExitApplication();
                }
                else {
                    BackIsClicked = true;
                    fw7.app.closeModal('.modal-in');
                }

            }
            else {
                fw7.app.closePanel();
                if ($('.modal-in').length > 0) {
                    fw7.app.closeModal('.modal-in');
                    return false;
                }
                else {
                    if (currentPage == 'activation') {
                        return false;
                    }

                    fw7.views[0].router.back();

                }
            }

        });

        window.document.addEventListener('pause', function (event) {

        });

        window.document.addEventListener('resume', function (event) {
            //var fw7 = myApp.fw7;
            //var currentPage = fw7.views[0].activePage.name;

            //if (isOffline == true) {
            //    fw7.views[0].router.loadPage({ pageName: 'connect' });
            //}
            //else {
            //    if (currentPage != 'editProfile' && currentPage != 'addAdvertisement') {
            //        fw7.views[0].router.loadPage({ pageName: 'home' });
            //    }
            //}

            
        });

        window.addEventListener('native.keyboardhide', keyboardHideHandler);
        window.addEventListener('native.keyboardshow', keyboardShowHandler);

        function ExitApplication() {
            if (navigator.app) {
                navigator.app.exitApp();
            }
            else if (navigator.device) {
                navigator.device.exitApp();
            }
        }

        document.addEventListener("offline", onOffline, false);
        document.addEventListener("online", onOnline, false);
        window.addEventListener('native.keyboardhide', keyboardHideHandler);
        window.addEventListener('native.keyboardshow', keyboardShowHandler);

        var isOffline = false;

        function onOffline() {
            var fw7 = myApp.fw7;
            var currentPage = fw7.views[0].activePage.name;

            isOffline = true;

            fw7.views[0].router.loadPage({ pageName: 'connect' });
        }

        function onOnline() {
            var fw7 = myApp.fw7;

            if (isOffline == true) {
                isOffline = false;
                //navigator.splashscreen.show();
                fw7.views[0].router.back();
                //fw7.views[0].router.loadPage({ pageName: 'home' });
            }
        }

        function keyboardHideHandler(e) {
            if ($('.ui-footer').hasClass('ui-fixed-hidden')) {
                $('.ui-footer').removeClass('ui-fixed-hidden');
            }
        }

        function keyboardShowHandler(e) {
            $('.ui-footer').addClass('ui-fixed-hidden');
        }


    }());

    return pub;

}]);
