﻿/// <reference path="../js/angular.js" />

(function () {

    myApp.angular.factory('appServices', ['$http', '$rootScope', '$log', 'helpers', 'CookieService', '$exceptionHandler', '$interval', function ($http, $rootScope, $log, helpers, CookieService, $exceptionHandler, $interval) {
        'use strict';

        function toCamel(o) {
            var newO, origKey, newKey, value
            if (o instanceof Array) {
                return o.map(function (value) {
                    if (typeof value === "object") {
                        value = toCamel(value)
                    }
                    return value
                })
            } else {
                newO = {}
                for (origKey in o) {
                    if (o.hasOwnProperty(origKey)) {
                        newKey = (origKey.charAt(0).toLowerCase() + origKey.slice(1) || origKey).toString()
                        value = o[origKey]
                        if (value instanceof Array || (value !== null && value.constructor === Object)) {
                            value = toCamel(value)
                        }
                        newO[newKey] = value
                    }
                }
            }
            return newO
        }

        function GetSignature(url, key) {
            var x = url.split('/').length;
            var y = url.split('?').length;

            x = parseFloat(x);
            y = parseFloat(y);
            key = parseFloat(key);

            var portion1 = 2 * key;
            portion1 = portion1 + x;
            portion1 = portion1 + y;
            portion1 = portion1 + 8;

            var portion2 = key + x;
            portion2 = portion2 - y;
            portion2 = portion2 - 2;

            var portion3 = portion1 + portion2;
            portion3 = portion3 + 5;

            var portion4 = x + y;

            var sign = portion3 + portion4;

            //var sign = (((((((2 * key * x) + y) + 8) * (((key + x) - y) - 2)) * key) + 5) * (x + y));
            return sign.toString();
        }

        var CallService = function (pageName, CallType, MethodName, dataVariables, callBack) {
            var counter = 0;
            var options = { dimBackground: true };

            if (!$rootScope.load) {
                $rootScope.load = false;
                if ((pageName == 'home' && MethodName.indexOf('api/Country/GetCountries') > -1) || pageName != 'home') {
                }
                else {
                    $rootScope.load = false;
                }
            }

            if (messageInterval) {
                helpers.clearTimer();
            }

            var lang = localStorage.getItem('Dalal_lang');


            if (MethodName.indexOf('api/Payment/SystemicBankTransfer') == -1 && MethodName.indexOf('api/v1/profile/update') == -1 || MethodName.indexOf('api/v1/advertisement/update') == -1 ||
                MethodName.indexOf('api/v1/advertisement/create') == -1) {
                messageInterval = $interval(function () {
                    counter++;
                    if (counter == 600) {
                        helpers.clearTimer();
                        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                            language.openFrameworkModal('تنبيه', 'الخدمة غير متوفرة الآن بسبب بطء الإتصال بالإنترنت,من فضلك أعد المحاولة مرة أخري .', 'alert', function () {
                            });
                        }
                        else {
                            language.openFrameworkModal('Error', 'Service Is Not Available Now Because Internet Is Too Slow , Please Retry Again.', 'alert', function () { });
                        }
                        
                        myApp.fw7.views[0].router.loadPage({ pageName: 'connect' });
                    }
                }, 1000);
            }
           
            var contentType = "application/json";
            var token = CookieService.getCookie('appToken');
            var deviceId = CookieService.getCookie('deviceId');

            if (MethodName.indexOf('api/account/CreateUserInCRMIFNotCreated') > -1) {
                contentType = "application/x-www-form-urlencoded";
            }

            if (dataVariables != '' && dataVariables != null) {
                if (typeof deviceId != 'undefined' && deviceId != null) {
                    dataVariables.playerId = deviceId;
                }

                if (typeof token != 'undefined' && token != null && MethodName.indexOf('api/account/login') == -1) {
                    dataVariables.api_token = token;
                }

                dataVariables = JSON.stringify(dataVariables);
            }
            else {
                if (typeof deviceId != 'undefined' && deviceId != null) {
                    dataVariables = { 'playerId': deviceId };
                }
                if (typeof token != 'undefined' && token != null && MethodName.indexOf('api/v1/login') == -1) {
                    dataVariables = { 'api_token': token };
                }
                dataVariables = dataVariables;
            }

            var lang = localStorage.getItem('Dalal_lang');

            var languageToCall = lang == 'AR' || lang == null || typeof lang == 'undefined' ? "ar/" : "en/";

            var apikey = "UGFzc05BU0FQSUBOYXNBUElVc2VyMTIzQFBhc3M6TmFzQVBJVXNlcjEyM0B1c2Vy#";
            var timex = Math.floor(1000 + Math.random() * 9000);
            var sign = GetSignature(serviceURL + languageToCall + MethodName, timex);

            var versionNumber = '5.0.31';
            if (typeof device != 'undefined' && device != null && device.platform.toLowerCase() == 'ios') {
                var versionNumber = '6.0.22';
            }
            cordova.getAppVersion.getVersionNumber(function (version) {
                console.log('Version is : ' + version);
                if (device.platform.toLowerCase() == "android" && typeof version != 'undefined' && version != null) {
                    versionNumber = version;
                }
                else if (device.platform.toLowerCase() == "ios") {
                    if (typeof version != 'undefined' && version != null) {
                        versionNumber = version;
                    }
                }

                var headers = {
                    "content-type": contentType,
                    "cache-control": "no-cache",
                    "Authorization": "bearer " + token,
                    "TimeX": timex,
                    "SignAuth": apikey + sign,
                    "source": 1,
                    'platform': device.platform,
                    'version': versionNumber
                };

                localStorage.setItem('Dalal_ServiceHeaders', JSON.stringify(headers));

                $http({
                    method: CallType,
                    url: serviceURL + languageToCall + MethodName,
                    headers: {
                        "content-type": contentType,
                        "cache-control": "no-cache",
                        "Authorization": "bearer " + token,
                        "TimeX": timex,
                        "SignAuth": apikey + sign,
                        "source": 1,
                        'platform': device.platform,
                        'version': versionNumber
                    },
                    data: dataVariables,
                    async: true,
                    crossDomain: true,
                }).then(
            function successCallback(response) {
                helpers.clearTimer();


                if (pageName == "terms") {
                    return callBack(response.data);
                }

                if (typeof response.data != 'undefined' && response.data != null && (response.data.state == true || (response.data.state == null && response.data.length > 0))) {
                    if (typeof response.data.data != 'undefined' && response.data.data != null) {
                        if (typeof response.data.data.Errors != 'undefined' && response.data.data.Errors != null && response.data.data.Errors.length > 0) {
                            language.openFrameworkModal('خطأ', response.data.data.Errors[0], 'alert', function () { });
                            callBack(null);
                        }
                        else {
                            if (typeof response.data.data == 'string'||typeof response.data.data === "boolean")
                        		callBack(response.data.data);
                            else
                                
                        		callBack(toCamel(response.data.data));
                        }
                    }
                    else if (typeof response.data != 'undefined' && response.data != null) {
                        callBack(toCamel(response.data));
                    }
                    else if (typeof response.data != 'undefined' && response.data != null && typeof response.data.message != 'undefined' && response.data.message != null) {
                        callBack(toCamel(response.data.message));
                    }
                    else if (typeof response.data.message.email != 'undefined' && response.data.message.email != null && response.data.message.email != '' && response.data.message.email.length > 0) {
                        language.openFrameworkModal('خطأ', response.data.message.email[0], 'alert', function () { });
                        callBack(true);
                    }
                    else {
                        if (response.data.message == 'Reset Code is invalid.') {
                            if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                                language.openFrameworkModal('خطأ', 'الكود غير صحيح', 'alert', function () { });
                            }
                            else {
                                language.openFrameworkModal('Error', 'Code Is Incorrect.', 'alert', function () { });
                            }

                        }
                        else if (response.data.message == 'Old Password is incorrect') {
                            if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                                language.openFrameworkModal('خطأ', 'كلمة السر القديمة غير صحيحة', 'alert', function () { });
                            }
                            else {
                                language.openFrameworkModal('Error', 'Old Password Is Incorrect.', 'alert', function () { });
                            }

                        }
                        else {
                            language.openFrameworkModal('خطأ', response.data.message, 'alert', function () { });
                        }
                        callBack(true);
                    }
                }
                else if (typeof response.data != 'undefined' && response.data != null && typeof response.data.user != 'undefined' && response.data.user != null) {
                    callBack(toCamel(response.data));
                }
                else {
                    if (response.data.token != 'undefined' && response.data.token != null) {
                        var result = JSON.parse(response.data.token);
                        if (result.error_description == 'The user name or password is incorrect.') {
                            if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                                language.openFrameworkModal('خطأ', 'رقم الجوال أو كلمة السر خاطئين', 'alert', function () { });
                            }
                            else {
                               language.openFrameworkModal('خطأ', result.error_description, 'alert', function () { });
                            }
                            
                            callBack(null);
                        }
                        else {
                            if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                                language.openFrameworkModal('خطأ', 'خطأ في الخدمة', 'alert', function () { });
                            }
                            else {
                                language.openFrameworkModal('Error', 'Error In Service', 'alert', function () { });
                            }

                            callBack(null);
                        }
                    }
                    else if (typeof response.data != 'undefined' && response.data != null && typeof response.data.data != 'undefined' && typeof response.data.data.message != 'undefined' && response.data.data.message != null) {
                        if (response.data.data.message == 'the account is not confirmed') {
                            callBack('notActive', response.data.data.user, response.data.data.code.code);
                        }
                        else if (response.data.data.message == 'رقم الهوية مدخل من قبل') {
                            if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                                language.openFrameworkModal('خطأ', 'رقم الهوية مدخل من قبل', 'alert', function () { });
                            }
                            else {
                                language.openFrameworkModal('Error', 'Id Number Is Saved Before.', 'alert', function () { });
                            }

                            callBack(null);
                        }
                        else if (response.data.data.message == 'Contact not found') {
                            if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                                language.openFrameworkModal('خطأ', 'لا يوجد بيانات حالية مسجلة لدينا لهذا العميل', 'alert', function () { });
                            }
                            else {
                                language.openFrameworkModal('Error', 'Contact is not found or some data is missing.', 'alert', function () { });
                            }

                            callBack(null);
                        }
                        else if (response.data.data.message == 'هذا العميل لديه طلبـات سابقة') {
                            if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                                language.openFrameworkModal('خطأ', 'رقم الجوال مسجل به طلبين أو أكثر بتاريخ أقل من يومين', 'alert', function () { });
                            }
                            else {
                                language.openFrameworkModal('Error', 'This Phone Number Has Already Two Recent Requests With Creation Dates Less Than Two Days.', 'alert', function () { });
                            }

                            callBack(null);
                        }
                        else if (response.data.data.message == 'No locations') {
                            //if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                            //    language.openFrameworkModal('خطأ', 'لا يوجد مواقع سابقة', 'alert', function () { });
                            //}
                            //else {
                            //    language.openFrameworkModal('Error', 'There Are No Previous Locations.', 'alert', function () { });
                            //}

                            callBack(null);
                        }
                        else {
                            language.openFrameworkModal('خطأ', response.data, 'alert', function () { });
                            callBack(null);
                        }
                    }
                    else if (typeof response.data != 'undefined' && response.data != null) {
                        if (response.data.data.indexOf('is already taken.') > -1) {
                            if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                                language.openFrameworkModal('خطأ', 'رقم الجوال مستخدم من قبل', 'alert', function () { });
                            }
                            else {
                                language.openFrameworkModal('Error', 'Mobile Phone Is Registered Before', 'alert', function () { });
                            }
                        }
                        else {
                            language.openFrameworkModal('خطأ', response.data.data, 'alert', function () { });
                        }
                        callBack(null);
                    }
                    else {
                        if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                            language.openFrameworkModal('خطأ', 'خطأ في الخدمة', 'alert', function () { });
                        }
                        else {
                            language.openFrameworkModal('Error', 'Error In Service', 'alert', function () { });
                        }
                        callBack(null);
                    }
                }
            },
            function errorCallback(error) {
                var error = error;
               if (error.status == 401) {
                    if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                        language.openFrameworkModal('خطأ', 'أنت غير مصرح لك بالدخول أو إستخدام التطبيق بسبب إنتهاء صلاحية كود التحقق الخاص بك , من فضلك أعد تسجيل دخولك مرة أخري', 'alert', function () { });
                    }
                    else {
                        language.openFrameworkModal('Error', 'Your Token Has Expired Or You Are Not Authorized To Access , Please Login Again', 'alert', function () { });
                    }

                    CookieService.removeCookie('appToken');
                    CookieService.removeCookie('USName');
                    CookieService.removeCookie('userLoggedIn');
                    CookieService.removeCookie('deviceId');
                    CookieService.setCookie('Visitor', true);
                    helpers.GoToPage('login', null);
                }
                else if (error.status == -1) {
                    callBack(null);
                }
                else if (error.status == 500) {
                    if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                        language.openFrameworkModal('خطأ', 'خطأ في الخدمة , من فضلك تواصل مع فريق العمل', 'alert', function () { });
                    }
                    else {
                        language.openFrameworkModal('Error', 'Internal Server Error , Please Contact Application Support Team', 'alert', function () { });
                    }
                    callBack(null);
                }
                else if (error.status == 400) {
                	var message = error.data.message;
                	if (message == 'The request is invalid.') {
                		if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                			language.openFrameworkModal('خطأ', 'البيانات غير صحيحة .', 'alert', function () { });
							  callBack(null);
                		}
                		else {
                			language.openFrameworkModal('Error', 'Data Is Incorrect.', 'alert', function () { });
                			callBack(null);
                		}
                	}
                    if (error.data.data.new_phone != 'undefined' && error.data.data.new_phone != null && error.data.data.new_phone.length > 0) {
                        if (error.data.data.new_phone[0] == 'the new phone has been token.') {
                            callBack('mobileExistBefore');
                        }
                        else {
                            language.openFrameworkModal('خطأ', message, 'alert', function () { });
                            callBack(null);
                        }
                    }
                    else {
                        if (typeof message != 'undefined' && message != null) {
                            if (message == 'your phone is not correct.') {
                                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                                    language.openFrameworkModal('خطأ', 'رقم الجوال الذي أدخلته غير صحيح .', 'alert', function () { });
                                }
                                else {
                                    language.openFrameworkModal('Error', 'Mobile Number You Entered Is Incorrect.', 'alert', function () { });
                                }
                            }
                            else {
                                language.openFrameworkModal('خطأ', message, 'alert', function () { });
                            }
                        }
                    }
                }
                callBack(null);
            });
            });


            
        }

        return {
            CallService: CallService
        }
    }]);

}());


