﻿var language = {
	ChangeLanguage: function () {
		var lang = localStorage.getItem('Dalal_lang');
		var mainCssId = 'mainCssFile';
		var styleCssId = 'styleCssFile';
		var loaderCssId = 'loaderCssFile';
		var frameworkCssId = 'fwMaterialCssFile';
		var frameworkRTLCssId = 'fwMaterialRTLCssFile';
		var fwMaterialColorsCssId = 'fwMaterialColorsCssFile';
		var head = document.getElementsByTagName('head')[0];
		var mainCssElement = document.getElementById(mainCssId);
		var styleCssElement = document.getElementById(styleCssId);
		var loaderCssElement = document.getElementById(loaderCssId);
		var framework7Element = document.getElementById(frameworkCssId);
		var framework7RTLElement = document.getElementById(frameworkRTLCssId);
		var framework7ColorElement = document.getElementById(fwMaterialColorsCssId);

		if (lang == 'AR' || lang == null || typeof lang == 'undefined') {
			lang = 'AR';

			$('body').css('direction', 'rtl');
			$('html').css('direction', 'rtl !important');
			$('html').attr('dir', 'rtl');

			$('.open-panel').attr('data-panel', 'right');
			$('#divSidePanel').attr('class', 'panel panel-right panel-cover');

			if (framework7Element) {
				framework7Element.parentNode.removeChild(framework7Element);
			}

			if (framework7RTLElement) {
				framework7RTLElement.parentNode.removeChild(framework7RTLElement);
			}

			if (framework7ColorElement) {
				framework7ColorElement.parentNode.removeChild(framework7ColorElement);
			}

			if (mainCssElement) {
				mainCssElement.parentNode.removeChild(mainCssElement);
			}

			if (styleCssElement) {
				styleCssElement.parentNode.removeChild(styleCssElement);
			}

			if (loaderCssElement) {
				loaderCssElement.parentNode.removeChild(loaderCssElement);
			}

			var link = document.createElement('link');
			link.id = frameworkCssId;
			link.rel = 'stylesheet';
			link.type = 'text/css';
			link.href = 'css/framework7.material.min.css';
			head.appendChild(link);

			var link = document.createElement('link');
			link.id = fwMaterialColorsCssId;
			link.rel = 'stylesheet';
			link.type = 'text/css';
			link.href = 'css/framework7.material.colors.min.css';
			head.appendChild(link);

			var link = document.createElement('link');
			link.id = frameworkRTLCssId;
			link.rel = 'stylesheet';
			link.type = 'text/css';
			link.href = 'css/framework7.material.rtl.min.css';
			head.appendChild(link);

			var link = document.createElement('link');
			link.id = styleCssId;
			link.rel = 'stylesheet';
			link.type = 'text/css';
			link.href = 'css/style-ar.css';
			head.appendChild(link);

			var link = document.createElement('link');
			link.id = mainCssId;
			link.rel = 'stylesheet';
			link.type = 'text/css';
			link.href = 'css/Main.css';
			head.appendChild(link);

			var link = document.createElement('link');
			link.id = loaderCssId;
			link.rel = 'stylesheet';
			link.type = 'text/css';
			link.href = 'css/loader.css';
			head.appendChild(link);

		}
		else {
			lang = 'EN';

			$('body').css('direction', 'ltr');
			$('html').css('direction', 'ltr !important');
			$('html').attr('dir', 'ltr');

			$('.open-panel').attr('data-panel', 'left');
			$('#divSidePanel').attr('class', 'panel panel-left panel-cover');

			if (framework7Element) {
				framework7Element.parentNode.removeChild(framework7Element);
			}

			if (framework7RTLElement) {
				framework7RTLElement.parentNode.removeChild(framework7RTLElement);
			}

			if (framework7ColorElement) {
				framework7ColorElement.parentNode.removeChild(framework7ColorElement);
			}

			if (mainCssElement) {
				mainCssElement.parentNode.removeChild(mainCssElement);
			}

			if (styleCssElement) {
				styleCssElement.parentNode.removeChild(styleCssElement);
			}

			if (loaderCssElement) {
				loaderCssElement.parentNode.removeChild(loaderCssElement);
			}

			var link = document.createElement('link');
			link.id = frameworkCssId;
			link.rel = 'stylesheet';
			link.type = 'text/css';
			link.href = 'css/framework7.material.min.css';
			head.appendChild(link);

			var link = document.createElement('link');
			link.id = fwMaterialColorsCssId;
			link.rel = 'stylesheet';
			link.type = 'text/css';
			link.href = 'css/framework7.material.colors.min.css';
			head.appendChild(link);

			//var link = document.createElement('link');
			//link.id = framework7RTLElement;
			//link.rel = 'stylesheet';
			//link.type = 'text/css';
			//link.href = 'css/framework7.material.rtl.min.css';
			//head.appendChild(link);

			var link = document.createElement('link');
			link.id = styleCssId;
			link.rel = 'stylesheet';
			link.type = 'text/css';
			link.href = 'css/style.css';
			head.appendChild(link);

			var link = document.createElement('link');
			link.id = mainCssId;
			link.rel = 'stylesheet';
			link.type = 'text/css';
			link.href = 'css/Main.css';
			head.appendChild(link);

			var link = document.createElement('link');
			link.id = loaderCssId;
			link.rel = 'stylesheet';
			link.type = 'text/css';
			link.href = 'css/loader.css';
			head.appendChild(link);

		}

		$('.smart-select').each(function (index) {
			if (lang == 'AR' || lang == null || typeof lang == 'undefined') {
				$(this).attr('data-picker-close-text', 'تم');
				$(this).attr('data-searchbar-placeholder', 'بحث عن حي');
			}
			else {
				$(this).attr('data-picker-close-text', 'Close');
				$(this).attr('data-searchbar-placeholder', 'Search Areas');
			}
		});

		$('.modal-button').each(function (index) {
			if (lang == 'AR' || lang == null || typeof lang == 'undefined') {
				$(this).html('تم');
			}
			else {
				$(this).html('Close');
			}
		});

		$('.close-picker').each(function (index) {
			if (lang == 'AR' || lang == null || typeof lang == 'undefined') {
				$(this).text('تم');
			}
			else {
				$(this).text('Close');
			}
		});

		if (lang == 'AR' || lang == null || typeof lang == 'undefined') {
			$('.ion-android-arrow-forward').each(function (index) {
				$(this).removeClass('ion-android-arrow-forward');
				$(this).addClass('ion-android-arrow-back');
			});
		}
		else {
			$('.ion-android-arrow-back').each(function (index) {
				$(this).removeClass('ion-android-arrow-back');
				$(this).addClass('ion-android-arrow-forward');
			});
		}

		$('.lang').each(function (index) {
			var propType = this.nodeName;
			var element = $(this);
			var options = {};
			if (propType === 'INPUT') {
				var txt = $(this).attr('placeholder').trim();
				if (typeof txt != 'undefined' && txt != null && txt != '' && txt != ' ') {
					var options = {
						Language: lang,
						value: txt
					};
					changeControlText(options, function (translatedValue) {
						$(element).attr('placeholder', translatedValue);
					});
				}
			}
			else {
				var txt = $(this).text().trim();
				if (typeof txt != 'undefined' && txt != null && txt != '' && txt != ' ') {
					var options = {
						Language: lang,
						value: txt
					};
					changeControlText(options, function (translatedValue) {
						propType = this.nodeName;
						$(element).html(translatedValue);
					});
				}
			}
		});
	},
	openFrameworkModal: function (title, text, type, callBack) {
		changeModalText(title, text, function (titleTranslated, textTranslated) {
			var fw7 = myApp.fw7;
			var app = myApp.fw7.app;

			lang = localStorage.getItem('Dalal_lang');

			//if (lang == 'AR' || lang == null || typeof lang == 'undefined') {
			//    app = new Framework7({
			//        swipeBackPage: false,
			//        swipePanel: false,
			//        panelsCloseByOutside: true,
			//        animateNavBackIcon: true,
			//        material: isAndroid ? true : false,
			//        materialRipple: false,
			//        modalButtonOk: 'تم',
			//        modalButtonCancel: 'إلغاء'
			//    });
			//}
			//else {
			//    app = new Framework7({
			//        swipeBackPage: false,
			//        swipePanel: false,
			//        panelsCloseByOutside: true,
			//        animateNavBackIcon: true,
			//        material: isAndroid ? true : false,
			//        materialRipple: false,
			//        modalButtonOk: 'OK',
			//        modalButtonCancel: 'Cancel'
			//    });
			//}

			if (type.toLowerCase() == 'alert') {
				//app.alert(textTranslated, titleTranslated, function () {
				//    callBack(true);
				//});

				if (window.plugins != null && textTranslated != null && textTranslated != '' && textTranslated != undefined) {

					app.alert(textTranslated, titleTranslated, function () {
						callBack(true);
					});

					//window.plugins.toast.showWithOptions({
					//    message: textTranslated,
					//    duration: "short",
					//    position: "bottom",
					//    styling: {
					//        opacity: 0.8,
					//        backgroundColor: '#293955',
					//        textColor: '#FFFFFF',
					//        textSize: 15,
					//        cornerRadius: 20,
					//        horizontalPadding: 20,
					//        verticalPadding: 16
					//    }
					//},
					//function () {
					//    callBack(true);
					//},
					//function () {
					//    callBack(false);
					//});
				}
			}
			else {
				app.confirm(textTranslated, titleTranslated, function () {
					callBack(true);
				});
			}
		});
	},
	alert: function (message) {
		window.plugins.toast.showLongBottom(message, function (a) {
			console.log('Toast success: ' + a);
		}, function (b) {
			console.log('Toast error: ' + b);
		});
	},
	ChangeTheme: function () {
		var fwMaterialCssId = 'fwMaterialCssFile';
		var fwMaterialColorsCssId = 'fwMaterialColorsCssFile';
		var fwRTLCssId = 'fwRTLCssFile';
		var fwIOSCssId = 'fwIOSCssFile';
		var fwIOSColorsCssId = 'fwIOSColorsCssFile';
		var fwIOSRTLCssId = 'fwIOSRTLCssFile';
		var ionicFontsCssId = 'ionicFontsCssFile';
		var rateYoCssId = 'rateYoCssFile';
		var fwMaterialCssIdElement = document.getElementById(fwMaterialCssId);
		var fwMaterialColorsCssIdElement = document.getElementById(fwMaterialColorsCssId);
		var fwRTLCssIdElement = document.getElementById(fwRTLCssId);

		var fwIOSCssIdElement = document.getElementById(fwIOSCssId);
		var fwIOSColorsCssIdElement = document.getElementById(fwIOSColorsCssId);
		var fwIOSRTLCssIdElement = document.getElementById(fwIOSRTLCssId);

		var ionicFontsCssIdElement = document.getElementById(ionicFontsCssId);
		var rateYoCssIdElement = document.getElementById(rateYoCssId);

		if (fwMaterialCssIdElement) {
			fwMaterialCssIdElement.parentNode.removeChild(fwMaterialCssIdElement);
		}
		if (fwMaterialColorsCssIdElement) {
			fwMaterialColorsCssIdElement.parentNode.removeChild(fwMaterialColorsCssIdElement);
		}
		if (fwRTLCssIdElement) {
			fwRTLCssIdElement.parentNode.removeChild(fwRTLCssIdElement);
		}

		if (fwIOSCssIdElement) {
			fwIOSCssIdElement.parentNode.removeChild(fwIOSCssIdElement);
		}
		if (fwIOSColorsCssIdElement) {
			fwIOSColorsCssIdElement.parentNode.removeChild(fwIOSColorsCssIdElement);
		}
		if (fwIOSRTLCssIdElement) {
			fwIOSRTLCssIdElement.parentNode.removeChild(fwIOSRTLCssIdElement);
		}


		if (ionicFontsCssIdElement) {
			ionicFontsCssIdElement.parentNode.removeChild(ionicFontsCssIdElement);
		}
		if (rateYoCssIdElement) {
			rateYoCssIdElement.parentNode.removeChild(rateYoCssIdElement);
		}

		if (device.platform == 'Android' || device.platform == 'android') {
			Dom7('head').append(
                '<link rel="stylesheet" href="css/framework7.material.min.css">' +
                '<link rel="stylesheet" href="css/framework7.material.colors.min.css">' +
                '<link rel="stylesheet" href="css/framework7.material.rtl.min.css">'
            );
		}
		else {
			Dom7('head').append(
                '<link rel="stylesheet" href="css/framework7.material.min.css">' +
                '<link rel="stylesheet" href="css/framework7.material.colors.min.css">' +
                '<link rel="stylesheet" href="css/framework7.material.rtl.min.css">'
            );
		}
	}
};

function changeModalText(title, text, callBack) {
	var titleTranslated = 'none';
	var textTranslated = 'none';
	var lang = localStorage.getItem('Dalal_lang');
	if (lang == 'AR' || lang == null || typeof lang == 'undefined') {
		lang = 'AR';
	}
	else {
		lang = 'EN';
	}
	var options = {
		Language: lang,
		value: title
	};
	changeControlText(options, function (translatedValue) {
		title = translatedValue;
		callBack(title, text);
	});


}

function changeControlText(options, callBack) {
	var language = options.Language;
	var value = options.value;

	GetAllControls(language, value, function (result) {
		callBack(result);
	});
}

function GetAllControls(lang, value, callBack) {
	var langDict = [];
	langDict.push(
            { valueAR: '', valueEN: 'none' },
            { valueAR: ' ', valueEN: 'none' },
            { valueAR: 'تسجيل الدخول', valueEN: 'Login' },
            { valueAR: 'تسجيل خروج', valueEN: 'Logout' },
            { valueAR: 'عن التطبيق', valueEN: 'About Application' },
            { valueAR: 'جاري تغيير اللغة', valueEN: 'Changing Language' },
            { valueAR: 'النشأة :', valueEN: 'Creation' },
            { valueAR: '”إيبي ديمو“ شـركة سـعودية مسـاهمة مغلقـة، يقـع مقرهـا الرئيسى فـي مدينـة الريـاض، ويبلـغ رأسـمالها 100 مليـون ريـال سـعودي. حصلـت الشـركة علـى ترخيـص رسـمي مـن وزارة العمـل بالمملكـة العربيـة السـعودية فـي مجـال التوظيـف. وتتخصـص الشـركة فـي توفـير القـوى العاملـة الأجنبية للشـركات والمؤسسـات فـي القطاعـات المختلفـة بالمملكـة، إضافـة إلـى تقديـم خدمـات العمالـة المنزليـة وتنفيـذ أعمـال الوسـاطة فـي هـذا المجـال.', valueEN: '"EPDemo" is a Saudi closed joint stock company headquartered in Riyadh, Its capital is SR 100 million. The company obtained an official license from the Ministry of Labor Saudi Arabia in the field of recruitment and employment. The company specializes in providing Foreign Manpower of Companies and Establishments in the Various Sectors of the Kingdom To provide domestic labor services and carry out mediation work in this area. ' },
            { valueAR: 'العراقة :', valueEN: 'The legacy' },
            { valueAR: '”إيبي ديمو“ ً فـي الواقـع ليسـت شـركة وليـدة، فقـد قامـت علـى خبرات عريقـة مـن مسـتثمرين فـي مجـال المقاولات والطـرق والصيانـة والتشـغيل والاستثمارات الماليـة والاستثمارات العقاريـة، وهـذا التنـوع فـي المجالات والخبرات منـح شـركة إيبي ديمو القدرة علـى فهـم متطلبـات واحتياجـات العملاء و تلبيتهـا علـى الوجـه الاكمل لمواكبـة تطلعاتهـم وخططهـم الاستراتيجية بمـا يتماشى مـع رؤيـة المملكـة العربيـة السـعودية 2030.', valueEN: '"EPDemo" is in fact not a nascent company, it has built on the experience of long-standing investors Contracting, roads, maintenance, operation, financial investments and real estate investments, This diversity of fields and expertise will give the company a replacement ability to understand the requirements and needs Customers to meet their expectations and strategic plans in line with their expectations With the vision of Saudi Arabia 2030. ' },
            { valueAR: 'قصة "إيبي ديمو" :', valueEN: 'EPDemo"s Story' },
            { valueAR: 'اخترنــا اســم ”إيبي ديمو“ ً الذى دومــا نبــذل قصــارى جهدنــا لإيجاد بدائــل متنوعــة ومتميــزة فــي إدارة المــوارد البشـرية مـن أجـل مسـاعدة عملائنا علـى تحقيـق أهدافهـم وتطلعاتهـم. إن مهمتنـا هـي تزويـد عملائنا بخيـارات متعـددة قـدر الإمكان، تفتـح لأعمالهم آفاقـا للتطـور والأزدهار.', valueEN: 'We chose the name "EPDemo" who always tries our best to find a variety of alternatives in Managing human resources in order to help our clients achieve their goals and aspirations. Our mission is to provide our customers with as many options as possible, which opens up their business for development And prosperity. ' },
            { valueAR: 'إضافة شكوي', valueEN: 'Add Complaint' },
            { valueAR: 'نوع المشكلة', valueEN: 'Problem Type' },
            { valueAR: 'يجب  اختيار نوع المشكلة', valueEN: 'You must select problem type' },
            { valueAR: 'العاملة', valueEN: 'Worker' },
            { valueAR: 'اختر العاملة', valueEN: 'Select Worker' },
            { valueAR: 'يجب اختيار العاملة', valueEN: 'You must select worker' },
            { valueAR: 'العقد', valueEN: 'Contract' },
            { valueAR: 'اختر العقد', valueEN: 'Select Contract' },
            { valueAR: 'يجب اختيار العقد', valueEN: 'You must select contract' },
            { valueAR: 'تفاصيل المشكلة', valueEN: 'Problem Details' },
            { valueAR: 'إضافة', valueEN: 'Add' },
            { valueAR: 'تحويل بنكي', valueEN: 'Bank Transfer' },
            { valueAR: 'ريال سعودي', valueEN: 'Saudi Riyal' },
            { valueAR: 'خطأ', valueEN: 'Error' },
            { valueAR: 'نجاح', valueEN: 'Success' },
            { valueAR: 'عقد', valueEN: 'Contract' },
            { valueAR: 'عزيزي العميل :', valueEN: 'Dear Customer' },
            { valueAR: 'رقم الهوية يجب أن يبدأ بالرقم 1 للسعودية والرقم 2 لباقي البلدان', valueEN: 'Id Number Must Starts With 1 For Saudi Arabia And 2 For All Other Countries.' },
            { valueAR: 'نرجوا من سعادتكم رفع صورة التحويل البنكى الخاص بالعقد', valueEN: 'Please raise your bank transfer picture of the contract' },
            { valueAR: 'السعر الإجمالي + الضريبة', valueEN: 'Total Price + Vat' },
            { valueAR: 'مدة التعاقد', valueEN: 'Contract Duration' },
            { valueAR: 'عدد العاملات', valueEN: 'Worker Number' },
            { valueAR: 'عدد الزيارات', valueEN: 'Visits Number' },
            { valueAR: 'رجاء تحويل المبلغ على حساب', valueEN: 'Please Transfer Money At Account' },
            { valueAR: 'رقم حساب الراجحى : 678608010012228', valueEN: 'Al Rajhi Account Number : 678608010012228' },
            { valueAR: 'ايبان :', valueEN: 'IPAN :' },
            { valueAR: 'صورة التحويل البنكي', valueEN: 'Bank Transfer Picture' },
            { valueAR: 'فروعنا', valueEN: 'Our Branches' },
            { valueAR: 'يجب اختيار صورة التحويل البنكي', valueEN: 'You Must Select Bank Transfer Image' },
            { valueAR: 'فرع الرياض', valueEN: 'Riyadh Branch' },
            { valueAR: 'العنوان: الطريق الدائري الشمالي حي التعاون , الرياض 12478', valueEN: 'Address: Northern Ring Road Al-Taamoon District, Riyadh 12478' },
            { valueAR: 'فرع حائل', valueEN: 'Hail Branch' },
            { valueAR: 'العنوان: حائل - طريق الملك فيصل حي العزيزية', valueEN: 'Address: Hail - King Faisal Road Azizia Area' },
            { valueAR: 'فرع القصيم', valueEN: 'Qassim Branch' },
            { valueAR: 'العنوان: القصيم - محافظة عنيزة حي الفاخرية - شارع زامل عبدالله السليم', valueEN: 'Address: Al-Qassim - Anayza Governorate Al-Fakhriya District - Zamil Abdullah Al-Saleem Street' },
            { valueAR: 'فرع الدمام', valueEN: 'Dammam Branch' },
            { valueAR: 'العنوان: المنطقة الشرقية – الدمام كورنيش الدمام - شارع الخليج - أبراج النخيل - الدور الخامس', valueEN: 'Address: Eastern Region - Dammam Dammam Corniche - Al Khaleej Street - Al Nakheel Towers - Fifth Floor' },
            { valueAR: 'فرع الخرج', valueEN: 'Al-Kharj Branch' },
            { valueAR: 'العنوان: الخرج طريق ابي بكر الصديق _ بجوار بنك ساب بتجاه الشمال - الدور الأول', valueEN: 'Address: Al-Kharj Abi Bakr Al-Siddiq Road, next to SABB Bank, towards the north, first floor' },
            { valueAR: 'فرع جدة', valueEN: 'Jeddah Branch' },
            { valueAR: 'العنوان: مدينة جدةشارع التحلية - مركز بن حمران – الدور السابع – مكتب 702 B -', valueEN: 'Address: Jeddah City Tahlia Street - Bin Hamran Center - Seventh Floor - Office 702 B -' },
            { valueAR: 'قطاع الأعمال', valueEN: 'Business sector' },
            { valueAR: 'توفير الموارد البشرية للمنشآت', valueEN: 'Providing human resources for the establishments' },
            { valueAR: 'تقـدم ”إيبي ديمو“ المـوارد البشـرية التـي تحتاجهـا المنشـآت المختلفـة فـي القطاعين العـام والخـاص خـلال مـدة زمنيـة وجيـزة وذلـك عبـر نظـام التعاقـد التشـغيلي طويـل المـدى، مـن أجـل تحقيـق أهدافهـم التشـغيلية، مـا يسـاعد علـى منـع تعثـر المشـاريع الناتـج عـن نقـص العمالة', valueEN: 'EPDemo provides the human resources needed by the various establishments in the public and private sectors within a short period of time through the long-term operational contracting system in order to achieve their operational objectives, which helps to prevent the failure of projects resulting from the lack of labor ' },
            { valueAR: 'توفير الموارد البشرية للمنشآت بعقود مؤقتة', valueEN: 'Provision of human resources for temporary contracts' },
            { valueAR: 'تقـدم ”إيبي ديمو“ المـوارد البشـرية التـي تحتاجهـا المنشـآت المختلفـة فـي القطاعين العـام والخـاص خـلال مـدة زمنيـة وجيـزة وذلـك عبـر نظـام التعاقــد التشــغيلي قصير المدى)مؤقــت(، مــن أجــل تحقيــق أهدافهــم التشــغيلية، مــا يســاعد علــى منــع تعطل المشــاريع الناتــج عــن نقــص العمالـة.', valueEN: 'EPDemo provides the human resources needed by the various establishments in the public and private sectors within a short period of time through the short-term (short-term) operational contracting system to achieve their operational objectives, which helps to prevent the disruption of projects resulting from the shortage of labor.' },
            { valueAR: 'خدمات الوساطة في الاستقدام للمنشآت', valueEN: 'WASATA services in the recruitment of enterprises' },
            { valueAR: 'تقــدم ”إيبي ديمو“ خدمــات الوســاطة فــي اســتقدام المــوارد البشــرية المنزليــة للعائلات التــي لديهــا تأشيــراتهم الخاصــة، حيــث تقــوم الشــركة بجلــب ً الكــوادر وفقــا لمتطلباتهــم، خلال فتــرة زمنيــة وجيــزة.', valueEN: 'EPDemo offers WASATA services in bringing home human resources to the establishments with their own visas. The company brings in cadres according to their requirements within a short period of time.' },
            { valueAR: 'اتصل بنا', valueEN: 'Contact Us' },
            { valueAR: 'تغيير كلمة المرور', valueEN: 'Change Password' },
            { valueAR: 'كلمة السر القديمة', valueEN: 'Old Password' },
            { valueAR: 'يجب إدخال كلمة السر القديمة', valueEN: 'You must enter old password' },
            { valueAR: 'كلمة السر القديمة يجب ألا تقل عن ستة أحرف', valueEN: 'Old password must be at least 6 charachters' },
            { valueAR: 'كلمة السر القديمة يجب ألا تزيد عن ثلاثين حرفا', valueEN: 'Old password must not exceed 30 charachters' },
            { valueAR: 'كلمة السر يجب ألا تحتوي علي مسافات', valueEN: 'Password must not contain spaces' },
            { valueAR: 'كلمة السر الجديدة', valueEN: 'New Password' },
            { valueAR: 'يجب إدخال كلمة السر الجديدة', valueEN: 'You must enter new password' },
            { valueAR: 'كلمة السر الجديدة يجب ألا تقل عن ستة أحرف', valueEN: 'New password must be at least 6 charachters' },
            { valueAR: 'كلمة السر الجديدة يجب ألا تزيد عن ثلاثين حرفا', valueEN: 'New password must not exceed 30 charachters' },
            { valueAR: 'كلمة السر يجب ألا تحتوي علي مسافات', valueEN: 'Password must not contain spaces' },
            { valueAR: 'تأكيد كلمة السر الجديدة', valueEN: 'Confirm Password' },
            { valueAR: 'تأكيد كلمة السر', valueEN: 'Confirm Password' },
            { valueAR: 'كلمة السر يجب ألا تقل عن ستة أحرف', valueEN: 'Password must be at least 6 charachters' },
            { valueAR: 'كلمة السر يجب ألا تزيد عن ثلاثين حرفا', valueEN: 'Password must be at least 6 charachters' },
            { valueAR: 'كلمتا السر غير متطابقتين', valueEN: 'Passwords not matching' },
            { valueAR: 'عودة', valueEN: 'back' },
            { valueAR: 'استكمال البيانات', valueEN: 'Complete Data' },
            { valueAR: 'الإسم بالكامل', valueEN: 'Full Name' },
            { valueAR: 'البريد الإلكتروني', valueEN: 'Email' },
            { valueAR: 'يجب إدخال البريد الإلكتروني', valueEN: 'You must enter email' },
            { valueAR: 'البريد الإلكتروني غير صحيح', valueEN: 'Email is incorrect' },
            { valueAR: 'رقم الهوية', valueEN: 'Id Number' },
            { valueAR: 'يجب إدخال رقم الهوية', valueEN: 'You must Enter Id Number' },
            { valueAR: 'رقم الهوية يجب أن يكون عشرة أرقام فقط', valueEN: 'Id Number must be 10 numbers only' },
            { valueAR: 'الدولة', valueEN: 'Country' },
            { valueAR: 'اختر الدولة', valueEN: 'Select Country' },
            { valueAR: 'يجب اختيار الدولة', valueEN: 'You must select country' },
            { valueAR: 'الجنسية', valueEN: 'Nationality' },
            { valueAR: 'اختر الجنسية', valueEN: 'Select Nationality' },
            { valueAR: 'يجب اختيار الجنسية', valueEN: 'You must select nationality' },
            { valueAR: 'المدينة', valueEN: 'City' },
            { valueAR: 'اختر المدينة', valueEN: 'Select City' },
            { valueAR: 'يجب اختيار المدينة', valueEN: 'You must select city' },
            { valueAR: 'النوع', valueEN: 'Gender' },
            { valueAR: 'اختر النوع', valueEN: 'Select Gender' },
            { valueAR: 'يجب اختيار النوع', valueEN: 'You must select gender' },
            { valueAR: 'الجنس', valueEN: 'Gender' },
            { valueAR: 'اختر الجنس', valueEN: 'Select Gender' },
            { valueAR: 'يجب اختيار الجنس', valueEN: 'You must select gender' },
            { valueAR: 'التالي', valueEN: 'Next' },
            { valueAR: 'لا يوجد اتصال بالإنترنت حاليا', valueEN: 'There is no internet connection' },
            { valueAR: 'بيانات العقد', valueEN: 'Contract Details' },
            { valueAR: 'البيانات الأساسية', valueEN: 'Main Information' },
            { valueAR: 'الحي', valueEN: 'Area' },
            { valueAR: 'عدد العاملات', valueEN: 'Workers Number' },
            { valueAR: 'عدد الزيارات', valueEN: 'Visits Number' },
            { valueAR: 'مدة التعاقد', valueEN: 'Contract Duration' },
            { valueAR: 'عدد الأيام', valueEN: 'Number Of Days' },
            { valueAR: 'الأيام المختارة', valueEN: 'Selected Days' },
            { valueAR: 'تاريخ أول زيارة', valueEN: 'First Visit Date' },
            { valueAR: 'الجنسية', valueEN: 'Nationality' },
            { valueAR: 'عدد الساعات', valueEN: 'Hours Number' },
            { valueAR: 'تفاصيل الباقة', valueEN: 'Package Details' },
            { valueAR: 'فترة الزيارة', valueEN: 'Visit Duration' },
            { valueAR: 'السعر', valueEN: 'Price' },
            { valueAR: 'الضريبة المضافة %', valueEN: 'Vat %' },
            { valueAR: 'قيمة الضريبة المضافة', valueEN: 'Vat Amount' },
            { valueAR: 'خصم نقاط ولاء', valueEN: 'Loyality Points Discount' },
            { valueAR: 'السعر الإجمالي + الضريبة', valueEN: 'Total Price + Vat' },
            { valueAR: 'السعر النهائي', valueEN: 'Final Price' },
            { valueAR: 'السابق', valueEN: 'Previous' },
            { valueAR: 'بيانات العقد', valueEN: 'Contract Details' },
            { valueAR: 'ادفع الآن', valueEN: 'Pay Now' },
            { valueAR: 'إلغاء العقد', valueEN: 'Cancel Contract' },
            { valueAR: 'البيانات الأساسية', valueEN: 'Main Details' },
            { valueAR: 'مدة التعاقد', valueEN: 'Contract Duration' },
            { valueAR: 'عقد جديد', valueEN: 'New Contract' },
            { valueAR: 'تقديم طلب عاملات بالساعة', valueEN: 'Apply hourly workers request' },
            { valueAR: 'اختر الحي', valueEN: 'Select Area' },
            { valueAR: 'يجب اختيار الحي', valueEN: 'You must select area' },
            { valueAR: 'العاملات', valueEN: 'Workers' },
            { valueAR: 'اختر العاملات', valueEN: 'Select Worker' },
            { valueAR: 'يجب اختيار العاملات', valueEN: 'You must select worker' },
            { valueAR: 'الزيارات', valueEN: 'Visits' },
            { valueAR: 'الزيارات في الأسبوع', valueEN: 'Select Visits Per Week' },
            { valueAR: 'يجب اختيار عدد الزيارات في الأسبوع', valueEN: 'You must select visits per week' },
            { valueAR: 'ساعات الزيارة', valueEN: 'Visit Hours' },
            { valueAR: 'اختر عدد ساعات الزيارة', valueEN: 'Select Visit Hours' },
            { valueAR: 'يجب اختيار الساعات', valueEN: 'You must select hours' },
            { valueAR: 'الأيام المتاحة :', valueEN: 'Available Days :' },
            { valueAR: 'لا يمكنك إختيار أيام أخري', valueEN: 'You cannot select another days' },
            { valueAR: 'يجب اختيار الأيام المتاحة', valueEN: 'You must select available days' },
            { valueAR: 'اختر تاريخ أول زيارة', valueEN: 'Pick First Visit Date' },
            { valueAR: 'يجب اختيار تاريخ أول زيارة', valueEN: 'You must pick first visit date' },
            { valueAR: 'كود الخصم', valueEN: 'Code' },
            { valueAR: 'مدة التعاقد', valueEN: 'Contract Duration' },
            { valueAR: 'اختر مدة التعاقد', valueEN: 'Select Contract Duration' },
            { valueAR: 'يجب اختيار مدة التعاقد', valueEN: 'You must select contract duration' },
            { valueAR: 'الجنسية', valueEN: 'Nationality' },
            { valueAR: 'اختر الجنسية', valueEN: 'Select Nationality' },
            { valueAR: 'يجب اختيار الجنسية', valueEN: 'You must select nationality' },
            { valueAR: 'اختر الباقة المناسبة لكم :', valueEN: 'Choose Suitable Package' },
            { valueAR: 'هذه الفترة غير متاحة , اختر ميعاد آخر أو جنسية أخري', valueEN: 'This duration is not available , Pick another date or nationality' },
            { valueAR: 'السعر الأصلي', valueEN: 'Original Price' },
            { valueAR: 'القيمة المضافة', valueEN: 'Vat Amount' },
            { valueAR: 'السعر بعد القيمة المضافة', valueEN: 'Price after vat amount' },
            { valueAR: 'ريال', valueEN: 'Riyal' },
            { valueAR: 'تعديل البيانات', valueEN: 'Update Profile' },
            { valueAR: 'يجب إدخال الإسم بالكامل', valueEN: 'You must enter full name' },
            { valueAR: 'رقم الجوال', valueEN: 'Mobile' },
            { valueAR: 'يجب إدخال رقم الجوال', valueEN: 'You must enter mobile' },
            { valueAR: 'رقم الجوال غير صحيح', valueEN: 'Mobile is incorrect' },
            { valueAR: 'رقم الجوال يجب أن يكون عشرة أرقام فقط', valueEN: 'Mobile must be 10 numbers only' },
            { valueAR: 'إعادة تعيين كلمة المرور', valueEN: 'Reset Password' },
            { valueAR: 'الرئيسية', valueEN: 'Home' },
            { valueAR: 'القطاعات', valueEN: 'Sectors' },
            { valueAR: 'تحديد الموقع', valueEN: 'Pick Location' },
            { valueAR: 'يجب اختيار موقعك علي الخريطة', valueEN: 'You must pick your location on map' },
            { valueAR: 'اختر موقعك على الخريطة', valueEN: 'pick your location on map' },
            { valueAR: 'نوع المنزل', valueEN: 'House Type' },
            { valueAR: 'اختر نوع المنزل', valueEN: 'Select House Type' },
            { valueAR: 'يجب اختيار نوع المنزل', valueEN: 'You must select house type' },
            { valueAR: 'رقم الطابق', valueEN: 'Floor Number' },
            { valueAR: 'اختر رقم الطابق', valueEN: 'Select Floor Number' },
            { valueAR: 'يجب اختيار رقم الطابق', valueEN: 'You must select floor number' },
            { valueAR: 'رقم المنزل', valueEN: 'House Number' },
            { valueAR: 'يجب اختيار رقم المنزل', valueEN: 'You must select house number' },
            { valueAR: 'رقم الشقة', valueEN: 'Appartment Number' },
            { valueAR: 'يجب اختيار رقم الشقة', valueEN: 'You must select appartment number' },
            { valueAR: 'العنوان .. مثال : بجوار مدرسة', valueEN: 'Address ... Example : Beside school' },
            { valueAR: 'كلمة المرور', valueEN: 'Password' },
            { valueAR: 'نسيت كلمة المرور ؟', valueEN: 'Forget Password ?' },
            { valueAR: 'دخول', valueEN: 'Login' },
            { valueAR: 'تسجيل حساب جديد', valueEN: 'Register' },
            { valueAR: 'تخطي', valueEN: 'Pass' },
            { valueAR: 'القطاع الطبي', valueEN: 'Medical Sector' },
            { valueAR: 'المنشأت الطبية', valueEN: 'Medical facilities' },
            { valueAR: 'الكـــــوادر الطـبــيــــة والاستشارية بـكــافــــــة تخصصاتها الدارجة أوالنادرة', valueEN: 'The medical and advisory staff in all its specialized or rare specialties' },
            { valueAR: 'التخصصات التمـريضية بكافة فئاتها', valueEN: 'Nursing specialties of all categories' },
            { valueAR: 'التخصـصـــات الـفــنــيـــــة والإدارية المساعــدة', valueEN: 'Technical and administrative assistance' },
            { valueAR: 'التخصـصـات المهنـيــــة والمـســانــــدة', valueEN: 'Professional and support specialties' },
            { valueAR: 'المرافق الشخصي', valueEN: 'Personal facilities' },
            { valueAR: 'المرافق الشخصى فى المستشفيات ورحلات العلاج', valueEN: 'Personal companion in hospitals and treatment trips' },
            { valueAR: 'العلاج الطبيعي المنزلي', valueEN: 'Home Physical Therapy' },
            { valueAR: 'العلاج الوظيفي المنزلي', valueEN: 'Home Work Therapy' },
            { valueAR: 'الممرضة المنزلية', valueEN: 'Home nurse' },
            { valueAR: 'المرافق الشخصي في المنازل', valueEN: 'Personal companion at home' },
            { valueAR: 'أفراد الشركة', valueEN: 'Members' },
            { valueAR: 'عبدالرحمن سليمان المحيميد', valueEN: 'Abdulrahman Sulaiman Al-Mohaimeed' },
            { valueAR: 'الرئيس التنفيذي لشركة إيبي ديمو.', valueEN: '2017 EPDemo - CEO.' },
            { valueAR: '2016 نائب الرئيس التنفيذي - شركة جال للموارد البشرية.', valueEN: '2016 Jal Human resources company - deputy CEO.' },
            { valueAR: '2012 نائب الرئيس التنفيذي - شركة الموارد للأستقدام', valueEN: '2012 Mawarid Recruitment Co - Deputy CEO.' },
            { valueAR: '2011 مدير عام الموارد البشرية والإدارية - شركة العيوني القابضة.', valueEN: '2011 Al-Ayouni holding Company - HR and Administrative Affairs Director.' },
            { valueAR: '2010 مدير عام الموارد البشرية والإدارية - شركة توليدو العربية  "أكوا القابضة".', valueEN: '2010 TOLID Company - HR and Administrative Affairs Director.' },
            { valueAR: '2005 نائب المدير العام ورئيس إدارة الشؤون المالية والإدارية - شركة الفهاد للمقاولات و التشغيل.', valueEN: '2005 Fahad Contracting Company – Deputy Director and head section of Administrative & financial affairs.' },
            { valueAR: '2002 مدير الموارد البشرية والإدارية - الشركة الوطنية الزراعية.', valueEN: '2002 National Company - HR and Administrative Affairs Director.' },
            { valueAR: '2000 محلل مالي - الحرس الوطني و شركة فينيل.', valueEN: '2000 National Guard and Vinyl ompany – Financial Analyst.' },
            { valueAR: 'سعيد محمد الحارثي', valueEN: 'Saeed Mohammed Al-Harithi' },
            { valueAR: 'مدير عام الأصول البشرية – شركة إيبي ديمو.', valueEN: '2017 Human Capital Director – EPDemo Co.' },
            { valueAR: '2016 مدير عام الموارد البشرية والادارية – شركة الأفضل - مجموعة العيسى القابضة.', valueEN: '2016 Manager of Human Resources and Administrative affairs - Al-Ettal Co «Al-Issa Holding Group»' },
            { valueAR: '2014 مدير عام الموارد البشرية والشؤون الادارية – شركة زهران القابضة.', valueEN: '2014 General Manager of HR & Administrative affairs - Zahran Holding Co.' },
            { valueAR: '2012 مدير الموارد البشرية والشؤون الادارية – شركة العيوني للاستثمار والمقاولات.', valueEN: '2012 HR & Administrative Director – Al-Ayuni Investment & Contracting Co.' },
            { valueAR: '2010 مشرف الموارد البشرية – شركة توليدو العربية ) أكوا القابضة (.', valueEN: '2010 HR Supervisor – Tolido Co.' },
            { valueAR: '2008 مشرف في الموارد البشرية – برنامج طائرات F15 - شركة الألكترونيات المتقدمة المحدودة.', valueEN: '2008 HR Supervisor – Advanced Electronics Co.' },
            { valueAR: '2007 أخصائي اتفاقيات - شركة ريثيون الشرق الأوسط - الدفاع الجوي .', valueEN: '2007 Agreements Specialist - Rethion Middle East.' },
            { valueAR: '1995 أخصائي عقود - شركة بوينج الشرق الأوسط - شركة لوكهيد مارتن - شركة السلام للطائرات - شركة بان نسما.', valueEN: '1995 Agreements Specialist - Boeing Middle East . Locking Martin Co . Salam Aircraft Co . Pan Nesma Co.' },
            { valueAR: 'محمد دواس العنزي', valueEN: 'Mohammed Dawas Al-Anzi' },
            { valueAR: 'مدير عام الإدارة الطبية - شركة إيبي ديمو2015 .', valueEN: '2015 Medical Administration Director - EPDemo.' },
            { valueAR: '2012 مدير الموارد البشرية - مستشفى الجزيرة.', valueEN: '2012 Human Resources Director – AlJazera Hospital.' },
            { valueAR: '2002 مدير الموارد البشرية - مستشفى رعاية الرياض.', valueEN: '2002 Human Resources Manager – Riyadh Care Hospital.' },
            { valueAR: '1991 مدير خدمات فندقية - فندق الأنتركونتناتل.', valueEN: '1991 Guest Service Manager – Intercontinental Hotel.' },
            { valueAR: 'هشام ابراهيم فكري', valueEN: 'Hisham Ibrahim Fekri' },
            { valueAR: 'المستشار المالي – شركة إيبي ديمو.', valueEN: '2017 Financial Advisor – EPDemo Co.' },
            { valueAR: '2015 مدير الإدارة المالية – شركة الأجير للتوظيف.', valueEN: '2015 Financial Management Director - Ajir Recruitment Company.' },
            { valueAR: '2013 مدير الإدارة المالية – شركة الموارد للتوظيف.', valueEN: '2013 Financial Management Director - Al Mawared Recruitment Company.' },
            { valueAR: '2009 مدير الإدارة المالية – شركة الأمتياز للتكنولوجيا المحدودة.', valueEN: '2009 Financial Management Director - Amtiaz Technology Co.' },
            { valueAR: '2007 رئيس حسابات – شركة المجال العربي القابضة.', valueEN: '2007 Accounts Chief - Arab Holding Company.' },
            { valueAR: 'يوسف محمد الشهري', valueEN: 'Yousef Mohammed Al-Shehri' },
            { valueAR: 'مدير المبيعات – شركة إيبي ديمو.', valueEN: '2015 Sales Manager – EPDemo Co.' },
            { valueAR: '2013 مدير مبيعات – شركة الموارد للتوظيف .', valueEN: '2013 Sales Manager – Mawarid Recruitment Co.' },
            { valueAR: '2008 مدير الخدمة المصرفية لكبار العملاء – البنك الفرنسي .', valueEN: '2008 Senior Banking Services Manager – France Bank' },
            { valueAR: 'عبدالله صالح الرشيد', valueEN: 'Abdullah Saleh Al-Rasheed' },
            { valueAR: 'مدير إدارة الافراد – إيبي ديمو.', valueEN: '2017 Individual director - EPDemo Co.' },
            { valueAR: '2011 مدير قسم الموارد البشرية - شركة البدائل للإستشارات الهندسية', valueEN: '2011 Human Resources Director - AlBadaal For Engineering Consultancy Co' },
            { valueAR: '2010 مدير العلاقات العامة – شركة توليدو العربية ) أكوا القابضة (.', valueEN: '2010 Public Relations Manager – TOLEDO Co.' },
            { valueAR: '2008 مدير إدارة الموارد البشرية – مؤسسة خدمات الأعمار للمقاولات', valueEN: '2008 Human Resources Director – Alaamar Services contracting Co.' },
            { valueAR: '2007 مدير علاقات حكومية – مؤسسة حدائق رند للمقاولات.', valueEN: '2007 Director of Government Relations - Rend Gardens Establishment for Contracting.' },
            { valueAR: 'طلال البرديس', valueEN: 'Talal Al-Bardis' },
            { valueAR: 'مدير عام إدارة التوظيف الدولي – شركة إيبي ديمو.', valueEN: '2017 General Manager for international Recruitment Department – EPDemo Co.' },
            { valueAR: '2013 مدير العمليات – شركة الموارد للتوظيف.', valueEN: '2013 Overseas Recruitment Manager – Mawarid Company.' },
            { valueAR: '2004 مدير العمليات – البنك السعودي الامريكى.', valueEN: '2004 Operation Manager – Saudi American Bank.' },
            { valueAR: 'أهداف الشركة', valueEN: 'Mission' },
            { valueAR: 'توفــر الاعداد الكافيــة مــن الموظفــن المتعاقديــن الذى تحتاجهــم الشــركات والمؤسســات فــي مختلــف القطاعــات، وبشـكلٍ يراعـي أهميـة الوقـت فـي تنفيـذ كل الإجراءات.', valueEN: 'Provide sufficient numbers of contracted employees needed by companies and institutions in different sectors, taking into account the importance of time in the implementation of all procedures.' },
            { valueAR: 'توفير موظفين متعاقدين مهرة ومتمكنين ً مهنيــا، يضيفــون إلــى المشــاريع المختلفــة قيمــة نوعيــة حقيقيــة، ويســهمون فــي تحقيــق أهــداف مختلــف الأعمال التجاريــة والصناعيــة والعقاريــة والخدميــة.', valueEN: 'To provide skilled and professionally qualified contracting personnel who add to the various projects real value and contribute to the achievement of the objectives of various business, industrial, real estate and service.' },
            { valueAR: 'تقديـم خدمـات متميـزة لعملائنا، عبر إدارة العلاقة معهــم بشــكل احترافى يضمــن ّاطلاعهم ورضاهـم ومتابعتهـم لـكل الأعمال المتعلقــة بموظفيهــم المتعاقديــن، بشــكل لحظــي، وعبر أحــدث وســائل التقنيــة.', valueEN: 'Providing excellent services to our clients, through the management of the relationship with them in a professional manner that ensures their knowledge, satisfaction and follow-up of all work related to their contracting employees, instantly and through the latest technology.' },
            { valueAR: 'رسالة الشركة :', valueEN: 'Company Message:' },
            { valueAR: 'العمـل علـى تمكين المنشـآت والأفراد مـن التركيـز علـى أعمالهـم الأساسية إضافـة إلـى مسـاعدتهم علـى تحقيـق الكفـاءة والفاعليـة عبر تزويدهم ً بمـا يحتاجونـه مـن مـوارد بشـرية، مـع إدارة شـؤونها كليـا ً أو جزئيـا ً نيابـة عنهـم.', valueEN: 'To empower enterprises and individuals to focus on their core businesses, as well as to help them achieve efficiency and effectiveness by providing them with the human resources they need, and managing their affairs in whole or in part on their behalf.' },
            { valueAR: 'رؤية الشركة المستقبلية :', valueEN: 'Future Vision:' },
            { valueAR: 'أن نكـون الشـريك المفضـل للمنشـآت والأفراد فـي مجـال اسـتقدام المـوارد البشـرية، وأن نكـون العلامة التجاريـة الأفضل بين شـركات الاستقدام والتشـغيل فـي المملكـة العربيـة السـعودية.', valueEN: 'To be the preferred partner for establishments and individuals in the field of recruiting human resources, and to be the best brand among the recruitment and operating companies in the Kingdom of Saudi Arabia.' },
            { valueAR: 'استراتيجية العمل :', valueEN: 'Business Strategy:' },
            { valueAR: 'يقـوم العمـل فـي »إيبي ديمو« علـى استـراتيجية عمـل واضحـة محـددة المعالـم والاهداف. وتؤمـن الشـركة بأنـه لـن يوجـد إنجـاز إذا لـم ُيكن هنـاك ُ أهـداف مرسـومة لتحقـق، ودون خطـة تسـهل الوصـول إلـى هـذه الأهداف. وبعد البحث والتحليل وتطوير مسارات الخطط جزءا مهما من عملية بناء الاستراتيجية فى إيبي ديمو فلا خطة مجدية بدون أسس معرفية صلبة يتم البناء عليها للمستقبل', valueEN: 'The work at "EPDemo" is based on a clearly defined strategy and defined objectives. The company believes that there will be no achievement if there are no goals to be achieved and no plan to facilitate access to these goals. After research, analysis and development of the plans, an important part of the process of building the strategy in exchange, there is no feasible plan without solid knowledge bases to be built for the future' },
            { valueAR: 'وبعـد ان تـم بنـاء الجانـب النظـري مـن اسـراتيجية العمـل، ضخـت ”إيبي ديمو“ المـوارد الماليـة الكافيـة لتحويـل هذه الاستراتيجة إلــى واقــع ملمــوس عــر تطبيــق كافــة مكوناتهــا. وتســعى »إيبي ديمو« خــلال الســنوات الخمــس القادمــة إلــى مضاعفــة إيراداتهـا والتوسـع فـي أسـواقها وأنشـطتها كمـاو ونوعـا ، لتصبـح كيانـا تجاريـا متعـدد المـوارد وذا مركـز مالـي قـوي ووضـعٍ استـراتيجي آمـن ومسـتقر', valueEN: 'Having built the theoretical side of the business strategy, "EPDemo" has injected enough financial resources to transform this strategy into a reality that is fully operational. Over the next five years, the company aims to double its revenues and expand its markets and activities in quantity and quality to become a multi-resource business entity with a strong financial position and a secure and stable strategic position.' },
            { valueAR: 'طريقة الدفع', valueEN: 'Payment Method' },
            { valueAR: 'اختر طريقة الدفع', valueEN: 'Choose Payment Method' },
            { valueAR: 'ادفع اونلاين', valueEN: 'Pay Online' },
            { valueAR: 'تحويل بنكي', valueEN: 'Bank Transfer' },
            { valueAR: 'الكود فقط للتجربة :', valueEN: 'Code For Trial Use Only :' },
            { valueAR: 'الكود', valueEN: 'Code' },
            { valueAR: ' العنوان: ', valueEN: 'Address : ' },
            { valueAR: 'يجب إدخال الكود', valueEN: 'You must Enter Code' },
            { valueAR: 'كلمة المرور الجديدة', valueEN: 'New Password' },
            { valueAR: 'يجب إدخال كلمة السر', valueEN: 'You must enter Password' },
            { valueAR: 'كلمة السر يجب ألا تقل عن ستة أحرف', valueEN: 'Password must be at least 6 charachters' },
            { valueAR: 'كلمة السر يجب ألا تزيد عن ثلاثين حرفا', valueEN: 'Password must not exceed 30 charachters' },
            { valueAR: 'تأكيد كلمة المرور الجديدة', valueEN: 'Confirm New Password' },
            { valueAR: 'يجب تأكيد كلمة السر', valueEN: 'You must confirm password' },
            { valueAR: 'كلمة السر يجب ألا تقل عن ستة أحرف', valueEN: 'Password must be at least 6 charachters' },
            { valueAR: 'كلمة السر يجب ألا تزيد عن ثلاثين حرفا', valueEN: 'Password must not exceed 30 charachters' },
            { valueAR: 'كلمتا السر غير متطابقتين', valueEN: 'Passwords not matching' },
            { valueAR: 'تغيير كلمة المرور', valueEN: 'Change Password' },
            { valueAR: 'مواعيد الزيارات', valueEN: 'Visits Durations' },
            { valueAR: 'الإسم الأول', valueEN: 'First Name' },
            { valueAR: 'يجب إدخال الإسم الأول', valueEN: 'You must enter first name' },
            { valueAR: 'الإسم الأوسط', valueEN: 'Middle Name' },
            { valueAR: 'يجب إدخال الإسم الأوسط', valueEN: 'You must enter middle name' },
            { valueAR: 'الإسم الأخير', valueEN: 'Last Name' },
            { valueAR: 'يجب إدخال الإسم الأخير', valueEN: 'You must enter last name' },
            { valueAR: 'كلمة السر', valueEN: 'Password' },
            { valueAR: 'تسجيل حساب', valueEN: 'Register' },
            { valueAR: 'قريبا', valueEN: 'Soon' },
            { valueAR: 'متابعة', valueEN: 'Continue' },
            { valueAR: 'نجاح', valueEN: 'Success' },
            { valueAR: 'انقطاع الخدمة', valueEN: 'Service Disconnected' },
            { valueAR: 'حالة التعاقد', valueEN: 'Contract Status' },
            { valueAR: 'تم التعاقد بنجاح', valueEN: 'Successfully contracted' },
            { valueAR: 'رقم العقد', valueEN: 'Contract Number' },
            { valueAR: 'قيمة الخصم', valueEN: 'Discount :' },
            { valueAR: 'من فضلك ادفع مبلغ', valueEN: 'Please Pay : ' },
            { valueAR: 'ريال سعودي فقط لاغير', valueEN: 'Saudi Riyals' },
            { valueAR: 'ادفع الان', valueEN: 'Pay Now' },
            { valueAR: 'الاتفاقيات', valueEN: 'Agreements' },
            { valueAR: 'هذه الخدمة تقدم', valueEN: 'This Service offers' },
            { valueAR: 'للعائلات فقط', valueEN: 'Families only' },
            { valueAR: 'و في حال عدم وجود سيدة بالمنزل نعتذر عن تقديم الخدمة.', valueEN: 'In the absence of a lady in the house, we apologize for the service.' },
            { valueAR: 'موافق', valueEN: 'Accept' },
            { valueAR: 'الغاء', valueEN: 'Cancel' },
            { valueAR: 'المساعدة', valueEN: 'Complaints' },
            { valueAR: 'رقم الشكوى', valueEN: 'Complaint Number' },
            { valueAR: 'تفاصيل الشكوي', valueEN: 'Complaint Details' },
            { valueAR: 'لا توجد شكاوي', valueEN: 'There is no complaints.' },
            { valueAR: 'اضافة شكوى جديدة', valueEN: 'Add new complaint' },
            { valueAR: 'العقود', valueEN: 'Contracts' },
            { valueAR: 'ريال', valueEN: 'SAR' },
            { valueAR: 'ريال سعودي', valueEN: 'SAR' },
            { valueAR: 'المبلغ :', valueEN: 'Amount :' },
            { valueAR: 'التاريخ :', valueEN: 'Date :' },
            { valueAR: 'لا يوجد عقود', valueEN: 'There is no contracts' },
            { valueAR: 'اضافة عقد جديد', valueEN: 'Add new contract' },
            { valueAR: 'حسابي', valueEN: 'My Account' },
            { valueAR: 'تعديل', valueEN: 'Update' },
            { valueAR: 'الشكاوى', valueEN: 'Complaints' },
            { valueAR: 'التعاقدات', valueEN: 'Contracts' },
            { valueAR: 'تعاقداتي', valueEN: 'My Contracts' },
            { valueAR: 'قطاع الأفراد', valueEN: 'Individual Sector' },
            { valueAR: 'القطاع الطبي', valueEN: 'Medical Sector' },
            { valueAR: 'قطاع الأعمال', valueEN: 'Business Sector' },
            { valueAR: 'فروعنا', valueEN: 'Our Branches' },
            { valueAR: 'عن التطبيق', valueEN: 'About Application' },
            { valueAR: 'برنامج مساعد - الكوادر الصحية للأفراد', valueEN: 'Musaad program - Health Personal Cadres' },
            { valueAR: 'تغيير اللغة', valueEN: 'Change Language' },
            { valueAR: 'EN | عربي', valueEN: 'EN | Arabic' },
            { valueAR: 'خدماتنا', valueEN: 'Our Services' },
            { valueAR: 'لقطاع الأعمال', valueEN: 'For business' },
            { valueAR: 'توفير الموارد البشرية للمنشآت:', valueEN: 'Providing human resources for the establishments:' },
            { valueAR: 'تقــــدم أبـــدال المـــوارد البشــريــة التي تحتــاجهــا المنشـــآت المختـلـفـــة فـــي القـطــــاعــــين العــــام والخاص خلال مدة زمنية وجيزة وذلك عبر نظام التعــاقـــد التشغيلـــي طــويـــل المــــدى، مـــن أجــل تحقيـــق أهدافهــــم التشغيــليــة، ممــا يســاعــد علـى منع تعثر المشاريع الناتج عن نقص', valueEN: 'The replacement of the human resources required by the various establishments in the public and private sectors within a short period of time through the long-term operational contracting system to achieve their operational objectives,' },
            { valueAR: 'برنامج جاهز :', valueEN: 'Jahiz Program' },
            { valueAR: 'تقـدم أبـــدال المـــوارد البشـــريــة التـــي تحتـــاجهـــا المنشـــآت المختـــلفــــــة فـــي القطــــاعــــين العـــام والخاص خلال مدة زمنية وجيزة وذلك عبر نظــام التعاقـد التشغيــلي قصــير المــدى(مــؤقــت)، مــن أجل تحقيق أهدافهم التشغيلية، ما يساعد على منع تعثر المشاريع الناتج عن نقص العمالة.', valueEN: 'The replacement of the human resources needed by the various establishments in the public and private sectors within a short period of time, through the short-term (short-term) operational contracting system, in order to achieve their operational objectives, which helps to prevent the failure of projects resulting from the shortage of labor.' },
            { valueAR: 'خدمات الوساطة في الاستقدام للمنشآت:', valueEN: 'Brokerage services in the recruitment of enterprises:' },
            { valueAR: 'تقـــدم أبــــدال خـــدمـــات الوساطــة فــي استقــدام الموارد البشرية التي لديها تأشيراتهم الخاصة، حيــث تقــوم الشــــركـــــة بجـلــــب الكـــــــــوادر وفــقـــــاً لمتطلباتهم، خلال فترة زمنية وجيزة.', valueEN: 'The replacement of mediation services in the recruitment of human resources that have their own visas, where the company to bring cadres according to their requirements, within a short period of time.' },
            { valueAR: 'خدمات الأعمال', valueEN: 'Business services' },
            { valueAR: 'تسجيل الدخول', valueEN: 'Login' },
            { valueAR: 'تقديم طلب قطاع أفراد', valueEN: 'Submitting individual sector' },
            { valueAR: 'بيانات الخدمة المطلوبة', valueEN: 'Required service data' },
            { valueAR: 'المهنة المطلوبة', valueEN: 'Required Profession' },
            { valueAR: 'المهنة المطلوبة', valueEN: 'Required profession' },
            { valueAR: 'يجب اختيار المهنة المطلوبة', valueEN: 'Must choose the required profession' },
            { valueAR: 'وصف الخدمة', valueEN: 'Service description' },
            { valueAR: 'بياناتك الشخصية', valueEN: 'Your personal data' },
            { valueAR: 'المسمى الوظيفي', valueEN: 'Job title' },
            { valueAR: 'ارسال', valueEN: 'Send' },
            { valueAR: 'رقم حساب الراجحى :', valueEN: 'Rajhi Bank No. :' },
            { valueAR: ':ايبان', valueEN: 'IPAN :' },
            { valueAR: 'معلومات عن الخدمة', valueEN: 'Service Information' },
            { valueAR: 'أو اختر موقع سابق', valueEN: 'Or Choose Predefined Location' },
            { valueAR: 'اختر موقع سابق', valueEN: 'Choose Predefined Location' },
            { valueAR: 'الشروط والأحكام', valueEN: 'Terms and Conditions' },
            { valueAR: 'موافق علي', valueEN: 'agreed on' },
            { valueAR: 'نوع المساعدة', valueEN: 'Problem Type' },
            { valueAR: 'عاملة', valueEN: 'Worker' },
            { valueAR: 'زيارة', valueEN: 'Visit' },
            { valueAR: 'الوقت المتوقع لوصول العاملة:', valueEN: 'Time of arrival of workers:' },
            { valueAR: 'الفترة الصباحية:   من 07:30 الى 10:00', valueEN: 'Morning: 7:30 a.m. to 10 a.m.' },
            { valueAR: 'الفترة المسائية :   من 15:30 الى 18:00', valueEN: 'Evening: 15:30 to 18:00' },
            { valueAR: 'إغلاق', valueEN: 'Close' },
            { valueAR: 'بعد السداد , قم برفع صورة التحويل البنكي بالأسفل هنا', valueEN: 'After Payment , Upload Your Bank Transfer Image Below' },
            { valueAR: 'لا يوجد مواقع سابقة', valueEN: 'There Are No Previous Locations' },
            { valueAR: 'شروط وسياسات برنامج دلال', valueEN: 'Dalal Program Terms and Policies' },
			{ valueAR: 'الجنسيات', valueEN: 'Nationalities' },
			{ valueAR: 'المهن', valueEN: 'Profission' },
			{ valueAR: 'الباقات', valueEN: 'Packages' },
			{ valueAR: 'ترتيب حسب:', valueEN: 'Order By:' },
			{ valueAR: 'البحث', valueEN: 'Search' },
			{ valueAR: 'عرض', valueEN: 'Show' },
        	{ valueAR: 'بيانات العميل', valueEN: 'Customer Data' },
			{ valueAR: 'قطاع العمل', valueEN: 'Working Sector' },
			{ valueAR: 'عنوان العمل', valueEN: 'Working Address' },
        	{ valueAR: 'مكان العمل', valueEN: 'Working Place' },
			{ valueAR: 'حكومي', valueEN: 'Governmental' },
			{ valueAR: 'قطاع خاص', valueEN: 'Private sector' },
			{ valueAR: 'يجب اختيار نوع العمل', valueEN: 'You must select Working Sector ' },
			{ valueAR: 'السعر قبل الخصم', valueEN: 'Price Before Discount' },
			{ valueAR: 'السعر بعد الخصم', valueEN: 'Price After Discount' },
			{ valueAR: 'لا يوجد افراد', valueEN: 'No Employee Found!' },
			{ valueAR: 'يجب اختيار صورة الإقامة او الهوية', valueEN: 'You must choose your residence or identity' },
			{ valueAR: 'شهري', valueEN: 'Monthly' },
			{ valueAR: 'السعر بدون الضريبة المضافه', valueEN: 'Price Without Vat' },
			{ valueAR: 'تاريخ الطلب', valueEN: 'Order Date' },
        	{ valueAR: 'الباقة', valueEN: 'Package' },
			{ valueAR: 'إسم العامل', valueEN: 'Name' },
			{ valueAR: 'الديانة', valueEN: 'Religion' },
			{ valueAR: 'تفاصيل العقد', valueEN: 'Contract Details' },
			{ valueAR: 'تفاصيل الطلب', valueEN: 'Request Details' },
			{ valueAR: 'إلغاء الطلب', valueEN: 'Cancel Request' },
			{ valueAR: 'الحاله الإجتماعيه', valueEN: 'Marital Status' },
	        { valueAR: 'عقودى', valueEN: 'My Contract' },
            { valueAR: 'عقود الأفراد', valueEN: 'Individual Contract' },
            { valueAR: 'عقود الساعات', valueEN: 'Hourly Contract' },
            { valueAR: 'طلباتي', valueEN: 'My Requests' },
            { valueAR: 'فواتيري', valueEN: 'My Invoices' },
            { valueAR: 'لا يوجد فواتير', valueEN: 'No Invoices Found!' },
            { valueAR: 'لا يوجد طلبات', valueEN: 'No Requests Found!' },
            { valueAR: 'اضافة طلب جديد', valueEN: 'Add new request' },
            { valueAR: 'تفاصيل الطلب', valueEN: 'Request Details' },
            { valueAR: 'رقم الطلب', valueEN: 'Request Number' },
            { valueAR: 'الحالة', valueEN: 'Status' },
            { valueAR: 'المهنة', valueEN: 'Profession' },
            { valueAR: 'تاريخ العقد', valueEN: 'Contract Date' },
            { valueAR: 'مقدم', valueEN: 'Advance' },
            { valueAR: 'فواتير العقد', valueEN: 'Contract\'s Invoices' },
			{ valueAR: 'لا فرق', valueEN: 'No difference' },
			{ valueAR: 'سبق له العمل', valueEN: 'Worked before' },
			{ valueAR: 'لم يسبق له العمل', valueEN: 'Never worked before' },
			{ valueAR: 'الخبرة', valueEN: 'Experience' },
			{ valueAR: 'غير محدد', valueEN: 'Undefined' },
			{ valueAR: 'مسلم', valueEN: 'Muslim' },
		    { valueAR: 'مسيحي', valueEN: 'Christian' },
			{ valueAR: 'ذكر', valueEN: 'Male' },
			{ valueAR: 'أنثي', valueEN: 'Female' },
			{ valueAR: 'لا', valueEN: 'No' },
            { valueAR: 'نقاط', valueEN: 'Points' },
            { valueAR: 'النقاط الغير مفعلة', valueEN: 'Not Active Points' },
            { valueAR: 'النقاط المفعلة', valueEN: 'Active Points' },
            { valueAR: 'النقاط المستهلكة', valueEN: 'Used Points' },
            { valueAR: 'تحويل', valueEN: 'Convert' },
            { valueAR: 'تحويل نقاطي', valueEN: 'Convert My Points' },
            { valueAR: 'نقاطي', valueEN: 'My Points' },
            { valueAR: 'تقديم طلب', valueEN: 'Add request' },
            { valueAR: 'تنظف', valueEN: 'clean' },
            { valueAR: 'سواقة', valueEN: 'drive' },
            { valueAR: 'طبخ', valueEN: 'cook' },
            { valueAR: 'حراسة', valueEN: 'security' },
            { valueAR: 'يجب إدخال تفاصيل الطلب', valueEN: 'Please enter Request Details' },
            { valueAR: 'تفاصيل اخري عن الطلب', valueEN: 'Another Details For Request' },

            { valueAR: 'العنوان', valueEN: 'Address' },
            { valueAR: 'اسم المنطقة', valueEN: 'Area Name' },
            { valueAR: 'يجب ادخال اسم المنطقة', valueEN: 'Please Enter Area Name' },
            { valueAR: 'اسم الشارع', valueEN: 'Street Name' },
            { valueAR: 'يجب ادخال اسم الشارع', valueEN: 'Please Enter Street Name' },
            { valueAR: 'بيانات الطلب', valueEN: 'Request Details' },
            { valueAR: '', valueEN: '' },



            { valueAR: 'تفاصيل الطلب', valueEN: 'Request Details' },
            { valueAR: 'تواصل معنا', valueEN: 'Contact Us' },
			{ valueAR: 'لديك نقاط ولاء', valueEN: 'You Have Walaa Points' },
			{ valueAR: 'ريال سعودي للاستخدام', valueEN: 'SAR For Use' },
            { valueAR: 'نعم', valueEN: 'Yes' },
            { valueAR: 'نوع الطلب', valueEN: 'Request Type' },
            { valueAR: 'يجب اختيار نوع الطلب', valueEN: 'You Must Select Request Type' },
            { valueAR: 'لا توجد نقاط', valueEN: 'No Points' },
            { valueAR: 'نقطة صالحة للاستخدام', valueEN: 'Points Valid To Use' },
			{ valueAR: 'العمر', valueEN: 'Age' }, { valueAR: 'لديك رصيد ولاء', valueEN: 'You Have Walaa Balance' },
            { valueAR: 'نقاتي', valueEN: 'My Points' },
			{ valueAR: 'النقاط الجديدة', valueEN: 'New Points' },
			{ valueAR: 'إجادة الطبخ والتنظيف', valueEN: 'Cooking and cleaning skills' },
			{ valueAR: 'إجادة المعاملة مع الأطفال', valueEN: 'Dealing with Children' },
			{ valueAR: 'إجادة معاملة كبار السن', valueEN: 'Dealing with the elderly' },
			{ valueAR: 'إجادة اللغة الإنجليزية', valueEN: 'English language proficiency' },
			{ valueAR: 'رقم الهوية او الإقامة', valueEN: 'Residence or Identity Number' },
			{ valueAR: 'إنشاء العقد', valueEN: 'Create Contract' },
	    	{ valueAR: 'نتائج البحث', valueEN: 'Search Results' },
	    	{ valueAR: 'البيانات الشخصية', valueEN: 'Personal data' },
	    	{ valueAR: 'يجب إدخال المسمى الوظيفي', valueEN: 'You must enter Job title' },

            { valueAR: 'رقم المنزل يجب ان يحتوي علي ارقام وحروف فقط', valueEN: 'House Number Must Contains Numbers Or Letters Only' },
            { valueAR: 'رقم الشقه يجب ان يحتوي علي ارقام وحروف فقط', valueEN: 'Appartment Number Must Contains Numbers Or Letters Only' }
			);

	var control = langDict.filter(function (obj) {
		return obj.valueAR.toLowerCase() === value.toLowerCase() || obj.valueEN.toLowerCase() === value.toLowerCase();
	})[0];

	if (typeof control != 'undefined' || control != null) {
		if (lang === 'EN') {
			callBack(control.valueEN);
		}
		else {
			callBack(control.valueAR);
		}
	}
	else {
		callBack('none');
	}
}
