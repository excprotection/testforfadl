﻿myApp.angular.factory('CookieService', [function () {
    'use strict';

    var getCookie = function getCookie(cookieName) {
        return localStorage.getItem('Dalal_' + cookieName) != "undefined" ? localStorage.getItem('Dalal_' + cookieName) : null;
    }

    var setCookie = function setCookie(cookieName, cookieValue) {
        localStorage.setItem('Dalal_' + cookieName, cookieValue);
    }

    var removeCookie = function (cookieName) {
        localStorage.removeItem('Dalal_' + cookieName);
    }

    return {
        getCookie: getCookie,
        setCookie: setCookie,
        removeCookie: removeCookie
    };
}]);