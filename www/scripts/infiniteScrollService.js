﻿myApp.angular.factory('myInfinite', ['$scope','$rootScope','CookieService', function ($scope, $rootScope,  CookieService) {
   
    var setInfinite = function setInfinite(methodType,methodName,externalData) {
        var fw7 = myApp.fw7;
        var app = myApp.fw7.app;
        CookieService.removeCookie('infinite-page-number');
        CookieService.setCookie('infinite-page-number', '1');
        $$('#'+ $rootScope.currentOpeningPage+'page').on('infinite', function () {
            if (loading) return;
            loading = true;
            appServices.CallService($rootScope.currentOpeningPage, methodType, methodName, dataVariables, function (infiniteResult) {
                if (infiniteResult && infiniteResult.length > 0) {
                    app.hideIndicator();
                    loading = false;

                    CookieService.setCookie('infinite-page-number', parseInt(CookieService.getCookie('infinite-page-number')) + 1);
                    angular.forEach(infiniteResult, function (event) {
                        event.imageUrl = event.imageUrl ? event.imageUrl : 'img/pic.jpg';
                        allEvents.push(event);
                    });
                    $scope.$digest();
                    if (infiniteResult && infiniteResult.length < 5) {
                        $$('#infiniteLoaderEvents').remove();
                        $('#infiniteLoaderEvents img').css('display', 'none');
                        app.detachInfiniteScroll('#divInfiniteEvents');
                        app.hideIndicator();
                        return;
                    }
                }
                else {
                    app.hideIndicator();
                }
            });                  
          });
    }

    var attachInfinite = function attachInfinite(pageId) {
        app.attachInfiniteScroll('#' + pageId);
    }
    var detachInfiniteScroll = function detachInfiniteScroll(pageId) {
        app.detachInfiniteScroll('#' + pageId);
    }

    return {
        setInfinite: setInfinite,
        attachInfinite: attachInfinite,
        detachInfiniteScroll: detachInfiniteScroll
    };
}]); 