﻿/// <reference path="../js/angular.js" />
/// <reference path="../js/framework7.js" />


myApp.angular.factory('helpers', ['$rootScope', '$exceptionHandler', 'CookieService', '$interval', function ($rootScope, $exceptionHandler, CookieService, $interval) {
	'use strict';

	var notificationOpenedCallback;

	var map;
	var markers = [];

	function CheckUseBoostrapOrNot(pageName) {
		var bootstrapCssElement = document.getElementById('boostrapFile');
		var fontsCssElement = document.getElementById('fontsFile');
		var head = document.getElementsByTagName('head')[0];

		if (pageName.indexOf('indv') == -1 || pageName.toLowerCase().indexOf('location') > -1 || pageName.toLowerCase().indexOf('details') > -1 || pageName.toLowerCase().indexOf('customerwork') > -1 || pageName.toLowerCase().indexOf('completeprofile') > -1 || pageName.toLowerCase().indexOf('success') > -1 || pageName.toLowerCase().indexOf('bank') > -1) {
			if (bootstrapCssElement) {
				bootstrapCssElement.parentNode.removeChild(bootstrapCssElement);
				fontsCssElement.parentNode.removeChild(fontsCssElement);
			}
		}
		else {
			if (!bootstrapCssElement) {
				var boostrapLink = document.createElement('link');
				boostrapLink.id = 'boostrapFile';
				boostrapLink.rel = 'stylesheet';
				boostrapLink.type = 'text/css';
				boostrapLink.href = 'css/bootstrap.min.css';
				head.appendChild(boostrapLink);

				var fontsLink = document.createElement('link');
				fontsLink.id = 'fontsFile';
				fontsLink.rel = 'stylesheet';
				fontsLink.type = 'text/css';
				fontsLink.href = 'fonts/fonts.css';
				head.appendChild(fontsLink);
			}
		}
	}

	var arabictonum = function arabictonum(arabicnumstr) {
		try {
			var num = 0;
			var c;
			for (var i = 0; i < arabicnumstr.length; i++) {
				c = arabicnumstr.charCodeAt(i);
				num += c - 1632;
				num *= 10;
			}
			return num / 10;
		}
		catch (exception) {
			return null;
			$exceptionHandler("An Error has occurred in Method: [Helpers.arabictonum()] , Error: [" + exception.name + "] - Message: [" + exception.message + "].");
		}
	}

	var parseArabic = function parseArabic(str) {
		try {
			var yas = str;
			yas = yas.replace(/[٠١٢٣٤٥٦٧٨٩]/g, function (d) {
				return d.charCodeAt(0) - 1632;
			}).replace(/[۰۱۲۳۴۵۶۷۸۹]/g, function (d) {
				return d.charCodeAt(0) - 1776;
			});

			str = yas;

			return str;
		}
		catch (exception) {
			return null;
			$exceptionHandler("An Error has occurred in Method: [Helpers.parseArabic()] , Error: [" + exception.name + "] - Message: [" + exception.message + "].");
		}
	}

	var groupBy = function groupBy(collection, property) {
		try {
			var i = 0, val, index,
				values = [], result = [];
			for (; i < collection.length; i++) {
				val = collection[i][property];
				index = values.indexOf(val);
				if (index > -1)
					result[index].push(collection[i]);
				else {
					values.push(val);
					result.push([collection[i]]);
				}
			}
			return result;
		}
		catch (exception) {
			return null;
			$exceptionHandler("An Error has occurred in Method: [Helpers.groupBy()] , Error: [" + exception.name + "] - Message: [" + exception.message + "].");
		}
	}

	var ClearBodyAfterGoogleMap = function ClearBodyAfterGoogleMap() {
		try {
			$('span').each(function () {
				var span = $(this);
				if ($(this).text() === 'BESbewy') {
					$(this).remove();
				}
			});

			$('div').each(function () {
				var div = $(this);
				if ($(this).hasClass('pac-container pac-logo hdpi') && $(this).children().length == 0) {
					$(this).remove();
				}
			});
		}
		catch (exception) {
			return null;
			$exceptionHandler("An Error has occurred in Method: [Helpers.ClearBodyAfterGoogleMap()] , Error: [" + exception.name + "] - Message: [" + exception.message + "].");
		}
	}

	var GetCurrentDateTime = function GetCurrentDateTime(date) {
		try {
			var currentdate = new Date();
			if (date == '') {
				currentdate = new Date();
			}
			else {
				currentdate = date;
			}

			var month = parseInt((currentdate.getMonth() + 1));
			var day = parseInt(currentdate.getDate());
			var datetime;

			if (month < 10) {
				if (day < 10) {
					datetime = currentdate.getFullYear() + "-0" + (currentdate.getMonth() + 1) + "-0" + currentdate.getDate();
					//+ " T"+ currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
				}
				else {
					datetime = currentdate.getFullYear() + "-0" + (currentdate.getMonth() + 1) + "-" + currentdate.getDate();
					//+ " T" + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
				}
			}
			else {
				if (day < 10) {
					datetime = currentdate.getFullYear() + "-" + (currentdate.getMonth() + 1) + "-0" + currentdate.getDate();
					//+ " T" + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
				}
				else {
					datetime = currentdate.getFullYear() + "-" + (currentdate.getMonth() + 1) + "-" + currentdate.getDate();
					//+ " T" + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
				}
			}

			return datetime;
		}
		catch (exception) {
			return null;
			$exceptionHandler("An Error has occurred in Method: [Helpers.GetCurrentDateTime()] , Error: [" + exception.name + "] - Message: [" + exception.message + "].");
		}
	}

	var ShowDatePicker = function ShowDatePicker() {
		try {
			var doctorBirthDate = document.getElementById('doctorBirthDate');
			doctorBirthDate.onclick = function () {
				var options = {
					date: new Date(),
					mode: 'date'
				};

				function onSuccess(date) {
					doctorBirthDate.value = GetCurrentDateTime(date);
				}

				function onError(error) { // Android only

				}

				datePicker.show(options, onSuccess, onError);
			}
		}
		catch (exception) {
			return null;
			$exceptionHandler("An Error has occurred in Method: [Helpers.ShowDatePicker()] , Error: [" + exception.name + "] - Message: [" + exception.message + "].");
		}
	}

	var geocodeLatLng = function geocodeLatLng(Lat, Lang, callBack) {
		try {
			var latlng = { lat: parseFloat(Lat), lng: parseFloat(Lang) };
			var geocoder = new google.maps.Geocoder;

			geocoder.geocode({ 'location': latlng }, function (results, status) {
				if (status === google.maps.GeocoderStatus.OK) {
					if (results[0]) {
						var result = results[0].formatted_address;
						callBack(result);
					} else {
						callBack('');
					}
				} else {
					callBack('');
				}
			});
		}
		catch (exception) {
			return null;
			$exceptionHandler("An Error has occurred in Method: [Helpers.geocodeLatLng()] , Error: [" + exception.name + "] - Message: [" + exception.message + "].");
		}
	}


	function GetLocationError(error, lang) {
		var errorText = '';
		if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
			switch (error.code) {
				case error.PERMISSION_DENIED:
					errorText = "تحديد الموقع مغلق لديك علي الهاتف , لا يمكن تحديد الموقع الحالي"
					break;
				case error.POSITION_UNAVAILABLE:
					errorText = "معلومات الموقع غير متوفرة."
					break;
				case error.TIMEOUT:
					errorText = "انتهت مهلة طلب الحصول على موقع المستخدم."
					break;
				case error.UNKNOWN_ERROR:
					errorText = "حدث خطأ غير معروف."
					break;
			}
		}
		else {
			switch (error.code) {
				case error.PERMISSION_DENIED:
					errorText = "Please Turn On Location From Settings On Your Device , Cannot Select Your Current Location."
					break;
				case error.POSITION_UNAVAILABLE:
					errorText = "Location information is unavailable."
					break;
				case error.TIMEOUT:
					errorText = "The request to get user location timed out."
					break;
				case error.UNKNOWN_ERROR:
					errorText = "An unknown error occurred."
					break;
			}
		}
		return errorText;
	}


	var InitMapSearchBox = function InitMapSearchBox(map, markers, selectedAddress) {
		try {

			var fw7 = myApp.fw7;
			var app = myApp.fw7.app;

			if (selectedAddress != '') {
				$('#pac-input').val(selectedAddress);
			}

			var geocoder = new google.maps.Geocoder();
			if (geocoder) {
				geocoder.geocode({
					'address': selectedAddress
				}, function (results, status) {
					if (status == google.maps.GeocoderStatus.OK) {
						if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
							map.setCenter(results[0].geometry.location);

							markers = [];

							// Create a marker for each place.
							markers.push(new google.maps.Marker({
								map: map,
								title: results[0].formatted_address,
								position: results[0].geometry.location
							}));


							var lat = results[0].geometry.location.lat();
							var lang = results[0].geometry.location.lng();

							CookieService.setCookie('StoredLatitude', lat);
							CookieService.setCookie('StoredLongtitude', lang);

							var infoWindow = new google.maps.InfoWindow();

							for (var i = 0; i < markers.length; i++) {
								var data = markers[i];
								var myLatlng = new google.maps.LatLng(data.position.lat(), data.position.lng());
								var marker = new google.maps.Marker({
									position: myLatlng,
									map: map,
									title: data.title
								});

								(function (marker, data) {
									google.maps.event.addListener(marker, "click", function (e) {
										infoWindow.setContent("<div style = 'width:200px;min-height:40px'>" + data.title + "</div>");
										infoWindow.open(map, marker);
									});
								})(marker, data);
							}

						}
						else {
							language.openFrameworkModal('خطأ', 'لا توجد نتائج', 'alert', function () { });
						}
					} else {
						language.openFrameworkModal('خطأ', 'خطأ في عملية أسترجاع العنوان', 'alert', function () { });
					}
				});
			}
		}
		catch (exception) {
			return null;
			$exceptionHandler("An Error has occurred in Method: [Helpers.InitMapSearchBox()] , Error: [" + exception.name + "] - Message: [" + exception.message + "].");
		}
	}

	function CenterControl(controlDiv, mabId, inputId) {
		mabId = (typeof mabId !== 'undefined') ? mabId : "tripMap";
		inputId = (typeof inputId !== 'undefined') ? inputId : 'pac-input';



		var lang = localStorage.getItem('Dalal_lang');

		// Set CSS for the control border.
		var controlUI = document.createElement('div');
		controlUI.style.backgroundColor = '#fff';
		controlUI.style.border = '2px solid #fff';
		controlUI.style.borderRadius = '3px';
		controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
		controlUI.style.cursor = 'pointer';
		controlUI.style.marginBottom = '22px';
		controlUI.style.textAlign = 'center';
		controlUI.id = 'currentLoc';
		if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
			controlUI.title = 'الموقع الحالي';
		}
		else {
			controlUI.title = 'Current Location';
		}

		controlDiv.appendChild(controlUI);

		// Set CSS for the control interior.
		var controlText = document.createElement('div');
		controlText.style.color = 'rgb(25,25,25)';
		controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
		controlText.style.fontSize = '16px';
		controlText.style.lineHeight = '38px';
		controlText.style.paddingLeft = '5px';
		controlText.style.paddingRight = '5px';
		if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
			controlText.innerHTML = 'الموقع الحالي <i class="fa fa-crosshairs"></i>';
		}
		else {
			controlText.innerHTML = 'Current Location <i class="fa fa-crosshairs"></i>';
		}
		controlUI.appendChild(controlText);

		// Setup the click event listeners: simply set the map.
		controlUI.addEventListener('click', function () {
			var projectMarkers = [];

			$('#currentLoc').prop("disabled", true);

			var onSuccess = function (position) {

				projectMarkers = [{ "title": 'myMap', "lat": position.coords.latitude, "lng": position.coords.longitude, "description": "currentLocation" }];
				CookieService.setCookie('StoredLatitude', position.coords.latitude);
				CookieService.setCookie('StoredLongtitude', position.coords.longitude);
				initAutocomplete(mabId, projectMarkers, false, inputId);
				geocodeLatLng(
				   position.coords.latitude,
				   position.coords.longitude, function (result) {
				   	if (result) {
				   		var input = document.getElementById(mabId);
				   		input.value = result;
				   		markers.push(new google.maps.Marker({
				   			map: map,
				   			title: result,
				   			position: position
				   		}));
				   	}
				   });
			//	$('#currentLoc').prop("disabled", false);
			}

			function onError(error) {
				console.log('Get Current Location Error: ');
				console.log(error);

				var message = GetLocationError(error, lang);
				if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
					language.openFrameworkModal('خطأ', message, 'alert', function () { });
				}
				else {
					language.openFrameworkModal('خطأ', message, 'alert', function () { });
				}
				projectMarkers = [{ "title": 'myMap', "lat": '24.713552', "lng": '46.675296', "description": "currentLocation" }];
				if (CookieService.getCookie('StoredLatitude')) CookieService.removeCookie('StoredLatitude');
				if (CookieService.getCookie('StoredLongtitude')) CookieService.removeCookie('StoredLongtitude');
				initAutocomplete(mabId, projectMarkers, false, inputId);

			}

			var devicePlatform = device.platform;

			if (devicePlatform == 'Android' || devicePlatform == 'android') {

				cordova.plugins.diagnostic.isGpsLocationEnabled(function (enabled) {

					if (enabled) {
						navigator.geolocation.getCurrentPosition(onSuccess, onError, { enableHighAccuracy: true, timeout: 10*1000, maximumAge: Infinity });
					}
					else {
						if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
							language.openFrameworkModal('خطأ', 'تحديد الموقع مغلق لديك علي الهاتف , لا يمكن تحديد الموقع الحالي', 'alert', function () { });
						}
						else {
							language.openFrameworkModal('خطأ', 'Address Cannot Be Generated For The Given Latitude And Longtitude', 'alert', function () { });
						}
					}

				}, function (error) {
					if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
						language.openFrameworkModal('خطأ', 'تحديد الموقع مغلق لديك علي الهاتف , لا يمكن تحديد الموقع الحالي', 'alert', function () { });
					}
					else {
						language.openFrameworkModal('خطأ', 'Address Cannot Be Generated For The Given Latitude And Longtitude', 'alert', function () { });
					}
				});



			}
			else {
				navigator.geolocation.getCurrentPosition(onSuccess, onError, { enableHighAccuracy: true, timeout: 2 * 1000, maximumAge: 0 });
			}


		});
	}

	var initAutocomplete = function (mapId, projectMarkers, isFirstLoaded, inputId) {
		inputId = (typeof inputId !== 'undefined') ? inputId : 'pac-input';
		if (isFirstLoaded) {
			map = new google.maps.Map(document.getElementById(mapId), {
				center: { lat: parseFloat(projectMarkers[0].lat), lng: parseFloat(projectMarkers[0].lng) },
				zoom: 17,
				mapTypeId: 'hybrid'
			});

			if (projectMarkers.length > 0) {
				var infoWindow = new google.maps.InfoWindow();

				for (var i = 0; i < projectMarkers.length; i++) {
					var data = projectMarkers[i];
					var myLatlng = new google.maps.LatLng(data.lat, data.lng);
					var marker = new google.maps.Marker({
						position: myLatlng,
						map: map,
						title: data.title
					});

					markers.push(marker);
				}
			}

			google.maps.event.addListener(map, 'click', function (event) {
				//isDraggable = !isDraggable;
				//this.setOptions({ 'draggable': isDraggable });

				// Clear out the old markers.
				markers.forEach(function (marker) {
					marker.setMap(null);
				});
				markers = [];

				markers.push(new google.maps.Marker({
					map: map,
					title: '',
					position: event.latLng
				}));

				CookieService.setCookie('StoredLatitude', event.latLng.lat());
				CookieService.setCookie('StoredLongtitude', event.latLng.lng());

				geocodeLatLng(
				   event.latLng.lat(),
				   event.latLng.lng(), function (result) {
				   	if (result) {
				   		var input = document.getElementById(inputId);
				   		input.value = result;
				   	}
				   });

			});

			var input = document.getElementById(inputId);
			var searchBox = new google.maps.places.SearchBox(input);

			//map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

			var centerControlDiv = document.createElement('div');
			var centerControl = new CenterControl(centerControlDiv, mapId, inputId);
			centerControlDiv.index = 1;
			map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDiv);

			map.addListener('bounds_changed', function () {
				searchBox.setBounds(map.getBounds());
			});

			setTimeout(function () {
				$$('body').on('touchend', '.pac-item', function (e) {
					e.stopImmediatePropagation();
					var placeText = $(this).text();
					//alert(placeText);
					InitMapSearchBox(map, markers, placeText);
				})

				map.addListener('places_changed', function () {
					var places = searchBox.getPlaces();

					if (places.length == 0) {
						$('.pac-container').css('display', 'none !important');
						$('.pac-container').hide();
						return;
					}

					markers.forEach(function (marker) {
						marker.setMap(null);
					});
					markers = [];

					var bounds = new google.maps.LatLngBounds();
					places.forEach(function (place) {
						if (!place.geometry) {
							console.log("Returned place contains no geometry");
							return;
						}

						markers.push(new google.maps.Marker({
							map: map,
							title: place.name,
							position: place.geometry.location
						}));

						CookieService.setCookie('StoredLatitude', place.geometry.location.lat());
						CookieService.setCookie('StoredLongtitude', place.geometry.location.lng());

						$('.pac-container').css('display', 'none !important');
						$('.pac-container').hide();

						if (place.geometry.viewport) {
							bounds.union(place.geometry.viewport);
						} else {
							bounds.extend(place.geometry.location);
						}
					});
					map.fitBounds(bounds);
					map.setZoom(17);
				});

				$$('body').on('touchstart', '.pac-container', function (e) { e.stopImmediatePropagation(); })
			}, 1000);


		}
		else {
			map.setCenter(new google.maps.LatLng(parseFloat(projectMarkers[0].lat), parseFloat(projectMarkers[0].lng)));
			map.setZoom(17);

			// Clear out the old markers.
			markers.forEach(function (marker) {
				marker.setMap(null);
			});
			markers = [];



			if (projectMarkers.length > 0) {
				var infoWindow = new google.maps.InfoWindow();

				for (var i = 0; i < projectMarkers.length; i++) {
					var data = projectMarkers[i];
					var myLatlng = new google.maps.LatLng(data.lat, data.lng);
					var marker = new google.maps.Marker({
						position: myLatlng,
						map: map,
						title: data.title
					});

					markers.push(marker);
				}
			}
		}
	}

	var initMap = function initMap(mapId, markers, fromPage, selectedAddress, callback) {
		try {

			var flightPlanCoordinates = [];
			var isDraggable = false;

			var mapDiv = document.getElementById(mapId);

			var map = new google.maps.Map(mapDiv, {
				center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
				zoom: 3,
				mapTypeId: 'hybrid',
				'draggable': true
			});

			if (markers.length > 0) {
				var infoWindow = new google.maps.InfoWindow();

				for (var i = 0; i < markers.length; i++) {
					var data = markers[i];
					var myLatlng = new google.maps.LatLng(data.lat, data.lng);
					var marker = new google.maps.Marker({
						position: myLatlng,
						map: map,
						title: data.title
					});

					markers.push(marker);
				}
			}

			google.maps.event.addListener(map, 'click', function (event) {
				//isDraggable = !isDraggable;
				//this.setOptions({ 'draggable': isDraggable });

				// Clear out the old markers.
				markers.forEach(function (marker) {
					marker.setMap(null);
				});
				markers = [];

				markers.push(new google.maps.Marker({
					map: map,
					title: '',
					position: event.latLng
				}));

				CookieService.setCookie('StoredLatitude', event.latLng.lat());
				CookieService.setCookie('StoredLongtitude', event.latLng.lng());

			});

			callback(map);
		}
		catch (exception) {
			callback(null);
			$exceptionHandler("An Error has occurred in Method: [Helpers.initMap()] , Error: [" + exception.name + "] - Message: [" + exception.message + "].");
		}
	}

	var DrawRoutes = function DrawRoutes(mapId, firstAddress, SecondAddress, callback) {
		var mapDiv = document.getElementById(mapId);
		var isDraggable = false;

		var directionsDisplay;
		var directionsService = new google.maps.DirectionsService();
		directionsDisplay = new google.maps.DirectionsRenderer();

		var map = new google.maps.Map(mapDiv, {
			center: new google.maps.LatLng(SecondAddress.lat(), SecondAddress.lng()),
			zoom: 5,
			'draggable': isDraggable
		});

		directionsDisplay.setMap(map);

		var start = new google.maps.LatLng(firstAddress.lat(), firstAddress.lng());
		var end = new google.maps.LatLng(SecondAddress.lat(), SecondAddress.lng());

		var bounds = new google.maps.LatLngBounds();
		bounds.extend(start);
		bounds.extend(end);
		map.fitBounds(bounds);
		var request = {
			origin: start,
			destination: end,
			travelMode: google.maps.TravelMode.DRIVING
		};
		directionsService.route(request, function (response, status) {
			if (status == google.maps.DirectionsStatus.OK) {
				directionsDisplay.setDirections(response);
				directionsDisplay.setMap(map);
			} else {
				console.log("Directions Request from " + start.toUrlValue(6) + " to " + end.toUrlValue(6) + " failed: " + status);
			}
		});

		google.maps.event.addListener(map, 'click', function (event) {
			isDraggable = !isDraggable;
			this.setOptions({ 'draggable': isDraggable });
		});

		callback(map);
	};

	var SetUserInLocalStorage = function SetUserInLocalStorage(usrName, usrPass, usrId) {
		CookieService.setCookie('USName', usrName);
		CookieService.setCookie('UPass', usrPass);
		CookieService.setCookie('UserId', usrId);
	}

	var ShowLoader = function ShowLoader(pageName) {
		var divPage = document.getElementById(pageName + 'Page');
		var divLoader = document.createElement('div');
		var hdrWait = document.createElement('h3');
		var loadImage = document.createElement('img');

		divLoader.className += 'loader divLoader';
		loadImage.src = 'img/load.svg';

		var lang = CookieService.getCookie('Dalal_lang');

		if (lang == 'AR' || lang == null || typeof lang == 'undefined') {
			hdrWait.innerHTML = 'برجاء الإنتظار';
		}
		else {
			hdrWait.innerHTML = 'Please wait';
		}

		divLoader.appendChild(loadImage);
		divLoader.appendChild(hdrWait);
		divPage.appendChild(divLoader);
		$(".loader").fadeIn("slow");
	}

	var HideLoader = function HideLoader() {
		$(".loader").fadeOut("slow");
		$('div').each(function () {
			var div = $(this);
			if ($(this).hasClass('loader divLoader')) {
				$(this).remove();
			}
		});
	}

	var GoToPage = function GoToPage(pageName, queryVariables) {
		var fw7 = myApp.fw7;
		$rootScope.currentOpeningPage = pageName;

		CheckUseBoostrapOrNot($rootScope.currentOpeningPage);

		if (pageName == "create" || pageName == "home") {
			if (CookieService.getCookie('StoredContract')) CookieService.removeCookie('StoredContract');
			if (CookieService.getCookie('StoredLatitude')) CookieService.removeCookie('StoredLatitude');
			if (CookieService.getCookie('StoredLongtitude')) CookieService.removeCookie('StoredLongtitude');
		}

		if (typeof queryVariables != 'undefined' && queryVariables != null && queryVariables != '' && queryVariables != ' ') {
			fw7.views[0].router.loadPage({ pageName: pageName, query: queryVariables });
		}
		else {
			fw7.views[0].router.loadPage({ pageName: pageName });
		}
	}

	var GoBack = function GoBack() {
		var fw7 = myApp.fw7;
		var currentPage = $rootScope.currentOpeningPage;

		if (fw7.views[0].history.length > 2) {
			var previousPageInHistory = fw7.views[0].history[fw7.views[0].history.length - 2];
			var previousPage = previousPageInHistory.substr(1, previousPageInHistory.length - 1);
			$rootScope.currentOpeningPage = previousPage;
		}

		CheckUseBoostrapOrNot($rootScope.currentOpeningPage);

		fw7.views[0].router.back();
	}

	var RegisterDevice = function RegisterDevice(isLoad, callBack) {
		if (isLoad == true) {
			myApp.fw7.app.showIndicator();
		}
		if (!CookieService.getCookie('deviceId')) {
			notificationOpenedCallback = function (jsonData) {
				//var adv = jsonData.notification.payload.additionalData.additional.notify.data.adv;
				//var convId = jsonData.notification.payload.additionalData.additional.notify.data.convId;
				//var nType = jsonData.notification.payload.additionalData.additional.notify.data.n_type;

				//if (parseInt(nType) == 0) {
				//    GoToPage('advertisementsDetails', { advertisement: adv });
				//}
				//else if (parseInt(nType) == 1) {
				//    GoToPage('chat', { conversation_Id: convId, adv_id: adv.id });
				//}
			};

			if (window.plugins != null) {
				window.plugins.OneSignal
				  .startInit("ffee2c3c-8c4a-4d16-9450-97d1bab895e0")
				  .handleNotificationOpened(notificationOpenedCallback)
				  .inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.Notification)
				  .endInit();

				window.plugins.OneSignal.getIds(function (ids) {
					console.log('getIds: ' + JSON.stringify(ids));
					CookieService.setCookie('deviceId', ids.userId);
					callBack(ids.userId);
				});



				window.plugins.OneSignal.enableNotificationsWhenActive(false);
			}
			else {
				callBack("");
			}
		}
		else {

			notificationOpenedCallback = function (jsonData) {
				//var adv = jsonData.notification.payload.additionalData.additional.notify.data.adv;
				//var convId = jsonData.notification.payload.additionalData.additional.notify.data.convId;
				//var nType = jsonData.notification.payload.additionalData.additional.notify.data.n_type;

				//if (parseInt(nType) == 0) {
				//    GoToPage('advertisementsDetails', { advertisement: adv });
				//}
				//else if (parseInt(nType) == 1) {
				//    GoToPage('chat', { conversation_Id: convId, adv_id: adv.id });
				//}
			};

			if (window.plugins != null) {
				window.plugins.OneSignal
				  .startInit("ffee2c3c-8c4a-4d16-9450-97d1bab895e0")
				  .handleNotificationOpened(notificationOpenedCallback)
				  .inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.Notification)
				  .endInit();

				window.plugins.OneSignal.enableNotificationsWhenActive(false);
			}
			else {
				callBack("");
			}

			callBack(true);
		}
	};

	var clearTimer = function clearTimer() {
		$interval.cancel(messageInterval);
		messageInterval = undefined;
	}

	var initialSwiper = function (app) {

		var advertisments = myApp.fw7.Advertisements;
		$scope.Advertisements = advertisments;
		if (advertisments !== undefined && advertisments.length > 0) {
			var counter = 0;
			var max = advertisments.length;

			var mySwiper = app.swiper('#FavoriteswiperAdvertisment', {
				pagination: '.swiper-pagination',
				loop: true,
				lazyLoading: true,
				preloadImages: false,
				onSlideNextStart: function (swiper) {
					var time = parseInt(advertisments[counter].secondNumber * 1000);
					setTimeout(function () {
						counter++;
						if (counter >= parseInt(max)) {
							counter = 0;
						}
						swiper.slideNext();
					}, time);
				}
			});
		}

	};

	var isNumber = function isNumber(n) {
		return !isNaN(parseFloat(n)) && isFinite(n);
	}

	return {
		arabictonum: arabictonum,
		parseArabic: parseArabic,
		groupBy: groupBy,
		ClearBodyAfterGoogleMap: ClearBodyAfterGoogleMap,
		GetCurrentDateTime: GetCurrentDateTime,
		ShowDatePicker: ShowDatePicker,
		geocodeLatLng: geocodeLatLng,
		InitMapSearchBox: InitMapSearchBox,
		initMap: initMap,
		SetUserInLocalStorage: SetUserInLocalStorage,
		ShowLoader: ShowLoader,
		HideLoader: HideLoader,
		GoToPage: GoToPage,
		GoBack: GoBack,
		RegisterDevice: RegisterDevice,
		clearTimer: clearTimer,
		initialSwiper: initialSwiper,
		DrawRoutes: DrawRoutes,
		isNumber: isNumber,
		initAutocomplete: initAutocomplete,
		CheckUseBoostrapOrNot:CheckUseBoostrapOrNot
	};
}]);