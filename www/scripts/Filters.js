﻿myApp.angular.filter('generateURL', function ($http) {
    return function (image) {

        if (image != null && image != '' && image != ' ' && image.indexOf('img/') == -1 && image.indexOf('http://placehold.it') == -1) {
            if (image.length > 200) {
                return 'data:image/jpeg;base64, ' + image;
            }
            else if (image.indexOf('file://') > -1) {
                return image;
            }
            else {
                if (image.indexOf('http') > -1) {
                    return image;
                }
                else {
                    return hostUrl + image;
                }
            }
            
        }
        else {
            if (image != null && image != '' && image != ' ' && (image.indexOf('puff.svg') > -1 || image.indexOf('avatar.jpg') > -1 || image.indexOf('img/') > -1)) {
                return image;
            }
            else {
                return 'http://none.com';
            }
        }
    };
}).filter('split', function () {
    return function (input, splitChar, splitIndex) {
        if (!splitIndex)
            splitIndex = 0;

        return input.split(splitChar)[splitIndex];
    }
}).filter('trustAsHtml', ['$sce', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };

}]).filter('trustAsResourceUrl', ['$sce', function ($sce) {
    return function (val) {
        return $sce.trustAsResourceUrl(val);
    };
}]);