﻿myApp.angular.factory('SidePanelService', ['$rootScope', 'CookieService', 'appServices', function ($rootScope, CookieService, appServices) {
    'use strict';

    function DrawMenu() {
        var IsVisitor = CookieService.getCookie('Visitor') ? CookieService.getCookie('Visitor') : false;
        var loginUsingSocial = CookieService.getCookie('loginUsingSocial') ? CookieService.getCookie('loginUsingSocial') : false;
        var userLoggedIn = CookieService.getCookie('userLoggedIn') ? JSON.parse(CookieService.getCookie('userLoggedIn')) : null;
        $rootScope.IsVisitor = IsVisitor;
        var lang = localStorage.getItem('Dalal_lang');

        var fw7 = myApp.fw7;

        if (IsVisitor == 'true' || IsVisitor == true) {
            $('#linkMenuToHome').css('display', 'block ');
            $('#linkMenuToMyContracts').css('display', 'none');
            $('#linkMenuToMyComplaints').css('display', 'none');
            $('#linkMenuToIndividuals').css('display', 'none');
            $('#linkMenuToMyRequests').css('display', 'none');
            $('#linkMenuToMyInvoices').css('display', 'none');
            $('#linkMenuToMedical').css('display', 'block');
            $('#linkMenuToBusiness').css('display', 'block');
            $('#linkMenuToMission').css('display', 'block');
            $('#linkMenuToMembers').css('display', 'block');
            $('#linkMenuToChangePassword').css('display', 'none');
            $('#linkMenuToContact').css('display', 'block');
            $('#linkMenuToAbout').css('display', 'block');
            $('#linkMenuToChangeLanguage').css('display', 'block');
            $('#linkMenuToLogin').css('display', 'block');
            $('#linkMenuToProfile').css('display', 'none');
            $('#linkMenuToMyPoints').css('display', 'none');

            $('#linkMenuToHomeReview').css('display', 'block ');
            $('#linkMenuToMedicalReview').css('display', 'block');
            $('#linkMenuToMissionReview').css('display', 'block');
            $('#linkMenuToMembersReview').css('display', 'block');
            $('#linkMenuToBranchesReview').css('display', 'block');
            $('#linkMenuToAboutReview').css('display', 'block');

            $('#linkMenuToChangeLanguageReview').css('display', 'block');
            $('#linkMenuToChangePasswordReview').css('display', 'none');
            $('#linkMenuToLoginReview').css('display', 'block');
            $('#linkMenuToProfileReview').css('display', 'none');
            $('#linkMenuToContactUsReview').css('display', 'none');
            if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                $('#lblMenuLoginText').html('تسجيل الدخول');
                $('#lblMenuLoginReviewText').html('تسجيل الدخول');
            }
            else {
                $('#lblMenuLoginText').html('Login');
                $('#lblMenuLoginReviewText').html('Login');
            }
            $$('#imgSideMenu').attr('src', 'img/logo.png');
        }
        else {
            $('#linkMenuToHome').css('display', 'block');
            $('#linkMenuToMyContracts').css('display', 'block');
            $('#linkMenuToMyComplaints').css('display', 'block');
            $('#linkMenuToIndividuals').css('display', 'block');
            $('#linkMenuToMyRequests').css('display', 'block');
            $('#linkMenuToMyInvoices').css('display', 'block');
            $('#linkMenuToMedical').css('display', 'block');
            $('#linkMenuToBusiness').css('display', 'block');
            $('#linkMenuToMission').css('display', 'block');
            $('#linkMenuToMembers').css('display', 'block');
            $('#linkMenuToContact').css('display', 'block');
            $('#linkMenuToAbout').css('display', 'block');
            $('#linkMenuToChangeLanguage').css('display', 'block');
            $('#linkMenuToLogin').css('display', 'block');
            $('#linkMenuToMyPoints').css('display', 'block');

            $('#linkMenuToHomeReview').css('display', 'block ');
            $('#linkMenuToMedicalReview').css('display', 'block');
            $('#linkMenuToMissionReview').css('display', 'block');
            $('#linkMenuToMembersReview').css('display', 'block');
            $('#linkMenuToBranchesReview').css('display', 'block');
            $('#linkMenuToAboutReview').css('display', 'block');
            $('#linkMenuToChangeLanguageReview').css('display', 'none');
            $('#linkMenuToLoginReview').css('display', 'block');
            $('#linkMenuToContactUsReview').css('display', 'block');
            if (typeof userLoggedIn != 'undefined' && userLoggedIn != null) {
                $('#linkMenuToProfileReview').css('display', 'block');
                if (loginUsingSocial == 'true') {
                    $('#linkMenuToChangePassword').css('display', 'none');
                    $('#linkMenuToChangePasswordReview').css('display', 'none');
                }
                else {
                    $('#linkMenuToChangePassword').css('display', 'block');
                    $('#linkMenuToChangePasswordReview').css('display', 'none');
                }
                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    $('#lblMenuLoginText').html('تسجيل خروج');
                    $('#lblMenuLoginReviewText').html('تسجيل خروج');
                }
                else {
                    $('#lblMenuLoginText').html('Logout');
                    $('#lblMenuLoginReviewText').html('Logout');
                }
            }
            else {
                $('#linkMenuToProfileReview').css('display', 'none');
                $('#linkMenuToChangePassword').css('display', 'none');
                if (lang == 'AR' || typeof lang == 'undefined' || lang == null) {
                    $('#lblMenuLoginText').html('تسجيل الدخول');
                    $('#lblMenuLoginReviewText').html('تسجيل الدخول');
                }
                else {
                    $('#lblMenuLoginText').html('Login');
                    $('#lblMenuLoginReviewText').html('Login');
                }
            }

            if (typeof userLoggedIn != 'undefined' && userLoggedIn != null) {
                if (typeof userLoggedIn.image != 'undefined' && userLoggedIn.image != null && userLoggedIn.image != '' && userLoggedIn.image != ' ') {
                    if (userLoggedIn.image.indexOf('http://') > -1 || userLoggedIn.image.indexOf('https://') > -1 || userLoggedIn.image.indexOf('http://placehold.it') > -1 ||
                        userLoggedIn.image.indexOf('https://placehold.it') > -1) {
                        $$('#imgSideMenu').attr('src', userLoggedIn.image);
                    }
                    else {
                        if (userLoggedIn.image.indexOf('placehold.it') > -1) {
                            $$('#imgSideMenu').attr('src', 'http:' + userLoggedIn.image);
                        }
                        else {
                            $$('#imgSideMenu').attr('src', hostUrl + userLoggedIn.image);
                        }
                    }
                }
                else {
                    $$('#imgSideMenu').attr('src', 'img/logo.png');
                }
            }
            else {
                $$('#imgSideMenu').attr('src', 'img/logo.png');
            }
        }

        setTimeout(function () {
            //$scope.$apply();
            // $rootScope.$apply();
        }, fw7.DelayBeforeScopeApply);
    }

    return {
        DrawMenu: DrawMenu
    }

}]);