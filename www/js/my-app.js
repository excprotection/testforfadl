// Initialize your app
var myApp = new Framework7({
    animateNavBackIcon:true,
    swipeBackPage: true,
    tapHold: true,
    material: true
});


// Export selectors engine
var $$ = Dom7;

// Add main View
var mainView = myApp.addView('.view-main', {
    // Enable dynamic Navbar
    dynamicNavbar: false,
    // Enable Dom Cache so we can use all inline pages
    domCache: true
});

// Open more popover
$$('.open-more').on('click', function () {
    var clickedLink = this;
    myApp.popover('.popover-more', clickedLink);
});
// Open more popover
$$('.open-advertisement').on('click', function () {
    var clickedLink = this;
    myApp.popover('.popover-advertisement', clickedLink);
});
// delete alert
$$('.alert-text').on('click', function () {
    myApp.alert('لقد تم حذف المنتج بنجاح', '');
    $(".modal-button").text("تم")
});

$$('.activationCode').on('click', function () {
    var activation = myApp.modal({
        title: 'كود التفعيل',
        text: '',
        afterText: '<div class="list-block">' +
        '<div class="m-auto">' +
        '<ul>' +
        '<li>' +
        '<div class="item-content error">' +
        '<div class="item-inner">' +
        '<div class="item-input">' +
        '<input type="number" name="phone" placeholder="كود التفعيل">' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<p>' +
        'رقم الجوال خطأ' +
        '</p>' +
        '</li>' +
        '</ul>' +
        '</div>' +
        '</div>',
        buttons: [
            {
                text: 'تفعيل'
            }
        ]
    })
});
$$('.editComment').on('click', function () {
    var activation = myApp.modal({
        title: 'تعديل التعليق',
        text: '',
        afterText: '<div class="list-block">' +
        '<div class="m-auto">' +
        '<ul>' +
        '<li>' +
        '<div class="item-content">' +
        '<div class="item-inner">' +
        '<div class="item-input">' +
        '<textarea name="editComment" placeholder="تعديل التعليق"></textarea>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</li>' +
        '</ul>' +
        '</div>' +
        '</div>',
        buttons: [
            {
                text: 'تعديل'
            }
        ]
    })
});
$$('.reportComment').on('click', function () {
    var activation = myApp.modal({
        title: 'إبلاغ عن التعليق',
        text: '',
        afterText: '<div class="list-block">' +
        '<div class="m-auto">' +
        '<ul>' +
        '<li>' +
        '<div class="item-content">' +
        '<div class="item-inner">' +
        '<div class="item-input">' +
        '<textarea name="reportComment" placeholder="إبلاغ عن التعليق"></textarea>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</li>' +
        '</ul>' +
        '</div>' +
        '</div>',
        buttons: [
            {
                text: 'إبلاغ'
            }
        ]
    })
});


/*=== Popup ===*/

$$('.pb-popup').on('click', function () {
    myPhotoBrowserPopup.open();
});

// Pull to refresh content
var ptrContent = $$('.pull-to-refresh-content');

// Add 'refresh' listener on it
ptrContent.on('ptr:refresh', function (e) {
    // Emulate 2s loading
    setTimeout(function () {
        // When loading done, we need to reset it
        myApp.pullToRefreshDone();
    }, 2000);
});

$$('.commentOption').on('taphold', function () {
    var clickedLink = this;
    myApp.popover('.popover-more', clickedLink);
});
$$('.delMsg').on('taphold', function () {
    var clickedLink = this;
    myApp.popover('.popover-delete', clickedLink);
});


var mySwiper = myApp.swiper('.swiper-container', {
    pagination: '.swiper-pagination',
    paginationHide: false,
    paginationClickable: true,
    spaceBetween: 0,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    autoPlay: 100,
    speed: 1500,
    watchSlidesProgress: true,
    watchVisibility: true,
});


// var mySwiper = myApp.swiper('.swiper-container', {
//     pagination:'.swiper-pagination'
// });