﻿/// <reference path="../../js/angular/angular.min.js" />

(function () {
    myApp.angular.directive('imageLoader', function () {
        return {
            restrict: 'AE',
            scope: {
                imageLoader: '@',
                placeHolder: '@',
                loader: '@',
                background: '@'
            },
            link: function (scope, element, attrs) {
                element[0].src = scope.placeHolder;
                scope.$watch("imageLoader", function (value) {
                    if (value && typeof (value) == 'string') {
                        element[0].src = scope.loader;
                        element.parent().addClass("image-loader");
                        element.addClass("loader-image");

                        if (value.indexOf("?url=") !== -1) {
                            if (value.indexOf("?v=") !== -1)
                               value= value.replace("?v=", "&v=");
                            var w = element.parent().width();
                            value += "&width=" + Math.ceil(w);
                        }

                        checkImage(value, function (src) {
                            setTimeout(function () {
                                element[0].src = src;
                                element.parent().removeClass("image-loader");
                                element.removeClass("loader-image");
                            }, 1000);

                        }, function () {
                            setTimeout(function () {
                                element[0].src = scope.placeHolder;
                                element.removeClass("loader-image");
                                element.parent().removeClass("image-loader");
                            }, 500);
                        });

                    }
                }, true);
            }
        };
    });
    function checkImage(src, success, error) {

        var dynamicImg = new Image();
        dynamicImg.onload = function () {
            success(src);
            dynamicImg = undefined;
            return;
        };
        dynamicImg.onerror = function (err) {
            error();
            err.preventDefault();
            dynamicImg = undefined;
            return;
        };
        dynamicImg.src = src;

    }
}());



//document.body.onload = function () {
//    var style = document.createElement("style");
//    style.innerHTML = ".image-loader{text-align:center;}";
//    style.innerHTML += ".card.comment .image-loader>img.loader-image{width: 100% !important;}";
//    document.head.appendChild(style);
//}